-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 20, 2020 at 11:28 AM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 5.6.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `nbaccounts`
--

-- --------------------------------------------------------

--
-- Table structure for table `system_config`
--

CREATE TABLE `system_config` (
  `configkey` varchar(50) NOT NULL,
  `configvalue` varchar(255) NOT NULL,
  `configdesc` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `system_config`
--

INSERT INTO `system_config` (`configkey`, `configvalue`, `configdesc`) VALUES
('company_name', 'บริษัท เอ็นบี แอคเคาท์ จำกัด', 'ชื่อหน่วยงาน'),
('DebugBar', 'false', 'แสดง DebugBar'),
('fixed_layout', 'false', 'เมนูด้านซ้ายคงที่ (true,false)'),
('footer_title', 'Copyright @2019 by NBAccounts.com ', 'ข้อความส่วนท้ายของเว็บ'),
('footer_version', '2.0', 'เวอร์ชั่นแสดงที่ส่วนท้ายของเว็บ'),
('show_all_menu', 'true', 'แสดงเมนูทั้งหมด'),
('show_notification', 'ture', 'แสดงรายการแจ้งเตือนด้านบน (true,false)'),
('show_onlineuser', 'true', 'แสดงจำนวนผู้ใช้งานปัจจุบัน'),
('web_fname', 'NB', 'ชื่อเว็บส่วนหน้า'),
('web_name', 'nbaccounts.com', 'ชื่อเว็บ'),
('web_sname', ' Account', 'ชื่อเว็บส่วนหลัง'),
('web_title', 'ระบบบริหารงานบุคคล', 'ไตเติ้ลเว็บ');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `system_config`
--
ALTER TABLE `system_config`
  ADD PRIMARY KEY (`configkey`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
