<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class ExportModel extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getEmployeeBySection($dateset, $dateset2, $empgroup,$gender,$nationality,$worktype, $DivisionID, $DepartmentID, $SectionID){

        if (!is_null($DivisionID) && !is_null($DepartmentID) && !is_null($SectionID)) {


            $sqlSection="SELECT *,a.Postcode as zipcode
            FROM Employee a
            LEFT OUTER JOIN Company b on b.CompID=a.CompID
            LEFT OUTER JOIN Division c on c.DivisionID=a.DivisionID
            LEFT OUTER JOIN Department d on d.DepartmentID=a.DepartmentID
            LEFT OUTER JOIN Section e on e.SectionID=a.SectionID
            LEFT OUTER JOIN Position f on f.PositionID=a.PositionID
            LEFT OUTER JOIN Title g on g.TitleID=a.TitleID
            LEFT OUTER JOIN Gender h on a.GenderID = h.GenderID
            LEFT OUTER JOIN Nationality i on a.NationalityID = i.NationlityID
            LEFT OUTER JOIN districts i1 on a.SubDistrictID = i1.DISTRICT_CODE
            LEFT OUTER JOIN amphures i2 on a.DistrictID = i2.AMPHUR_ID
            LEFT OUTER JOIN provinces i3 on a.ProvinceID = i3.ProvinceID


            WHERE (a.CompID={$this->session->CompID})
            AND a.SectionID=".$SectionID."

            AND (a.GenderID = $gender or $gender=0)
            AND (NationalityID = $nationality or $nationality=0)
            AND (WorkingType = $worktype or $worktype=0)
            AND ServiceStartDate between '$dateset' AND '$dateset2'
            ";

            if($empgroup=="Resign") $sqlSection.=" and (EffectiveDateOut<='$dateset2') and ResignationTypeStatusOut=1 ";
            else if($empgroup=="Current") $sqlSection.=" and ResignationTypeStatusOut=0 ";
            else if($empgroup=="All") $sqlSection.=" and (EffectiveDateOut<='$dateset2') ";


            $sqlSection .=" order by a.EmployeeCode ";

            $query=$this->db->query($sqlSection);
            if ($query->num_rows() > 0) {
                // echo "<pre>".$sqlSection.'---'.$empgroup;
                // exit();
                return $query->result();
            } else {
                return false;
            }
        }

    }


}
?>
