<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
// เปลี่ยนชื่อโมเดล (MemberModel) ให้เป็นชื่อโมเดล อักษาตัวแรกให้เป็นตัวใหญ่และตามด้วยคำว่า Model
class HrModel extends CI_Model
{
    public function __construct(){
        parent::__construct();
    }
    public function getEmployeeByID($EmployeeID){
    	
    	$query_str="
        select *,a.ProvinceID as ProvinceIDEM,a.DistrictID as DistrictIDEM,a.SubDistrictID as SubDistrictIDEM,a.Postcode as PostcodeEM,a.CompID as CompEM
        from Employee a 
        left outer join Company b on b.CompID=a.CompID
        left outer join Division c on c.DivisionID=a.DivisionID
        left outer join Department d on d.DepartmentID=a.DepartmentID
        left outer join Section e on e.SectionID=a.SectionID
        left outer join Position f on f.PositionID=a.PositionID
        left outer join ContractType g on g.ContractID = a.ContractType
		left outer join provinces h on h.ProvinceID = a.ProvinceID
		left outer join Title i on i.TitleID = a.TitleID
        where (a.EmployeeID=".$EmployeeID.")  
        ";
        // echo $query_str;
        // exit();
        $query=$this->db->query($query_str);
        if ($query->num_rows() > 0) {
           return $query->result(); 
        } else {
            return false;
        }
    }
public function getEmployeeByAllID($EmployeeID){
    	
    	$query_str="
        select *
        from Employee a 
        left outer join Company b on b.CompID=a.CompID
		left outer join Title i on i.TitleID = a.TitleID
        where (a.EmployeeID=".$EmployeeID.")  
        ";
        $query=$this->db->query($query_str);
        if ($query->num_rows() > 0) {
           return $query->result(); 
        } else {
            return false;
        }
    }	
    public function getEmployeeAll(){
    	
    	$query_str="
        select *
        from Employee a 
        left outer join Company b on b.CompID=a.CompID
        left outer join Division c on c.DivisionID=a.DivisionID
        left outer join Department d on d.DepartmentID=a.DepartmentID
        left outer join Section e on e.SectionID=a.SectionID
        left outer join Position f on f.PositionID=a.PositionID
		left outer join Title i on i.TitleID = a.TitleID
        where (a.CompID=".$this->session->CompID.") and a.ResignationTypeStatusOut <> 1 
        
        ";

        // and a.ResignationTypeStatusOut <> 1 ถ้าต้องการให้สามารถแก้ไขข้อมูลการลาออกได้ ต้องตัดคำนี้ออกจากคำสั่งด้านบน
        $query=$this->db->query($query_str);
        if ($query->num_rows() > 0) {
           return $query->result(); 
        } else {
            return false;
        }
    } 
	public function getEmployeeAllResignation(){
    	
    	$query_str="
        select *
        from Employee a 
        left outer join Company b on b.CompID=a.CompID
        left outer join Division c on c.DivisionID=a.DivisionID
        left outer join Department d on d.DepartmentID=a.DepartmentID
        left outer join Section e on e.SectionID=a.SectionID
        left outer join Position f on f.PositionID=a.PositionID
        where (a.CompID=".$this->session->CompID.") and ResignationTypeStatusOut = 1
        ORDER BY ResignationTypeStatusOut,EmployeeID ASC";
        
		
        $query=$this->db->query($query_str);
		
        if ($query->num_rows() > 0) {
           return $query->result(); 
        } else {
            return false;
        }
    }	
    public function getEmployeeMovementByID($EmployeeID){
        $query = $this->db->select('*')
        ->join('MovementType', 'MovementType.MovementTypeID = EmployeeMovement.MovementTypeID')
        ->join('Position', 'Position.PositionID = EmployeeMovement.PositionID','left')
        ->join('Department', 'Department.DepartmentID = EmployeeMovement.DepartmentID','left')
		->where('EmployeeID',$EmployeeID)
		->get('EmployeeMovement');
        
        if ($query->num_rows() > 0) {
           return $query->result(); 
        } else {
            return false;
        }
    }  
    public function getEmployeeMovementByMID($EmployeeMID){
        $query = $this->db->select('*')
		->where('EmployeeMID',$EmployeeMID)
		->get('EmployeeMovement');
        
        if ($query->num_rows() > 0) {
           return $query->result(); 
        } else {
            return false;
        }
    }    
	public function fetch_company()
	{
		$this->db->select('*');
		$query = $this->db->get('Company');
		$this->db->from('Company');
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			foreach($query->result() as $row)
			{
				$data[] = $row;
			}
			return $data;
		}
		return FALSE;
	}
	public function fetch_title()
	{
		$this->db->select('*');
		$query = $this->db->get('Title');
		$this->db->from('Title');
		$this->db->where('CompID=',$this->session->CompUSERID);
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			foreach($query->result() as $row)
			{
				$data[] = $row;
			}
			return $data;
		}
		return FALSE;
	}
	public function fetch_gender()
	{
		$this->db->select('*');
		$query = $this->db->get('Gender');
		$this->db->from('Gender');
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			foreach($query->result() as $row)
			{
				$data[] = $row;
			}
			return $data;
		}
		return FALSE;
	}
	public function fetch_Nationality()
	{
		$this->db->select('*');
		$query = $this->db->get('Nationality');
		$this->db->from('Nationality');
		$this->db->where('CompID=',$this->session->CompUSERID);
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			foreach($query->result() as $row)
			{
				$data[] = $row;
			}
			return $data;
		}
		return FALSE;
	}
	public function fetch_Race()
	{
		$this->db->select('*');
		$query = $this->db->get('Race');
		$this->db->from('Race');
		$this->db->where('CompID=',$this->session->CompUSERID);
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			foreach($query->result() as $row)
			{
				$data[] = $row;
			}
			return $data;
		}
		return FALSE;
	}
	public function fetch_FamilyStatus()
	{
		$this->db->select('*');
		$query = $this->db->get('FamilyStatus');
		$this->db->from('FamilyStatus');
		$this->db->where('CompID=',$this->session->CompUSERID);
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			foreach($query->result() as $row)
			{
				$data[] = $row;
			}
			return $data;
		}
		return FALSE;
	}
	public function fetch_WorkType()
	{
		$this->db->select('*');
		$query = $this->db->get('WorkType');
		$this->db->from('WorkType');
		$this->db->where('CompID=',$this->session->CompUSERID);
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			foreach($query->result() as $row)
			{
				$data[] = $row;
			}
			return $data;
		}
		return FALSE;
	}
	public function fetch_EmployeeGroup()
	{
		$this->db->select('*');
		$query = $this->db->get('EmployeeGroup');
		$this->db->from('EmployeeGroup');
		$this->db->where('CompID=',$this->session->CompUSERID);
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			foreach($query->result() as $row)
			{
				$data[] = $row;
			}
			return $data;
		}
		return FALSE;
	}
	public function fetch_EmployeeLevel()
	{
		$this->db->select('*');
		$query = $this->db->get('EmplyeeLevel');
		$this->db->from('EmplyeeLevel');
		$this->db->where('CompID=',$this->session->CompUSERID);
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			foreach($query->result() as $row)
			{
				$data[] = $row;
			}
			return $data;
		}
		return FALSE;
	}
	public function fetch_ContractType()
	{
		$this->db->select('*');
		$query = $this->db->get('ContractType');
		$this->db->from('ContractType');
		$this->db->where('CompID=',$this->session->CompUSERID);
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			foreach($query->result() as $row)
			{
				$data[] = $row;
			}
			return $data;
		}
		return FALSE;
	}
	public function fetch_SSOCode()
	{
		$this->db->select('*');
		$query = $this->db->get('SSOCode');
		$this->db->from('SSOCode');
		// $this->db->where('CompID=',$this->session->CompUSERID);
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			foreach($query->result() as $row)
			{
				$data[] = $row;
			}
			return $data;
		}
		return FALSE;
	}
	public function fetch_SalaryTypeID()
	{
		$this->db->select('*');
		$query = $this->db->get('SalaryType');
		$this->db->from('SalaryType');
		$this->db->where('CompID=',$this->session->CompUSERID);
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			foreach($query->result() as $row)
			{
				$data[] = $row;
			}
			return $data;
		}
		return FALSE;
	}
	public function fetch_PaymentMethod()
	{
		$this->db->select('*');
		$query = $this->db->get('PaymentMethod');
		$this->db->from('PaymentMethod');
		$this->db->where('CompID=',$this->session->CompUSERID);
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			foreach($query->result() as $row)
			{
				$data[] = $row;
			}
			return $data;
		}
		return FALSE;
	}
	public function fetch_PITCode()
	{
		$this->db->select('*');
		$query = $this->db->get('PITCode');
		$this->db->from('PITCode');
		// $this->db->where('CompID=',$this->session->CompUSERID);
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			foreach($query->result() as $row)
			{
				$data[] = $row;
			}
			return $data;
		}
		return FALSE;
	}
	public function fetch_SocialID()
	{
		$this->db->select('*');
		$this->db->from('SocialSecurityRate');
		$this->db->where('CompID=',$this->session->CompUSERID);
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			foreach($query->result() as $row)
			{
				$data[] = $row;
			}
			return $data;
		}
		return FALSE;
	}
	function getCompany(){
    $response = array();
    $this->db->select('*');
	$this->db->where('CompID=',$this->session->CompID);
    $q = $this->db->get('Company');
    $response = $q->result_array();
    return $response;
	}
	function getDivision(){//
    $response = array();
    $this->db->select('*');
	$this->db->where('CompID=',$this->session->CompUSERID);
    $q = $this->db->get('Division');//Division
    $response = $q->result_array();
    return $response;
	}
	function getDepartment($postData){
    $response = array();
    $this->db->select('DepartmentID,DepartmentNameThai');
    $this->db->where('DivisionID', $postData['DivisionID']);
    $q = $this->db->get('Department');//Department
    $response = $q->result_array();
    return $response;
	}
	function getSection($postData){
    $response = array();
    $this->db->select('SectionID,SectionNameThai');
    $this->db->where('DepartmentID', $postData['DepartmentID']);
    $q = $this->db->get('Section');//Section
    $response = $q->result_array();
    return $response;
	}
	function getPosition($postData){
    $response = array();
    $this->db->select('PositionID,PositionNameThai');
    $this->db->where('SectionID', $postData['SectionID']);
    $q = $this->db->get('Position');//Position
    $response = $q->result_array();
    return $response;
	}
	public function fetch_MovementType()
	{
		$this->db->select('*');
		$query = $this->db->get('MovementType');
		$this->db->from('MovementType');
		$this->db->where('CompID=',$this->session->CompUSERID);
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			foreach($query->result() as $row)
			{
				$data[] = $row;
			}
			return $data;
		}
		return FALSE;
	}
	function getDataTable($table){
    $response = array();
    $this->db->select('*');
    // $this->db->where($field, $id);
    $q = $this->db->get($table);
    $response = $q->result_array();
    return $response;
	}
	function getDataTableByID($table,$field,$id){
    $response = array();
    $this->db->select('*');
    $this->db->where($field, $id);
    $q = $this->db->get($table);
    $response = $q->result_array();
    return $response;
	}
	function getProvince(){
    $response = array();
    $this->db->select('*');
    $q = $this->db->get('provinces');
    $response = $q->result_array();
    return $response;
	}
	function getAmphure($postData){
    $response = array();
    $this->db->select('AMPHUR_ID,AMPHUR_CODE,AMPHUR_NAME,AMPHUR_NAME_ENG');
    $this->db->where('AMPHUR_PROVINCE_ID', $postData['AMPHUR_PROVINCE_ID']);
    $q = $this->db->get('amphures');//Department
    $response = $q->result_array();
    return $response;
	}
	function getSubDistrict($postData){
    $response = array();
    $this->db->select('DISTRICT_CODE,DISTRICT_NAME,DISTRICT_NAME_ENG');
    $this->db->where('DISTRICT_AMPHUR_ID', $postData['DISTRICT_AMPHUR_ID']);
    $q = $this->db->get('districts');//Section
    $response = $q->result_array();
    return $response;
	}
	function getPostcode($postData){
    $response = array();
    $this->db->select('district_code,zipcode');
    $this->db->where('district_code', $postData['district_code']);
    $q = $this->db->get('zipcodes');//Zipcode
    $response = $q->result_array();
    return $response;
	}
	function getCountry($postData){
    $response = array();
    $this->db->select('CountryID,CountryLL,CountryEN');
    $this->db->where('CountryID', $postData['CountryID']);
    $q = $this->db->get('Country');//Country
    $response = $q->result_array();
    return $response;
	}
	public function fetch_ResignationTypeOut()
	{
		$this->db->select('*');
		$query = $this->db->get('ResignationType');
		$this->db->from('ResignationType');
		$this->db->where('CompID=',$this->session->CompUSERID);
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			foreach($query->result() as $row)
			{
				$data[] = $row;
			}
			return $data;
		}
		return FALSE;
	}
	public function fetch_ResignationReason()
	{
		$this->db->select('*');
		$query = $this->db->get('ResignationReason');
		$this->db->from('ResignationReason');
		$this->db->where('CompID=',$this->session->CompUSERID);
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			foreach($query->result() as $row)
			{
				$data[] = $row;
			}
			return $data;
		}
		return FALSE;
	}
    public function getTypeNewEmployee(){    	
    	$query_str=" 
    	select MovementTypeID from MovementType 
    	where (CompID=".$this->session->CompUSERID.") 
    	and NewEmployee = 1
    	";
        $query=$this->db->query($query_str);
        if ($query->num_rows() > 0){
        	foreach($query->result() as $row) $data = $row->MovementTypeID;
        	return $data; 
        }
        else return false;
    }
    public function getTypeNewEmployeeTXT($MovementTypeID){    	
    	$query_str=" 
    	select MovementTypeNameEn from MovementType 
    	where (CompID=".$this->session->CompUSERID.") 
    	and MovementTypeID = $MovementTypeID
    	";
        $query=$this->db->query($query_str);
        if ($query->num_rows() > 0){
        	foreach($query->result() as $row) $data = $row->MovementTypeNameEn;
        	return $data; 
        }
        else return false;
    }
    public function getBank(){
        $query_str=" select * from Bank  where (CompID=".$this->session->CompUSERID.")  order by BankNameEN";
        $query=$this->db->query($query_str);
        if ($query->num_rows() > 0) return $query->result(); 
        else  return false;
    }
    public function getReligion(){
        $query_str=" select * from Religion  where (CompID=".$this->session->CompUSERID.")  order by ReligionNameEN";
        $query=$this->db->query($query_str);
        if ($query->num_rows() > 0) return $query->result(); 
        else  return false;
    }
    public function getQualification(){
        $query_str=" select * from Qualification  where (CompID=".$this->session->CompUSERID.")  order by QualificationID";
        $query=$this->db->query($query_str);
        if ($query->num_rows() > 0) return $query->result(); 
        else  return false;
    }
    public function getMajor(){
        $query_str=" select QualificationID,QualificationMajorEN,QualificationMajorLL from Qualification  where (CompID=".$this->session->CompUSERID.")  group by QualificationMajorEN,QualificationMajorLL";
        $query=$this->db->query($query_str);
        if ($query->num_rows() > 0) return $query->result(); 
        else  return false;
    }
    public function getEmployeeQualification($EmployeeID){
        $query_str=" select * from EmployeeQualification  where (EmployeeID=$EmployeeID) order by EmpQID";
        $query=$this->db->query($query_str);
        if ($query->num_rows() > 0) return $query->result(); 
        else  return false;
    }
    public function getSkills(){
        $query_str=" select * from Skills  where (CompID=".$this->session->CompUSERID.")  order by SkillNameEN";
        $query=$this->db->query($query_str);
        if ($query->num_rows() > 0) return $query->result(); 
        else  return false;
    } 

    public function getEmployeeSkills($EmployeeID){
        echo $query_str=" select * from EmployeeSkills  where (EmployeeID=$EmployeeID) order by EmpSKID";
        $query=$this->db->query($query_str);
        if ($query->num_rows() > 0) return $query->result(); 
        else  return false;
    }
    public function getEmployeeFMRelation($EmployeeID){
        echo $query_str=" select * from EmployeeFMRelation  where (EmployeeID=$EmployeeID) order by EmpFMRID";
        $query=$this->db->query($query_str);
        if ($query->num_rows() > 0) return $query->result(); 
        else  return false;
    }
    public function getRelationship(){
        $query_str=" select * from FamilyRelation  where (CompID=".$this->session->CompUSERID.")  order by FMRelationID";
        $query=$this->db->query($query_str);
        if ($query->num_rows() > 0) return $query->result(); 
        else  return false;
    }
}
