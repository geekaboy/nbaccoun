
<script type="text/javascript">
  $(document).ready(function(){
    $('.selectpicker').selectpicker({style: 'btn-warning',size: 5});
    $('#username').focus();
    $('#btn_login').click(function(){
      $.ajax({
        url:'<?php echo base_url(); ?>index.php/home/login',
        data:'username='+$('#username').val()+'&password='+$('#password').val(),
        type:'POST',
        success:function(res){
          if(res=='true'){
            swal({title : 'แสดงเมือทำงานสำเร็จ',text : '',type : 'success'},
              function(){window.location.replace("<?php echo base_url() ?>index.php/member");
            }
            );
          }else{swal({title : 'แสดงเมื่อทำงานไม่สำเร็จ',text : '',type : 'error'});}
        },error:function(err){swal({title : 'เกิดข้อผิดพลาด',text : err,type : 'error'});}
      });
    });
  });
</script>
<head>
  <meta charset="UTF-8">
  <title><?php echo $this->uri->segment(2);?></title>
  <link href="<?php echo base_url();?>assets/bootstrap-select/css/bootstrap-select.css" rel="stylesheet">
  <link href="<?php echo base_url();?>assets/bootstrap/css/datepicker.css" rel="stylesheet">
  <link href="<?php echo base_url();?>assets/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">
  <link href="<?php echo base_url();?>assets/bootstrap-toggle-master/css/bootstrap-toggle.min.css" rel="stylesheet">
  
<style>
  canvas{
    -moz-user-select: none;
    -webkit-user-select: none;
    -ms-user-select: none;
  }
  </style>
</head>
<div class="box box-success">
  <div class="box-header">
        <i class="fa fa-sign-in"></i>
    <h3 class="box-title"><?php echo $this->systemmodel->get_menuname($this->uri->segment(1) . '/' . $this->uri->segment(2)); // แสดงชื่อเมนู  ?></h3>
  </div>

  
  <div class="box-body" align="left">
    <div class="row clearfix">
      <?php
      if(!empty($DataSearch)){
          foreach ($DataSearch as $row) {
      ?>
          <div class="col-md-6">
            <p><b><?=$this->systemmodel->changeLng("ชื่อพนักงาน")?></b>  : <?php echo $row->FullNameLL." | ".$row->FirstName." ".$row->LastName." (".$row->MiddleName.")";?></p>
            <p><b><?=$this->systemmodel->changeLng("บริษัท")?></b>  : <?php echo $row->CompNameEng." | ".$row->CompNameThai;?></p>
            <p><b><?=$this->systemmodel->changeLng("ฝ่าย")?></b>  : <?php echo $row->DivisionNameEng." | ".$row->DivisionNameThai;?></p>
            <p><b><?=$this->systemmodel->changeLng("แผนก")?></b>  : <?php echo $row->DepartmentNameEng." | ".$row->DepartmentNameThai;?></p>
            <p><b><?=$this->systemmodel->changeLng("หน่วย")?></b>  : <?php echo $row->SectionNameEng." | ".$row->SectionNameThai;?></p>
            <p><b><?=$this->systemmodel->changeLng("ตำแหน่ง")?></b>  : <?php echo $row->PositionNameEng." | ".$row->PositionNameThai;?></p>
            <p><b><?=$this->systemmodel->changeLng("กลุ่มพนักงาน")?></b>  : <?php echo "";?></p>
          </div>
          <div class="col-md-6">
            <p><b><?=$this->systemmodel->changeLng("วันที่เริ่มให้บริการ")?></b>  : <?php echo $row->ServiceStartDate;?></p>
            <p><b><?=$this->systemmodel->changeLng("ประเภทสัญญา")?></b>  : <?php echo $row->ContractNameEN;?></p>
            <p><b><?=$this->systemmodel->changeLng("วันที่เริ่มสัญญาจ้าง")?></b>  : <?php echo $row->ContractStartDate;?></p>
            <p><b><?=$this->systemmodel->changeLng("วันที่สิ้นสุดสัญญาจ้าง")?></b>  : <?php echo $row->ContractEndDate;?></p>
            <p><b><?=$this->systemmodel->changeLng("วันที่ผ่านการทดลองงาน")?></b>  : <?php echo $row->ProbationEndDate;?></p>
            <!--<p><b><?=$this->systemmodel->changeLng("วันที่มีผลบังคับ")?></b>  : <?php echo $row->EffectiveDate;?></p>-->
          </div>
      </div>
      <?php
    }
  }
    ?>
  </div>

  <div class="box box-success">
    <div class="box-header"><b><?=$this->systemmodel->changeLng("รายการเปลี่ยนแปลง")?></b></div>
    <div class="box-body" align="left">
      <!-- ตาราง แสดงข้อมูล -->
      <table class="table table-bordered table-striped table-hover dataTable  js-exportable2">
        <thead>
          <tr>
            <th scope="col" width="10"><center><?=$this->systemmodel->changeLng("รายการ")?></center></th>
            <th scope="col"><left><?=$this->systemmodel->changeLng("ประเภทการเปลี่ยนแปลง")?></center></th>
            <th scope="col"><left><?=$this->systemmodel->changeLng("เงินเดือน")?></center></th
            >
            <th scope="col"><left><?=$this->systemmodel->changeLng("ตำแหน่ง")?></center></th
            >
           <th scope="col"><left><?=$this->systemmodel->changeLng("แผนก")?></center></th
            >
            <th scope="col"><left><?=$this->systemmodel->changeLng("วันที่มีผลบังคับ")?></center></th
            >
            <th scope="col"><left><?=$this->systemmodel->changeLng("หมายเหตุ")?></center></th
            >
          </tr>
        </thead>
        <tbody>
        <?php 
      $i=1;
      if(!empty($DataSearch2)){
        foreach ($DataSearch2 as $row) {
        ?>
          <tr style="cursor: pointer;" onclick='btn_click("Edit",<?php echo $row->EmployeeID;?>,<?php echo $row->EmployeeMID;?>)'>
            <td scope="col"><?php echo $i;?></td>
            <td scope="col"><?php echo $row->MovementTypeNameEN;?></td>
            <td scope="col"><?php echo number_format($row->Salary,2);?></td>
            <td scope="col"><?php echo $row->PositionNameEng."|".$row->PositionNameThai;?></td>
            <td scope="col"><?php echo $row->DepartmentNameEng."|".$row->DepartmentNameThai;?></td>
            <td scope="col"><?php echo $row->MMDate;?></td>
            <td scope="col"><?php echo $row->Remark;?></td>
          </tr>
        <?php
        
        $i++;
      } 
      }  
        ?>
        </tbody>
      </table>
    </div>
    <div class="box-body" align="right">
      <button type='button' class='btn btn-info' onclick='btn_click("Add",<?php echo $row->EmployeeID;?>,0)'>
      <span class='glyphicon glyphicon-edit' aria-hidden='true'></span>&nbsp;&nbsp;<?=$this->systemmodel->changeLng("เพิ่มรายการเปลี่ยนแปลงของพนักงาน")?>
      </button><BR><BR>
    </div>
  </div>

</div>

<div id="DIVAddEmployeeMovement">
</div>

<script>
function btn_click(ModeDB,eid,mid){
$.ajax({
  url:'<?=base_url()?>index.php/hr/AddEmployeeMovement',
  type: "post",
  data: {
         ModeDB: ModeDB,
         EmployeeID: eid,
         EmployeeMID: mid
        },
  beforeSend: function () {$(".loading").show();},
  complete: function () {$(".loading").hide();},
  success: function (data) {$("#DIVAddEmployeeMovement").html(data);}
});
}
</script>
<!-- Bootstrap-Select -->
<script src="<?php echo base_url();?>assets/bootstrap-select/js/bootstrap-select.js"></script>
<script src="<?php echo base_url();?>assets/chartjs/Chart.bundle.js"></script>
<script src="<?php echo base_url();?>assets/chartjs/Chart.js"></script>
<script src="<?php echo base_url();?>assets/bootstrap/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url();?>assets/jquery-datatable/jquery.dataTables.js"></script>
<script src="<?php echo base_url();?>assets/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
<script src="<?php echo base_url();?>assets/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url();?>assets/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
<script src="<?php echo base_url();?>assets/jquery-datatable/extensions/export/jszip.min.js"></script>
<script src="<?php echo base_url();?>assets/jquery-datatable/extensions/export/pdfmake.min.js"></script>
<script src="<?php echo base_url();?>assets/jquery-datatable/extensions/export/vfs_fonts.js"></script>
<script src="<?php echo base_url();?>assets/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
<script src="<?php echo base_url();?>assets/jquery-datatable/extensions/export/buttons.print.min.js"></script>