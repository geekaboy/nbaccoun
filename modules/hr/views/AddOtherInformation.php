
<script type="text/javascript">
  $(document).ready(function(){
    $('.selectpicker').selectpicker({style: 'btn-warning',size: 5});
    $('#username').focus();
    $('#btn_login').click(function(){
      $.ajax({
        url:'<?php echo base_url(); ?>index.php/home/login',
        data:'username='+$('#username').val()+'&password='+$('#password').val(),
        type:'POST',
        success:function(res){
          if(res=='true'){
            swal({title : 'แสดงเมือทำงานสำเร็จ',text : '',type : 'success'},
              function(){window.location.replace("<?php echo base_url() ?>index.php/member");
            }
            );
          }else{swal({title : 'แสดงเมื่อทำงานไม่สำเร็จ',text : '',type : 'error'});}
        },error:function(err){swal({title : 'เกิดข้อผิดพลาด',text : err,type : 'error'});}
      });
    });
  });
</script>
<head>
  <meta charset="UTF-8">
  <title><?php echo $this->uri->segment(2);?></title>
  <link href="<?php echo base_url();?>assets/bootstrap-select/css/bootstrap-select.css" rel="stylesheet">
  <link href="<?php echo base_url();?>assets/bootstrap/css/datepicker.css" rel="stylesheet">
  <link href="<?php echo base_url();?>assets/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">
  <link rel="stylesheet" href="<?php echo base_url();?>assets/jQuery-File-Upload/css/jquery.fileupload.css">
  <link href="<?php echo base_url();?>assets/bootstrap-toggle-master/css/bootstrap-toggle.min.css" rel="stylesheet">
  <script src="<?php echo base_url();?>assets/bootstrap-toggle-master/js/bootstrap-toggle.min.js"></script>
<style>
  canvas{
    -moz-user-select: none;
    -webkit-user-select: none;
    -ms-user-select: none;
  }
  span.alogo2 {
  display: inline-block;
  border-radius: 60px;
  box-shadow: 0px 0px 2px #888;
  padding: 0.5em 0.6em;
  text-align: center;
}
  </style>
</head>
<form action="" id="form" name="form" method="post">
<div class="box box-success">
  <div class="box-header"
    <h3 class="box-title">
          <span class="glyphicon glyphicon-user"></span> Edit | 
          <?php foreach($results_Employee as $result){echo $result->FullNameLL;}?> 
    </h3>
  </div>
  <div class="box-body">
    <ul class="nav nav-tabs">
      <li class="active"><a data-toggle="tab" href="#home"><span class="alogo glyphicon glyphicon-question-sign"></span> <?=$this->systemmodel->changeLng("รายละเอียดอื่น")?></a></li>
      <li><a data-toggle="tab" href="#menu1"><span class="alogo glyphicon glyphicon-usd"></span> <?=$this->systemmodel->changeLng("ธนาคาร")?></a></li>
      <li><a data-toggle="tab" href="#menu2"><span class="alogo glyphicon glyphicon-open-file"></span> <?=$this->systemmodel->changeLng("การจัดเก็บเอกสาร")?></a></li>
      <li><a data-toggle="tab" href="#menu3"><span class="alogo glyphicon glyphicon-transfer"></span> <?=$this->systemmodel->changeLng("รายการลดหย่อนภาษี")?></a></li>
      <li><a data-toggle="tab" href="#menu4"><span class="alogo glyphicon glyphicon-transfer"></span> <?=$this->systemmodel->changeLng("กองทุนสำรองเลี้ยงชีพ")?></a></li>
      <li><a data-toggle="tab" href="#menu5"><span class="alogo glyphicon glyphicon-transfer"></span> <?=$this->systemmodel->changeLng("วุฒิการศึกษา")?></a></li>
      <li><a data-toggle="tab" href="#menu6"><span class="alogo glyphicon glyphicon-transfer"></span> <?=$this->systemmodel->changeLng("ความสามารถพิเศษ")?></a></li>
      <li><a data-toggle="tab" href="#menu7"><span class="alogo glyphicon glyphicon-transfer"></span> <?=$this->systemmodel->changeLng("ความสัมพันธ์ทางครอบครัว")?></a></li>
    </ul>
    <div class="tab-content">
      <div id="home" class="tab-pane fade in active">
        <h3></h3>
        <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="panel-title"><?=$this->systemmodel->changeLng("รายละเอียดอื่น")?></h4>
          </div>
          <div class="panel-body " id="collapse1">
            <!-- The container for the uploaded files -->
            <img src="" id="EmployeePhoto_show" class="img-responsive" width="100px;">
            <div >
              <image id="imgFiles" width="128"></image>
              <input type='text' class="form-control" id='EmployeePhoto'/>
            </div>
            <p></p>
            <div id="divUpload">
              <span class="btn btn-success fileinput-button" id="btn_upload">
                <i class="glyphicon glyphicon-plus"></i>
                <span>Select Photo...</span>
                <input id="fileupload" type="file" name="files[]" multiple>
              </span>
              <span class='btn btn-danger ' onclick='RemovePhoto()' id="btn_remove" style="display:none;"><i class='glyphicon glyphicon-trash'></i><span> Remove Photo...</span></span>              
            </div>
              <br>
              <!-- The global progress bar -->
              <div id="progress" class="progress">
                  <div class="progress-bar progress-bar-success"></div>
              </div>


            <?=$this->systemmodel->changeLng("หมายเลขมือถือ")?> <input type='text' class="form-control" id='CellPhone'/>
            <?=$this->systemmodel->changeLng("กรุ๊ปเลือด")?> <input type='text' class="form-control" id='BloodGroup' />
            <?=$this->systemmodel->changeLng("อีเมล์")?> <input type='text' class="form-control" id='Email' />
            <?=$this->systemmodel->changeLng("รหัสไลน์")?> <input type='text' class="form-control" id='LineID' />
          </div>
        </div>
      </div>
      <div id="menu1" class="tab-pane fade">
        <h3></h3>
        <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="panel-title"><?=$this->systemmodel->changeLng("ธนาคาร")?></h4>
          </div>
          <!--iCha ปิดการรับข้อมูล วันที่มีผลของธนาคาร
          <div class="panel-body">
            <div class="col-md-6">
              <p></p>
              <div class="panel panel-default panel-body">
                <?=$this->systemmodel->changeLng("วันที่มีผล")?>
                 <div class='input-group date' id='datetimepicker'>
                  <input type='text' class="form-control" id='EffectiveDate' name="EffectiveDate" />
                  <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                  </span>
                </div>
            <script>
            $(document).ready(function(){
              var date_input=$('input[name="EffectiveDate"]');
              var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
              date_input.datepicker({
                format: 'yyyy-mm-dd',
                container: container,
                todayHighlight: true,
                autoclose: true,
              })
            })
            </script> -->
                <?=$this->systemmodel->changeLng("ธนาคาร")?>
                <!-- <input type='text' class="form-control" id='Bank'/> -->
                <select class="form-control" name="Bank" id='Bank'>
                      <option value="">
                        <?=$this->systemmodel->changeLng("กรุณาเลือก")?>
                      </option>
                      <?php
                      foreach($results_Bank as $result){
                        ?>
                        <option value="<?php echo $result->BankID; ?>">
                          <?php echo $result->BankNameEN." | ".$result->BankNameLL; ?>
                        </option>
                        <?php
                      } ?>
                 </select>
                <?=$this->systemmodel->changeLng("ชื่อบัญชี")?>
                <input type='text' class="form-control" id='BankAccountName'/>
                <?=$this->systemmodel->changeLng("หมายเลขบัญชี")?>
                <input type='text' class="form-control" id='BankAccountNo' />
              </div>
            </div>
            <div class="col-md-6">
              <p></p>
              <div class="panel panel-default panel-body">
                <?=$this->systemmodel->changeLng("สาขาธนาคาร")?>
                <input type='text' class="form-control" id='BankBranch' />
                <!--<label>
                  <BR><?=$this->systemmodel->changeLng("เป็นธนาคารหลัก")?>&nbsp;&nbsp;<BR>
                  <input type="checkbox" data-toggle="toggle" id="PrimaryBank">
                </label>
                <label>
                  <BR><?=$this->systemmodel->changeLng("ยังใช้อยู่")?>&nbsp;&nbsp;<BR>
                  <input type="checkbox" data-toggle="toggle" id="Activate">
                </label> --> 
              </div>
            </div>
          </div>
        </div>
      </div>
      
      <!--เป็นการเรียกใช้ เมนูที่ 2 ขึ้นมาทำงาน -->
      <div id="menu2" class="tab-pane fade">
        <h3></h3>
        <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="panel-title"><?=$this->systemmodel->changeLng("การจัดเก็บเอกสาร")?></h4>
          </div>
          <div class="panel-body">
            <div class="panel panel-default panel-body">
            	<a href="#" class="btn btn-info btn-md" id="doc1_btn"><span class="glyphicon glyphicon-file"></span></a><BR>&nbsp;
            <!-- The container for the uploaded files -->
            <div id="divUpload1">
            <div>
            	<input type='text' class="form-control" id='Doc1' readonly />
            </div>
            <p></p>
            <div id="divUpload">
              <span class="btn btn-success fileinput-button" id="btnDoc1">
                <i class="glyphicon glyphicon-plus"></i>
                <span><?=$this->systemmodel->changeLng("บัตรประชาชน/ทะเบียนบ้าน")?></span>
                <input id="Doc1upload" type="file" name="files[]" multiple>
              </span>
              <span class='btn btn-danger ' onclick='RemoveDoc1()' id="btnDoc1Remove" style="display:none;"><i class='glyphicon glyphicon-trash'></i><span> Remove <?=$this->systemmodel->changeLng("บัตรประชาชน/ทะเบียนบ้าน")?></span></span>
            </div>
              <br>
              <!-- The global progress bar -->
              <div id="progressDoc1" class="progress">
                  <div class="progress-bar progress-bar-success"></div>
              </div>
            </div>
            <a href="#" class="btn btn-warning btn-md" id="doc2_btn"><span class="glyphicon glyphicon-file"></span></a><BR>&nbsp;
            <div id="divUpload2">
            <div >
              <input type='text' class="form-control" id='Doc2' readonly />
            </div>
            <p></p>
            <div id="divUpload">
              <span class="btn btn-success fileinput-button" id="btnDoc2">
                <i class="glyphicon glyphicon-plus"></i>
                <span><?=$this->systemmodel->changeLng("วุฒิการศึกษา")?></span>
                <input id="Doc2upload" type="file" name="files[]" multiple>
              </span>
              <span class='btn btn-danger ' onclick='RemoveDoc2()' id="btnDoc2Remove" style="display:none;"><i class='glyphicon glyphicon-trash'></i><span> Remove <?=$this->systemmodel->changeLng("วุฒิการศึกษา")?></span></span>
            </div>
              <br>
              <!-- The global progress bar -->
              <div id="progressDoc2" class="progress">
                  <div class="progress-bar progress-bar-success"></div>
              </div>
            </div>
            <a href="#" class="btn btn-primary btn-md" id="doc3_btn"><span class="glyphicon glyphicon-file"></span></a><BR>&nbsp;
            <div id="divUpload3">
            <div >
              <input type='text' class="form-control" id='Doc3' readonly />
            </div>
            <p></p>
            <div id="divUpload">
              <span class="btn btn-success fileinput-button" id="btnDoc3">
                <i class="glyphicon glyphicon-plus"></i>
                <span><?=$this->systemmodel->changeLng("เอกสารอื่น")?></span>
                <input id="Doc3upload" type="file" name="files[]" multiple>
              </span>
              <span class='btn btn-danger ' onclick='RemoveDoc3()' id="btnDoc3Remove" style="display:none;"><i class='glyphicon glyphicon-trash'></i><span> Remove <?=$this->systemmodel->changeLng("เอกสารอื่น")?></span></span>
            </div>
              <br>
              <!-- The global progress bar -->
              <div id="progressDoc3" class="progress">
                  <div class="progress-bar progress-bar-success"></div>
              </div>
            </div>


            </div>
          </div>
        </div>
      </div>
      <div id="menu3" class="tab-pane fade">
        <h3></h3>
        <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="panel-title">PITAllowance</h4>
          </div>
          <div class="panel-body ">
            <div class="col-md-6">
              <p></p>
              <div class="panel panel-default panel-body">
                <div class="panel panel-default">
                  <!-- <div class="panel-heading"><b>Interest on Loan</b></div> -->
                  <div class="panel-heading"><b><?=$this->systemmodel->changeLng("ดอกเบี้ยเงินกู้ยืม")?></b></div>
                  <div class="panel-body">
                    <span><?=$this->systemmodel->changeLng("ผู้มีเงินได้")?></span>
                    <!--Personnel -->
                    <input type='text' class="form-control" id='PFC_Personnel' />
                    <span><?=$this->systemmodel->changeLng("คู่สมรส")?></span>
                    <!-- Spouse -->
                    <input type='text' class="form-control" id='PFC_Spouse' />
                  </div>
                </div>
                <div class="panel panel-default">
                  <div class="panel-heading"><b><?=$this->systemmodel->changeLng("เบี้ยประกันชีวิต")?></b></div>
                  <div class="panel-body">
                    <span><?=$this->systemmodel->changeLng("ผู้มีเงินได้")?></span>
                    
                    <input type='text' class="form-control" id='LIP_Personnel' />
                    <span><?=$this->systemmodel->changeLng("คู่สมรส")?></span>

                    <input type='text' class="form-control" id='LIP_Spouse' />
                  </div>
                </div>
                <div class="panel panel-default">
                  <div class="panel-heading"><b><?=$this->systemmodel->changeLng("กองทุนรวมเพื่อการเลี้ยงชีพ")?></b></div>
                  <div class="panel-body">
                    <span><?=$this->systemmodel->changeLng("ผู้มีเงินได้")?></span>
                    
                    <input type='text' class="form-control" id='RMF_Personnel' />
                    <span><?=$this->systemmodel->changeLng("คู่สมรส")?></span>

                    <input type='text' class="form-control" id='RMF_Spouse' />
                  </div>
                </div>
                <div class="panel panel-default">
                  <div class="panel-heading"><b><?=$this->systemmodel->changeLng("กองทุนรวมระยะยาว")?></b></div>
                  <div class="panel-body">
                    <span><?=$this->systemmodel->changeLng("ผู้มีเงินได้")?></span>
                    
                    <input type='text' class="form-control" id='LTQF_Personnel' />
                    <span><?=$this->systemmodel->changeLng("คู่สมรส")?></span>

                    <input type='text' class="form-control" id='LTQF_Spouse' />
                  </div>
                </div>
                <div class="panel panel-default">
                  <div class="panel-heading"><b><?=$this->systemmodel->changeLng("เงินบริจาค")?></b></div>
                  <div class="panel-body">
                    <span><?=$this->systemmodel->changeLng("ผู้มีเงินได้")?></span>
                    
                    <input type='text' class="form-control" id='CC_Personnel' />
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <p></p>
              <div class="panel panel-default panel-body">
                <div class="panel panel-default">
                  <div class="panel-heading"><b><?=$this->systemmodel->changeLng("เงินบริจาคเพื่อการศึกษา")?></b></div>
                  <div class="panel-body">
                    <span><?=$this->systemmodel->changeLng("ผู้มีเงินได้")?></span>
                    
                    <input type='text' class="form-control" id='CTE_Personnel' />
                    <span><?=$this->systemmodel->changeLng("คู่สมรส")?></span>

                    <input type='text' class="form-control" id='CTE_Spouse' />
                  </div>
                </div>
                <div class="panel panel-default">
                  <div class="panel-heading"><b><?=$this->systemmodel->changeLng("เบี้ยประกันสุขภาพมารดา")?></b></div>
                  <div class="panel-body">
                    <span><?=$this->systemmodel->changeLng("ผู้มีเงินได้")?></span>
                    
                    <input type='text' class="form-control" id='HIM_Personnel' />
                    <span><?=$this->systemmodel->changeLng("คู่สมรส")?></span>

                    <input type='text' class="form-control" id='HIM_Spouse' />
                  </div>
                </div>
                <div class="panel panel-default">
                  <div class="panel-heading"><b><?=$this->systemmodel->changeLng("เบี้ยประกันสุขภาพบิดา")?></b></div>
                  <div class="panel-body">
                    <span><?=$this->systemmodel->changeLng("ผู้มีเงินได้")?></span>
                    
                    <input type='text' class="form-control" id='HIF_Personnel' />
                    <span><?=$this->systemmodel->changeLng("คู่สมรส")?></span>
                    
                    <input type='text' class="form-control" id='HIF_Spouse' />
                  </div>
                </div>
                <div class="panel panel-default">
                  <div class="panel-heading"><b><?=$this->systemmodel->changeLng("เงินอุปการะผู้มีเงินได้")?></b></div>
                  <div class="panel-body">
                    <span><?=$this->systemmodel->changeLng("บิดา")?></span>
                    
                    <input type='text' class="form-control" id='FAP_Farther' />
                    <span><?=$this->systemmodel->changeLng("มารดา")?></span>
                    
                    <input type='text' class="form-control" id='FAP_Mother' />
                  </div>
                </div>
                <div class="panel panel-default">
                  <div class="panel-heading"><b><?=$this->systemmodel->changeLng("เงินอุปการะคู่สมรส")?></b></div>
                  <div class="panel-body">
                    <span><?=$this->systemmodel->changeLng("บิดา")?></span>
                    
                    <input type='text' class="form-control" id='FAS_Farther' />
                    <span><?=$this->systemmodel->changeLng("มารดา")?></span>
                    
                    <input type='text' class="form-control" id='FAS_Mother' />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div id="menu4" class="tab-pane fade">
        <h3></h3>
        <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="panel-title"><?=$this->systemmodel->changeLng("กองทุนสำรองเลี้ยงชีพ")?></h4>
          </div>
          <div class="panel-body ">
              <!-- Provident Fund Code
              <input type='text' class="form-control" id='PFCode' />
              Provident Fund Name
              <input type='text' class="form-control" id='PFName' /> -->
              <?=$this->systemmodel->changeLng("วันที่เข้าร่วม")?>
               <div class='input-group date' id='datetimepicker1'>
                <input type='text' class="form-control" id='JoinedDate' name="JoinedDate" />
                <span class="input-group-addon">
                  <span class="glyphicon glyphicon-calendar"></span>
                </span>
              </div>
            <script>
            $(document).ready(function(){
              var date_input=$('input[name="JoinedDate"]');
              var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
              date_input.datepicker({
                format: 'yyyy-mm-dd',
                container: container,
                todayHighlight: true,
                autoclose: true,
              })
            })
            </script>
              <?=$this->systemmodel->changeLng("วันที่ลาออกจากกองทุน")?>
               <div class='input-group date' id='datetimepicker2'>
                <input type='text' class="form-control" id='ResignedDate' name="ResignedDate" />
                <span class="input-group-addon">
                  <span class="glyphicon glyphicon-calendar"></span>
                </span>
              </div>
            <script>
            $(document).ready(function(){
              var date_input=$('input[name="ResignedDate"]');
              var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
              date_input.datepicker({
                format: 'yyyy-mm-dd',
                container: container,
                todayHighlight: true,
                autoclose: true,
              })
            })
            </script>
              <?=$this->systemmodel->changeLng("เปอร์เซ็นการมีส่วนร่วมของพนักงาน")?>
              <input type='text' class="form-control" id='EmployeeContributionPercentage' />
              <?=$this->systemmodel->changeLng("เปอร์เซ็นการมีส่วนร่วมของบริษัท")?>
              <input type='text' class="form-control" id='CompaneeContribtutionPercentage' />
          </div>
        </div>
      </div>
      <div id="menu5" class="tab-pane fade">
        <h3></h3>
        <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="panel-title"><?=$this->systemmodel->changeLng("วุฒิการศึกษา")?></h4>
          </div>
          <div class="panel-body ">
            <table class="table table-bordered table-striped table-hover">
              <thead>
                <tr>
                  <td><?=$this->systemmodel->changeLng("ลำดับที่")?></td>
                  <td><?=$this->systemmodel->changeLng("วุฒิการศึกษา")?></td>
                  <td><?=$this->systemmodel->changeLng("วิชาเอก")?></td>
                  <td><?=$this->systemmodel->changeLng("สถาบัน")?></td>
                  <td><?=$this->systemmodel->changeLng("ปีที่สำเร็จการศึกษา")?></td>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>1.</td>
                  <td>
                    <select class="form-control" id='QualificationID1'>
                      <option value="0">
                        <?=$this->systemmodel->changeLng("กรุณาเลือก")?>
                      </option>
                      <?php
                      foreach($results_Qualification as $result){
                        ?>
                        <option value="<?php echo $result->QualificationID; ?>">
                          <?php echo $result->QualificationNameEN." | ".$result->QualificationNameLL; ?>
                        </option>
                        <?php
                      } ?>
                    </select>
                  </td>
                  <td>
                    <select class="form-control" id='MajorID1'>
                      <option value="0">
                        <?=$this->systemmodel->changeLng("กรุณาเลือก")?>
                      </option>
                      <?php
                      foreach($results_Major as $result){
                        ?>
                        <option value="<?php echo $result->QualificationID; ?>">
                          <?php echo $result->QualificationMajorEN." | ".$result->QualificationMajorLL; ?>
                        </option>
                        <?php
                      } ?>
                    </select>
                  </td>
                  <td><input type='text' class="form-control" id='Institution1' /></td>
                  <td><input type='text' class="form-control" id='GraduateYear1' /></td>
                </tr>
                <tr>
                  <td>2.</td>
                  <td>
                    <select class="form-control" id='QualificationID2'>
                      <option value="0">
                        <?=$this->systemmodel->changeLng("กรุณาเลือก")?>
                      </option>
                      <?php
                      foreach($results_Qualification as $result){
                        ?>
                        <option value="<?php echo $result->QualificationID; ?>">
                          <?php echo $result->QualificationNameEN." | ".$result->QualificationNameLL; ?>
                        </option>
                        <?php
                      } ?>
                    </select>
                  </td>
                  <td>
                    <select class="form-control" id='MajorID2'>
                      <option value="0">
                        <?=$this->systemmodel->changeLng("กรุณาเลือก")?>
                      </option>
                      <?php
                      foreach($results_Major as $result){
                        ?>
                        <option value="<?php echo $result->QualificationID; ?>">
                          <?php echo $result->QualificationMajorEN." | ".$result->QualificationMajorLL; ?>
                        </option>
                        <?php
                      } ?>
                    </select>
                  </td>
                  <td><input type='text' class="form-control" id='Institution2' /></td>
                  <td><input type='text' class="form-control" id='GraduateYear2' /></td>
                </tr>
                <tr>
                  <td>3.</td>
                  <td>
                    <select class="form-control" id='QualificationID3'>
                      <option value="0">
                        <?=$this->systemmodel->changeLng("กรุณาเลือก")?>
                      </option>
                      <?php
                      foreach($results_Qualification as $result){
                        ?>
                        <option value="<?php echo $result->QualificationID; ?>">
                          <?php echo $result->QualificationNameEN." | ".$result->QualificationNameLL; ?>
                        </option>
                        <?php
                      } ?>
                    </select>
                  </td>
                  <td>
                    <select class="form-control" id='MajorID3'>
                      <option value="0">
                        <?=$this->systemmodel->changeLng("กรุณาเลือก")?>
                      </option>
                      <?php
                      foreach($results_Major as $result){
                        ?>
                        <option value="<?php echo $result->QualificationID; ?>">
                          <?php echo $result->QualificationMajorEN." | ".$result->QualificationMajorLL; ?>
                        </option>
                        <?php
                      } ?>
                    </select>
                  </td>
                  <td><input type='text' class="form-control" id='Institution3' /></td>
                  <td><input type='text' class="form-control" id='GraduateYear3' /></td>
                </tr>
                <tr>
                  <td>4.</td>
                  <td>
                    <select class="form-control" id='QualificationID4'>
                      <option value="0">
                        <?=$this->systemmodel->changeLng("กรุณาเลือก")?>
                      </option>
                      <?php
                      foreach($results_Qualification as $result){
                        ?>
                        <option value="<?php echo $result->QualificationID; ?>">
                          <?php echo $result->QualificationNameEN." | ".$result->QualificationNameLL; ?>
                        </option>
                        <?php
                      } ?>
                    </select>
                  </td>
                  <td>
                    <select class="form-control" id='MajorID4'>
                      <option value="0">
                        <?=$this->systemmodel->changeLng("กรุณาเลือก")?>
                      </option>
                      <?php
                      foreach($results_Major as $result){
                        ?>
                        <option value="<?php echo $result->QualificationID; ?>">
                          <?php echo $result->QualificationMajorEN." | ".$result->QualificationMajorLL; ?>
                        </option>
                        <?php
                      } ?>
                    </select>
                  </td>
                  <td><input type='text' class="form-control" id='Institution4' /></td>
                  <td><input type='text' class="form-control" id='GraduateYear4' /></td>
                </tr>
                <tr>
                  <td>5.</td>
                  <td>
                    <select class="form-control" id='QualificationID5'>
                      <option value="0">
                        <?=$this->systemmodel->changeLng("กรุณาเลือก")?>
                      </option>
                      <?php
                      foreach($results_Qualification as $result){
                        ?>
                        <option value="<?php echo $result->QualificationID; ?>">
                          <?php echo $result->QualificationNameEN." | ".$result->QualificationNameLL; ?>
                        </option>
                        <?php
                      } ?>
                    </select>
                  </td>
                  <td>
                    <select class="form-control" id='MajorID5'>
                      <option value="0">
                        <?=$this->systemmodel->changeLng("กรุณาเลือก")?>
                      </option>
                      <?php
                      foreach($results_Major as $result){
                        ?>
                        <option value="<?php echo $result->QualificationID; ?>">
                          <?php echo $result->QualificationMajorEN." | ".$result->QualificationMajorLL; ?>
                        </option>
                        <?php
                      } ?>
                    </select>
                  </td>
                  <td><input type='text' class="form-control" id='Institution5' /></td>
                  <td><input type='text' class="form-control" id='GraduateYear5' /></td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div id="menu6" class="tab-pane fade">
        <h3></h3>
        <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="panel-title">Skills</h4>
          </div>
          <div class="panel-body ">
            <table class="table table-bordered table-striped table-hover">
              <thead>
                <tr>
                  <td><?=$this->systemmodel->changeLng("ลำดับที่")?></td>
                  <td><?=$this->systemmodel->changeLng("รายละเอียด")?></td>
                  <td><?=$this->systemmodel->changeLng("ใบอนุญาต")?></td>
                  <td><?=$this->systemmodel->changeLng("หมายเหตุ")?></td>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>1.</td>
                  <td>
                    <select class="form-control" id='SkillID1'>
                      <option value="0"><?=$this->systemmodel->changeLng("กรุณาเลือก")?></option>
                      <?php foreach($results_Skills as $result){ ?>
                        <option value="<?php echo $result->SkillID; ?>">
                          <?php echo $result->SkillNameEN." | ".$result->SkillNameLL; ?>
                        </option>
                      <?php } ?>
                    </select>
                  </td>
                  <td><input type='text' class="form-control" id='License1' /></td>
                  <td><input type='text' class="form-control" id='Remarks1' /></td>
                </tr>
                <tr>
                  <td>2.</td>
                  <td>
                    <select class="form-control" id='SkillID2'>
                      <option value="0"><?=$this->systemmodel->changeLng("กรุณาเลือก")?></option>
                      <?php foreach($results_Skills as $result){ ?>
                        <option value="<?php echo $result->SkillID; ?>">
                          <?php echo $result->SkillNameEN." | ".$result->SkillNameLL; ?>
                        </option>
                      <?php } ?>
                    </select>
                  </td>
                  <td><input type='text' class="form-control" id='License2' /></td>
                  <td><input type='text' class="form-control" id='Remarks2' /></td>
                </tr>
                <tr>
                  <td>3.</td>
                  <td>
                    <select class="form-control" id='SkillID3'>
                      <option value="0"><?=$this->systemmodel->changeLng("กรุณาเลือก")?></option>
                      <?php foreach($results_Skills as $result){ ?>
                        <option value="<?php echo $result->SkillID; ?>">
                          <?php echo $result->SkillNameEN." | ".$result->SkillNameLL; ?>
                        </option>
                      <?php } ?>
                    </select>
                  </td>
                  <td><input type='text' class="form-control" id='License3' /></td>
                  <td><input type='text' class="form-control" id='Remarks3' /></td>
                </tr>
                <tr>
                  <td>4.</td>
                  <td>
                    <select class="form-control" id='SkillID4'>
                      <option value="0"><?=$this->systemmodel->changeLng("กรุณาเลือก")?></option>
                      <?php foreach($results_Skills as $result){ ?>
                        <option value="<?php echo $result->SkillID; ?>">
                          <?php echo $result->SkillNameEN." | ".$result->SkillNameLL; ?>
                        </option>
                      <?php } ?>
                    </select>
                  </td>
                  <td><input type='text' class="form-control" id='License4' /></td>
                  <td><input type='text' class="form-control" id='Remarks4' /></td>
                </tr>
                <tr>
                  <td>5.</td>
                  <td>
                    <select class="form-control" id='SkillID5'>
                      <option value="0"><?=$this->systemmodel->changeLng("กรุณาเลือก")?></option>
                      <?php foreach($results_Skills as $result){ ?>
                        <option value="<?php echo $result->SkillID; ?>">
                          <?php echo $result->SkillNameEN." | ".$result->SkillNameLL; ?>
                        </option>
                      <?php } ?>
                    </select>
                  </td>
                  <td><input type='text' class="form-control" id='License5' /></td>
                  <td><input type='text' class="form-control" id='Remarks5' /></td>
                </tr>
                <tr>
                  <td>6.</td>
                  <td>
                    <select class="form-control" id='SkillID6'>
                      <option value="0"><?=$this->systemmodel->changeLng("กรุณาเลือก")?></option>
                      <?php foreach($results_Skills as $result){ ?>
                        <option value="<?php echo $result->SkillID; ?>">
                          <?php echo $result->SkillNameEN." | ".$result->SkillNameLL; ?>
                        </option>
                      <?php } ?>
                    </select>
                  </td>
                  <td><input type='text' class="form-control" id='License6' /></td>
                  <td><input type='text' class="form-control" id='Remarks6' /></td>
                </tr>
                <tr>
                  <td>7.</td>
                  <td>
                    <select class="form-control" id='SkillID7'>
                      <option value="0"><?=$this->systemmodel->changeLng("กรุณาเลือก")?></option>
                      <?php foreach($results_Skills as $result){ ?>
                        <option value="<?php echo $result->SkillID; ?>">
                          <?php echo $result->SkillNameEN." | ".$result->SkillNameLL; ?>
                        </option>
                      <?php } ?>
                    </select>
                  </td>
                  <td><input type='text' class="form-control" id='License7' /></td>
                  <td><input type='text' class="form-control" id='Remarks7' /></td>
                </tr>
                <tr>
                  <td>8.</td>
                  <td>
                    <select class="form-control" id='SkillID8'>
                      <option value="0"><?=$this->systemmodel->changeLng("กรุณาเลือก")?></option>
                      <?php foreach($results_Skills as $result){ ?>
                        <option value="<?php echo $result->SkillID; ?>">
                          <?php echo $result->SkillNameEN." | ".$result->SkillNameLL; ?>
                        </option>
                      <?php } ?>
                    </select>
                  </td>
                  <td><input type='text' class="form-control" id='License8' /></td>
                  <td><input type='text' class="form-control" id='Remarks8' /></td>
                </tr>
                <tr>
                  <td>9.</td>
                  <td>
                    <select class="form-control" id='SkillID9'>
                      <option value="0"><?=$this->systemmodel->changeLng("กรุณาเลือก")?></option>
                      <?php foreach($results_Skills as $result){ ?>
                        <option value="<?php echo $result->SkillID; ?>">
                          <?php echo $result->SkillNameEN." | ".$result->SkillNameLL; ?>
                        </option>
                      <?php } ?>
                    </select>
                  </td>
                  <td><input type='text' class="form-control" id='License9' /></td>
                  <td><input type='text' class="form-control" id='Remarks9' /></td>
                </tr>
                <tr>
                  <td>10.</td>
                  <td>
                    <select class="form-control" id='SkillID10'>
                      <option value="0"><?=$this->systemmodel->changeLng("กรุณาเลือก")?></option>
                      <?php foreach($results_Skills as $result){ ?>
                        <option value="<?php echo $result->SkillID; ?>">
                          <?php echo $result->SkillNameEN." | ".$result->SkillNameLL; ?>
                        </option>
                      <?php } ?>
                    </select>
                  </td>
                  <td><input type='text' class="form-control" id='License10' /></td>
                  <td><input type='text' class="form-control" id='Remarks10' /></td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div id="menu7" class="tab-pane fade">
        <h3></h3>
        <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="panel-title"><?=$this->systemmodel->changeLng("ความสัมพันธ์ทางครอบครัว")?></h4>
          </div>
          <div class="panel-body ">
            <table class="table table-bordered table-striped table-hover">
              <thead>
                <tr>
                  <td><?=$this->systemmodel->changeLng("ลำดับที่")?></td>
                  <td><?=$this->systemmodel->changeLng("ชื่อ-นามสกุล")?></td>
                  <td><?=$this->systemmodel->changeLng("หมายเลขโทรศัพท์")?></td>
                  <td><?=$this->systemmodel->changeLng("ความสัมพันธ์")?></td>
                  <td><?=$this->systemmodel->changeLng("สถานที่อยู่")?></td>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>1.</td>
                  <td><input type='text' class="form-control" id='fullname1' /></td>
                  <td><input type='text' class="form-control" id='phonenumber1' /></td>
                  <td>
                    <select class="form-control" id='FMRelationID1'>
                      <option value="0"><?=$this->systemmodel->changeLng("กรุณาเลือก")?></option>
                      <?php foreach($results_Relationship as $result){ ?>
                        <option value="<?php echo $result->FMRelationID; ?>">
                          <?php echo $result->FMRelationNameEN." | ".$result->FMRelationNameLL; ?>
                        </option>
                        <?php } ?>
                    </select>
                  </td>
                  <td><input type='text' class="form-control" id='Address1' /></td>
                </tr>
                <tr>
                  <td>2.</td>
                  <td><input type='text' class="form-control" id='fullname2' /></td>
                  <td><input type='text' class="form-control" id='phonenumber2' /></td>
                  <td>
                    <select class="form-control" id='FMRelationID2'>
                      <option value="0"><?=$this->systemmodel->changeLng("กรุณาเลือก")?></option>
                      <?php foreach($results_Relationship as $result){ ?>
                        <option value="<?php echo $result->FMRelationID; ?>">
                          <?php echo $result->FMRelationNameEN." | ".$result->FMRelationNameLL; ?>
                        </option>
                        <?php } ?>
                    </select>
                  </td>
                  <td><input type='text' class="form-control" id='Address2' /></td>
                </tr>
                <tr>
                  <td>3.</td>
                  <td><input type='text' class="form-control" id='fullname3' /></td>
                  <td><input type='text' class="form-control" id='phonenumber3' /></td>
                  <td>
                    <select class="form-control" id='FMRelationID3'>
                      <option value="0"><?=$this->systemmodel->changeLng("กรุณาเลือก")?></option>
                      <?php foreach($results_Relationship as $result){ ?>
                        <option value="<?php echo $result->FMRelationID; ?>">
                          <?php echo $result->FMRelationNameEN." | ".$result->FMRelationNameLL; ?>
                        </option>
                        <?php } ?>
                    </select>
                  </td>
                  <td><input type='text' class="form-control" id='Address3' /></td>
                </tr>
                <tr>
                  <td>4.</td>
                  <td><input type='text' class="form-control" id='fullname4' /></td>
                  <td><input type='text' class="form-control" id='phonenumber4' /></td>
                  <td>
                    <select class="form-control" id='FMRelationID4'>
                      <option value="0"><?=$this->systemmodel->changeLng("กรุณาเลือก")?></option>
                      <?php foreach($results_Relationship as $result){ ?>
                        <option value="<?php echo $result->FMRelationID; ?>">
                          <?php echo $result->FMRelationNameEN." | ".$result->FMRelationNameLL; ?>
                        </option>
                        <?php } ?>
                    </select>
                  </td>
                  <td><input type='text' class="form-control" id='Address4' /></td>
                </tr>
                <tr>
                  <td>5.</td>
                  <td><input type='text' class="form-control" id='fullname5' /></td>
                  <td><input type='text' class="form-control" id='phonenumber5' /></td>
                  <td>
                    <select class="form-control" id='FMRelationID5'>
                      <option value="0"><?=$this->systemmodel->changeLng("กรุณาเลือก")?></option>
                      <?php foreach($results_Relationship as $result){ ?>
                        <option value="<?php echo $result->FMRelationID; ?>">
                          <?php echo $result->FMRelationNameEN." | ".$result->FMRelationNameLL; ?>
                        </option>
                        <?php } ?>
                    </select>
                  </td>
                  <td><input type='text' class="form-control" id='Address5' /></td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-2" align="right">
      <button type="submit" class="btn btn-primary btn-md btn-block" id="btn_save"><?=$this->systemmodel->changeLng("บันทึก")?></button>
    </div>
    <!-- จบส่วนแสดงผล -->
  </div>
</div>
</form>
<div id="showreult">
</div>


<!-- Bootstrap-Select -->
<script src="<?php echo base_url();?>assets/bootstrap-select/js/bootstrap-select.js"></script>
<script src="<?php echo base_url();?>assets/chartjs/Chart.bundle.js"></script>
<script src="<?php echo base_url();?>assets/chartjs/Chart.js"></script>
<script src="<?php echo base_url();?>assets/bootstrap/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url();?>assets/jquery-datatable/jquery.dataTables.js"></script>
<script src="<?php echo base_url();?>assets/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
<script src="<?php echo base_url();?>assets/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url();?>assets/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
<script src="<?php echo base_url();?>assets/jquery-datatable/extensions/export/jszip.min.js"></script>
<script src="<?php echo base_url();?>assets/jquery-datatable/extensions/export/pdfmake.min.js"></script>
<script src="<?php echo base_url();?>assets/jquery-datatable/extensions/export/vfs_fonts.js"></script>
<script src="<?php echo base_url();?>assets/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
<script src="<?php echo base_url();?>assets/jquery-datatable/extensions/export/buttons.print.min.js"></script>
<script src="<?php echo base_url();?>assets/jQuery-File-Upload/js/vendor/jquery.ui.widget.js"></script>
<script src="<?php echo base_url();?>assets/jQuery-File-Upload/js/jquery.iframe-transport.js"></script>
<script src="<?php echo base_url();?>assets/jQuery-File-Upload/js/jquery.fileupload.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<script type="text/javascript">
$('form').submit(function(event) {
	var pb_val = $('#PrimaryBank').prop('checked') ?  1 :  0;
	var at_val = $('#Activate').prop('checked') ?  1 :  0;
  $.ajax({
	url:'<?=base_url()?>index.php/hr/updateotherinformation',
    type: "post",
    data: {
    EmployeeID: <?php echo $this->uri->segment(4);?>,
    EmployeePhoto: $("#EmployeePhoto").val(),
    CellPhone:$("#CellPhone").val(),
    BloodGroup:$("#BloodGroup").val(),
    Email:$("#Email").val(),
    LineID:$("#LineID").val(),

    EffectiveDate:$("#EffectiveDate").val(),
    Bank:$("#Bank").val(),
    BankAccountName:$("#BankAccountName").val(),
    BankAccountNo:$("#BankAccountNo").val(),
    BankBranch:$("#BankBranch").val(),
    PrimaryBank:pb_val,
    Activate:at_val,

    
    Doc1:$("#Doc1").val(),
    Doc2:$("#Doc2").val(),
    Doc3:$("#Doc3").val(),

    PFC_Personnel:$("#PFC_Personnel").val(),
    PFC_Spouse:$("#PFC_Spouse").val(),
    LIP_Personnel:$("#LIP_Personnel").val(),
    LIP_Spouse:$("#LIP_Spouse").val(),
    RMF_Personnel:$("#RMF_Personnel").val(),
    RMF_Spouse:$("#RMF_Spouse").val(),
    LTQF_Personnel:$("#LTQF_Personnel").val(),
    LTQF_Spouse:$("#LTQF_Spouse").val(),
    CC_Personnel:$("#CC_Personnel").val(),
    CTE_Personnel:$("#CTE_Personnel").val(),
    CTE_Spouse:$("#CTE_Spouse").val(),
    HIM_Personnel:$("#HIM_Personnel").val(),
    HIM_Spouse:$("#HIM_Spouse").val(),
    HIF_Personnel:$("#HIF_Personnel").val(),
    HIF_Spouse:$("#HIF_Spouse").val(),
    FAP_Farther:$("#FAP_Farther").val(),
    FAP_Mother:$("#FAP_Mother").val(),
    FAS_Farther:$("#FAS_Farther").val(),
    FAS_Mother:$("#FAS_Mother").val(),

    QualificationID1:$("#QualificationID1").val(),
    MajorID1:$("#MajorID1").val(),
    Institution1:$("#Institution1").val(),
    GraduateYear1:$("#GraduateYear1").val(),
    QualificationID2:$("#QualificationID2").val(),
    MajorID2:$("#MajorID2").val(),
    Institution2:$("#Institution2").val(),
    GraduateYear2:$("#GraduateYear2").val(),
    QualificationID3:$("#QualificationID3").val(),
    MajorID3:$("#MajorID3").val(),
    Institution3:$("#Institution3").val(),
    GraduateYear3:$("#GraduateYear3").val(),
    QualificationID4:$("#QualificationID4").val(),
    MajorID4:$("#MajorID4").val(),
    Institution4:$("#Institution4").val(),
    GraduateYear4:$("#GraduateYear4").val(),
    QualificationID5:$("#QualificationID5").val(),
    MajorID5:$("#MajorID5").val(),
    Institution5:$("#Institution5").val(),
    GraduateYear5:$("#GraduateYear5").val(),

    SkillID1:$("#SkillID1").val(),
    License1:$("#License1").val(),
    Remarks1:$("#Remarks1").val(),
    SkillID2:$("#SkillID2").val(),
    License2:$("#License2").val(),
    Remarks2:$("#Remarks2").val(),
    SkillID3:$("#SkillID3").val(),
    License3:$("#License3").val(),
    Remarks3:$("#Remarks3").val(),
    SkillID4:$("#SkillID4").val(),
    License4:$("#License4").val(),
    Remarks4:$("#Remarks4").val(),
    SkillID5:$("#SkillID5").val(),
    License5:$("#License5").val(),
    Remarks5:$("#Remarks5").val(),
    SkillID6:$("#SkillID6").val(),
    License6:$("#License6").val(),
    Remarks6:$("#Remarks6").val(),
    SkillID7:$("#SkillID7").val(),
    License7:$("#License7").val(),
    Remarks7:$("#Remarks7").val(),
    SkillID8:$("#SkillID8").val(),
    License8:$("#License8").val(),
    Remarks8:$("#Remarks8").val(),
    SkillID9:$("#SkillID9").val(),
    License9:$("#License9").val(),
    Remarks9:$("#Remarks9").val(),
    SkillID10:$("#SkillID10").val(),
    License10:$("#License10").val(),
    Remarks10:$("#Remarks10").val(),

    fullname1:$("#fullname1").val(),
    phonenumber1:$("#phonenumber1").val(),
    FMRelationID1:$("#FMRelationID1").val(),
    Address1:$("#Address1").val(),
    fullname2:$("#fullname2").val(),
    phonenumber2:$("#phonenumber2").val(),
    FMRelationID2:$("#FMRelationID2").val(),
    Address2:$("#Address2").val(),
    fullname3:$("#fullname3").val(),
    phonenumber3:$("#phonenumber3").val(),
    FMRelationID3:$("#FMRelationID3").val(),
    Address3:$("#Address3").val(),
    fullname4:$("#fullname4").val(),
    phonenumber4:$("#phonenumber4").val(),
    FMRelationID4:$("#FMRelationID4").val(),
    Address4:$("#Address4").val(),
    fullname5:$("#fullname5").val(),
    phonenumber5:$("#phonenumber5").val(),
    FMRelationID5:$("#FMRelationID5").val(),
    Address5:$("#Address5").val(),



    PFCode:$("#PFCode").val(),
    PFName:$("#PFName").val(),
    JoinedDate:$("#JoinedDate").val(),
    ResignedDate:$("#ResignedDate").val(),
    EmployeeContributionPercentage:$("#EmployeeContributionPercentage").val(),
    CompaneeContribtutionPercentage:$("#CompaneeContribtutionPercentage").val()
          },
    beforeSend: function () {$(".loading").show();},
    complete: function () {$(".loading").hide();},
    success: function (data) {
    swal({
      title: "Add Employee Movement Complete",
      text: "Are you need to exit!",
      icon: "success",
      buttons: true,
      dangerMode: false,
    })
    .then((willDelete) => {
      if (willDelete) {
        $(location).attr('href', '/index.php/hr/PerInformation');
      } else {
        
      }
    });
  //     $("#showreult").html(data);
  //     swal({
		//   title: "Other Information",
		//   text: "You update other information success.",
		//   type: "success",
		//   showCancelButton: false,
		//   confirmButtonClass: "btn-primary",
		//   confirmButtonText: "OK",
		//   closeOnConfirm: false
		// },
		// function(){
		//   $(location).attr('href', '/index.php/hr/PerInformation');
		// });
    }
}).done(function(data) {console.log(data);});event.preventDefault();});
</script>
<?php //print_r($results_EmployeeFMRelation);?>
<script>
$(function(){
  <?php foreach($results_Employee as $result){ ?>
    $("#EmployeePhoto").val("<?php echo $result->EmployeePhoto; ?>");
    $("#EmployeePhoto_show").attr("src","<?php echo base_url(); ?>assets/uploads/company/nc/files/<?php echo $result->EmployeePhoto; ?>");
    $("#CellPhone").val("<?php echo $result->CellPhone; ?>");
    $("#BloodGroup").val("<?php echo $result->BloodGroup; ?>");
    $("#Email").val("<?php echo $result->Email; ?>");
    $("#LineID").val("<?php echo $result->LineID; ?>");

    $("#EffectiveDate").val("<?php echo $result->EffectiveDate; ?>");
    $("#Bank").val("<?php echo $result->Bank; ?>");
    $("#BankAccountName").val("<?php echo $result->BankAccountName; ?>");
    $("#BankAccountNo").val("<?php echo $result->BankAccountNo; ?>");
    $("#BankBranch").val("<?php echo $result->BankBranch; ?>");
    $("#PrimaryBank").prop("checked", <?php echo $result->PrimaryBank; ?>).change();
    $("#Activate").prop("checked", <?php echo $result->Activate; ?>).change();

    $("#Doc1").val("<?php echo $result->Doc1; ?>");
    $("#Doc2").val("<?php echo $result->Doc2; ?>");
    $("#Doc3").val("<?php echo $result->Doc3; ?>");
    $("#doc1_btn").attr("href","<?php echo base_url(); ?>assets/uploads/company/nc/documentstore/files/<?php echo $result->Doc1; ?>");
    $("#doc2_btn").attr("href","<?php echo base_url(); ?>assets/uploads/company/nc/documentstore/files/<?php echo $result->Doc2; ?>");
    $("#doc3_btn").attr("href","<?php echo base_url(); ?>assets/uploads/company/nc/documentstore/files/<?php echo $result->Doc3; ?>");

    $("#PFC_Personnel").val("<?php echo $result->PFC_Personnel; ?>");
    $("#PFC_Spouse").val("<?php echo $result->PFC_Spouse; ?>");
    $("#LIP_Personnel").val("<?php echo $result->LIP_Personnel; ?>");
    $("#LIP_Spouse").val("<?php echo $result->LIP_Spouse; ?>");
    $("#RMF_Personnel").val("<?php echo $result->RMF_Personnel; ?>");
    $("#RMF_Spouse").val("<?php echo $result->RMF_Spouse; ?>");
    $("#LTQF_Personnel").val("<?php echo $result->LTQF_Personnel; ?>");
    $("#LTQF_Spouse").val("<?php echo $result->LTQF_Spouse; ?>");
    $("#CC_Personnel").val("<?php echo $result->CC_Personnel; ?>");
    $("#CTE_Personnel").val("<?php echo $result->CTE_Personnel; ?>");
    $("#CTE_Spouse").val("<?php echo $result->CTE_Spouse; ?>");
    $("#HIM_Personnel").val("<?php echo $result->HIM_Personnel; ?>");
    $("#HIM_Spouse").val("<?php echo $result->HIM_Spouse; ?>");
    $("#HIF_Personnel").val("<?php echo $result->HIF_Personnel; ?>");
    $("#HIF_Spouse").val("<?php echo $result->HIF_Spouse; ?>");
    $("#FAP_Farther").val("<?php echo $result->FAP_Farther; ?>");
    $("#FAP_Mother").val("<?php echo $result->FAP_Mother; ?>");
    $("#FAS_Farther").val("<?php echo $result->FAS_Farther; ?>");
    $("#FAS_Mother").val("<?php echo $result->FAS_Mother; ?>");


    $("#PFCode").val("<?php echo $result->PFCode; ?>");
    $("#PFName").val("<?php echo $result->PFName; ?>");
    $("#JoinedDate").val("<?php echo $result->JoinedDate; ?>");
    $("#ResignedDate").val("<?php echo $result->ResignedDate; ?>");
    $("#EmployeeContributionPercentage").val("<?php echo $result->EmployeeContributionPercentage; ?>");
    $("#CompaneeContribtutionPercentage").val("<?php echo $result->CompaneeContribtutionPercentage; ?>");
  <?php } ?>

  <?php

   $i=0; foreach($results_EmployeeQualification as $result){ $i++; 
    if($i==1){?> 
      $("#QualificationID1").val("<?php echo $result->QualificationID; ?>");
      $("#MajorID1").val("<?php echo $result->MajorID; ?>");
      $("#Institution1").val("<?php echo $result->Institution; ?>");
      $("#GraduateYear1").val("<?php echo $result->GraduateYear; ?>");
  <?php } 
    else if($i==2){?> 
      $("#QualificationID2").val("<?php echo $result->QualificationID; ?>");
      $("#MajorID2").val("<?php echo $result->MajorID; ?>");
      $("#Institution2").val("<?php echo $result->Institution; ?>");
      $("#GraduateYear2").val("<?php echo $result->GraduateYear; ?>");
  <?php } 
    else if($i==3){?> 
      $("#QualificationID3").val("<?php echo $result->QualificationID; ?>");
      $("#MajorID3").val("<?php echo $result->MajorID; ?>");
      $("#Institution3").val("<?php echo $result->Institution; ?>");
      $("#GraduateYear3").val("<?php echo $result->GraduateYear; ?>");
  <?php } 
    else if($i==4){?> 
      $("#QualificationID4").val("<?php echo $result->QualificationID; ?>");
      $("#MajorID4").val("<?php echo $result->MajorID; ?>");
      $("#Institution4").val("<?php echo $result->Institution; ?>");
      $("#GraduateYear4").val("<?php echo $result->GraduateYear; ?>");
  <?php } 
    else if($i==5){?> 
      $("#QualificationID5").val("<?php echo $result->QualificationID; ?>");
      $("#MajorID5").val("<?php echo $result->MajorID; ?>");
      $("#Institution5").val("<?php echo $result->Institution; ?>");
      $("#GraduateYear5").val("<?php echo $result->GraduateYear; ?>");
  <?php } 


   } ?>

    <?php

   $i=0; foreach($results_EmployeeSkills as $result){ $i++; 
    if($i==1){?>  $("#SkillID1").val("<?php echo $result->SkillID; ?>");
      $("#License1").val("<?php echo $result->License; ?>");
      $("#Remarks1").val("<?php echo $result->Remarks; ?>"); <?php } 
    else if($i==2){?>  $("#SkillID2").val("<?php echo $result->SkillID; ?>");
      $("#License2").val("<?php echo $result->License; ?>");
      $("#Remarks2").val("<?php echo $result->Remarks; ?>"); <?php } 
    else if($i==3){?>  $("#SkillID3").val("<?php echo $result->SkillID; ?>");
      $("#License3").val("<?php echo $result->License; ?>");
      $("#Remarks3").val("<?php echo $result->Remarks; ?>"); <?php } 
    else if($i==4){?>  $("#SkillID4").val("<?php echo $result->SkillID; ?>");
      $("#License4").val("<?php echo $result->License; ?>");
      $("#Remarks4").val("<?php echo $result->Remarks; ?>"); <?php } 
    else if($i==5){?>  $("#SkillID5").val("<?php echo $result->SkillID; ?>");
      $("#License5").val("<?php echo $result->License; ?>");
      $("#Remarks5").val("<?php echo $result->Remarks; ?>"); <?php } 
    else if($i==6){?>  $("#SkillID6").val("<?php echo $result->SkillID; ?>");
      $("#License6").val("<?php echo $result->License; ?>");
      $("#Remarks6").val("<?php echo $result->Remarks; ?>"); <?php } 
    else if($i==7){?>  $("#SkillID7").val("<?php echo $result->SkillID; ?>");
      $("#License7").val("<?php echo $result->License; ?>");
      $("#Remarks7").val("<?php echo $result->Remarks; ?>"); <?php } 
    else if($i==8){?>  $("#SkillID8").val("<?php echo $result->SkillID; ?>");
      $("#License8").val("<?php echo $result->License; ?>");
      $("#Remarks8").val("<?php echo $result->Remarks; ?>"); <?php } 
    else if($i==9){?>  $("#SkillID9").val("<?php echo $result->SkillID; ?>");
      $("#License9").val("<?php echo $result->License; ?>");
      $("#Remarks9").val("<?php echo $result->Remarks; ?>"); <?php } 
    else if($i==10){?>  $("#SkillID10").val("<?php echo $result->SkillID; ?>");
      $("#License10").val("<?php echo $result->License; ?>");
      $("#Remarks10").val("<?php echo $result->Remarks; ?>"); <?php } 


   } ?>


  <?php

   $i=0; foreach($results_EmployeeFMRelation as $result){ $i++; 
    if($i==1){?> 
      $("#FMRelationID1").val("<?php echo $result->FMRelationID; ?>");
      $("#fullname1").val("<?php echo $result->fullname; ?>");
      $("#phonenumber1").val("<?php echo $result->phonenumber; ?>");
      $("#Address1").val("<?php echo $result->address; ?>");
  <?php } 
    else if($i==2){?> 
      $("#FMRelationID2").val("<?php echo $result->FMRelationID; ?>");
      $("#fullname2").val("<?php echo $result->fullname; ?>");
      $("#phonenumber2").val("<?php echo $result->phonenumber; ?>");
      $("#Address2").val("<?php echo $result->address; ?>");
  <?php } 
    else if($i==3){?> 
      $("#FMRelationID3").val("<?php echo $result->FMRelationID; ?>");
      $("#fullname3").val("<?php echo $result->fullname; ?>");
      $("#phonenumber3").val("<?php echo $result->phonenumber; ?>");
      $("#Address3").val("<?php echo $result->address; ?>");
  <?php } 
    else if($i==4){?> 
      $("#FMRelationID4").val("<?php echo $result->FMRelationID; ?>");
      $("#fullname4").val("<?php echo $result->fullname; ?>");
      $("#phonenumber4").val("<?php echo $result->phonenumber; ?>");
      $("#Address4").val("<?php echo $result->address; ?>");
  <?php } 
    else if($i==5){?> 
      $("#FMRelationID5").val("<?php echo $result->FMRelationID; ?>");
      $("#fullname5").val("<?php echo $result->fullname; ?>");
      $("#phonenumber5").val("<?php echo $result->phonenumber; ?>");
      $("#Address5").val("<?php echo $result->address; ?>");
  <?php } 


   } ?>

});
</script>

<script type="text/javascript">
$(function () {
    'use strict';
    // Change this to the location of your server-side upload handler:
    var url = '<?php echo base_url(); ?>assets/uploads/company/nc/';
    var url2 = '<?php echo base_url(); ?>assets/uploads/company/nc/documentstore/';

    $('#fileupload').fileupload({
        url: url,dataType: 'json',
        done: function (e, data) {
            $.each(data.result.files, function (index, file) {
              $('#EmployeePhoto').val(file.name);
              $('#btn_upload').hide();
              $('#btn_remove').show();
            });
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#progress .progress-bar').css('width',progress + '%');
        }
    }).prop('disabled', !$.support.fileInput).parent().addClass($.support.fileInput ? undefined : 'disabled');

    $('#Doc1upload').fileupload({
        url: url2,dataType: 'json',
        done: function (e, data) {
            $.each(data.result.files, function (index, file) {
              $('#Doc1').val(file.name);
              $('#btnDoc1').hide();
              $('#btnDoc1Remove').show();
            });
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#progressDoc1 .progress-bar').css('width',progress + '%');
        }
    }).prop('disabled', !$.support.fileInput).parent().addClass($.support.fileInput ? undefined : 'disabled');

    $('#Doc2upload').fileupload({
        url: url2,dataType: 'json',
        done: function (e, data) {
            $.each(data.result.files, function (index, file) {
              $('#Doc2').val(file.name);
              $('#btnDoc2').hide();
              $('#btnDoc2Remove').show();
            });
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#progressDoc2 .progress-bar').css('width',progress + '%');
        }
    }).prop('disabled', !$.support.fileInput).parent().addClass($.support.fileInput ? undefined : 'disabled');

    $('#Doc3upload').fileupload({
        url: url2,dataType: 'json',
        done: function (e, data) {
            $.each(data.result.files, function (index, file) {
              $('#Doc3').val(file.name);
              $('#btnDoc3').hide();
              $('#btnDoc3Remove').show();
            });
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#progressDoc3 .progress-bar').css('width',progress + '%');
        }
    }).prop('disabled', !$.support.fileInput).parent().addClass($.support.fileInput ? undefined : 'disabled');



});
function RemovePhoto(){
  $('#btn_upload').show();
  $('#btn_remove').hide();
  $('#EmployeePhoto').val("");
  $('#progress .progress-bar').css('width','0%');
}
function RemoveDoc1(){
  $('#btnDoc1').show();
  $('#btnDoc1Remove').hide();
  $('#Doc1').val("");
  $('#progressDoc1 .progress-bar').css('width','0%');
}
function RemoveDoc2(){
  $('#btnDoc2').show();
  $('#btnDoc2Remove').hide();
  $('#Doc2').val("");
  $('#progressDoc2 .progress-bar').css('width','0%');
}
function RemoveDoc3(){
  $('#btnDoc3').show();
  $('#btnDoc3Remove').hide();
  $('#Doc3').val("");
  $('#progressDoc3 .progress-bar').css('width','0%');
}
</script>