<script type="text/javascript">
  $(document).ready(function(){
    //เพิ่มเงื่อนไขตาราง
    $('.js-exportable').DataTable({
        dom: 'Blfrtip',responsive: true,paging: true,info: true,
        buttons: [
            {extend:'copyHtml5',text:' <i class="fa fa-files-o fa-2x "></i> ',titleAttr: ' Copy '},
            {extend:'excelHtml5',text:' <i class="fa fa-file-excel-o fa-2x "></i> ',titleAttr: ' Excel '},
            {extend:'print',text:' <i class="fa fa-print fa-2x "></i> <BR>&nbsp;',titleAttr: ' Print '}
          ,
        ]
        ,"lengthMenu": [ [10, 25, 50, -1], [10, 25, 50, "All"] ],
      });

    $('.js-exportable2').DataTable({
        dom: 'Bfrtip',responsive: true,paging: true,info: false,
        buttons: [
            {extend:'copyHtml5',text:' <i class="fa fa-files-o fa-2x "></i> ',titleAttr: ' Copy '},
            {extend:'excelHtml5',text:' <i class="fa fa-file-excel-o fa-2x "></i> ',titleAttr: ' Excel '},
            {extend:'print',text:' <i class="fa fa-print fa-2x "></i> ',titleAttr: ' Print '
            // ,exportOptions:{columns:[0,1,2,3,4,5,6]}
          },
        ]
        // ,"columnDefs": [{"targets": [ 0,1,2,3,4 ],"visible": false,"searchable": true}]
      });
//กรณีต้องการปิดการแสดงบาง column แต่สามารถ export file ได้
    $('.js-exportable3').DataTable({
        dom: 'Bfrtip',responsive: true,paging: true,info: false,
        buttons: [
            {extend:'copyHtml5',text:' <i class="fa fa-files-o fa-2x "></i> ',titleAttr: ' Copy '},
            {extend:'excelHtml5',text:' <i class="fa fa-file-excel-o fa-2x "></i> ',titleAttr: ' Excel '},
            {extend:'print',text:' <i class="fa fa-print fa-2x "></i> ',titleAttr: ' Print '
            ,exportOptions:{columns:[0,1,2,3,4,5,6]}
          },
        ],
        "columnDefs": [{"targets": [ 5 ],"visible": false,"searchable": true}]
      });
  });
</script>
<!-- begin add require script -->
<head>
  <link href="<?php echo base_url();?>assets/bootstrap-select/css/bootstrap-select.css" rel="stylesheet">
  <link href="<?php echo base_url();?>assets/bootstrap/css/datepicker.css" rel="stylesheet">
  <link href="<?php echo base_url();?>assets/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">
  <meta charset="UTF-8">
  
  <style>
  @media print {
    canvas {
      min-height: 100%;max-width: 100%;max-height: 100%;height: auto!important;width: auto!important;
    }
    table {
      min-height: 100%;max-width: 100%;max-height: 100%;height: auto!important;width: auto!important;
      page-break-inside: : auto;
    }
    tr{
      page-break-inside: avoid;
      page-break-after: auto;

    }
  }
  canvas{-moz-user-select: none;-webkit-user-select: none;-ms-user-select: none;}
  </style>
</head>

<!-- end add require script -->
<div class="box box-success">
  <div class="box-header" align="left">
        <i class="fa fa-sign-in"></i>
    <h3 class="box-title"><?php echo 'View '.$this->systemmodel->get_menuname($this->uri->segment(1) . '/' . $this->uri->segment(2)); // แสดงชื่อเมนู  ?></h3>
  </div>
  <div class="box-body">
    <!-- ส่วนแสดงผล -->

<div class="box box-success">
  <div class="box-header">
    <b></b>
  </div>
  <div class="box-body" align="left">

        <!-- ตาราง แสดงข้อมูล -->
        <table class="table table-bordered table-striped table-hover dataTable  js-exportable">
          <thead>
            <tr>
              <th scope="col"><center>No</center></th>
              <th scope="col"><center>Employee Code  </center></th>
              <th scope="col"><center><span class="glyphicon glyphicon-user"></span></center></th>
              <th scope="col"><center>Name  </center></th>
              <th scope="col"><center>Position </center></th>
              <th scope="col"><center>Start Date </center></th>
              <th scope="col"><center>Resign Date </center></th>
              <th scope="col"><center>Resignation</center></th>
            </tr>
          </thead>
          <tbody>
          <?php 
        $i=0;
        if(!empty($results_All)){
          foreach ($results_All as $row) {
          ?>
            <tr>
              <td scope="col"><?php echo $i+1;?></td>
              <td scope="col"><?php echo $row->EmployeeCode;?></td>
              <td scope="col">
                <!-- Trigger the modal with a button -->
                <?php if(!empty($row->EmployeePhoto)){?>
                <img src="<?php echo base_url(); ?>assets/uploads/company/nc/files/<?php echo $row->EmployeePhoto;?>" id="EmployeePhoto_show" class="img-circle" width="50px;"data-toggle="modal" data-target="#myModal<?php echo $i;?>" style="cursor: pointer;">
                <!-- Modal -->
                <div id="myModal<?php echo $i;?>" class="modal fade" role="dialog">
                  <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">                      
                      <div class="modal-body">
                        <img src="<?php echo base_url(); ?>assets/uploads/company/nc/files/<?php echo $row->EmployeePhoto;?>" id="EmployeePhoto_show" class="img-responsive">
                      </div>
                    </div>

                  </div>
                </div>
              <?php }?>
              </td>
              <td scope="col"><?php echo $row->FullNameLL;?></td>
              <td scope="col"><?php echo $row->PositionNameEng;?></td>
              <td scope="col"><?php echo $row->ServiceStartDate;?></td>
              <td scope="col"><?php echo $row->LeavingDate;?></td>
              <td scope="col" align="center">
				<?php if($row->ResignationTypeStatusOut==0){?> 
				<i class='btn btn-success btn-xs'>ปกติ</i>
				<button type="button" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#myModalEmployeeID" onclick="setEmployeeID(<?php echo $row->EmployeeID;?>,'<?php echo $row->FullNameLL;?>')"><span class="glyphicon glyphicon-edit"></span>Manage</button>
				<?php }else{?>
				<i class='btn btn-warning btn-xs'>ลาออก</i>
				<button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#myModalCheck<?php echo $row->EmployeeID;?>"><span class="glyphicon glyphicon-check"></span> Check</button>
				<div class="modal fade" id="myModalCheck<?php echo $row->EmployeeID;?>" role="dialog">
                  <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">ข้อมูลพนักงาน | <?php echo $row->FullNameLL;?></h4>
                      </div>
                      <div class="modal-body">
					  <table class="table table-bordered table-striped table-hover">
                                  <thead>
                                    <tr>
                                      <th colspan="2">Resignation information</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    <tr>
                                      <td align="right" width="25%">Resignation Type (*)</td>
                                      <td>
										<?php foreach($results_ResignationTypeOut as $result){ ?>
											<?php if($result->ResignID == $row->ResignationTypeOut) echo "".$result->ResignNameLL." | ".$result->ResignNameEN.""; ?>
										<?php } ?>
									  </td>
                                    </tr>
                                    <tr>
                                      <td align="right">Notification Days Required</td>
                                      <td><?php echo $row->NotificationDaysRequired; ?> Day</td>
                                    </tr>
									<tr>
                                      <td align="right">Resignation Reason (*)</td>
                                      <td>
									  <?php foreach($results_ResignationReason as $result){ ?>
											<?php if($result->ResignationReasonID == $row->ResignationReason) echo "".$result->ResignationReasonLL." | ".$result->ResignationReasonNameEN.""; ?>
										<?php } ?>
									  </td>
                                    </tr>
									<tr>
                                      <td align="right">Notification Date</td>
                                      <td><?php echo $row->NotificationDate; ?></td>
                                    </tr>
									<tr>
                                      <td align="right">Notification Expiration Date</td>
                                      <td><?php echo $row->NotificationExpirationDate; ?></td>
                                    </tr>
									<tr>
                                      <td align="right">Last working Day</td>
                                      <td><?php echo $row->LastworkingDay; ?></td>
                                    </tr>
									<tr>
                                      <td align="right">Leaving Date (*)</td>
                                      <td><?php echo $row->LeavingDate; ?></td>
                                    </tr>
									<tr>
                                      <td align="right">Last Day of Employment Date(*)</td>
                                      <td><?php echo $row->EffectiveDateOut; ?></td>
                                    </tr>
									<tr>
                                      <td align="right">Remark</td>
                                      <td><?php echo $row->Remark; ?></td>
                                    </tr>
                                  </tbody>
                                </table>
                      </div>
                    </div>
                    
                  </div>
                </div>
				<?php } ?>
              </td>
            </tr>
          <?php
          
          $i++;
        } 
        }  
          ?>
          </tbody>
        </table>
  </div>
</div>
</div>



<div class="modal fade" id="myModalEmployeeID" role="dialog">
                  <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title" id = "modalHeader"></h4>
                      </div>
                      <div class="modal-body">
            <form action="<?php echo  base_url('index.php/hr/updateResignation'); ?>" id="form" name="form" method="post">
            <table class="table table-bordered table-striped table-hover">
            <input type="hidden" id="EmployeeID" name="EmployeeID" value="">
                                  <thead>
                                    <tr>
                                      <th colspan="2">Resignation information</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    <tr>
                                      <td align="right" width="25%">Resignation Type (*)</td>
                                      <td>
                    <select class="form-control" name="ResignationTypeOut" id='ResignationTypeOut' required>
                    <option value="">Select Resignation Type</option>
                    <?php
                    foreach($results_ResignationTypeOut as $result){
                      ?>
                      <option value="<?php echo $result->ResignID; ?>">
                        <?php echo $result->ResignNameLL; ?> | <?php echo $result->ResignNameEN; ?>
                      </option>
                      <?php
                    } ?>
                    </select>
                    </td>
                                    </tr>
                                    <tr>
                                      <td align="right">Notification Days Required</td>
                                      <td><input type='number' class="form-control" id='NotificationDaysRequired' name="NotificationDaysRequired" required onfocusout="CalNotiExpire()"></td>
                                    </tr>
                  <tr>
                                      <td align="right">Resignation Reason (*)</td>
                                      <td>
                    <select class="form-control" name="ResignationReason" id='ResignationReason' required>
                    <option value="">Select Resignation Reason</option>
                    <?php
                    foreach($results_ResignationReason as $result){
                      ?>
                      <option value="<?php echo $result->ResignationReasonID; ?>">
                        <?php echo $result->ResignationReasonLL; ?> | <?php echo $result->ResignationReasonNameEN; ?>
                      </option>
                      <?php
                    } ?>
                    </select>
                    </td>
                                    </tr>
                  <tr>
                                      <td align="right">Notification Date</td>
                                      <td>
                    <div class='input-group date' id='datetimepicker'>
                    <input type='text' class="form-control" id='NotificationDate' name="NotificationDate" onchange="CalNotiExpire()" />
                    <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                    </div>
                    <script>
                    $(document).ready(function(){
                    var date_input=$('input[name="NotificationDate"]'); //our date input has the name "date"
                    var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
                    date_input.datepicker({
                      format: 'yyyy-mm-dd',
                      container: container,
                      todayHighlight: true,
                      autoclose: true,
                    })
                    })
                    </script>
                    </td>
                                    </tr>
                  <tr>
                                      <td align="right">Notification Expiration Date</td>
                                      <td>
                    <div class='input-group date' id='datetimepicker'>
                    <input type='text' class="form-control" id='NotificationExpirationDate' name="NotificationExpirationDate" readonly="true"/>
                    <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                    </div>
                    <script>
                    // $(document).ready(function(){
                    // var date_input=$('input[name="NotificationExpirationDate"]'); //our date input has the name "date"
                    // var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
                    // date_input.datepicker({
                    //   format: 'yyyy-mm-dd',
                    //   container: container,
                    //   todayHighlight: true,
                    //   autoclose: true,
                    // })
                    // })
                    </script>
                    </td>
                                    </tr>
                  <tr>
                                      <td align="right">Last working Day</td>
                                      <td>
                    <div class='input-group date' id='datetimepicker'>
                    <input type='text' class="form-control" id='LastworkingDay' name="LastworkingDay"/>
                    <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                    </div>
                    <script>
                    $(document).ready(function(){
                    var date_input=$('input[name="LastworkingDay"]'); //our date input has the name "date"
                    var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
                    date_input.datepicker({
                      format: 'yyyy-mm-dd',
                      container: container,
                      todayHighlight: true,
                      autoclose: true,
                    })
                    })
                    </script>
                    </td>
                                    </tr>
                  <tr>
                                      <td align="right">Leaving Date (*)</td>
                                      <td>
                    <div class='input-group date' id='datetimepicker'>
                    <input type='text' class="form-control" id='LeavingDate' name="LeavingDate" required/>
                    <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                    </div>
                    <script>
                    $(document).ready(function(){
                    var date_input=$('input[name="LeavingDate"]'); //our date input has the name "date"
                    var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
                    date_input.datepicker({
                      format: 'yyyy-mm-dd',
                      container: container,
                      todayHighlight: true,
                      autoclose: true,
                    })
                    })
                    </script>
                    </td>
                                    </tr>
                  <tr>
                                      <td align="right">Last Day of Employment Date(*)</td>
                                      <td>
                    <div class='input-group date' id='datetimepicker'>
                    <input type='text' class="form-control" id='EffectiveDateOut' name="EffectiveDateOut" required/>
                    <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                    </div>
                    <script>
                    $(document).ready(function(){
                    var date_input=$('input[name="EffectiveDateOut"]'); //our date input has the name "date"
                    var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
                    date_input.datepicker({
                      format: 'yyyy-mm-dd',
                      container: container,
                      todayHighlight: true,
                      autoclose: true,
                    })
                    })
                    </script>
                    </td>
                                    </tr>
                  <tr>
                                      <td align="right">Remark</td>
                                      <td><textarea name="Remark"  class="form-control" id="Remark" cols="50" rows="8"></textarea></td>
                                    </tr>
                                  </tbody>
                                </table>
            <button type="submit" class="btn btn-primary" id="btn_save">Save</button>
            </form>
            <script type="text/javascript">
  var requiredElements = document.getElementById("form").querySelectorAll("[required]"),
  c = document.getElementById("btn_save");

  $('#btn_save').on('click',function(e){
    e.preventDefault();
    var s = "";
    var j=0;
    for (var i = 0; i < requiredElements.length; i++) {
      var e = requiredElements[i];
      if(!e.value.length){
        s += "* " + e.id + " : " + (e.value.length ? "Filled" : "Not Filled") + "\n";
        j++;
      }
    }
    if(s !="" ){
      swal("Warning", s, "warning");
    }
    if(j==0){
      var form = $(this).parents('form');
      swal({
          title: "Resignation information",
          text: "Update Resignation information Complete",
          type: "success",
          showCancelButton: false,
          confirmButtonText: "OK",
          closeOnConfirm: true
      }, function(isConfirm){
          if (isConfirm) form.submit();
      });
    }
  });





</script>
                      </div>
                    </div>
                    
                  </div>
                </div>



<script type="text/javascript">
 function MovementEdit(id) {
      $.ajax({
        url:'<?=base_url()?>index.php/hr/view_employee_movement',
        type: "post",
        data: {
               control1: id
              },
        beforeSend: function () {$(".loading").show();},
        complete: function () {$(".loading").hide();},
        success: function (data) {$("#movementForm").html(data);}
    });
  }
function setEmployeeID(EmployeeID,EmployeeName) {
  $('#EmployeeID').val(EmployeeID);
  $('#modalHeader').html("ข้อมูลพนักงาน | "+EmployeeName);
}
function CalNotiExpire(){
  val1 = parseInt($('#NotificationDaysRequired').val());
  val2 = $('#NotificationDate').val();

var dat = new Date(val2);
var ret = dat.toISOString().substring(0, 10).split('-').reverse()

//rearranging to whatever format you want
var t = ret[0];
ret[0] = ret[1];
ret[1] = t;
// NotificationExpirationDate = ret.join('/');

    var date = new Date(ret.join('/'));
    var newdate = new Date(date);

    newdate.setDate(newdate.getDate() + val1);
    
    var dd = newdate.getDate();
    var mm = newdate.getMonth() + 1;
    var y = newdate.getFullYear();

    dd = "0"+dd;
    dd = dd.substr(dd.length - 2);
    mm = "0"+mm;
    mm = mm.substr(mm.length - 2);


    var NotificationExpirationDate = y + '-' + mm + '-' + dd;

  $('#NotificationExpirationDate').val(NotificationExpirationDate);


}
</script>

<script src="<?php echo base_url();?>assets/bootstrap/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url();?>assets/jquery-datatable/jquery.dataTables.js"></script>
<script src="<?php echo base_url();?>assets/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
<script src="<?php echo base_url();?>assets/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url();?>assets/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
<script src="<?php echo base_url();?>assets/jquery-datatable/extensions/export/jszip.min.js"></script>
<script src="<?php echo base_url();?>assets/jquery-datatable/extensions/export/pdfmake.min.js"></script>
<script src="<?php echo base_url();?>assets/jquery-datatable/extensions/export/vfs_fonts.js"></script>
<script src="<?php echo base_url();?>assets/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
<script src="<?php echo base_url();?>assets/jquery-datatable/extensions/export/buttons.print.min.js"></script>