<style type="text/css">
  .tree, .tree ul {
    margin:0;
    padding:0;
    list-style:none
}
.tree ul {
    margin-left:1em;
    position:relative
}
.tree ul ul {
    margin-left:.5em
}
.tree ul:before {
    content:"";
    display:block;
    width:0;
    position:absolute;
    top:0;
    bottom:0;
    left:0;
    border-left:1px solid
}
.tree li {
    margin:0;
    padding:0 1em;
    line-height:2em;
    color:#369;
    font-weight:700;
    position:relative
}
.tree ul li:before {
    content:"";
    display:block;
    width:10px;
    height:0;
    border-top:1px solid;
    margin-top:-1px;
    position:absolute;
    top:1em;
    left:0
}
.tree ul li:last-child:before {
    background:#fff;
    height:auto;
    top:1em;
    bottom:0
}
.indicator {
    margin-right:5px;
}
.tree li a {
    text-decoration: none;
    color:#369;
}
.tree li button, .tree li button:active, .tree li button:focus {
    text-decoration: none;
    color:#369;
    border:none;
    background:transparent;
    margin:0px 0px 0px 0px;
    padding:0px 0px 0px 0px;
    outline: 0;
}
</style>
<div class="container">
  <div class="row">
    <div class="col-md-6">
      <ul id="tree1">
          <?php
            $this->load->model('HrModel'); 
            //Company
           foreach($results_Company as $result){
            echo "<li><a href='#'>".$result['CompNameEng']." | Division</a>";
            $CompID = $result['CompID'];

            //Division
            // $results_Division = $this->HrModel->getDataTableByID('Division','CompID',$CompID);
            echo "     <span class='badge'>".sizeof($results_Division)."</span>";
            $i=0;
            foreach($results_Division as $resultDV){
              if($i==0) echo "<ul>";
              echo "<li><a href='#'>".$resultDV['DivisionNameEng']." | Department</a>";
              $DivisionID = $resultDV['DivisionID'];

              //Department            
              $results_Department = $this->HrModel->getDataTableByID('Department','DivisionID',$DivisionID);
              echo "     <span class='badge'>".sizeof($results_Department)."</span>";
              $j=0;
              foreach($results_Department as $resultDP){
                if($j==0) echo "<ul>";
                echo "<li><a href='#'>".$resultDP['DepartmentNameEng']." | Section</a>";
                $DepartmentID = $resultDP['DepartmentID'];

                //Section            
                $results_Section = $this->HrModel->getDataTableByID('Section','DepartmentID',$DepartmentID);
                echo "     <span class='badge'>".sizeof($results_Section)."</span>";
                $k=0;
                foreach($results_Section as $resultST){
                  if($k==0) echo "<ul>";
                  echo "<li><a href='#'>".$resultST['SectionNameEng']." Position</a>";
                  $SectionID = $resultST['SectionID'];

                  //Position            
                  $results_Position = $this->HrModel->getDataTableByID('Position','SectionID',$SectionID);
                  echo "     <span class='badge badge-warning'>".sizeof($results_Position)."</span>";
                  $l=0;
                  foreach($results_Position as $resultPT){
                    if($l==0) echo "<ul>";
                    echo "<li>".$resultPT['PositionNameEng']."
                      <a style='cursor:pointer;' class='btn btn-warning btn-xs' data-dismiss='modal' onclick='selectPosition(
                      ".$result['CompID'].",
                      ".$resultDV['DivisionID'].",
                      ".$resultDP['DepartmentID'].",
                      ".$resultST['SectionID'].",
                      ".$resultPT['PositionID'].",
                      );getPositionModal();'><span class='glyphicon glyphicon-plus'></span> Assign
                    </a>";
                    $l++;
                  }
                  if($l>0) echo "</ul>";
                  echo "</li>";
                  $k++;
                }
                if($k>0) echo "</ul>";
                echo "</li>";
                $j++;
              }
              if($j>0) echo "</ul>";
              echo "</li>";
              $i++;
            }
            if($i>0) echo "</ul>";
            echo "</li>";
           }
           ?>
          </ul>
      </div>
    <div class="col-md-6">
      <input type="hidden" id="CompIDModal">
      <input type="hidden" id="DivisionIDModal">
      <input type="hidden" id="DepartmentIDModal">
      <input type="hidden" id="SectionIDModal">
      <input type="hidden" id="PositionIDModal">

    </div>  
  </div>
</div>
    <script type="text/javascript">
  $.fn.extend({
    treed: function (o) {
      
      var openedClass = 'glyphicon-minus-sign';
      var closedClass = 'glyphicon-plus-sign';
      
      if (typeof o != 'undefined'){
        if (typeof o.openedClass != 'undefined'){
        openedClass = o.openedClass;
        }
        if (typeof o.closedClass != 'undefined'){
        closedClass = o.closedClass;
        }
      };
      
        //initialize each of the top levels
        var tree = $(this);
        tree.addClass("tree");
        tree.find('li').has("ul").each(function () {
            var branch = $(this); //li with children ul
            branch.prepend("<i class='indicator glyphicon " + closedClass + "' style='cursor:pointer;'></i>");
            branch.addClass('branch');
            branch.on('click', function (e) {
                if (this == e.target) {
                    var icon = $(this).children('i:first');
                    icon.toggleClass(openedClass + " " + closedClass);
                    $(this).children().children().toggle();
                }
            })
            branch.children().children().toggle();
        });
        //fire event from the dynamically added icon
      tree.find('.branch .indicator').each(function(){
        $(this).on('click', function () {
            $(this).closest('li').click();
        });
      });
        //fire event to open branch if the li contains an anchor instead of text
        tree.find('.branch>a').each(function () {
            $(this).on('click', function (e) {
                $(this).closest('li').click();
                e.preventDefault();
            });
        });
        //fire event to open branch if the li contains a button instead of text
        tree.find('.branch>button').each(function () {
            $(this).on('click', function (e) {
                $(this).closest('li').click();
                e.preventDefault();
            });
        });
    }
});

//Initialization of treeviews

$('#tree1').treed();

$('#tree2').treed({openedClass:'glyphicon-folder-open', closedClass:'glyphicon-folder-close'});

$('#tree3').treed({openedClass:'glyphicon-chevron-right', closedClass:'glyphicon-chevron-down'});

function selectPosition(cmpid,dvid,dpid,stid,ptid){
  $('#CompIDModal').val(cmpid);
  $('#DivisionIDModal').val(dvid);
  $('#DepartmentIDModal').val(dpid);
  $('#SectionIDModal').val(stid);
  $('#PositionIDModal').val(ptid);
}
// function getPositionModal(){
//   alert($('#DivisionIDModal').val());
// }

</script>