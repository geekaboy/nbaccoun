
<head>
  <meta charset="UTF-8">
  <link href="<?php echo base_url();?>assets/bootstrap-toggle-master/css/bootstrap-toggle.min.css" rel="stylesheet">
<style>
  canvas{
    -moz-user-select: none;
    -webkit-user-select: none;
    -ms-user-select: none;
  }
  span.alogo2 {
  display: inline-block;
  border-radius: 60px;
  box-shadow: 0px 0px 2px #888;
  padding: 0.5em 0.6em;
  text-align: center;
}
  </style>
</head>
<form action="" id="form" name="form" method="post">
<div class="box box-success">
  <div class="box-body">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="panel-title"><?php echo $this->systemmodel->get_menuname($this->uri->segment(1) . '/' . $this->uri->segment(2)); // แสดงชื่อเมนู  ?></h4>
          </div>
          <div class="panel-body">
            <div class="col-md-6">
              <p></p>
              <div class="panel panel-default panel-body">
                <B><?=$this->systemmodel->changeLng("วันที่มีผลบังคับ")?></B>
                 <div class='input-group date' id='datetimepicker'>
                  <input type='text' class="form-control" id='MMDate' name="MMDate"value="<?=date('yy-m-d')?>" />
                  <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                  </span>
                </div>
            <script>
            $(document).ready(function(){
              var date_input=$('input[name="MMDate"]');
              var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
              date_input.datepicker({
                format: 'yyyy-mm-dd',
                container: container,
                todayHighlight: true,
                autoclose: true,
              })
            })
            </script>
                <b><?=$this->systemmodel->changeLng("ประเภทการเปลี่ยนแปลง")?>(*)</b>
                <select class="form-control" name="MovementTypeID" id='MovementTypeID' required>
                      <option value=""><?=$this->systemmodel->changeLng("กรุณาเลือก")?></option>
                      <?php
                      foreach($results_MovementType as $result){
                        ?>
                        <option value="<?php echo $result->MovementTypeID; ?>"  >
                          <?php echo $result->MovementTypeNameEN." | ".$result->MovementTypeNameLL; ?>
                        </option>
                        <?php
                      } ?>
                 </select>

                <b><?=$this->systemmodel->changeLng("ประเภทของพนักงาน")?>(*)</b>
                <select class="form-control" name="WorkTypeID" id='WorkTypeID' required>
                      <option value="">
                        <?=$this->systemmodel->changeLng("กรุณาเลือก")?>
                      </option>
                      <?php
                      foreach($results_WorkType as $result){
                        ?>
                        <option value="<?php echo $result->WorkTypeID; ?>"  >
                          <?php echo $result->WorkTypeNameEng." | ".$result->WorkTypeNameThai; ?>
                        </option>
                        <?php
                      } ?>
                 </select>

                <!-- ของเดิมเป็นการบันทึกชั่่วโมงการทำงาน แต่ไม่น่าจะมีความจำเป็นต้องใช้ iCha ขอเปลี่ยนเองเป็นเอา ประเภทของสัญญา มาแทน
                <?=$this->systemmodel->changeLng("ชั่วโมงการทำงาน")?> <input type='number' class="form-control" id='HourperDay' name="HourperDay" /> -->

                  <b><?=$this->systemmodel->changeLng("ประเภทของสัญญา")?>(*)</b>
                <select class="form-control" name="ContractType" id='ContractType'onchange="setdat(this)" >
                      <option value="">
                        <?=$this->systemmodel->changeLng("กรุณาเลือก")?>
                      </option>
                      <?php
                      foreach($results_ContractType as $result){
                        ?>
                        <option value="<?php echo $result->ContractID; ?>"  >
                          <?php echo $result->ContractNameEN." | ".$result->ContractNameLL; ?>
                        </option>
                        <?php
                      } ?>
                 </select>
                

                

                <b><?=$this->systemmodel->changeLng("กลุ่มพนักงาน")?></b>
                <select class="form-control" name="EmpGroupID" id='EmpGroupID' >
                      <option value="">
                        <?=$this->systemmodel->changeLng("กรุณาเลือก")?>
                      </option>
                      <?php
                      foreach($results_EmployeeGroup as $result){
                        ?>
                        <option value="<?php echo $result->EmpGroupID; ?>">
                          <?php echo $result->EmpGroupNameEN; ?> | <?php echo $result->EmpGroupNameLL; ?>
                        </option>
                        <?php
                      } ?>
                 </select>
                <b><?=$this->systemmodel->changeLng("ระดับพนักงาน")?></b>
                <select class="form-control" name="EmpLevelID" id='EmpLevelID' >
                      <option value="">
                        <?=$this->systemmodel->changeLng("กรุณาเลือก")?>
                      </option>
                      <?php
                      foreach($results_EmployeeLevel as $result){
                        ?>
                        <option value="<?php echo $result->EmpLevelID; ?>">
                          <?php echo $result->EmpLevelNameEN; ?> | <?php echo $result->EmpLevelNameLL; ?>
                        </option>
                        <?php
                      } ?>
                 </select>
                
                  <b><?=$this->systemmodel->changeLng("รหัสการจ่ายภาษีเงินได้")?>(*)</b>
                  <select class="form-control" name="PITCodeTypeID" id='PITCodeTypeID' required >
                        <option value="">
                          <?=$this->systemmodel->changeLng("กรุณาเลือก")?>
                        </option>
                        <?php
                        foreach($results_PITCode as $result){
                          ?>
                          <option value="<?php echo $result->PITCodeTypeID; ?>">
                            <?php echo $result->PITCodeTypeNameEN; ?> | <?php echo $result->PITCodeTypeNameLL; ?>
                          </option>
                          <?php
                        } ?>
                   </select>
                   <!--เลือกรหัสการจ่ายประกันสังคม -->
                   <b><?=$this->systemmodel->changeLng("รหัสการจ่ายประกันสังคม")?>(*)</b>
        <select class="form-control" name="SocialID" id='SocialID' required="">
              <option value="">
                <?=$this->systemmodel->changeLng("กรุณาเลือก")?>
              </option>
              <?php
              foreach($results_SSOCode as $result){
                ?>
                <option value="<?php echo $result->SSOCodeTypeID; ?>">
                  <?php echo $result->SSOCodeTypeNameEN." | ".$result->SSOCodeTypeNameLL; ?>
                </option>
                <?php
              } ?>
         </select>
            <b><?=$this->systemmodel->changeLng("หมายเหตุ")?></b>
            <input type='text' class="form-control" id='Remark' />
                  

              </div>
            </div>
            <div class="col-md-6">
              <p></p>
              <div class="panel panel-default panel-body">
                
                <!--<b><?=$this->systemmodel->changeLng("ประเภทเงินเดือน")?> (*)</b>
                <select class="form-control" name="SalaryTypeID" id='SalaryTypeID' required>
                      <option value="">
                        <?=$this->systemmodel->changeLng("กรุณาเลือก")?>
                      </option>
                      <?php
                      foreach($results_SalaryTypeID as $result){
                        ?>
                        <option value="<?php echo $result->SalaryTypeID; ?>" >
                      <?php echo $result->SalaryTypeEN; ?> | <?php echo $result->SalaryTypeLL; ?>
                        </option>
                        <?php
                      } ?>
                 </select> ประเภทเงินเดือนไม่น่าจะเปลี่ยน -->

                <b><?=$this->systemmodel->changeLng("เงินเดือน")?> (*)</b>
                <input type='text' class="form-control" id='Salary' required />
                <b><?=$this->systemmodel->changeLng("ค่าตอบแทนพิเศษ")?></b>
                <input type='text' class="form-control" id='Allowance' />

                <!--<b><?=$this->systemmodel->changeLng("วิธีการจ่ายเงิน")?></b>
                <select class="form-control" name="PaymentMethodTypeID" id='PaymentMethodTypeID' >
                      <option value="">
                        <?=$this->systemmodel->changeLng("กรุณาเลือก")?>
                      </option>
                      <?php
                      foreach($results_PaymentMethod as $result){
                        ?>
                        <option value="<?php echo $result->PaymentMethodTypeID; ?>">
                          <?php echo $result->PaymentMethodTypeNameEN; ?> | <?php echo $result->PaymentMethodTypeNameLL; ?>
                        </option>
                        <?php
                      } ?>
                 </select> วิธีการจ่ายเงินไม่น่าจะต้องเปลี่ยนแปลงตรงนี้ให้เปลี่ยนที่หน้าแรกของพนักงานเลย -->

              <!-- <label><BR>Gross Salary&nbsp;&nbsp;<BR><input type="checkbox" data-toggle="toggle" id="GrossSalary"></label>
                <label><BR>Change Position&nbsp;&nbsp;<BR><input type="checkbox" data-toggle="toggle" id="ChangePosition"></label></BR>
                <label><BR>Change Salary&nbsp;&nbsp;<BR><input type="checkbox" data-toggle="toggle" id="ChangeSalary"></label>
                <label><BR>Change Contract&nbsp;&nbsp;<BR><input type="checkbox" data-toggle="toggle" id="ChangeContract"></label></BR> !-->
 <script type="text/javascript">
            // $('#btn_Modal').hide();
            $.ajax({
            url:'<?=base_url()?>index.php/hr/companyst',
              type: "post",
              data: {},
              beforeSend: function () {$(".loading").show();},
              complete: function () {$(".loading").hide();},
              success: function (data) {
                $('#CompanyStructureDIV2').html(data);
                }
              });

          function getPositionModal(){
            // alert($('#DivisionIDModal').val());
            $('#CompID').val($('#CompIDModal').val());
            $('#DivisionID').val($('#DivisionIDModal').val());
            $('#DepartmentID').val($('#DepartmentIDModal').val());
            $('#SectionID').val($('#SectionIDModal').val());
            $('#PositionID').val($('#PositionIDModal').val());
            $('#CompIDTXT').val($('#CompIDModal').val());
            $('#DivisionIDTXT').val($('#DivisionIDModal').val());
            $('#DepartmentIDTXT').val($('#DepartmentIDModal').val());
            $('#SectionIDTXT').val($('#SectionIDModal').val());
            $('#PositionIDTXT').val($('#PositionIDModal').val());
          }
          </script>




                <BR>
                 <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#myModal" id="btn_Modal"><b><?=$this->systemmodel->changeLng("เลือกตำแหน่งพนักงาน")?></b></button>
                 <BR>
                  <b><?= $this->systemmodel->changeLng("บริษัท") ?></b>
                  <input type="hidden" name="CompID" id="CompID">
                  <select class="form-control" name="CompIDTXT" id='CompIDTXT' required disabled>
                  <option><?=$this->systemmodel->changeLng("กรุณาเลือกบริษัท") ?></option>
                    <?php
                    foreach ($results_Company as $result) {
                      echo "<option value='" . $result['CompID'] . "'>" . $result['CompNameEng'] . "</option>";
                    }
                    ?>
                  </select>
                  <b><?= $this->systemmodel->changeLng("ฝ่าย") ?></b>
                  <input type="hidden" name="DivisionID" id="DivisionID">
                  <select class="form-control" name="DivisionIDTXT" id='DivisionIDTXT' required disabled>
                    <option><?= $this->systemmodel->changeLng("กรุณาเลือกฝ่าย") ?></option>
                    <?php
                    foreach ($results_Division as $result) {
                      echo "<option value='" . $result['DivisionID'] . "'>" . $result['DivisionNameEng'] . "</option>";
                    }
                    ?>
                  </select>
                  <b><?= $this->systemmodel->changeLng("แผนก") ?></b>
                  <input type="hidden" name="DepartmentID" id="DepartmentID">
                  <select class="form-control" name="DepartmentIDTXT" id="DepartmentIDTXT" required disabled>
                    <option><?= $this->systemmodel->changeLng("กรุณาเลือกแผนก") ?></option>
                    <?php
                    foreach ($results_Department as $result) {
                      echo "<option value='" . $result['DepartmentID'] . "'>" . $result['DepartmentNameEng'] . "</option>";
                    }
                    ?>
                  </select>
                  <b><?= $this->systemmodel->changeLng("หน่วย") ?></b>
                  <input type="hidden" name="SectionID" id="SectionID">
                  <select class="form-control" name="SectionIDTXT" id='SectionIDTXT' required disabled>
                    <option><?= $this->systemmodel->changeLng("กรุณาเลือกหน่วย") ?></option>
                    <?php
                    foreach ($results_Section as $result) {
                      echo "<option value='" . $result['SectionID'] . "'>" . $result['SectionNameEng'] . "</option>";
                    }
                    ?>
                  </select>
                  <b><?= $this->systemmodel->changeLng("ตำแหน่ง") ?> (*)</b>
                  <input type="hidden" name="PositionID" id="PositionID">
                  <select class="form-control" name="PositionIDTXT" id='PositionIDTXT' required disabled>
                    <option><?= $this->systemmodel->changeLng("กรุณาเลือกตำแหน่ง") ?></option>
                    <?php
                    foreach ($results_Position as $result) {
                      echo "<option value='" . $result['PositionID'] . "'>" . $result['PositionNameEng'] . "</option>";
                    }
                    ?>
                  </select>
              </div>
            </div>
          </div>
        </div>

         <!-- Modal -->
          <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog">

              <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Select Company Division Department Section Position</h4>
                </div>
                <div class="modal-body" id="CompanyStructureDIV2">
                  <p>Some text in the modal.</p>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
              </div>

            </div>
          </div>
         





    <div class="col-md-2" align="right">
      <button type="button" class="btn btn-primary btn-md btn-block" id="btn_save">
        <?php 
          if($ModeDB=="Add") echo "Save";
          else echo "Update";
        ?>
      </button>
    </div>
    <!-- จบส่วนแสดงผล -->
  </div>
</div>
</form>
<div id="showreult">
</div>
<script type="text/javascript">
  var requiredElements = document.getElementById("form").querySelectorAll("[required]"),
  c = document.getElementById("btn_save");
  c.addEventListener("click", function() {
    var s = "";
    for (var i = 0; i < requiredElements.length; i++) {
      var e = requiredElements[i];
      if(!e.value.length) s += "* " + e.id + " : " + (e.value.length ? "Filled" : "Not Filled") + "\n";
    }
    if(s !="" ){
      swal("Warning", s, "warning");
    }
    else{
      var gs_val = $('#GrossSalary').prop('checked') ?  1 :  0;
      var cp_val = $('#ChangePosition').prop('checked') ?  1 :  0;
      var cs_val = $('#ChangeSalary').prop('checked') ?  1 :  0;
      var cc_val = $('#ChangeContract').prop('checked') ?  1 :  0;
$.ajax({
        url:'<?=base_url()?>index.php/hr/AddEmployeeMovementDB',
        type: "post",
        data: {
        ModeDB: '<?php echo $ModeDB;?>',
        EmployeeID: <?php echo $EmployeeID;?>,
        EmployeeMID: <?php echo $EmployeeMID;?>,
        MMDate: $("#MMDate").val(),
        MovementTypeID: $("#MovementTypeID").val(),
        WorkTypeID:$("#WorkTypeID").val(),
        HourperDay:$("#HourperDay").val(),
        Remark:$("#Remark").val(),
        EmpGroupID:$("#EmpGroupID").val(),
        EmpLevelID:$("#EmpLevelID").val(),
        PITCodeTypeID:$("#PITCodeTypeID").val(),

        SalaryTypeID:$("#SalaryTypeID").val(),
        Salary:$("#Salary").val(),
        Allowance:$("#Allowance").val(),
        PaymentMethodTypeID:$("#PaymentMethodTypeID").val(),
        GrossSalary:gs_val,
        ChangePosition:cp_val,
        ChangeSalary:cs_val,
        ChangeContract:cc_val,
        CompID:$("#CompID").val(),
        DivisionID:$("#DivisionID").val(),
        DepartmentID:$("#DepartmentID").val(),
        SectionID:$("#SectionID").val(),
        PositionID:$("#PositionID").val(),



              },
        beforeSend: function () {$(".loading").show();},
        complete: function () {$(".loading").hide();},

        success: function (data) {
          swal({
          title: "Add Employee Movement Complete",
          text: "You update  success.",
          type: "success",
          showCancelButton: false,
          confirmButtonClass: "btn-primary",
          confirmButtonText: "OK",
          closeOnConfirm: false
          },
          function(){
             $(location).attr('href', '/index.php/hr/form_employee_movement/edit/<?php echo $EmployeeID;?>');
          });
        }
         // success: function (data) {$("#showreult").html(data);}
    });
    }
  });
</script>
<script>
$(function(){
  <?php foreach($results_EmployeeMovement as $result){ ?>
    $("#MMDate").val("<?php echo $result->MMDate; ?>");
    $("#MovementTypeID").val("<?php echo $result->MovementTypeID; ?>");
    $("#WorkTypeID").val("<?php echo $result->WorkTypeID; ?>");
    $("#ContractType").val("<?php echo $result->ContractType; ?>");
    $("#Remark").val("<?php echo $result->Remark; ?>");
    $("#EmpGroupID").val("<?php echo $result->EmpGroupID; ?>");
    $("#EmpLevelID").val("<?php echo $result->EmpLevelID; ?>");
    $("#PITCodeTypeID").val("<?php echo $result->PITCodeTypeID; ?>");
    $("#SocialID").val("<?php echo $result->SocialID; ?>");
    $("#SalaryTypeID").val("<?php echo $result->SalaryTypeID; ?>");
    $("#Salary").val("<?php echo $result->Salary; ?>");
    $("#Allowance").val("<?php echo $result->Allowance; ?>");
    $("#PaymentMethodTypeID").val("<?php echo $result->PaymentMethodTypeID; ?>");
    $("#CompID").val(<?php echo $result->CompID; ?>);
    $("#DivisionID").val(<?php echo $result->DivisionID; ?>);
    $("#DepartmentID").val(<?php echo $result->DepartmentID; ?>);
    $("#SetionID").val(<?php echo $result->SectionID; ?>);
    $("#PositionID").val(<?php echo $result->PositionID; ?>);

    //ของเดิม เขียนไว้ลัษณะนี้ ต่างก้นตรง .Prop("Check") เปลี่ยนเป็น .val
    //$("#CompID").prop("checked", <?php echo $result->CompID; ?>);
    //$("#DivisionID").prop("checked", <?php echo $result->DivisionID; ?>);
    //$("#DepartmentID").prop("checked", <?php echo $result->DepartmentID; ?>);
    //$("#SetionID").prop("checked", <?php echo $result->SectionID; ?>);
    //$("#PositionID").prop("checked", <?php echo $result->PositionID; ?>);
  <?php } ?>
});





</script>


  <script type='text/javascript'>
  var baseURL= "<?php echo base_url();?>";
  $(document).ready(function(){
    $('#Division').change(function(){
      var DivisionID = $(this).val();
      $.ajax({
        url:'<?=base_url()?>index.php/hr/getDepartment',
        method: 'post',
        data: {DivisionID: DivisionID},
        dataType: 'json',
        success: function(response){
          $('#Section').find('option').not(':first').remove();
          $('#Department').find('option').not(':first').remove();
          $.each(response,function(index,data){
             $('#Department').append('<option value="'+data['DepartmentID']+'">'+data['DepartmentNameThai']+'</option>');
          });
        }
     });
   });
   $('#Department').change(function(){
     var DepartmentID = $(this).val();
     $.ajax({
       url:'<?=base_url()?>index.php/hr/getSection',
       method: 'post',
       data: {DepartmentID: DepartmentID},
       dataType: 'json',
       success: function(response){
         $('#Section').find('option').not(':first').remove();
         $.each(response,function(index,data){
           $('#Section').append('<option value="'+data['SectionID']+'">'+data['SectionNameThai']+'</option>');
         });
       }
    });
  });
  $('#Section').change(function(){
     var SectionID = $(this).val();
     $.ajax({
       url:'<?=base_url()?>index.php/hr/getPosition',
       method: 'post',
       data: {SectionID: SectionID},
       dataType: 'json',
       success: function(response){
         $('#Position').find('option').not(':first').remove();
         $.each(response,function(index,data){
           $('#Position').append('<option value="'+data['PositionID']+'">'+data['PositionNameThai']+'</option>');
         });
       }
    });
  });
  
 });







 </script>

