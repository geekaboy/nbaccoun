
<!-- begin add require script -->
<head>
  <meta charset="UTF-8">
  
  <style>
  @media print {
    canvas {
      min-height: 100%;max-width: 100%;max-height: 100%;height: auto!important;width: auto!important;
    }
    table {
      min-height: 100%;max-width: 100%;max-height: 100%;height: auto!important;width: auto!important;
      page-break-inside: : auto;
    }
    tr{
      page-break-inside: avoid;
      page-break-after: auto;

    }
  }
  canvas{-moz-user-select: none;-webkit-user-select: none;-ms-user-select: none;}
  </style>
</head>

<!-- end add require script -->
<div class="box box-success">
  <div class="box-header" align="left">
        <i class="fa fa-sign-in"></i>
    <h3 class="box-title"><?php echo $this->systemmodel->get_menuname($this->uri->segment(1) . '/' . $this->uri->segment(2)); // แสดงชื่อเมนู  ?></h3>
  </div>
  <div class="box-body">
    <!-- ส่วนแสดงผล -->

<div class="box box-success">
  <div class="box-header">
    <b></b>
  </div>
  <div class="box-body" align="left">
    <div class="row clearfix">
      <?php
      if(!empty($DataSearch)){
          foreach ($DataSearch as $row) {
      ?>
          <div class="col-md-6">
            <p><b>Employee Name</b>: <?php echo $row->FirstName." ".$row->LastName;?></p>
            <p><b>Company</b>: <?php echo $row->CompNameEng;?></p>
            <p><b>Division</b>: <?php echo $row->DivisionNameEng;?></p>
            <p><b>Department</b>: <?php echo $row->DepartmentNameEng;?></p>
            <p><b>Section</b>: <?php echo $row->SectionNameEng;?></p>
            <p><b>Group</b>: <?php echo "";?></p>
            <p><b>Position</b>: <?php echo $row->PositionNameEng;?></p>
          </div>
          <div class="col-md-6" align="center">
            <p><b>Service Start date</b>: <?php echo $row->ServiceStartDate;?></p>
            <p><b>Contract Type</b>: <?php echo $row->ContractType;?></p>
            <p><b>Contract Start Date</b>: <?php echo $row->ContractStartDate;?></p>
            <p><b>Contract End Date</b>: <?php echo $row->ContractEndDate;?></p>
            <p><b>Probation End Date</b>: <?php echo $row->ProbationEndDate;?></p>
            <p><b>Efficetive Date</b>: <?php echo $row->EffectiveDate;?></p>
          </div>
      </div>
      <?php
    }
  }
    ?>
  </div>
</div>
    


<div class="box box-success">
  <div class="box-header">
    <b>Transaction</b>
  </div>
  <div class="box-body" align="left">

    <button type='button' class='btn btn-info' onclick='btn_click(<?php echo $row->EmployeeID;?>)'>
    <span class='glyphicon glyphicon-edit' aria-hidden='true'></span>&nbsp;&nbsp;Add Transaction
    </button><BR><BR>
    <script>
    function btn_click(id){
        document.location = 'Add_Transaction/add/'+id;
    }
    </script>

        <!-- ตาราง แสดงข้อมูล -->
        <table class="table table-bordered table-striped table-hover dataTable  js-exportable2">
          <thead>
            <tr>
              <th scope="col"><center>Effective Date</center></th>
              <th scope="col"><center>Movement Type</center></th>
            </tr>
          </thead>
          <tbody>
          <?php 
        $i=0;
        if(!empty($DataSearch2)){
          foreach ($DataSearch2 as $row) {
          ?>
            <tr>
              <td scope="col"><?php echo "ss";?></td>
              <td scope="col"><?php echo $i+1;?></td>
            </tr>
          <?php
          
          $i++;
        } 
        }  
          ?>
          </tbody>
        </table>
  </div>
</div>
    <!-- จบส่วนแสดงผล -->
  </div>
</div>
