<script type="text/javascript">
  $(document).ready(function(){
    $('.selectpicker').selectpicker({style: 'btn-warning',size: 5});
    $('#username').focus();
    $('#btn_login').click(function(){
      $.ajax({
        url:'<?php echo base_url(); ?>index.php/home/login',
        data:'username='+$('#username').val()+'&password='+$('#password').val(),
        type:'POST',
        success:function(res){
          if(res=='true'){
            swal({title : 'แสดงเมือทำงานสำเร็จ',text : '',type : 'success'},
              function(){window.location.replace("<?php echo base_url() ?>index.php/member");
            }
            );
          }else{swal({title : 'แสดงเมื่อทำงานไม่สำเร็จ',text : '',type : 'error'});}
        },error:function(err){swal({title : 'เกิดข้อผิดพลาด',text : err,type : 'error'});}
      });
    });
  });
</script>
<head>
  <meta charset="UTF-8">
  <title><?php echo $this->uri->segment(2);?></title>
  <link href="<?php echo base_url();?>assets/bootstrap-select/css/bootstrap-select.css" rel="stylesheet">
  <link href="<?php echo base_url();?>assets/bootstrap/css/datepicker.css" rel="stylesheet">
  <link href="<?php echo base_url();?>assets/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">
<style>
  canvas{
    -moz-user-select: none;
    -webkit-user-select: none;
    -ms-user-select: none;
  }
  </style>
  <script language="javascript">
		function fnccheck(){
		var ServiceStartDate = form.ServiceStartDate.value;
		document.form.ContractStartDate.value=ServiceStartDate;
		document.form.ProbationEndDate.value='0000-00-00';
		document.form.ContractType.value='';
		}
		function setdat(dateSelect) {
        var val;
		var ServiceStartDate = form.ServiceStartDate.value;
		var ContractStartDate = form.ContractStartDate.value;
			function calendarAddDay(dateStr, day){  
			var date = new Date(dateStr);		
			date.setDate(date.getDate()+day);  
			return date.getFullYear()+"-"+(date.getMonth()+1)+"-"+date.getDate();
			}
			function calendarAddMonth(dateStr, month){  
			var date = new Date(dateStr);		
			date.setMonth(date.getMonth()+month);  
			return date.getFullYear()+"-"+(date.getMonth()+1)+"-"+date.getDate();
			} 
			function calendarAddYear(dateStr, year){  
			var date = new Date(dateStr);		
			date.setYear(date.getFullYear()+year);  
			return date.getFullYear()+"-"+(date.getMonth()+1)+"-"+date.getDate();
			}
		x=ServiceStartDate;
		m=ContractStartDate;
		/*date2 = new Date(x);
		date2.setDate(date2.getDate()+119);
		val1=(date2.getFullYear()+"-"+(parseInt(date2.getMonth())+1)+"-"+ date2.getDate());
		*/
		var next119day=calendarAddDay(x,119);
		console.log(next119day);
		var next1m=calendarAddMonth(m,1);
		console.log(next1m);
		var next2m=calendarAddMonth(m,2);
		console.log(next2m);
		var next3m=calendarAddMonth(m,3);
		console.log(next3m);
		var next1y=calendarAddYear(m,1);
		console.log(next1y);
		var next2y=calendarAddYear(m,2);
		console.log(next2y);
		var next3y=calendarAddYear(m,3);
		console.log(next3y);
        switch (dateSelect.options[dateSelect.selectedIndex].value) {
                                            case '1': 
											val = '<?php echo $strNewDate = date ("Y-m-d", strtotime("+119 day", strtotime('2018-08-01'))); ?>'; 
											document.getElementById('ProbationEndDate').value = next119day;
											document.getElementById('ContractStartDate').value = '0000-00-00';
											document.getElementById('ContractEndDate').value = '0000-00-00';
											break;
		}
		switch (dateSelect.options[dateSelect.selectedIndex].value) {
											case '2': 
											val = '<?php echo $strNewDate = date ("Y-m-d", strtotime("+1 month", strtotime('2018-08-01'))); ?>'; 
											document.getElementById('ContractStartDate').value = m;
											document.getElementById('ContractEndDate').value = next1m;
											document.getElementById('ProbationEndDate').value = '0000-00-00';
											break;
											case '3': 
											val = '<?php echo $strNewDate = date ("Y-m-d", strtotime("+1 year", strtotime('2018-08-01'))); ?>'; 
											document.getElementById('ContractStartDate').value = m;
											document.getElementById('ContractEndDate').value = next1y;
											document.getElementById('ProbationEndDate').value = '0000-00-00';
											break;
											case '4': 
											val = '<?php echo $strNewDate = date ("Y-m-d", strtotime("+2 month", strtotime('2018-08-01'))); ?>'; 
											document.getElementById('ContractStartDate').value = m;
											document.getElementById('ContractEndDate').value = next2m;
											document.getElementById('ProbationEndDate').value = '0000-00-00';
											break;
											case '5': 
											val = '<?php echo $strNewDate = date ("Y-m-d", strtotime("+2 year", strtotime('2018-08-01'))); ?>'; 
											document.getElementById('ContractStartDate').value = m;
											document.getElementById('ContractEndDate').value = next2y;
											document.getElementById('ProbationEndDate').value = '0000-00-00';
											break;
											case '6': 
											val = '<?php echo $strNewDate = date ("Y-m-d", strtotime("+3 month", strtotime('2018-08-01'))); ?>'; 
											document.getElementById('ContractStartDate').value = m;
											document.getElementById('ContractEndDate').value = next3m;
											document.getElementById('ProbationEndDate').value = '0000-00-00';
											break;
											case '7': 
											val = '<?php echo $strNewDate = date ("Y-m-d", strtotime("+3 year", strtotime('2018-08-01'))); ?>'; 
											document.getElementById('ContractStartDate').value = m;
											document.getElementById('ContractEndDate').value = next3y;
											document.getElementById('ProbationEndDate').value = '0000-00-00';
											break;
											}
                                      
		}
		setdat(document.getElementById('ContractType'));
</script>
</head>
<?php echo $this->session->flashdata('msginfo'); ?>
<form action="<?php echo  base_url('index.php/hr/updateAddNewStaff'); ?>" id="form" name="form" method="post">
<div class="box box-success">
  <div class="box-header">
    <h3 class="box-title">
          <span class="glyphicon glyphicon-user"></span>
          <?php foreach($results_Employee as $result){echo $result->FullNameLL;}?> 
    </h3>
  </div>
  <div class="box-body">
    <ul class="nav nav-tabs">
      <li class="active"><a data-toggle="tab" href="#home"><span class="glyphicon glyphicon-user"></span><b><?=$this->systemmodel->changeLng("บัตรประจำตัว")?></b></a></li>
      <li><a data-toggle="tab" href="#menu1"><span class="glyphicon glyphicon-open"></span><b> <?=$this->systemmodel->changeLng("พนักงาน")?></b></a></li>
      <li><a data-toggle="tab" href="#menu2"><span class="glyphicon glyphicon-briefcase"></span><b> <?=$this->systemmodel->changeLng("บริษัท")?></b></a></li>
      <li><a data-toggle="tab" href="#menu4"><span class="glyphicon glyphicon-earphone"></span> <b><?=$this->systemmodel->changeLng("สัญญา")?></b></a></li>
      <li><a data-toggle="tab" href="#menu5"><span class="glyphicon glyphicon-usd"></span><b> <?=$this->systemmodel->changeLng("เงินเดือน")?></b></a></li>
      <li><a data-toggle="tab" href="#menu6"><span class="glyphicon glyphicon-home"></span> <b><?=$this->systemmodel->changeLng("ที่อยู่")?></b></a></li>
      <li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button type="submit" class="btn btn-primary" id="btn_save">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b><?=$this->systemmodel->changeLng("บันทึก")?></b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</button></li>
    </ul>
    <div class="tab-content">
      <div id="home" class="tab-pane fade in active">
        <h3></h3>
        <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="panel-title"><?=$this->systemmodel->changeLng("บัตรประจำตัวประชาชน")?></h4>
          </div>
          <div class="panel-body " id="collapse1">
            <b><?=$this->systemmodel->changeLng("เลขประจำตัวประชาชน")?> (*)</b> <?php echo $this->session->flashdata('error_CardID'); ?>
			<input type="hidden" name="EmployeeID" value="<?php echo $this->uri->segment(4);?>">
            <input type='text' class="form-control" id='CardID' name="CardID" required />
      <b><?=$this->systemmodel->changeLng("เลขประจำตัวประกันสังคม")?> (*) </b>
      <div class="input-group">
              <span class="input-group-addon btn btn-warning" id="copyCardid"> <span class="glyphicon glyphicon-copy"></span><b> <?=$this->systemmodel->changeLng("คัดลอกเลขประจำตัวประชาชน")?></b></span>
              <input type="text" class="form-control" id="SSOID" name="SSOID" aria-describedby="basic-addon1" required>
            </div>
            <b><?=$this->systemmodel->changeLng("เกิดวันที่")?></b>
			 <div class='input-group date' id='datetimepicker1'>
                     <input type='text' class="form-control" id='Birthday' name="Birthday" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
			<script>
              $(document).ready(function(){
                var date_input=$('input[name="Birthday"]');
                var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
                date_input.datepicker({
                  format: 'yyyy-mm-dd',
                  container: container,
                  todayHighlight: true,
                  autoclose: true,
                })
              })
            </script>
			<b><?=$this->systemmodel->changeLng("วันที่ออกบัตร")?></b>
			 <div class='input-group date' id='datetimepicker1'>
                     <input type='text' class="form-control" id='IssueDate' name="IssueDate" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
  
			<script>
              $(document).ready(function(){
                var date_input=$('input[name="IssueDate"]');
                var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
                date_input.datepicker({
                  format: 'yyyy-mm-dd',
                  container: container,
                  todayHighlight: true,
                  autoclose: true,
                })
              })
            </script>
            <b><?=$this->systemmodel->changeLng("สถานที่ออกบัตร")?></b>
            <input type='text' class="form-control" id='IssuePlace' name="IssuePlace" />
            <b><?=$this->systemmodel->changeLng("วันบัตรหมดอายุ")?></b>
			<div class='input-group date' id='datetimepicker1'>
                    <input type='text' class="form-control" id='ExpiringDate' name="ExpiringDate" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            
			<script>
              $(document).ready(function(){
                var date_input=$('input[name="ExpiringDate"]');
                var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
                date_input.datepicker({
                  format: 'yyyy-mm-dd',
                  container: container,
                  todayHighlight: true,
                  autoclose: true,
                })
              })
            </script>
          </div>
        </div>
      </div>
      <div id="menu1" class="tab-pane fade">
        <h3></h3>
        <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="panel-title"><?=$this->systemmodel->changeLng("รายละเอียดพนักงาน")?></h4>
          </div>
          <div class="panel-body" id="collapse2">
            <div class="col-md-6">
              <p></p>
              <div class="panel panel-default panel-body">
          <b><?=$this->systemmodel->changeLng("รหัสพนักงาน")?>  (*)</b>
                <input type='text' class="form-control" id='EmployeeCode' name="EmployeeCode" required />
                <b><?=$this->systemmodel->changeLng("คำนำหน้า")?> (*)</b>
				<select class="form-control" name="TitleID" id='TitleID' required>
							<option value="">
								<?=$this->systemmodel->changeLng("กรุณาเลือก")?>
							</option>
							<?php
							foreach($results_title as $result){
								?>
								<option value="<?php echo $result->TitleID; ?>">
									<?php echo $result->TitleEN." | ".$result->TitleTH; ?>
								</option>
								<?php
							} ?>
			   </select>
                <b><?=$this->systemmodel->changeLng("ชื่อตัว")?>  (*)</b>
                <input type='text' class="form-control" id='FirstName' name="FirstName" required />
                <!--<?=$this->systemmodel->changeLng("ชื่อกลาง")?> 
                <input type='text' class="form-control" id='MiddleName' name="MiddleName"  /> -->
                <b><?=$this->systemmodel->changeLng("ชื่อนามสกุล")?>  (*)</b>
                <input type='text' class="form-control" id='LastName' name="LastName" required />
				        <b><?=$this->systemmodel->changeLng("ชื่อเล่น")?> </b>
                <input type='text' class="form-control" id='MiddleName' name="MiddleName"  />

                <b><?=$this->systemmodel->changeLng("ชื่อเต็ม (ภาษาอื่น)")?> (*)</b>
                <input type='text' class="form-control" id='FullNameLL' name="FullNameLL" required />
              </div>
            </div>
            <div class="col-md-6">
              <p></p>
              <div class="panel panel-default panel-body">
                
                <b><?=$this->systemmodel->changeLng("เพศ")?></b>
				<select class="form-control" name="GenderID" id='GenderID' >
							<option value="">
								<?=$this->systemmodel->changeLng("กรุณาเลือก")?>
							</option>
							<?php
							foreach($results_gender as $result){
								?>
								<option value="<?php echo $result->GenderID; ?>">
									<?php echo $result->GenderEN." | ".$result->GenderTH; ?>
								</option>
								<?php
							} ?>
			   </select>
                <b><?=$this->systemmodel->changeLng("สัญชาติ")?></b>
				<select class="form-control" name="NationalityID" id='NationalityID' >
							<option value="">
								<?=$this->systemmodel->changeLng("กรุณาเลือก")?>
							</option>
							<?php
							foreach($results_Nationality as $result){
								?>
								<option value="<?php echo $result->NationlityID; ?>">
									<?php echo $result->NationalityNameEN." | ".$result->NationalityNameLL; ?>
								</option>
								<?php
							} ?>
			   </select>
                <b><?=$this->systemmodel->changeLng("เชื้อชาติ")?></b>
				<select class="form-control" name="RaceID" id='RaceID' >
							<option value="">
								<?=$this->systemmodel->changeLng("กรุณาเลือก")?>
							</option>
							<?php
							foreach($results_Race as $result){
								?>
								<option value="<?php echo $result->RaceID; ?>">
									<?php echo $result->RaceNameEN." | ".$result->RaceNameLL; ?>
								</option>
								<?php
							} ?>
			   </select>
               <b><?=$this->systemmodel->changeLng("สถานะทางครอบครัว")?></b>
				<select class="form-control" name="FamilyStatusID" id='FamilyStatusID' >
							<option value="">
								<?=$this->systemmodel->changeLng("กรุณาเลือก")?>
							</option>
							<?php
							foreach($results_FamilyStatus as $result){
								?>
								<option value="<?php echo $result->FamilyStatusID; ?>">
									<?php echo $result->FamilyStatusNameEN." | ".$result->FamilyStatusNameLL; ?>
								</option>
								<?php
							} ?>
			   </select>

                <b><?=$this->systemmodel->changeLng("ศาสนา")?></b>
        <select class="form-control" name="ReligionID" id='ReligionID' >
              <option value="">
                <?=$this->systemmodel->changeLng("กรุณาเลือก")?>
              </option>
              <?php
              foreach($results_Religion as $result){
                ?>
                <option value="<?php echo $result->ReligionID; ?>">
                  <?php echo $result->ReligionNameEN." | ".$result->ReligionNameLL; ?>
                </option>
                <?php
              } ?>
         </select>
         <b><?=$this->systemmodel->changeLng("โรคประจำตัว")?></b>
              <input type='text' class="form-control" id='CongenitalDisease' name="CongenitalDisease" />
              </div>
            </div>
          </div>
        </div>
      </div>
      <div id="menu2" class="tab-pane fade">
        <h3></h3>
        <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="panel-title"><?=$this->systemmodel->changeLng("รายละเอียดบริษัท")?></h4>
          </div>
          <div class="panel-body" id="collapse2">
            <div class="col-md-6">
              <p></p>
              <div class="panel panel-default panel-body">
              <b><?=$this->systemmodel->changeLng("กลุ่มของพนักงาน")?></b>
        <select class="form-control" name="EmployeeGroup" id='EmployeeGroup' >
              <option value="">
                <?=$this->systemmodel->changeLng("กรุณาเลือก")?>
              </option>
              <?php
              foreach($results_EmployeeGroup as $result){
                ?>
                <option value="<?php echo $result->EmpGroupID; ?>">
                  <?php echo $result->EmpGroupNameEN; ?> | <?php echo $result->EmpGroupNameLL; ?>
                </option>
                <?php
              } ?>
         </select>
              <b><?=$this->systemmodel->changeLng("ระดับของพนักงาน")?></b>
        <select class="form-control" name="EmployeeLevel" id='EmployeeLevel' >
              <option value="">
                <?=$this->systemmodel->changeLng("กรุณาเลือก")?>
              </option>
              <?php
              foreach($results_EmployeeLevel as $result){
                ?>
                <option value="<?php echo $result->EmpLevelID; ?>">
                  <?php echo $result->EmpLevelNameEN; ?> | <?php echo $result->EmpLevelNameLL; ?>
                </option>
                <?php
              } ?>
         </select>
         <BR>
         <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#myModal" id="btn_Modal"><?=$this->systemmodel->changeLng("เลือกตำแหน่งพนักงาน")?></button>

          <!-- Modal -->
          <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog">

              <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title"><?=$this->systemmodel->changeLng("เลือก บริษัท ฝ่าย แผนก หน่วย ตำแหน่ง")?></h4>
                </div>
                <div class="modal-body" id="CompanyStructureDIV2">
                  <p>Some text in the modal.</p>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
              </div>

            </div>
          </div>

          <script type="text/javascript">
            // $('#btn_Modal').hide();
            $.ajax({
            url:'<?=base_url()?>index.php/hr/companyst',
              type: "post",
              data: {},
              beforeSend: function () {$(".loading").show();},
              complete: function () {$(".loading").hide();},
              success: function (data) {$('#CompanyStructureDIV2').html(data);}
              });
          function getPositionModal(){
            $('#CompID').val($('#CompIDModal').val());
            $('#DivisionID').val($('#DivisionIDModal').val());
            $('#DepartmentID').val($('#DepartmentIDModal').val());
            $('#SectionID').val($('#SectionIDModal').val());
            $('#PositionID').val($('#PositionIDModal').val());
            $('#CompIDTXT').val($('#CompIDModal').val());
            $('#DivisionIDTXT').val($('#DivisionIDModal').val());
            $('#DepartmentIDTXT').val($('#DepartmentIDModal').val());
            $('#SectionIDTXT').val($('#SectionIDModal').val());
            $('#PositionIDTXT').val($('#PositionIDModal').val());
          }
          </script>



              </div>
            </div>
            <div class="col-md-6">
              <p></p>
              <div class="panel panel-default panel-body">
                <b><?=$this->systemmodel->changeLng("บริษัท")?></b><input type="hidden" name="CompID" id="CompID">
         <select class="form-control" name="CompIDTXT" id='CompIDTXT'  disabled>
           <option><?=$this->systemmodel->changeLng("กรุณาเลือกบริษัท")?></option>
           <?php
           foreach($results_Company as $result){
           echo "<option value='".$result['CompID']."'>".$result['CompNameEng']."</option>";
           }
           ?>
        </select>
              <b><?=$this->systemmodel->changeLng("ฝ่าย")?></b><input type="hidden" name="DivisionID" id="DivisionID">
         <select class="form-control" name="DivisionIDTXT" id='DivisionIDTXT'  disabled>
           <option><?=$this->systemmodel->changeLng("กรุณาเลือกฝ่าย")?></option>
           <?php
           foreach($results_Division as $result){
           echo "<option value='".$result['DivisionID']."'>".$result['DivisionNameEng']."</option>";
           }
           ?>
        </select>
              <b><?=$this->systemmodel->changeLng("แผนก")?></b><input type="hidden" name="DepartmentID" id="DepartmentID">
			  <select class="form-control" name="DepartmentIDTXT" id="DepartmentIDTXT"  disabled>
			  <option><?=$this->systemmodel->changeLng("กรุณาเลือกแผนก")?></option>
           <?php
           foreach($results_Department as $result){
           echo "<option value='".$result['DepartmentID']."'>".$result['DepartmentNameEng']."</option>";
           }
           ?>
			  </select>
              <b><?=$this->systemmodel->changeLng("หน่วย")?></b><input type="hidden" name="SectionID" id="SectionID">
			  <select class="form-control" name="SectionIDTXT" id='SectionIDTXT'  disabled>
				<option><?=$this->systemmodel->changeLng("กรุณาเลือกหน่วย")?></option>
           <?php
           foreach($results_Section as $result){
           echo "<option value='".$result['SectionID']."'>".$result['SectionNameEng']."</option>";
           }
           ?>
			  </select>
              <b><?=$this->systemmodel->changeLng("ตำแหน่ง")?></b><input type="hidden" name="PositionID" id="PositionID">
              <select class="form-control" name="PositionIDTXT" id='PositionIDTXT'  disabled>
				<option><?=$this->systemmodel->changeLng("กรุณาเลือกตำแหน่ง")?></option>
           <?php
           foreach($results_Position as $result){
           echo "<option value='".$result['PositionID']."'>".$result['PositionNameEng']."</option>";
           }
           ?>
			  </select>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div id="menu4" class="tab-pane fade">
        <h3></h3>
        <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="panel-title"><?=$this->systemmodel->changeLng("รายละเอียดของสัญญา")?></h4>
          </div>
          <div class="panel-body ">
              <b><?=$this->systemmodel->changeLng("วันเริ่มบริการ")?></b>
			<div class='input-group date' id='datetimepicker'>
                    <input type='text' class="form-control" id='ServiceStartDate' name="ServiceStartDate" Onchange="JavaScript:return fnccheck();"/>
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
               </div>
			   <script>
              $(document).ready(function(){
                var date_input=$('input[name="ServiceStartDate"]'); //our date input has the name "date"
                var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
                date_input.datepicker({
                  format: 'yyyy-mm-dd',
                  container: container,
                  todayHighlight: true,
                  autoclose: true,
                })
              })
            </script>
			  <b><?=$this->systemmodel->changeLng("ประเภทของงาน")?> </b>
			  <select class="form-control" name="WorkingType" id='WorkingType' >
							<option value="">
							<?=$this->systemmodel->changeLng("กรุณาเลือก")?>	
							</option>
							<?php
							foreach($results_WorkType as $result){
								?>
								<option value="<?php echo $result->WorkTypeID; ?>">
									<?php echo $result->WorkTypeNameEng." | ".$result->WorkTypeNameThai; ?>
								</option>
								<?php
							} ?>
			   </select>
              <b><?=$this->systemmodel->changeLng("ชั่วโมงทำงานต่อวัน")?></b>
        <select class="form-control" name="HourperDay" id='HourperDay' >
          <option value=""><?=$this->systemmodel->changeLng("กรุณาเลือก")?></option>
          <?php $i=1; while($i<=24){?><option value="<?php echo $i; ?>"><?php echo $i; ?></option><?php $i++;} ?>
         </select>
              <b><?=$this->systemmodel->changeLng("ประเภทสัญญา")?></b>
			  <select class="form-control" name="ContractType" id='ContractType' onchange="setdat(this)" >
							<option value="">
								<?=$this->systemmodel->changeLng("กรุณาเลือก")?>
							</option>
							<?php
							foreach($results_ContractType as $result){
								?>
								<option value="<?php echo $result->ContractID; ?>">
									<?php echo $result->ContractNameEN." | ".$result->ContractNameLL; ?>
								</option>
								<?php
							} ?>
			   </select>
              <b><?=$this->systemmodel->changeLng("วันเริ่มสัญญาจ้าง")?></b>
              <div class='input-group date' id='datetimepicker'>
                    <input type='text' class="form-control" id='ContractStartDate' name="ContractStartDate"  Onchange="JavaScript:return fnccheck1();"/>
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
               </div>
			   <script>
              $(document).ready(function(){
                var date_input=$('input[name="ContractStartDate"]'); //our date input has the name "date"
                var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
                date_input.datepicker({
                  format: 'yyyy-mm-dd',
                  container: container,
                  todayHighlight: true,
                  autoclose: true,
                })
              })
            </script>
              <b><?=$this->systemmodel->changeLng("วันสิ้นสุดสัญญาจ้าง")?></b>
			  <div class='input-group date' id='datetimepicker'>
                    <input type='text' class="form-control" id='ContractEndDate' name="ContractEndDate" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
               </div>
			   <script>
              $(document).ready(function(){
                var date_input=$('input[name="ContractEndDate"]'); //our date input has the name "date"
                var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
                date_input.datepicker({
                  format: 'yyyy-mm-dd',
                  container: container,
                  todayHighlight: true,
                  autoclose: true,
                })
              })
            </script>
              <b><?=$this->systemmodel->changeLng("วันสิ้นสุดวันทดลองงาน")?></b>
              <div class='input-group date' id='datetimepicker'>
                    <input type='text' class="form-control" id='ProbationEndDate' name="ProbationEndDate" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
               </div>
			   <script>
              $(document).ready(function(){
                var date_input=$('input[name="ProbationEndDate"]'); //our date input has the name "date"
                var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
                date_input.datepicker({
                  format: 'yyyy-mm-dd',
                  container: container,
                  todayHighlight: true,
                  autoclose: true,
                })
              })
            </script>
          </div>
        </div>
      </div>
      <div id="menu5" class="tab-pane fade">
        <h3></h3>
        <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="panel-title"><?=$this->systemmodel->changeLng("รายละเอียดของเงินเดือน")?></h4>
          </div>
          <div class="panel-body ">
             <b> <?=$this->systemmodel->changeLng("ประเภทการจ่ายเงินเดือน")?></b>
			  <select class="form-control" name="SalaryTypeID" id='SalaryTypeID' >
							<option value="">
								<?=$this->systemmodel->changeLng("กรุณาเลือก")?>
							</option>
							<?php
							foreach($results_SalaryTypeID as $result){
								?>
								<option value="<?php echo $result->SalaryTypeID; ?>">
							<?php echo $result->SalaryTypeEN; ?> | <?php echo $result->SalaryTypeLL; ?>
								</option>
								<?php
							} ?>
			   </select>
              <b><?=$this->systemmodel->changeLng("เงินเดือน")?> (*)</b>
              <input type='number' class="form-control" id='MonthlySalary' name="MonthlySalary" required />
              <b><?=$this->systemmodel->changeLng("เงินตอบแทนพิเศษ")?></b>
              <input type='text' class="form-control" id='MonthlyAllowance' name="MonthlyAllowance" />
              <?=$this->systemmodel->changeLng("วิธีการจ่ายเงิน")?> (*)
			  <select class="form-control" name="PaymentMethod" id='PaymentMethod' required>
							<option value="">
								<?=$this->systemmodel->changeLng("กรุณาเลือก")?>
							</option>
							<?php
							foreach($results_PaymentMethod as $result){
								?>
								<option value="<?php echo $result->PaymentMethodTypeID; ?>">
									<?php echo $result->PaymentMethodTypeNameEN; ?> | <?php echo $result->PaymentMethodTypeNameLL; ?>
								</option>
								<?php
							} ?>
			   </select>
              <b><?=$this->systemmodel->changeLng("รหัสการจ่ายภาษีเงินได้")?></b> (*)
			  <select class="form-control" name="PITCode" id='PITCode' required>
							<option value="">
								<?=$this->systemmodel->changeLng("กรุณาเลือก")?>
							</option>
							<?php
							foreach($results_PITCode as $result){
								?>
								<option value="<?php echo $result->PITCodeTypeID; ?>">
									<?php echo $result->PITCodeTypeNameEN." | ".$result->PITCodeTypeNameLL; ?>
								</option>
								<?php
							} ?>
			   </select>
              <b><?=$this->systemmodel->changeLng("รหัสการจ่ายประกันสังคม")?> (*)</b>
        <select class="form-control" name="SocialID" id='SocialID' required>
              <option value="">
                <?=$this->systemmodel->changeLng("กรุณาเลือก")?>
              </option>
              <?php
              foreach($results_SSOCode as $result){
                ?>
                <option value="<?php echo $result->SSOCodeTypeID; ?>">
                  <?php echo $result->SSOCodeTypeNameEN." | ".$result->SSOCodeTypeNameLL; ?>
                </option>
                <?php
              } ?>
         </select>
          </div>
        </div>
      </div>
      <div id="menu6" class="tab-pane fade">
        <h3></h3>
        <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="panel-title"><?=$this->systemmodel->changeLng("รายละเอียดของที่อยู่")?></h4>
          </div>
          <div class="panel-body" id="collapse2">
            <div class="col-md-6">
              <p></p>
              <div class="panel panel-default panel-body">
              <b><?=$this->systemmodel->changeLng("ที่อยู่ตามบัตรประชาชน")?></b>
              <input type='text' class="form-control" id='AddressPermanent' name="AddressPermanent" />
              <b><?=$this->systemmodel->changeLng("จังหวัด")?></b>
			  <select class="form-control" name="ProvinceID" id='ProvinceID' >
			  <option><?=$this->systemmodel->changeLng("กรุณาเลือกจังหวัด")?></option>
				<?php foreach($results_Province as $result){
				echo "<option value='".$result['ProvinceID']."'>".$result['ProvinceNameInter']." | ".$result['ProvinceNameLocal']."</option>";
				}
				?>
			  </select>
              <b><?=$this->systemmodel->changeLng("เขต/อำเภอ")?></b>
        <select class="form-control" name="DistrictID" id='DistrictID' >
        <option><?=$this->systemmodel->changeLng("กรุณาเลือกอำเภอ")?></option>

        </select>
              <b><?=$this->systemmodel->changeLng("แขวง/ตำบล")?> </b>
        <select class="form-control" name="SubDistrictID" id='SubDistrictID' >
        <option><?=$this->systemmodel->changeLng("กรุณาเลือกตำบล")?></option>

        </select>
              <b><?=$this->systemmodel->changeLng("รหัสไปรษณีย์")?></b>
        <select class="form-control" name="Postcode" id='Postcode' >
        <option><?=$this->systemmodel->changeLng("กรุณาเลือกรหัสไปรษณีย์")?></option>

        </select>

              <b><?=$this->systemmodel->changeLng("ประเทศ")?> </b>
              <input type='text' class="form-control" id='CountryID' name="CountryID" />
              </div>
            </div>
            <div class="col-md-6">
              <p></p>
              <div class="panel panel-default panel-body">
              <b><?=$this->systemmodel->changeLng("ที่อยู่ปัจจุบัน")?></b>
            <div class="input-group">
              <span class="input-group-addon btn btn-warning" id="copyAddress"> <span class="glyphicon glyphicon-copy"></span><b><?=$this->systemmodel->changeLng("สำเนาที่อยู่ตามบัตรประชาชน")?></b></span>
              <input type="text" class="form-control" id="AddressResidence" name="AddressResidence" aria-describedby="basic-addon1">
            </div>
              <b><?=$this->systemmodel->changeLng("จังหวัด")?></b>
              <select class="form-control" name="ProvinceRID" id='ProvinceRID' >
              <option><?=$this->systemmodel->changeLng("กรุณาเลือกจังหวัด")?></option>
              <?php foreach($results_Province as $result){
              echo "<option value='".$result['ProvinceID']."'>".$result['ProvinceNameInter']." | ".$result['ProvinceNameLocal']."</option>";
              }
              ?>
              </select>
              <b><?=$this->systemmodel->changeLng("เขต/อำเภอ")?></b>
              <select class="form-control" name="DistrictRID" id='DistrictRID' >
              <option><?=$this->systemmodel->changeLng("กรุณาเลือกอำเภอ")?></option>

              </select>
                    <b><?=$this->systemmodel->changeLng("แขวง/ตำบล")?></b>
              <select class="form-control" name="SubDistrictRID" id='SubDistrictRID' >
              <option><?=$this->systemmodel->changeLng("กรุณาเลือกตำบล")?></option>

              </select>
                   <b> <?=$this->systemmodel->changeLng("รหัสไปรษณีย์")?></b>
              <select class="form-control" name="PostcodeR" id='PostcodeR' >
              <option><?=$this->systemmodel->changeLng("กรุณาเลือกรหัสไปรษณีย์")?></option>
            </select>
              <!-- Province -->
              <!-- <input type='text' class="form-control" id='ProvinceRID' name="ProvinceRID" /> -->
              <!-- District
              <input type='text' class="form-control" id='DistrictRID' name="DistrictRID" />
              Sub District
              <input type='text' class="form-control" id='SubDistrictRID' name="SubDistrictRID" />
              Postcode
              <input type='text' class="form-control" id='PostcodeR' name="PostcodeR" /> -->
              <b><?=$this->systemmodel->changeLng("ประเทศ")?></b>
              <input type='text' class="form-control" id='CountryRID' name="CountryRID" />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- จบส่วนแสดงผล -->
  </div>
</div>
</form>

<script type="text/javascript">
  var requiredElements = document.getElementById("form").querySelectorAll("[required]"),
  c = document.getElementById("btn_save");

  $('#btn_save').on('click',function(e){
    e.preventDefault();
    var s = "";
    var j=0;
    for (var i = 0; i < requiredElements.length; i++) {
      var e = requiredElements[i];
      if(!e.value.length){
        s += "* " + e.id + " : " + (e.value.length ? "Filled" : "Not Filled") + "\n";
        j++;
      }
    }
    if(s !="" ){
      swal("Warning", s, "warning");
    }
    if(j==0){
      var form = $(this).parents('form');
      swal({
          title: "Personnel Information",
          text: "Update Personnel Information Complete",
          type: "success",
          showCancelButton: false,
          confirmButtonText: "OK",
          closeOnConfirm: true
      }, function(isConfirm){
          if (isConfirm) form.submit();
      });
    }
  });
  

$('#copyCardid').click(function(){
  $('#SSOID').val($('#CardID').val());
});

</script>
  <script type='text/javascript'>
  var baseURL= "<?php echo base_url();?>";
  $(document).ready(function(){
    $('#DivisionID').change(function(){
      var DivisionID = $(this).val();
      $.ajax({
        url:'<?=base_url()?>index.php/hr/getDepartment',
        method: 'post',
        data: {DivisionID: DivisionID},
        dataType: 'json',
        success: function(response){
          $('#SectionID').find('option').not(':first').remove();
          $('#DepartmentID').find('option').not(':first').remove();
          $.each(response,function(index,data){
             $('#DepartmentID').append('<option value="'+data['DepartmentID']+'">'+data['DepartmentNameThai']+'</option>');
          });
        }
     });
   });
   $('#DepartmentID').change(function(){
     var ID = $(this).val();
     $.ajax({
       url:'<?=base_url()?>index.php/hr/getSection',
       method: 'post',
       data: {DepartmentID: ID},
       dataType: 'json',
       success: function(response){
         $('#SectionID').find('option').not(':first').remove();
         $.each(response,function(index,data){
           $('#SectionID').append('<option value="'+data['SectionID']+'">'+data['SectionNameThai']+'</option>');
         });
       }
    });
	});
	$('#SectionID').change(function(){
     var SectionID = $(this).val();
     $.ajax({
       url:'<?=base_url()?>index.php/hr/getPosition',
       method: 'post',
       data: {SectionID: SectionID},
       dataType: 'json',
       success: function(response){
         $('#PositionID').find('option').not(':first').remove();
         $.each(response,function(index,data){
           $('#PositionID').append('<option value="'+data['PositionID']+'">'+data['PositionNameThai']+'</option>');
         });
       }
    });
	});
	
 });

 
$('#copyAddress').click(function(){
  $('#AddressResidence').val($('#AddressPermanent').val());
  $('#ProvinceRID').val($('#ProvinceID').val());

  var AMPHUR_PROVINCE_ID = $('#ProvinceID').val();
    $.ajax({
        url:'<?=base_url()?>index.php/hr/getAmphure',
        method: 'post',
        data: {AMPHUR_PROVINCE_ID: AMPHUR_PROVINCE_ID},
        dataType: 'json',
        success: function(response){
          $('#SubDistrictRID').find('option').not(':first').remove();
          $('#DistrictRID').find('option').not(':first').remove();
          $.each(response,function(index,data){
             $('#DistrictRID').append('<option value="'+data['AMPHUR_ID']+'">'+data['AMPHUR_NAME_ENG']+' | '+data['AMPHUR_NAME']+'</option>');
          });
          $('#DistrictRID').val($('#DistrictID').val());

             var DISTRICT_AMPHUR_ID = $('#DistrictID').val();
      // alert(DISTRICT_AMPHUR_ID);
             $.ajax({
               url:'<?=base_url()?>index.php/hr/getSubDistrict',
               method: 'post',
               data: {DISTRICT_AMPHUR_ID: DISTRICT_AMPHUR_ID},
               dataType: 'json',
               success: function(response){
                 $('#SubDistrictRID').find('option').not(':first').remove();
                 $('#PostcodeR').find('option').not(':first').remove();
                 $.each(response,function(index,data){
                   $('#SubDistrictRID').append('<option value="'+data['DISTRICT_CODE']+'">'+data['DISTRICT_NAME_ENG']+' | '+data['DISTRICT_NAME']+'</option>');
                 });
                 $('#SubDistrictRID').val($('#SubDistrictID').val());

                  var district_code = $('#SubDistrictID').val();
                   $.ajax({
                     url:'<?=base_url()?>index.php/hr/getPostcode',
                     method: 'post',
                     data: {district_code: district_code},
                     dataType: 'json',
                     success: function(response){
                       $('#PostcodeR').find('option').not(':first').remove();
                       $.each(response,function(index,data){
                         $('#PostcodeR').append('<option value="'+data['zipcode']+'">'+data['zipcode']+'</option>');
                       });
                       $('#PostcodeR').val($('#Postcode').val());
                     }
                  });

                   
               }
            });

          
            


        }
     });     

    


    

  
  
  
  $('#CountryRID').val($('#CountryID').val());
}); 
 
 </script>
<script type='text/javascript'>
  var baseURL= "<?php echo base_url();?>";
  $(document).ready(function(){
    $('#ProvinceID').change(function(){
      var AMPHUR_PROVINCE_ID = $(this).val();
      $.ajax({
        url:'<?=base_url()?>index.php/hr/getAmphure',
        method: 'post',
        data: {AMPHUR_PROVINCE_ID: AMPHUR_PROVINCE_ID},
        dataType: 'json',
        success: function(response){
          $('#SubDistrictID').find('option').not(':first').remove();
          $('#DistrictID').find('option').not(':first').remove();
	
          $.each(response,function(index,data){
             $('#DistrictID').append('<option value="'+data['AMPHUR_ID']+'">'+data['AMPHUR_NAME_ENG']+' | '+data['AMPHUR_NAME']+'</option>');
          });
        }
     });
   });

    $('#ProvinceRID').change(function(){
      var AMPHUR_PROVINCE_ID = $(this).val();
      $.ajax({
        url:'<?=base_url()?>index.php/hr/getAmphure',
        method: 'post',
        data: {AMPHUR_PROVINCE_ID: AMPHUR_PROVINCE_ID},
        dataType: 'json',
        success: function(response){
          $('#SubDistrictRID').find('option').not(':first').remove();
          $('#DistrictRID').find('option').not(':first').remove();
  
          $.each(response,function(index,data){
             $('#DistrictRID').append('<option value="'+data['AMPHUR_ID']+'">'+data['AMPHUR_NAME_ENG']+' | '+data['AMPHUR_NAME']+'</option>');
          });
        }
     });
   });

   $('#DistrictID').change(function(){
     var DISTRICT_AMPHUR_ID = $(this).val();
     $.ajax({
       url:'<?=base_url()?>index.php/hr/getSubDistrict',
       method: 'post',
       data: {DISTRICT_AMPHUR_ID: DISTRICT_AMPHUR_ID},
       dataType: 'json',
       success: function(response){
         $('#SubDistrictID').find('option').not(':first').remove();
		 $('#Postcode').find('option').not(':first').remove();
         $.each(response,function(index,data){
           $('#SubDistrictID').append('<option value="'+data['DISTRICT_CODE']+'">'+data['DISTRICT_NAME_ENG']+' | '+data['DISTRICT_NAME']+'</option>');
         });
       }
    });
	});

   $('#DistrictRID').change(function(){
     var DISTRICT_AMPHUR_ID = $(this).val();
     $.ajax({
       url:'<?=base_url()?>index.php/hr/getSubDistrict',
       method: 'post',
       data: {DISTRICT_AMPHUR_ID: DISTRICT_AMPHUR_ID},
       dataType: 'json',
       success: function(response){
         $('#SubDistrictRID').find('option').not(':first').remove();
         $('#PostcodeR').find('option').not(':first').remove();
         $.each(response,function(index,data){
           $('#SubDistrictRID').append('<option value="'+data['DISTRICT_CODE']+'">'+data['DISTRICT_NAME_ENG']+' | '+data['DISTRICT_NAME']+'</option>');
         });
       }
    });
  });

	$('#SubDistrictID').change(function(){
     var district_code = $(this).val();
     $.ajax({
       url:'<?=base_url()?>index.php/hr/getPostcode',
       method: 'post',
       data: {district_code: district_code},
       dataType: 'json',
       success: function(response){
         $('#Postcode').find('option').not(':first').remove();
         $.each(response,function(index,data){
           $('#Postcode').append('<option value="'+data['zipcode']+'">'+data['zipcode']+'</option>');
         });
       }
    });
	});

  $('#SubDistrictRID').change(function(){
     var district_code = $(this).val();
     $.ajax({
       url:'<?=base_url()?>index.php/hr/getPostcode',
       method: 'post',
       data: {district_code: district_code},
       dataType: 'json',
       success: function(response){
         $('#PostcodeR').find('option').not(':first').remove();
         $.each(response,function(index,data){
           $('#PostcodeR').append('<option value="'+data['zipcode']+'">'+data['zipcode']+'</option>');
         });
       }
    });
  });

 });
 </script>
<!-- Bootstrap-Select -->
<script src="<?php echo base_url();?>assets/bootstrap-select/js/bootstrap-select.js"></script>
<script src="<?php echo base_url();?>assets/chartjs/Chart.bundle.js"></script>
<script src="<?php echo base_url();?>assets/chartjs/Chart.js"></script>
<script src="<?php echo base_url();?>assets/bootstrap/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url();?>assets/jquery-datatable/jquery.dataTables.js"></script>
<script src="<?php echo base_url();?>assets/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
<script src="<?php echo base_url();?>assets/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url();?>assets/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
<script src="<?php echo base_url();?>assets/jquery-datatable/extensions/export/jszip.min.js"></script>
<script src="<?php echo base_url();?>assets/jquery-datatable/extensions/export/pdfmake.min.js"></script>
<script src="<?php echo base_url();?>assets/jquery-datatable/extensions/export/vfs_fonts.js"></script>
<script src="<?php echo base_url();?>assets/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
<script src="<?php echo base_url();?>assets/jquery-datatable/extensions/export/buttons.print.min.js"></script>
<script>
$(function(){
  <?php foreach($results_Employee as $result){ ?>
    $("#CardID").val("<?php echo $result->CardID; ?>");
	$("#Birthday").val("<?php echo $result->Birthday; ?>");
  $("#IssueDate").val("<?php echo $result->IssueDate; ?>");
	$("#SSOID").val("<?php echo $result->SSOID; ?>");
	$("#IssuePlace").val("<?php echo $result->IssuePlace; ?>");
	$("#ExpiringDate").val("<?php echo $result->ExpiringDate; ?>");
  $("#EmployeeCode").val("<?php echo $result->EmployeeCode; ?>");
	$("#TitleID").val("<?php echo $result->TitleID; ?>");
	$("#FirstName").val("<?php echo $result->FirstName; ?>");
	$("#MiddleName").val("<?php echo $result->MiddleName; ?>");
	$("#LastName").val("<?php echo $result->LastName; ?>");
	$("#FullNameLL").val("<?php echo $result->FullNameLL; ?>");
	$("#GenderID").val("<?php echo $result->GenderID; ?>");
	$("#NationalityID").val("<?php echo $result->NationalityID; ?>");
	$("#RaceID").val("<?php echo $result->RaceID; ?>");
	$("#FamilyStatusID").val("<?php echo $result->FamilyStatusID; ?>");
	
  $("#CompID").val("<?php echo $result->CompEM; ?>");
  $("#DivisionID").val("<?php echo $result->DivisionID; ?>");
  $("#DepartmentID").val("<?php echo $result->DepartmentID; ?>");
  $("#SectionID").val("<?php echo $result->SectionID; ?>");
  $("#PositionID").val("<?php echo $result->PositionID; ?>");

  $("#CompIDTXT").val("<?php echo $result->CompEM; ?>");
  $("#DivisionIDTXT").val("<?php echo $result->DivisionID; ?>");
  $("#DepartmentIDTXT").val("<?php echo $result->DepartmentID; ?>");
  $("#SectionIDTXT").val("<?php echo $result->SectionID; ?>");
  $("#PositionIDTXT").val("<?php echo $result->PositionID; ?>");
  
	$("#EmployeeGroup").val("<?php echo $result->EmployeeGroup; ?>");
	$("#EmployeeLevel").val("<?php echo $result->EmployeeLevel; ?>");
	$("#ServiceStartDate").val("<?php echo $result->ServiceStartDate; ?>");
	$("#WorkingType").val("<?php echo $result->WorkingType; ?>");
	$("#HourperDay").val("<?php echo $result->HourperDay; ?>");
	$("#ContractType").val("<?php echo $result->ContractType; ?>");
	$("#ContractStartDate").val("<?php echo $result->ContractStartDate; ?>");
	$("#ContractEndDate").val("<?php echo $result->ContractEndDate; ?>");
	$("#ProbationEndDate").val("<?php echo $result->ProbationEndDate; ?>");
	$("#SalaryTypeID").val("<?php echo $result->SalaryTypeID; ?>");
	$("#MonthlySalary").val("<?php echo $result->MonthlySalary; ?>");
	$("#MonthlyAllowance").val("<?php echo $result->MonthlyAllowance; ?>");
	$("#PaymentMethod").val("<?php echo $result->PaymentMethod; ?>");
	$("#PITCode").val("<?php echo $result->PITCode; ?>");
	$("#SocialID").val("<?php echo $result->SocialID; ?>");
	
	$("#AddressPermanent").val("<?php echo $result->AddressPermanent; ?>");
  $("#ProvinceID").val("<?php echo $result->ProvinceID; ?>");
  $("#ProvinceRID").val("<?php echo $result->ProvinceRID; ?>");
  $("#ReligionID").val("<?php echo $result->ReligionID; ?>");
	$("#CongenitalDisease").val("<?php echo $result->CongenitalDisease; ?>");

  var isresign = <?php echo $result->ResignationTypeStatusOut; ?>

  if(isresign){
    $('#form input').attr('readonly', 'readonly');
    $('#form select').attr('readonly', 'readonly');
    $('#btn_save').hide();
  }


  // alert(<?php echo $result->DistrictIDEM; ?>);

	var AMPHUR_PROVINCE_ID = <?php echo $result->ProvinceIDEM; ?>;
    $.ajax({
        url:'<?=base_url()?>index.php/hr/getAmphure',
        method: 'post',
        data: {AMPHUR_PROVINCE_ID: AMPHUR_PROVINCE_ID},
        dataType: 'json',
        success: function(response){
          $('#SubDistrictID').find('option').not(':first').remove();
          $('#DistrictID').find('option').not(':first').remove();
          $.each(response,function(index,data){
             $('#DistrictID').append('<option value="'+data['AMPHUR_ID']+'">'+data['AMPHUR_NAME_ENG']+' | '+data['AMPHUR_NAME']+'</option>');
          });
          $("#DistrictID").val("<?php echo $result->DistrictIDEM; ?>");
          var DISTRICT_AMPHUR_ID = <?php echo $result->DistrictIDEM; ?>;
           $.ajax({
             url:'<?=base_url()?>index.php/hr/getSubDistrict',
             method: 'post',
             data: {DISTRICT_AMPHUR_ID: DISTRICT_AMPHUR_ID},
             dataType: 'json',
             success: function(response){
               $('#SubDistrictID').find('option').not(':first').remove();
           $('#Postcode').find('option').not(':first').remove();
               $.each(response,function(index,data){
                 $('#SubDistrictID').append('<option value="'+data['DISTRICT_CODE']+'">'+data['DISTRICT_NAME_ENG']+' | '+data['DISTRICT_NAME']+'</option>');
               });
               $("#SubDistrictID").val("<?php echo $result->SubDistrictIDEM; ?>");

                 var district_code = <?php echo $result->SubDistrictIDEM; ?>;
                 $.ajax({
                   url:'<?=base_url()?>index.php/hr/getPostcode',
                   method: 'post',
                   data: {district_code: district_code},
                   dataType: 'json',
                   success: function(response){
                     $('#Postcode').find('option').not(':first').remove();
                     $.each(response,function(index,data){
                       $('#Postcode').append('<option value="'+data['zipcode']+'">'+data['zipcode']+'</option>');
                     });
                     $("#Postcode").val("<?php echo $result->PostcodeEM; ?>");
                   }
                });
                 
             }
          });
        }
     }); 

  var AMPHUR_PROVINCE_ID = <?php echo $result->ProvinceRID; ?>;
    $.ajax({
        url:'<?=base_url()?>index.php/hr/getAmphure',
        method: 'post',
        data: {AMPHUR_PROVINCE_ID: AMPHUR_PROVINCE_ID},
        dataType: 'json',
        success: function(response){
          $('#SubDistrictRID').find('option').not(':first').remove();
          $('#DistrictRID').find('option').not(':first').remove();
          $.each(response,function(index,data){
             $('#DistrictRID').append('<option value="'+data['AMPHUR_ID']+'">'+data['AMPHUR_NAME_ENG']+' | '+data['AMPHUR_NAME']+'</option>');
          });
          $("#DistrictRID").val("<?php echo $result->DistrictRID; ?>");


           var DISTRICT_AMPHUR_ID = <?php echo $result->DistrictRID; ?>;
             $.ajax({
               url:'<?=base_url()?>index.php/hr/getSubDistrict',
               method: 'post',
               data: {DISTRICT_AMPHUR_ID: DISTRICT_AMPHUR_ID},
               dataType: 'json',
               success: function(response){
                 $('#SubDistrictRID').find('option').not(':first').remove();
             $('#Postcode').find('option').not(':first').remove();
                 $.each(response,function(index,data){
                   $('#SubDistrictRID').append('<option value="'+data['DISTRICT_CODE']+'">'+data['DISTRICT_NAME_ENG']+' | '+data['DISTRICT_NAME']+'</option>');
                 });
                 $("#SubDistrictRID").val("<?php echo $result->SubDistrictRID; ?>");

                  var district_code = <?php echo $result->SubDistrictRID; ?>;
                   $.ajax({
                     url:'<?=base_url()?>index.php/hr/getPostcode',
                     method: 'post',
                     data: {district_code: district_code},
                     dataType: 'json',
                     success: function(response){
                       $('#PostcodeR').find('option').not(':first').remove();
                       $.each(response,function(index,data){
                         $('#PostcodeR').append('<option value="'+data['zipcode']+'">'+data['zipcode']+'</option>');
                       });
                       $("#PostcodeR").val("<?php echo $result->PostcodeR; ?>");
                     }
                  });



               }
            });


             
        }
     });     





  
     
    
  
  

	// $("#DistrictID").val("<?php echo $result->DistrictIDEM; ?>");
	// $("#SubDistrictID").val("<?php echo $result->SubDistrictIDEM; ?>");
	// $("#Postcode").val("<?php echo $result->Postcode; ?>");
	$("#CountryID").val("<?php echo $result->CountryID; ?>");
	
	$("#AddressResidence").val("<?php echo $result->AddressResidence; ?>");
	// $("#ProvinceRID").val("<?php echo $result->ProvinceRID; ?>");
	// $("#DistrictRID").val("<?php echo $result->DistrictRID; ?>");
	// $("#SubDistrictRID").val("<?php echo $result->SubDistrictRID; ?>");
	// $("#PostcodeR").val("<?php echo $result->PostcodeR; ?>");
	$("#CountryRID").val("<?php echo $result->CountryRID; ?>");
	
  <?php } ?>

});
</script>