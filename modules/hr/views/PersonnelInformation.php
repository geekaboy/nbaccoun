<script type="text/javascript">
  $(document).ready(function(){
    //เพิ่มเงื่อนไขตาราง
    $('.js-exportable').DataTable({
        dom: 'Blfrtip',responsive: true,paging: true,info: true,
        buttons: [
            {extend:'copyHtml5',text:' <i class="fa fa-files-o fa-2x "></i> ',titleAttr: ' Copy '},
            {extend:'excelHtml5',text:' <i class="fa fa-file-excel-o fa-2x "></i> ',titleAttr: ' Excel '},
            {extend:'print',text:' <i class="fa fa-print fa-2x "></i><BR>&nbsp; ',titleAttr: ' Print '}
          ,
        ]
        ,"lengthMenu": [ [10, 25, 50, -1], [10, 25, 50, "All"] ],
      });

    $('.js-exportable2').DataTable({
        dom: 'Bfrtip',responsive: true,paging: true,info: false,
        buttons: [
            {extend:'copyHtml5',text:' <i class="fa fa-files-o fa-2x "></i> ',titleAttr: ' Copy '},
            {extend:'excelHtml5',text:' <i class="fa fa-file-excel-o fa-2x "></i> ',titleAttr: ' Excel '},
            {extend:'print',text:' <i class="fa fa-print fa-2x "></i> ',titleAttr: ' Print '
            // ,exportOptions:{columns:[0,1,2,3,4,5,6]}
          },
        ]
        // ,"columnDefs": [{"targets": [ 0,1,2,3,4 ],"visible": false,"searchable": true}]
      });
//กรณีต้องการปิดการแสดงบาง column แต่สามารถ export file ได้
    $('.js-exportable3').DataTable({
        dom: 'Bfrtip',responsive: true,paging: true,info: false,
        buttons: [
            {extend:'copyHtml5',text:' <i class="fa fa-files-o fa-2x "></i> ',titleAttr: ' Copy '},
            {extend:'excelHtml5',text:' <i class="fa fa-file-excel-o fa-2x "></i> ',titleAttr: ' Excel '},
            {extend:'print',text:' <i class="fa fa-print fa-2x "></i> ',titleAttr: ' Print '
            ,exportOptions:{columns:[0,1,2,3,4,5,6]}
          },
        ],
        "columnDefs": [{"targets": [ 5 ],"visible": false,"searchable": true}]
      });
  });
</script>
<!-- begin add require script -->
<head>
  <link href="<?php echo base_url();?>assets/bootstrap-select/css/bootstrap-select.css" rel="stylesheet">
  <link href="<?php echo base_url();?>assets/bootstrap/css/datepicker.css" rel="stylesheet">
  <link href="<?php echo base_url();?>assets/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">
  <meta charset="UTF-8">
  
  <style>
  @media print {
    canvas {
      min-height: 100%;max-width: 100%;max-height: 100%;height: auto!important;width: auto!important;
    }
    table {
      min-height: 100%;max-width: 100%;max-height: 100%;height: auto!important;width: auto!important;
      page-break-inside: : auto;
    }
    tr{
      page-break-inside: avoid;
      page-break-after: auto;

    }
  }
  canvas{-moz-user-select: none;-webkit-user-select: none;-ms-user-select: none;}
  </style>
</head>

<!-- end add require script -->
<div class="box box-success">
  <div class="box-header" align="left">
        <i class="fa fa-sign-in"></i>
    <h3 class="box-title"><?php echo $this->systemmodel->changeLng($this->systemmodel->get_menuname($this->uri->segment(1).'/'.$this->uri->segment(2)));?></h3>
  </div>
  <div class="box-body">
    <!-- ส่วนแสดงผล -->
<div class="box box-success">
  <div class="box-header">
    <b></b>
  </div>
  <div class="box-body" align="left">

        <!-- ตาราง แสดงข้อมูล -->
        <table class="table table-bordered table-striped table-hover dataTable  js-exportable">
          <thead>
            <tr >

              <th color="Blue" scope="col"><center><?=$this->systemmodel->changeLng("ลำดับที่")?></center></th>
              <th scope="col"><center><?=$this->systemmodel->changeLng("รหัสพนักงาน")?></center></th>
              <th scope="col"><center><span class="glyphicon glyphicon-user"></span></center></th>
              <th scope="col"><center><?=$this->systemmodel->changeLng("ชื่อพนักงาน")?></center></th>
              <th scope="col"><center><?=$this->systemmodel->changeLng("ตำแหน่ง")?></center></th>
              <th scope="col"><center><?=$this->systemmodel->changeLng("ประวัติพนักงาน")?> </center></th>
              <th scope="col"><center><?=$this->systemmodel->changeLng("ข้อมูลอื่นพนักงาน")?></center></th>
              <th scope="col"><center><?=$this->systemmodel->changeLng("การเปลี่ยนแปลง")?></center></th>
              <th scope="col"><center><?=$this->systemmodel->changeLng("การลาออก")?></center></th>
            </tr>
          </thead>
          <tbody>
          <?php 
        $i=0;
        if(!empty($results_All)){
          foreach ($results_All as $row) {
          ?>
            <tr>
              <td scope="col"><?php echo $i+1;?></td>
              <td scope="col"><?php echo $row->EmployeeCode;?></td>
              <td scope="col">
                <!-- Trigger the modal with a button -->
                <?php if(!empty($row->EmployeePhoto)){?>
                <img src="<?php echo base_url(); ?>assets/uploads/company/nc/files/<?php echo $row->EmployeePhoto;?>" id="EmployeePhoto_show" class="img-circle" width="50px;"data-toggle="modal" data-target="#myModal<?php echo $i;?>" style="cursor: pointer;">
                <!-- Modal -->
                <div id="myModal<?php echo $i;?>" class="modal fade" role="dialog">
                  <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">                      
                      <div class="modal-body">
                        <img src="<?php echo base_url(); ?>assets/uploads/company/nc/files/<?php echo $row->EmployeePhoto;?>" id="EmployeePhoto_show" class="img-responsive">
                      </div>
                    </div>

                  </div>
                </div>
              <?php }?>
              </td>
              <td scope="col"><?php echo $row->FullNameLL." | ".$row->TitleEN." ".$row->FirstName." ".$row->LastName;?></td>
              <td scope="col"><?php echo $row->PositionNameEng;?></td>
              <td scope="col" align="center">
                <a href="/index.php/hr/EditAddNewStaff/edit/<?php echo $row->EmployeeID?>" class="btn btn-success btn-xs">
                  <span class="glyphicon glyphicon-edit"></span> <?php if($row->ResignationTypeStatusOut) echo "View"; else echo "Edit";?>
                </a>
              </td>
              <td scope="col" align="center">
                <a href="/index.php/hr/AddOtherInformation/edit/<?php echo $row->EmployeeID?>" class="btn btn-info btn-xs">
                  <span class="glyphicon glyphicon-edit"></span> Edit
                </a>
              </td>
              <td scope="col" align="center">
                <a href="/index.php/hr/form_employee_movement/edit/<?php echo $row->EmployeeID?>" class="btn btn-warning btn-xs">
                  <span class="glyphicon glyphicon-edit"></span> Edit
                </a>
              </td>
              <td scope="col" align="center">
              	<button type="button" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#myModalEmployeeID" onclick="setEmployeeID(<?php echo $row->EmployeeID;?>,'<?php echo $row->FullNameLL;?>')"><span class="glyphicon glyphicon-edit"></span> Edit</button>
				
        <!-- น่าจะเป็นบริเวณที่เอสั่งให้พนักงานลาออกแล้วมาแสดงเพื่อแก้ไขได้ แต่เราไม่ต้องการให้แก้ไขได้แล้ว-->
        <?php if($row->ResignationTypeStatusOut==0){?> 
				<i class='btn btn-success btn-xs'>ปกติ</i>

				<!--<button type="button" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#myModalEmployeeID" onclick="setEmployeeID(<?php echo $row->EmployeeID;?>,'<?php echo $row->FullNameLL;?>')"><span class="glyphicon glyphicon-edit"></span> Edit</button>-->
				
				<?php }else{?>
				<i class='btn btn-warning btn-xs'>ลาออก</i>
				<button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#myModalCheck<?php echo $row->EmployeeID;?>"><span class="glyphicon glyphicon-check"></span> Check</button>
				<div class="modal fade" id="myModalCheck<?php echo $row->EmployeeID;?>" role="dialog">
                  <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">ข้อมูลพนักงาน | <?php echo $row->FullNameLL;?></h4>
                      </div>
                      <div class="modal-body">
					  <table class="table table-bordered table-striped table-hover">
                                  <thead>
                                    <tr>
                                      <th colspan="2"><?=$this->systemmodel->changeLng("รายละเอียดการลาออก")?></th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    <tr>
                                      <td align="right" width="25%"><?=$this->systemmodel->changeLng("ประเภทการลาออก")?>(*)</td>
                                      <td>
										<?php foreach($results_ResignationTypeOut as $result){ ?>
											<?php if($result->ResignID == $row->ResignationTypeOut) echo "".$result->ResignNameLL." | ".$result->ResignNameEN.""; ?>
										<?php } ?>
									  </td>
                                    </tr>
                                    <tr>
                                      <td align="right"><?=$this->systemmodel->changeLng("จำนวนวันที่ต้องแจ้งล่วงหน้า")?></td>
                                      <td><?php echo $row->NotificationDaysRequired; ?> Day</td>
                                    </tr>
									<tr>
                                      <td align="right">Resignation Reason (*)</td>
                                      <td>
									  <?php foreach($results_ResignationReason as $result){ ?>
											<?php if($result->ResignationReasonID == $row->ResignationReason) echo "".$result->ResignationReasonLL." | ".$result->ResignationReasonNameEN.""; ?>
										<?php } ?>
									  </td>
                                    </tr>
									<tr>
                                      <td align="right">Notification Date</td>
                                      <td><?php echo $row->NotificationDate; ?></td>
                                    </tr>
									<tr>
                                      <td align="right">Notification Expiration Date</td>
                                      <td><?php echo $row->NotificationExpirationDate; ?></td>
                                    </tr>
									<tr>
                                      <td align="right">Last working Day</td>
                                      <td><?php echo $row->LastworkingDay; ?></td>
                                    </tr>
									<tr>
                                      <td align="right">Leaving Date (*)</td>
                                      <td><?php echo $row->LeavingDate; ?></td>
                                    </tr>
									<tr>
                                      <td align="right">Last Day of Employment Date(*)</td>
                                      <td><?php echo $row->EffectiveDateOut; ?></td>
                                    </tr>
									<tr>
                                      <td align="right">Remark</td>
                                      <td><?php echo $row->Remark; ?></td>
                                    </tr>
                                  </tbody>
                                </table>
                      </div>
                    </div>
                    
                  </div>
                </div>
				<?php } ?>
              </td>
            </tr>
          <?php
          
          $i++;
        } 
        }  
          ?>
          </tbody>
        </table>
  </div>
</div>
</div>



<div class="modal fade" id="myModalEmployeeID" role="dialog">
                  <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title" id = "modalHeader"></h4>
                      </div>
                      <div class="modal-body">
            <form action="<?php echo  base_url('index.php/hr/updateResignation'); ?>" id="form" name="form" method="post">
            <table class="table table-bordered table-striped table-hover">
            <input type="hidden" id="EmployeeID" name="EmployeeID" value="">
                                  <thead>
                                    <tr>
                                      <th colspan="2"><?=$this->systemmodel->changeLng("รายละเอียดการลาออก")?></th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    <tr>
                                      <td align="right" width="25%"><?=$this->systemmodel->changeLng("ประเภทการลาออก")?>(*)</td>
                                      <td>
                    <select class="form-control" name="ResignationTypeOut" id='ResignationTypeOut' required>
                    <option value=""><?=$this->systemmodel->changeLng("เลือกประเภทการลาออก")?></option>
                    <?php
                    foreach($results_ResignationTypeOut as $result){
                      ?>
                      <option value="<?php echo $result->ResignID; ?>">
                        <?php echo $result->ResignNameLL; ?> | <?php echo $result->ResignNameEN; ?>
                      </option>
                      <?php
                    } ?>
                    </select>
                    </td>
                                    </tr>
                                    <tr>
                                      <td align="right"><?=$this->systemmodel->changeLng("จำนวนวันที่ต้องแจ้งล่วงหน้า")?></td>
                                      <td><input type='number' class="form-control" id='NotificationDaysRequired' name="NotificationDaysRequired" required onfocusout="CalNotiExpire()"></td>
                                    </tr>
                  <tr>
                                      <td align="right"><?=$this->systemmodel->changeLng("เหตุผลในการลาออก")?>(*)</td>
                                      <td>
                    <select class="form-control" name="ResignationReason" id='ResignationReason' required>
                    <option value=""><?=$this->systemmodel->changeLng("เลือกเหตุผลในการลาออก")?></option>
                    <?php
                    foreach($results_ResignationReason as $result){
                      ?>
                      <option value="<?php echo $result->ResignationReasonID; ?>">
                        <?php echo $result->ResignationReasonLL; ?> | <?php echo $result->ResignationReasonNameEN; ?>
                      </option>
                      <?php
                    } ?>
                    </select>
                    </td>
                                    </tr>
                  <tr>
                                      <td align="right"><?=$this->systemmodel->changeLng("วันที่แจ้งลาออก")?></td>
                                      <td>
                    <div class='input-group date' id='datetimepicker'>
                    <input type='text' class="form-control" id='NotificationDate' name="NotificationDate" onchange="CalNotiExpire()" />
                    <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                    </div>
                    <script>
                    $(document).ready(function(){
                    var date_input=$('input[name="NotificationDate"]'); //our date input has the name "date"
                    var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
                    date_input.datepicker({
                      format: 'yyyy-mm-dd',
                      container: container,
                      todayHighlight: true,
                      autoclose: true,
                    })
                    })
                    </script>
                    </td>
                                    </tr>
                  <tr>
                                      <td align="right"><?=$this->systemmodel->changeLng("วันที่มีผลในการแจ้งลาออก")?></td>
                                      <td>
                    <div class='input-group date' id='datetimepicker'>
                    <input type='text' class="form-control" id='NotificationExpirationDate' name="NotificationExpirationDate" readonly="true" />
                    <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                    </div>
                    <script>
                    // $(document).ready(function(){
                    // var date_input=$('input[name="NotificationExpirationDate"]'); //our date input has the name "date"
                    // var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
                    // date_input.datepicker({
                    //   format: 'yyyy-mm-dd',
                    //   container: container,
                    //   todayHighlight: true,
                    //   autoclose: true,
                    // })
                    // })
                    </script>
                    </td>
                                    </tr>
                  <tr>
                                      <td align="right"><?=$this->systemmodel->changeLng("วันทำงานวันสุดท้าย")?></td>
                                      <td>
                    <div class='input-group date' id='datetimepicker'>
                    <input type='text' class="form-control" id='LastworkingDay' name="LastworkingDay"/>
                    <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                    </div>
                    <script>
                    $(document).ready(function(){
                    var date_input=$('input[name="LastworkingDay"]'); //our date input has the name "date"
                    var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
                    date_input.datepicker({
                      format: 'yyyy-mm-dd',
                      container: container,
                      todayHighlight: true,
                      autoclose: true,
                    })
                    })
                    </script>
                    </td>
                                    </tr>
                  <tr>
                                      <td align="right"><?=$this->systemmodel->changeLng("วันที่ย้ายออกจากที่ทำงาน")?>(*)</td>
                                      <td>
                    <div class='input-group date' id='datetimepicker'>
                    <input type='text' class="form-control" id='LeavingDate' name="LeavingDate" required/>
                    <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                    </div>
                    <script>
                    $(document).ready(function(){
                    var date_input=$('input[name="LeavingDate"]'); //our date input has the name "date"
                    var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
                    date_input.datepicker({
                      format: 'yyyy-mm-dd',
                      container: container,
                      todayHighlight: true,
                      autoclose: true,
                    })
                    })
                    </script>
                    </td>
                                    </tr>
                  <tr>
                                      <td align="right"><?=$this->systemmodel->changeLng("วันสิ้นสุดการจ้างงาน")?>(*)</td>
                                      <td>
                    <div class='input-group date' id='datetimepicker'>
                    <input type='text' class="form-control" id='EffectiveDateOut' name="EffectiveDateOut" required/>
                    <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                    </div>
                    <script>
                    $(document).ready(function(){
                    var date_input=$('input[name="EffectiveDateOut"]'); //our date input has the name "date"
                    var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
                    date_input.datepicker({
                      format: 'yyyy-mm-dd',
                      container: container,
                      todayHighlight: true,
                      autoclose: true,
                    })
                    })
                    </script>
                    </td>
                                    </tr>
                  <tr>
                                      <td align="right"><?=$this->systemmodel->changeLng("หมายเหตุ")?></td>
                                      <td><textarea name="Remark"  class="form-control" id="Remark" cols="50" rows="8"></textarea></td>
                                    </tr>
                                  </tbody>
                                </table>
            <button type="submit" class="btn btn-primary" id="btn_save">Save</button>
            </form>
                      </div>
                    </div>
                    
                  </div>
                </div>


<script type="text/javascript">
 function MovementEdit(id) {
      $.ajax({
        url:'<?=base_url()?>index.php/hr/view_employee_movement',
        type: "post",
        data: {
               control1: id
              },
        beforeSend: function () {$(".loading").show();},
        complete: function () {$(".loading").hide();},
        success: function (data) {$("#movementForm").html(data);}
    });
  }
function setEmployeeID(EmployeeID,EmployeeName) {
  $('#EmployeeID').val(EmployeeID);
  $('#modalHeader').html("ข้อมูลพนักงาน | "+EmployeeName);
}
function CalNotiExpire(){
  val1 = parseInt($('#NotificationDaysRequired').val());
  val2 = $('#NotificationDate').val();

var dat = new Date(val2);
var ret = dat.toISOString().substring(0, 10).split('-').reverse()

//rearranging to whatever format you want
var t = ret[0];
ret[0] = ret[1];
ret[1] = t;
// NotificationExpirationDate = ret.join('/');

    var date = new Date(ret.join('/'));
    var newdate = new Date(date);

    newdate.setDate(newdate.getDate() + val1);
    
    var dd = newdate.getDate();
    var mm = newdate.getMonth() + 1;
    var y = newdate.getFullYear();

    dd = "0"+dd;
    dd = dd.substr(dd.length - 2);
    mm = "0"+mm;
    mm = mm.substr(mm.length - 2);


    var NotificationExpirationDate = y + '-' + mm + '-' + dd;

  $('#NotificationExpirationDate').val(NotificationExpirationDate);


}
</script>

<script src="<?php echo base_url();?>assets/bootstrap/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url();?>assets/jquery-datatable/jquery.dataTables.js"></script>
<script src="<?php echo base_url();?>assets/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
<script src="<?php echo base_url();?>assets/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url();?>assets/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
<script src="<?php echo base_url();?>assets/jquery-datatable/extensions/export/jszip.min.js"></script>
<script src="<?php echo base_url();?>assets/jquery-datatable/extensions/export/pdfmake.min.js"></script>
<script src="<?php echo base_url();?>assets/jquery-datatable/extensions/export/vfs_fonts.js"></script>
<script src="<?php echo base_url();?>assets/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
<script src="<?php echo base_url();?>assets/jquery-datatable/extensions/export/buttons.print.min.js"></script>