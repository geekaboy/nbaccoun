<script type="text/javascript">
  $(document).ready(function(){
    //เพิ่มเงื่อนไขตาราง
    $('.js-exportable').DataTable({
        dom: 'Bfrtip',responsive: true,paging: true,info: false,
        buttons: [
            {extend:'copyHtml5',text:' <i class="fa fa-files-o fa-2x "></i> ',titleAttr: ' Copy '},
            {extend:'excelHtml5',text:' <i class="fa fa-file-excel-o fa-2x "></i> ',titleAttr: ' Excel '},
            {extend:'print',text:' <i class="fa fa-print fa-2x "></i> ',titleAttr: ' Print '}
          ,
        ]
      });

    $('.js-exportable2').DataTable({
        dom: 'Bfrtip',responsive: true,paging: true,info: false,
        buttons: [
            {extend:'copyHtml5',text:' <i class="fa fa-files-o fa-2x "></i> ',titleAttr: ' Copy '},
            {extend:'excelHtml5',text:' <i class="fa fa-file-excel-o fa-2x "></i> ',titleAttr: ' Excel '},
            {extend:'print',text:' <i class="fa fa-print fa-2x "></i> ',titleAttr: ' Print '
            // ,exportOptions:{columns:[0,1,2,3,4,5,6]}
          },
        ]
        // ,"columnDefs": [{"targets": [ 0,1,2,3,4 ],"visible": false,"searchable": true}]
      });
//กรณีต้องการปิดการแสดงบาง column แต่สามารถ export file ได้
    $('.js-exportable3').DataTable({
        dom: 'Bfrtip',responsive: true,paging: true,info: false,
        buttons: [
            {extend:'copyHtml5',text:' <i class="fa fa-files-o fa-2x "></i> ',titleAttr: ' Copy '},
            {extend:'excelHtml5',text:' <i class="fa fa-file-excel-o fa-2x "></i> ',titleAttr: ' Excel '},
            {extend:'print',text:' <i class="fa fa-print fa-2x "></i> ',titleAttr: ' Print '
            ,exportOptions:{columns:[0,1,2,3,4,5,6]}
          },
        ],
        "columnDefs": [{"targets": [ 5 ],"visible": false,"searchable": true}]
      });
  });
</script>
<!-- begin add require script -->
<head>
  <link href="<?php echo base_url();?>assets/bootstrap-select/css/bootstrap-select.css" rel="stylesheet">
  <link href="<?php echo base_url();?>assets/bootstrap/css/datepicker.css" rel="stylesheet">
  <link href="<?php echo base_url();?>assets/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">
  <meta charset="UTF-8">
  
  <style>
  @media print {
    canvas {
      min-height: 100%;max-width: 100%;max-height: 100%;height: auto!important;width: auto!important;
    }
    table {
      min-height: 100%;max-width: 100%;max-height: 100%;height: auto!important;width: auto!important;
      page-break-inside: : auto;
    }
    tr{
      page-break-inside: avoid;
      page-break-after: auto;

    }
  }
  canvas{-moz-user-select: none;-webkit-user-select: none;-ms-user-select: none;}
  </style>
</head>

<!-- end add require script -->
<div class="box box-success">
  <div class="box-header" align="left">
        <i class="fa fa-sign-in"></i>
    <h3 class="box-title"><?php echo 'View '.$this->systemmodel->get_menuname($this->uri->segment(1) . '/' . $this->uri->segment(2)); // แสดงชื่อเมนู  ?></h3>
  </div>
  <div class="box-body">
    <!-- ส่วนแสดงผล -->

<div class="box box-success">
  <div class="box-header">
    <b></b>
  </div>
  <div class="box-body" align="left" id="CompanyStructureDIV">
  </div>
</div>
</div>

<button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal" id="btn_Modal">Open Modal</button>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Select Company Division Department Section Position</h4>
      </div>
      <div class="modal-body" id="CompanyStructureDIV2">
        <p>Some text in the modal.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<script type="text/javascript">
  $('#btn_Modal').hide();
  $.ajax({
  url:'<?=base_url()?>index.php/hr/companyst',
    type: "post",
    data: {},
    beforeSend: function () {$(".loading").show();},
    complete: function () {$(".loading").hide();},
    success: function (data) {
      $('#CompanyStructureDIV').html(data);
      }
    });
function getPositionModal(){
  // alert($('#DivisionIDModal').val());
}
</script>

<script src="<?php echo base_url();?>assets/bootstrap/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url();?>assets/jquery-datatable/jquery.dataTables.js"></script>
<script src="<?php echo base_url();?>assets/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
<script src="<?php echo base_url();?>assets/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url();?>assets/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
<script src="<?php echo base_url();?>assets/jquery-datatable/extensions/export/jszip.min.js"></script>
<script src="<?php echo base_url();?>assets/jquery-datatable/extensions/export/pdfmake.min.js"></script>
<script src="<?php echo base_url();?>assets/jquery-datatable/extensions/export/vfs_fonts.js"></script>
<script src="<?php echo base_url();?>assets/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
<script src="<?php echo base_url();?>assets/jquery-datatable/extensions/export/buttons.print.min.js"></script>