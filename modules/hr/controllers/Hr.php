<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

// เปลี่ยนชื่อคลาส (Name) ให้เป็นชื่อคอนโทรลเลอร์ อักษาตัวแรกให้เป็นตัวใหญ่
class Hr extends Smart_Controller
{

    public function __construct(){
        parent::__construct();
		$this->load->helper(array('form', 'url'));
        // ตรวจสอบการเข้าสู่ระบบ ถ้ายังไม่เข้าสู่ระบบให้กลับไปที่หน้า login
        if (!isset($_SESSION['username'])) {
            redirect(base_url() . 'index.php/home/login_form');
        }
        // จบ ตรวจสอบการเข้าสู่ระบบ
        // ตรวจสอบสิทธิ์การใช้งานเมนู
        $res_permission = $this->systemmodel->get_menulink_permission($this->session->person_id);
        if (!in_array($this->uri->segment(1) . '/' . $this->uri->segment(2), $res_permission)) {
            redirect(base_url() . 'index.php/member/nopermission');
        }

        // จบ ตรวจสอบสิทธิ์การใช้งานเมนู

        $this->load->library('grocery_CRUD');
    }

    public function index(){
		$this->load->helper('url');
        // หน้าแรกที่จะแสดงเมื่อเข้าสู่โมดูล
        //$data = 'member/ready';
        
        // ส่งตัวแปรหน้าแรกที่จะแสดงผลไปแสดงผลในไฟล์แทมเพลต
        $this->load->view('templates/index', $data);
    }
    public function _example_output($output = null)
    {
        // ส่วนแสดงผลใช้กับ grocery crud
        $data['content'] = 'system/grocery_crud';
        $data['output']  = $output;
        $this->load->view('templates/index', $data);
    }
	public function getDepartment(){ 
    $postData = $this->input->post();
    $this->load->model('HrModel');
    $data = $this->HrModel->getDepartment($postData);
    echo json_encode($data); 
	}
	public function getSection(){ 
    $postData = $this->input->post();
    $this->load->model('HrModel');
    $data = $this->HrModel->getSection($postData);
    echo json_encode($data); 
	}
	public function getPosition(){ 
    $postData = $this->input->post();
    $this->load->model('HrModel');
    $data = $this->HrModel->getPosition($postData);
    echo json_encode($data); 
	}
	public function getAmphure(){ 
    $postData = $this->input->post();
    $this->load->model('HrModel');
    $data = $this->HrModel->getAmphure($postData);
    echo json_encode($data); 
	}
	public function getSubDistrict(){ 
    $postData = $this->input->post();
    $this->load->model('HrModel');
    $data = $this->HrModel->getSubDistrict($postData);
    echo json_encode($data); 
	}
	public function getPostcode(){ 
    $postData = $this->input->post();
    $this->load->model('HrModel');
    $data = $this->HrModel->getPostcode($postData);
    echo json_encode($data); 
	}
	public function getCountry(){ 
    $postData = $this->input->post();
    $this->load->model('HrModel');
    $data = $this->HrModel->getCountry($postData);
    echo json_encode($data); 
	}
    public function AddNewStaff()
    {
        $this->load->model('HrModel'); 
        $data = array(
            'content' => 'hr/AddNewStaff',
			'results_title' => 	$this->HrModel->fetch_title(),
			'results_gender' => $this->HrModel->fetch_gender(),
			'results_Nationality' => $this->HrModel->fetch_Nationality(),
			'results_Race' => $this->HrModel->fetch_Race(),
			'results_FamilyStatus' => $this->HrModel->fetch_FamilyStatus(),
            'results_Company' => $this->HrModel->getCompany(),
			'results_Division' => $this->HrModel->getDivision(),
            'results_Department' => $this->HrModel->getDataTable('Department'),
            'results_Section' => $this->HrModel->getDataTable('Section'),
            'results_Position' => $this->HrModel->getDataTable('Position'),
			'results_WorkType' => $this->HrModel->fetch_WorkType(),
			'results_EmployeeGroup' => $this->HrModel->fetch_EmployeeGroup(),
			'results_EmployeeLevel' => $this->HrModel->fetch_EmployeeLevel(),
			'results_ContractType' => $this->HrModel->fetch_ContractType(),
			'results_SalaryTypeID' => $this->HrModel->fetch_SalaryTypeID(),
			'results_PaymentMethod' => $this->HrModel->fetch_PaymentMethod(),
			'results_PITCode' => $this->HrModel->fetch_PITCode(),
            'results_Employee' => $this->HrModel->getEmployeeByID(0),
			'results_SSOCode' => $this->HrModel->fetch_SSOCode(),
            'results_Province' => $this->HrModel->getProvince(),
            //ทำไมไม่มี Districe, subdistric, zipcode บัญชา จึงเพิ่มเอง
           // 'results_Amphure' => $this->HrModel->getAmphure(),
          //  'results_SubDistrict' => $this->HrModel->getSubDistrict(),
           // 'results_Zipcode' => $this->HrModel->getZipcode(),
             // บัญชา เพิ่มมาถึงตรงนี้นะครับ
			'results_Religion' => $this->HrModel->getReligion()
            );
        $this->load->view('templates/index', $data);
    }
	public function SaveAddNewStaff()
	{
		$data = array(
			'CardID' => $this->input->post('CardID'),
			'EmployeeCode' => $this->input->post('EmployeeCode'),
			'SSOID' => $this->input->post('SSOID'),
			'Birthday' => $this->input->post('Birthday'),
			'IssueDate' => $this->input->post('IssueDate'),
			'IssuePlace' => $this->input->post('IssuePlace'),
			'ExpiringDate' => $this->input->post('ExpiringDate'),
			'TitleID' => $this->input->post('TitleID'),
			'FirstName' => $this->input->post('FirstName'),
			'MiddleName' => $this->input->post('MiddleName'),
			'LastName' => $this->input->post('LastName'),
			'FullNameLL' => $this->input->post('FullNameLL'),
			'GenderID' => $this->input->post('GenderID'),
			'NationalityID' => $this->input->post('NationalityID'),
			'RaceID' => $this->input->post('RaceID'),
			'FamilyStatusID' => $this->input->post('FamilyStatusID'),
            'CompID' => $this->input->post('CompID'),
			'DivisionID' => $this->input->post('DivisionID'),
			'DepartmentID' => $this->input->post('DepartmentID'),
			'SectionID' => $this->input->post('SectionID'),
			'PositionID' => $this->input->post('PositionID'),
			'EmployeeGroup' => $this->input->post('EmployeeGroup'),
			'EmployeeLevel' => $this->input->post('EmployeeLevel'),
			'ServiceStartDate' => $this->input->post('ServiceStartDate'),
			'WorkingType' => $this->input->post('WorkingType'),
			'HourperDay' => $this->input->post('HourperDay'),
			'ContractType' => $this->input->post('ContractType'),
			'ContractStartDate' => $this->input->post('ContractStartDate'),
			'ContractEndDate' => $this->input->post('ContractEndDate'),
			'ProbationEndDate' => $this->input->post('ProbationEndDate'),
			'SalaryTypeID' => $this->input->post('SalaryTypeID'),
			'MonthlySalary' => $this->input->post('MonthlySalary'),
			'MonthlyAllowance' => $this->input->post('MonthlyAllowance'),
			'PaymentMethod' => $this->input->post('PaymentMethod'),
			'PITCode' => $this->input->post('PITCode'),
			'SocialID' => $this->input->post('SocialID'),
			'AddressPermanent' => $this->input->post('AddressPermanent'),
			'ProvinceID' => $this->input->post('ProvinceID'),
			'DistrictID' => $this->input->post('DistrictID'),
			'SubDistrictID' => $this->input->post('SubDistrictID'),
			'Postcode' => $this->input->post('Postcode'),
			'CountryID' => $this->input->post('CountryID'),
			'AddressResidence' => $this->input->post('AddressResidence'),
			'ProvinceRID' => $this->input->post('ProvinceRID'),
			'DistrictRID' => $this->input->post('DistrictRID'),
			'SubDistrictRID' => $this->input->post('SubDistrictRID'),
			'PostcodeR' => $this->input->post('PostcodeR'),
            'CountryRID' => $this->input->post('CountryRID'),
            'ReligionID' => $this->input->post('ReligionID'),
            'CongenitalDisease' => $this->input->post('CongenitalDisease'),
			'person_id' => $this->session->person_id
			);
		$this->db->insert('Employee', $data);
        $EmployeeID = $this->db->insert_id();
        
        // echo $this->db->last_query()."11111";

        $this->load->model('HrModel'); 
        $MovementTypeID = $this->HrModel->getTypeNewEmployee();
        //bancha said ตรงนี้น่าจะเป็นการนำข้อมูลพนักงานเข้าใหม่ไปบันทึกใน employee movement เพื่อนำไปแสดงสถานะของพนักงานว่ามีการเปลี่ยนปลงไปอย่างไร แต่ที่ดูไม่ได้ update ว่าเป็นพนักงานใหม่
        $data2 = array(
            'EmployeeID' => $EmployeeID
            ,'MovementTypeID'=>$MovementTypeID
            ,'MMDate'=>$this->input->post('ServiceStartDate')
            ,'Salary'=>$this->input->post('MonthlySalary')
            ,'PITCodeTypeID'=>$this->input->post('PITCode'),
            'CompID' => $this->input->post('CompID'),
            'DivisionID' => $this->input->post('DivisionID'),
            'DepartmentID' => $this->input->post('DepartmentID'),
            'SectionID' => $this->input->post('SectionID'),
            'PositionID' => $this->input->post('PositionID')
            
        );
        $this->db->insert('EmployeeMovement', $data2);
        // echo $this->db->last_query()."222222";


		redirect('hr/PerInformation', 'refresh');
	}
	public function updateAddNewStaff(){
        $this->load->model('HrModel'); 
        $data = array(
			'CardID' => $this->input->post('CardID'),
			'EmployeeCode' => $this->input->post('EmployeeCode'),
			'SSOID' => $this->input->post('SSOID'),
			'Birthday' => $this->input->post('Birthday'),
			'IssueDate' => $this->input->post('IssueDate'),
			'IssuePlace' => $this->input->post('IssuePlace'),
			'ExpiringDate' => $this->input->post('ExpiringDate'),
			'TitleID' => $this->input->post('TitleID'),
			'FirstName' => $this->input->post('FirstName'),
			'MiddleName' => $this->input->post('MiddleName'),
			'LastName' => $this->input->post('LastName'),
			'FullNameLL' => $this->input->post('FullNameLL'),
			'GenderID' => $this->input->post('GenderID'),
			'NationalityID' => $this->input->post('NationalityID'),
			'RaceID' => $this->input->post('RaceID'),
			'FamilyStatusID' => $this->input->post('FamilyStatusID'),
            'CompID' => $this->input->post('CompID'),
			'DivisionID' => $this->input->post('DivisionID'),
			'DepartmentID' => $this->input->post('DepartmentID'),
			'SectionID' => $this->input->post('SectionID'),
			'PositionID' => $this->input->post('PositionID'),
			'EmployeeGroup' => $this->input->post('EmployeeGroup'),
			'EmployeeLevel' => $this->input->post('EmployeeLevel'),
			'ServiceStartDate' => $this->input->post('ServiceStartDate'),
			'WorkingType' => $this->input->post('WorkingType'),
			'HourperDay' => $this->input->post('HourperDay'),
			'ContractType' => $this->input->post('ContractType'),
			'ContractStartDate' => $this->input->post('ContractStartDate'),
			'ContractEndDate' => $this->input->post('ContractEndDate'),
			'ProbationEndDate' => $this->input->post('ProbationEndDate'),
			'SalaryTypeID' => $this->input->post('SalaryTypeID'),
			'MonthlySalary' => $this->input->post('MonthlySalary'),
			'MonthlyAllowance' => $this->input->post('MonthlyAllowance'),
			'PaymentMethod' => $this->input->post('PaymentMethod'),
			'PITCode' => $this->input->post('PITCode'),
			'SocialID' => $this->input->post('SocialID'),
			'AddressPermanent' => $this->input->post('AddressPermanent'),
			'ProvinceID' => $this->input->post('ProvinceID'),
			'DistrictID' => $this->input->post('DistrictID'),
			'SubDistrictID' => $this->input->post('SubDistrictID'),
			'Postcode' => $this->input->post('Postcode'),
			'CountryID' => $this->input->post('CountryID'),
			'AddressResidence' => $this->input->post('AddressResidence'),
			'ProvinceRID' => $this->input->post('ProvinceRID'),
			'DistrictRID' => $this->input->post('DistrictRID'),
			'SubDistrictRID' => $this->input->post('SubDistrictRID'),
			'PostcodeR' => $this->input->post('PostcodeR'),
			'CountryRID' => $this->input->post('CountryRID'),
            'ReligionID' => $this->input->post('ReligionID'),
            'CongenitalDisease' => $this->input->post('CongenitalDisease')
			);
		$this->db->where('EmployeeID', $this->input->post('EmployeeID'));
		$this->db->update('Employee', $data); 

        $MovementTypeID = $this->HrModel->getTypeNewEmployee();
        
        $data2 = array(
            'MMDate' => $this->input->post('ServiceStartDate'),
            'Salary' => $this->input->post('MonthlySalary'),
            'PITCodeTypeID' => $this->input->post('PITCode'),
            
            );
        $this->db->where('EmployeeID', $this->input->post('EmployeeID'));
        $this->db->where('MovementTypeID', $MovementTypeID);
        $this->db->update('EmployeeMovement', $data2); 

        $EmployeeID =  $this->input->post('EmployeeID');
        $sql = "select EmployeeID from EmployeeMovement where EmployeeID = ".$EmployeeID;
        $query=$this->db->query($sql);
        $sql2 = "select EmployeeID from EmployeeMovement where MovementTypeID=0 and EmployeeID = ".$EmployeeID;
        $query2=$this->db->query($sql2);
        if ($query->num_rows() == 0 || $query2->num_rows()==1) {
            $data2 = array(
                'EmployeeID' => $EmployeeID
                ,'MovementTypeID'=>$MovementTypeID
                ,'MMDate'=>$this->input->post('ServiceStartDate')
                ,'Salary'=>$this->input->post('MonthlySalary')
                ,'PITCodeTypeID'=>$this->input->post('PITCode')
               
            );
            $this->db->insert('EmployeeMovement', $data2);
        }

        // echo $this->db->last_query();
		redirect('hr/PerInformation', 'refresh');
    }
    public function EditAddNewStaff()
    {
        $id = intval($this->uri->segment(4));
        $this->load->model('HrModel'); 
        // print_r($this->HrModel->getEmployeeByID($id));
        // exit();
        $data = array(
            'content' => 'hr/EditAddNewStaff',
			'results_title' => 	$this->HrModel->fetch_title(),
			'results_gender' => $this->HrModel->fetch_gender(),
			'results_Nationality' => $this->HrModel->fetch_Nationality(),
			'results_Race' => $this->HrModel->fetch_Race(),
			'results_FamilyStatus' => $this->HrModel->fetch_FamilyStatus(),
            'results_Company' => $this->HrModel->getCompany(),
			'results_Division' => $this->HrModel->getDivision(),
            'results_Department' => $this->HrModel->getDataTable('Department'),
            'results_Section' => $this->HrModel->getDataTable('Section'),
            'results_Position' => $this->HrModel->getDataTable('Position'),
			'results_WorkType' => $this->HrModel->fetch_WorkType(),
			'results_EmployeeGroup' => $this->HrModel->fetch_EmployeeGroup(),
			'results_EmployeeLevel' => $this->HrModel->fetch_EmployeeLevel(),
			'results_ContractType' => $this->HrModel->fetch_ContractType(),
			'results_SalaryTypeID' => $this->HrModel->fetch_SalaryTypeID(),
			'results_PaymentMethod' => $this->HrModel->fetch_PaymentMethod(),
			'results_PITCode' => $this->HrModel->fetch_PITCode(),
            'results_SSOCode' => $this->HrModel->fetch_SSOCode(),
			'results_Province' => $this->HrModel->getProvince(),
            // ทำไมไม่มีการอ้างถึง distric, subdistric and zipcode บัญชาเลยใส่เพิ่ม
            //'results_Amphure' => $this->HrModel->getAmphure(),
            //'results_Subdistrict' => $this->HrModel->getSubDistrict(),
            //'results_Zipcode' => $this->HrModel->getZipcode(),
            
            // บัญชา เพิ่มมาถึงตรงนี้นะครับ

            'id' => $id,

            'results_Employee' => $this->HrModel->getEmployeeByID($id),
            'results_Religion' => $this->HrModel->getReligion()
            );
        $this->load->view('templates/index', $data);
    }
	public function AddOtherInformation()
    {
        $id = intval($this->uri->segment(4));
        $this->load->model('HrModel'); 
        $data = array(
            'content' => 'hr/AddOtherInformation',
            'id' => $id,
            'results_Bank' => $this->HrModel->getBank(),
            'results_Qualification' => $this->HrModel->getQualification(),
            'results_Major' => $this->HrModel->getMajor(),
            'results_Skills' => $this->HrModel->getSkills(),
            'results_Relationship' => $this->HrModel->getRelationship(),
            'results_Employee' => $this->HrModel->getEmployeeByID($id),
            'results_EmployeeQualification' => $this->HrModel->getEmployeeQualification($id),
            'results_EmployeeSkills' => $this->HrModel->getEmployeeSkills($id),
			'results_EmployeeFMRelation' => $this->HrModel->getEmployeeFMRelation($id),
            );
        $this->load->view('templates/index', $data);
    }
    public function updateotherinformation(){
        
        $sql = "UPDATE Employee set 

        EmployeePhoto = '".$this->input->post("EmployeePhoto")."',
        CellPhone = '".$this->input->post("CellPhone")."',
        BloodGroup = '".$this->input->post("BloodGroup")."',
        Email = '".$this->input->post("Email")."',
        LineID = '".$this->input->post("LineID")."',

        EffectiveDate = '".$this->input->post("EffectiveDate")."',
        Bank = '".$this->input->post("Bank")."',
        BankAccountName = '".$this->input->post("BankAccountName")."',
        BankAccountNo = '".$this->input->post("BankAccountNo")."',
        BankBranch = '".$this->input->post("BankBranch")."',
        PrimaryBank = '".$this->input->post('PrimaryBank')."',
        Activate = '".$this->input->post('Activate')."',

        
        Doc1 = '".$this->input->post("Doc1")."',
        Doc2 = '".$this->input->post("Doc2")."',
        Doc3 = '".$this->input->post("Doc3")."',

        PFC_Personnel = '".$this->input->post("PFC_Personnel")."',
        PFC_Spouse = '".$this->input->post("PFC_Spouse")."',
        LIP_Personnel = '".$this->input->post("LIP_Personnel")."',
        LIP_Spouse = '".$this->input->post("LIP_Spouse")."',
        RMF_Personnel = '".$this->input->post("RMF_Personnel")."',
        RMF_Spouse = '".$this->input->post("RMF_Spouse")."',
        LTQF_Personnel = '".$this->input->post("LTQF_Personnel")."',
        LTQF_Spouse = '".$this->input->post("LTQF_Spouse")."',
        CC_Personnel = '".$this->input->post("CC_Personnel")."',
        CTE_Personnel = '".$this->input->post("CTE_Personnel")."',
        CTE_Spouse = '".$this->input->post("CTE_Spouse")."',
        HIM_Personnel = '".$this->input->post("HIM_Personnel")."',
        HIM_Spouse = '".$this->input->post("HIM_Spouse")."',
        HIF_Personnel = '".$this->input->post("HIF_Personnel")."',
        HIF_Spouse = '".$this->input->post("HIF_Spouse")."',
        FAP_Farther = '".$this->input->post("FAP_Farther")."',
        FAP_Mother = '".$this->input->post("FAP_Mother")."',
        FAS_Farther = '".$this->input->post("FAS_Farther")."',
        FAS_Mother = '".$this->input->post("FAS_Mother")."',

        PFCode = '".$this->input->post("PFCode")."',
        PFName = '".$this->input->post("PFName")."',
        JoinedDate = '".$this->input->post("JoinedDate")."',
        ResignedDate = '".$this->input->post("ResignedDate")."',
        EmployeeContributionPercentage = '".$this->input->post("EmployeeContributionPercentage")."',
        CompaneeContribtutionPercentage = '".$this->input->post("CompaneeContribtutionPercentage")."'

        where EmployeeID=".$this->input->post("EmployeeID");
        // echo $sql;
        $this->db->query($sql);
        $sql = "delete from EmployeeQualification where EmployeeID=".$this->input->post("EmployeeID");
        $this->db->query($sql);

        $sql = "insert into EmployeeQualification(EmployeeID,QualificationID,MajorID,Institution,GraduateYear) 
                values
                (".$this->input->post("EmployeeID").",".$this->input->post("QualificationID1").",
                ".$this->input->post("MajorID1").",'".$this->input->post("Institution1")."'
                ,'".$this->input->post("GraduateYear1")."')
                ,(".$this->input->post("EmployeeID").",".$this->input->post("QualificationID2").",
                ".$this->input->post("MajorID2").",'".$this->input->post("Institution2")."'
                ,'".$this->input->post("GraduateYear2")."')
                ,(".$this->input->post("EmployeeID").",".$this->input->post("QualificationID3").",
                ".$this->input->post("MajorID3").",'".$this->input->post("Institution3")."'
                ,'".$this->input->post("GraduateYear3")."'),
                (".$this->input->post("EmployeeID").",".$this->input->post("QualificationID4").",
                ".$this->input->post("MajorID4").",'".$this->input->post("Institution4")."',
                '".$this->input->post("GraduateYear4")."'),
                (".$this->input->post("EmployeeID").",".$this->input->post("QualificationID5").",
                ".$this->input->post("MajorID5").",'".$this->input->post("Institution5")."',
                '".$this->input->post("GraduateYear5")."')
                ";
        $this->db->query($sql);


        $this->db->query($sql);
        $sql = "delete from EmployeeSkills where EmployeeID=".$this->input->post("EmployeeID");
        $this->db->query($sql);
        
       echo  $sql = "insert into EmployeeSkills(EmployeeID,SkillID,License,Remarks) 
                values
                (".$this->input->post("EmployeeID").",".$this->input->post("SkillID1").",
                '".$this->input->post("License1")."','".$this->input->post("Remarks1")."'),
                (".$this->input->post("EmployeeID").",".$this->input->post("SkillID2").",
                '".$this->input->post("License2")."','".$this->input->post("Remarks2")."'),
                (".$this->input->post("EmployeeID").",".$this->input->post("SkillID3").",
                '".$this->input->post("License3")."','".$this->input->post("Remarks3")."'),
                (".$this->input->post("EmployeeID").",".$this->input->post("SkillID4").",
                '".$this->input->post("License4")."','".$this->input->post("Remarks4")."'),
                (".$this->input->post("EmployeeID").",".$this->input->post("SkillID5").",
                '".$this->input->post("License5")."','".$this->input->post("Remarks5")."'),
                (".$this->input->post("EmployeeID").",".$this->input->post("SkillID6").",
                '".$this->input->post("License6")."','".$this->input->post("Remarks6")."'),
                (".$this->input->post("EmployeeID").",".$this->input->post("SkillID7").",
                '".$this->input->post("License7")."','".$this->input->post("Remarks7")."'),
                (".$this->input->post("EmployeeID").",".$this->input->post("SkillID8").",
                '".$this->input->post("License8")."','".$this->input->post("Remarks8")."'),
                (".$this->input->post("EmployeeID").",".$this->input->post("SkillID9").",
                '".$this->input->post("License9")."','".$this->input->post("Remarks9")."'),
                (".$this->input->post("EmployeeID").",".$this->input->post("SkillID10").",
                '".$this->input->post("License10")."','".$this->input->post("Remarks10")."')
                ";
        $this->db->query($sql);

        $sql = "delete from EmployeeFMRelation where EmployeeID=".$this->input->post("EmployeeID");
        $this->db->query($sql);
        
       echo  $sql = "insert into EmployeeFMRelation(EmployeeID,FMRelationID,fullname,phonenumber,address) 
                values
                (".$this->input->post("EmployeeID").",".$this->input->post("FMRelationID1").",
                '".$this->input->post("fullname1")."','".$this->input->post("phonenumber1")."','".$this->input->post("Address1")."'),
                (".$this->input->post("EmployeeID").",".$this->input->post("FMRelationID2").",
                '".$this->input->post("fullname2")."','".$this->input->post("phonenumber2")."','".$this->input->post("Address2")."'),
                (".$this->input->post("EmployeeID").",".$this->input->post("FMRelationID3").",
                '".$this->input->post("fullname3")."','".$this->input->post("phonenumber3")."','".$this->input->post("Address3")."'),
                (".$this->input->post("EmployeeID").",".$this->input->post("FMRelationID4").",
                '".$this->input->post("fullname4")."','".$this->input->post("phonenumber4")."','".$this->input->post("Address4")."'),
                (".$this->input->post("EmployeeID").",".$this->input->post("FMRelationID5").",
                '".$this->input->post("fullname5")."','".$this->input->post("phonenumber6")."','".$this->input->post("Address5")."')
                ";
        $this->db->query($sql);
    }
	
    public function PerInformation()
    {
        $this->load->model('HrModel'); 
        $data = array(
			'results_ResignationTypeOut' => 	$this->HrModel->fetch_ResignationTypeOut(),
			'results_ResignationReason' => 	$this->HrModel->fetch_ResignationReason(),
            'content' => 'hr/PersonnelInformation',
            'results_All' => $this->HrModel->getEmployeeAll(),
            );
        $this->load->view('templates/index', $data);
    }
	public function Resignation()
    {
        $this->load->model('HrModel'); 
        $data = array(
            'content' => 'hr/Resignation',
			'results_ResignationTypeOut' => 	$this->HrModel->fetch_ResignationTypeOut(),
			'results_ResignationReason' => 	$this->HrModel->fetch_ResignationReason(),
            'results_All' => $this->HrModel->getEmployeeAllResignation(),
            );
        $this->load->view('templates/index', $data);
    }
	public function updateResignation(){
        
        $data = array(
		'ResignationTypeOut' => $this->input->post('ResignationTypeOut'),
		'NotificationDaysRequired' => $this->input->post('NotificationDaysRequired'),
		'ResignationReason' => $this->input->post('ResignationReason'),
		'NotificationDate' => $this->input->post('NotificationDate'),
		'NotificationExpirationDate' => $this->input->post('NotificationExpirationDate'),
		'LastworkingDay' => $this->input->post('LastworkingDay'),
		'LeavingDate' => $this->input->post('LeavingDate'),
		'EffectiveDateOut' => $this->input->post('EffectiveDateOut'),
		'Remark' => $this->input->post('Remark'),
        'ResignationTypeStatusOut' => '1',
		'EmployeeType' => 'Resign'
		);
		$this->db->where('EmployeeID', $this->input->post('EmployeeID'));
		$this->db->update('Employee', $data); 
		redirect('hr/Resignation', 'refresh');
    }
    public function OtherInformation(){
        $crud = new grocery_CRUD();
        $crud->set_table('Employee')
        ->set_subject('Employee');

        $crud->fields(
        'FirstName','LastName','CellPhone','BloodGroup','Email', 'LineID', 'EmployeePhoto');

        // hidden field
        $crud->set_field_upload('EmployeePhoto', 'assets/uploads/company/'.$this->session->CompID, 'jpeg|jpg|png');
        $crud->field_type('FirstName', 'readonly');
        $crud->field_type('LastName', 'readonly');
        $crud->unset_add();
        $crud->unset_delete();
        $crud->unset_back_to_list();
        
        $output = $crud->render();
        $this->_example_output($output);
    }
    public function EmployeeBankInformation(){
        $crud = new grocery_CRUD();
        $crud->set_table('Employee')
        ->set_subject('Employee')
        ->columns(
        'EffectiveDate', 'Bank', 'BankAccountName', 'BankAccountNo', 'BankBranch', 'PrimaryBank', 'Activate');

        $crud->fields(
        'FirstName','LastName','EffectiveDate', 'Bank', 'BankAccountName', 'BankAccountNo', 'BankBranch', 'PrimaryBank', 'Activate');

        // hidden field
        $crud->field_type('FirstName', 'readonly');
        $crud->field_type('LastName', 'readonly');
        $crud->unset_add();
        $crud->unset_delete();
        $crud->unset_back_to_list();
        
        $output = $crud->render();
        $this->_example_output($output);
    }
    public function DocumentStore(){
        $crud = new grocery_CRUD();
        $crud->set_table('Employee')
        ->set_subject('Employee');

        $crud->fields(
        'FirstName','LastName','Doc1','Doc2','Doc3');

        // hidden field
        $crud->set_field_upload('Doc1', 'assets/uploads/company/'.$this->session->CompID, 'jpeg|jpg|png|pdf');
        $crud->set_field_upload('Doc2', 'assets/uploads/company/'.$this->session->CompID, 'jpeg|jpg|png|pdf');
        $crud->set_field_upload('Doc3', 'assets/uploads/company/'.$this->session->CompID, 'jpeg|jpg|png|pdf');
        $crud->field_type('FirstName', 'readonly');
        $crud->field_type('LastName', 'readonly');
        $crud->unset_add();
        $crud->unset_delete();
        $crud->unset_back_to_list();
        
        $output = $crud->render();
        $this->_example_output($output);
    }
    public function PITAllowance(){
        $crud = new grocery_CRUD();
        $crud->set_table('Employee')
        ->set_subject('Employee');

        $crud->fields(
        'FirstName','LastName'
        ,'ProvidentFundContribution'
        ,'LifeInsurancePremium'
        ,'RetirementMutualFund'
        , 'LongTermEquityFund'
        , 'CharitibleContribution'
        ,'ContributionToEducation'
        ,'HealthInsuranceMother'
        ,'HealthInsuranceFarther'
        ,'FamilyAllowancePersonnel'
        ,'FamilyAllowanceSpouse'
    );

        // hidden field
        $crud->field_type('FirstName', 'readonly');
        $crud->field_type('LastName', 'readonly');
        $crud->set_relation('ProvidentFundContribution','PIT_PFC','PFCName');
        $crud->set_relation('LifeInsurancePremium','PIT_LIP','LIPName');
        $crud->set_relation('RetirementMutualFund','PIT_RMF','RMFName');
        $crud->set_relation('LongTermEquityFund','PIT_LTEF','LTEFName');
        $crud->set_relation('CharitibleContribution','PIT_CC','CCName');
        $crud->set_relation('ContributionToEducation','PIT_CTE','CTEName');
        $crud->set_relation('HealthInsuranceMother','PIT_HIM','HIMName');
        $crud->set_relation('HealthInsuranceFarther','PIT_HIF','HIFName');
        $crud->set_relation('FamilyAllowancePersonnel','PIT_FAP','FAPName');
        $crud->set_relation('FamilyAllowanceSpouse','PIT_FAS','FASName');
        $crud->unset_add();
        $crud->unset_delete();
        $crud->unset_back_to_list();
        
        $output = $crud->render();
        $this->_example_output($output);
    }
    public function ProvidentFund(){
        $crud = new grocery_CRUD();
        $crud->set_table('Employee')
        ->set_subject('Employee')
        ->display_as('PFCode','Provident Fund Code')
        ->display_as('PFName','Provident Fund Name');

        $crud->fields(
        'FirstName','LastName','PFCode','PFName','JoinedDate', 'ResignedDate', 'EmployeeContributionPercentage','CompaneeContribtutionPercentage');

        // hidden field
        $crud->field_type('FirstName', 'readonly');
        $crud->field_type('LastName', 'readonly');
        $crud->unset_add();
        $crud->unset_delete();
        $crud->unset_back_to_list();
        
        $output = $crud->render();
        $this->_example_output($output);
    }

    public function Add_Transaction(){
        $crud = new grocery_CRUD();
        $crud->set_table('EmployeeMovement')
        ->set_subject('EmployeeMovement');

        $crud->fields(
        //'MovementTypeID','WoringType','HourPerDay','Remark','EmployeeLevel','EmployeeCategory', 'EmployeeGroup', 'PITCode','SalaryType','Salary','Allowance','PaymentMethod','GrossSalary','ChangePosition','ChangeSalary','ChangeContract','EmployeeID'); ตรงนี้เป็นของเดิม ทดลองทำด้านล่าง โดยมีการเปลี่ยนชื่อ ฟิลด์ในตาราง movement เพื่อให้ระบบทำการ update ข้อมูลเกี่ยวกับบริษัท เข้าไปด้วย
        

        //bancha said this area should add field "ChangeCompany"

         'EmployeeID','WorktypeID','ContractID','Remark','EmployeeID','EmployeeCategory','EmployeeGroupID','PITCodeTypeID','SocialID','SalaryTypeID','Salary','Allowance','PaymentMethodTypeID','CompID','Division','DepartmentID','SectionID','MovementTypeID','MMdate','EmployeeID');





        // hidden field
        $crud->callback_add_field('EmployeeID', function () {
            return '<input type="hidden" maxlength="50" value="'.$this->uri->segment(4).'" name="EmployeeID" id="EmployeeID">';
        });


        $crud->field_type('EmployeeID','hidden');
        
        // $crud->unset_add();
        $crud->unset_delete();
        $crud->unset_back_to_list();
        
        $output = $crud->render();
        $this->_example_output($output);
    }



    public function form_employee_movement(){
        $this->load->model('HrModel'); 
        $EmployeeID = intval($this->uri->segment(4));
        $data = array(
            'content' => 'hr/form_employee_movement',
            'DataSearch' => $this->HrModel->getEmployeeByID($EmployeeID),
            'DataSearch2' => $this->HrModel->getEmployeeMovementByID($EmployeeID),
            );
        $this->load->view('templates/index', $data);
    }
    public function view_employee_movement(){
        $EmployeeID = $this->input->post("EmployeeID");
        $this->load->model('HrModel'); 
        $data = array(
            'content' => 'hr/view_employee_movement',
            'DataSearch' => $this->HrModel->getEmployeeByID($EmployeeID),
            'DataSearch2' => $this->HrModel->getEmployeeMovementByID($EmployeeID),
        );
        $this->load->view('hr/view_employee_movement', $data);
    }
    //บริเวณด่านล่างนี้น่าจะเป็นบริเวณที่สามารถเลือกตำแหน่งและแผนกได้ แต่ไม่รู้ตรงไหน
    public function AddEmployeeMovement(){
        $this->load->model('HrModel'); 
        $data = array(
            'content' => 'hr/AddEmployeeMovement',
            'ModeDB' => $this->input->post("ModeDB"),
            'EmployeeID' => $this->input->post("EmployeeID"),
            'EmployeeMID' => $this->input->post("EmployeeMID"),
            'results_EmployeeMovement' => $this->HrModel->getEmployeeMovementByMID($this->input->post("EmployeeMID")),
            'results_WorkType' => $this->HrModel->fetch_WorkType(),
            'results_ContractType' => $this->HrModel->fetch_ContractType(),
            'results_MovementType' => $this->HrModel->fetch_MovementType(),
            'results_SalaryTypeID' => $this->HrModel->fetch_SalaryTypeID(),
            'results_EmployeeGroup' => $this->HrModel->fetch_EmployeeGroup(),
            'results_EmployeeLevel' => $this->HrModel->fetch_EmployeeLevel(),
            'results_PaymentMethod' => $this->HrModel->fetch_PaymentMethod(),
            'results_PITCode' => $this->HrModel->fetch_PITCode(),
            'results_SSOCode' => $this->HrModel->fetch_SSOCode(),
            'results_Company' => $this->HrModel->getCompany(),
            'results_Division' => $this->HrModel->getDivision(),
            'results_Department' => $this->HrModel->getDataTable('Department'),
            'results_Section' => $this->HrModel->getDataTable('Section'),
            'results_Position' => $this->HrModel->getDataTable('Position'),



        );
        $this->load->view('hr/AddEmployeeMovement', $data);
    }
    public function AddEmployeeMovementDB(){
        $this->load->model('HrModel'); 
        $ModeDB = $this->input->post("ModeDB");
        $EmployeeID = $this->input->post("EmployeeID");
        $EmployeeMID = $this->input->post("EmployeeMID");
       $data = array(
        'EmployeeID' => $EmployeeID,
        'MMDate' => $this->input->post('MMDate'),
        'MovementTypeID' => $this->input->post('MovementTypeID'),
        //'WorkTypeID' => $this->input->post('WorkTypeID'),
        'ContractType' => $this->input->post('ContractType'),
        //'HourperDay' => $this->input->post('HourperDay'),
        'EmpGroupID' => $this->input->post('EmpGroupID'),
        'EmpLevelID' => $this->input->post('EmpLevelID'),
        'PITCodeTypeID' => $this->input->post('PITCodeTypeID'),
        'SocialID' => $this->input->post('SocialID'),
        'Remark' => $this->input->post('Remark'),
        //'SalaryTypeID' => $this->input->post('SalaryTypeID'),
        'Salary' => $this->input->post('Salary'),
        'Allowance' => $this->input->post('Allowance'),
        //'PaymentMethodTypeID' => $this->input->post('PaymentMethodTypeID'),
        'CompID' => $this->input->post('CompID'),
        'DivisionID' => $this->input->post('DivisionID'),
        'DepartmentID' => $this->input->post('DepartmentID'),
        'SectionID' => $this->input->post('SectionID'),
        'PositionID' => $this->input->post('PositionID'),
        //'GrossSalary' => $this->input->post('GrossSalary'),
        //'ChangePosition' => $this->input->post('ChangePosition'),
        //'ChangeSalary' => $this->input->post('ChangeSalary'),
        //'ChangeContract' => $this->input->post('ChangeContract')
            );
        if($ModeDB=="Add") $this->db->insert('EmployeeMovement', $data);
        else{
            $this->db->where('EmployeeMID', $EmployeeMID);
            $this->db->update('EmployeeMovement', $data);
        }
        $MovementTypeNameEn = $this->HrModel->getTypeNewEmployeeTXT($this->input->post('MovementTypeID'));
        $data = array( 'EmployeeType' => $MovementTypeNameEn );
        $this->db->where('EmployeeID', $EmployeeID);
        $this->db->update('Employee', $data);
         echo $this->db->last_query();
    }
    public function CompanyStructure(){
        $this->load->model('HrModel'); 
        $data = array(
            'content' => 'hr/CompanyStructure',
            'results_Company' => $this->HrModel->getCompany(),
            'results_Division' => $this->HrModel->getDivision(),
        );        
        $this->load->view('templates/index', $data);
    }

    public function companyst()
    {
        $this->load->model('HrModel'); 
        $data = array(
            'content' => 'hr/companyst',
            'results_Company' => $this->HrModel->getCompany(),
            'results_Division' => $this->HrModel->getDivision(),
            );
        $this->load->view('hr/companyst', $data);
    }

}
