<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

// เปลี่ยนชื่อคลาส (Name) ให้เป็นชื่อคอนโทรลเลอร์ อักษาตัวแรกให้เป็นตัวใหญ่
class Hrreport extends Smart_Controller
{

    public function __construct(){
        parent::__construct();
        // ตรวจสอบการเข้าสู่ระบบ ถ้ายังไม่เข้าสู่ระบบให้กลับไปที่หน้า login
        if (!isset($_SESSION['username'])) {
            redirect(base_url() . 'index.php/home/login_form');
        }
        // จบ ตรวจสอบการเข้าสู่ระบบ
        // ตรวจสอบสิทธิ์การใช้งานเมนู
        $res_permission = $this->systemmodel->get_menulink_permission($this->session->person_id);
        if (!in_array($this->uri->segment(1) . '/' . $this->uri->segment(2), $res_permission)) {
            redirect(base_url() . 'index.php/member/nopermission');
        }

        // จบ ตรวจสอบสิทธิ์การใช้งานเมนู

        $this->load->library('grocery_CRUD');
    }

    public function index(){
        // หน้าแรกที่จะแสดงเมื่อเข้าสู่โมดูล
        $data = 'member/ready';
        // ส่งัวแปรหน้าแรกที่จะแสดงผลไปแสดงผลในไฟล์แทมเพลต
        $this->load->view('templates/index', $data);
    }
    public function form_historical_info_ep(){
        $this->load->model('HrreportModel');
        $data = array(
            'content' => 'hrreport/form_historical_info_ep',
            //'department' => $this->HrreportModel->sel_department($this->session->company_id),
        );
        $this->load->view('templates/index', $data);
    }
    public function view_historical_info_ep(){
        $this->load->model('HrreportModel');
        $positionid = $this->input->post('search1');
        $showyear = $this->input->post('showyear');
        $data = array(
            'content' => 'hrreport/view_historical_info_ep',
            'showyear'=>$showyear,
            'positionid' => $positionid,
            'showlabel' => $this->HrreportModel->show_school(),
            'all_project' => $this->HrreportModel->show_school(),
        );
        $this->load->view('hrreport/view_historical_info_ep', $data);
    }

    public function form_ep_info(){
        $this->load->model('HrreportModel');
        $dateset=$this->input->post('dateset');
        $dateset2=$this->input->post('dateset2');
        $levelreport=$this->input->post('levelreport');
        $empgroup=$this->input->post('empgroup');
        $gender=$this->input->post('gender');
        $nationality=$this->input->post('nationality');
        $worktype=$this->input->post('worktype');

        if(empty($dateset)) $dateset=date("Y-m-d");
        if(empty($dateset2)) $dateset2=date("Y-m-d");
        if(empty($levelreport)) $levelreport=0;
        if(empty($empgroup)) $empgroup="All";
        if(empty($gender)) $gender=0;
        if(empty($nationality)) $nationality=0;
        if(empty($worktype)) $worktype=0;

        $data = array(
            'content' => 'hrreport/form_ep_info',
            'getGender' => $this->HrreportModel->getGender(),
            'getNationality' => $this->HrreportModel->getNationality(),
            'getWorkType' => $this->HrreportModel->getWorkType(),
            'dateset' => $dateset,
            'dateset2' => $dateset2,
            'levelreport' => $levelreport,
            'empgroup' => $empgroup,
            'gender' => $gender,
            'nationality' => $nationality,
            'worktype' => $worktype,
        );
        $this->load->view('templates/index', $data);
    }
    public function form_ep_contract(){
        $this->load->model('HrreportModel');
        $dateset=$this->input->post('dateset');
        $dateset2=$this->input->post('dateset2');
        $levelreport=$this->input->post('levelreport');
        $empgroup=$this->input->post('empgroup');
        $gender=$this->input->post('gender');
        $nationality=$this->input->post('nationality');
        $worktype=$this->input->post('worktype');

        if(empty($dateset)) $dateset=date("Y-m-d");
        if(empty($dateset2)) $dateset2=date("Y-m-d");
        if(empty($levelreport)) $levelreport=0;
        if(empty($empgroup)) $empgroup="All";
        if(empty($gender)) $gender=0;
        if(empty($nationality)) $nationality=0;
        if(empty($worktype)) $worktype=0;

        $data = array(
            'content' => 'hrreport/form_ep_contract',
            'getGender' => $this->HrreportModel->getGender(),
            'getNationality' => $this->HrreportModel->getNationality(),
            'getWorkType' => $this->HrreportModel->getWorkType(),
            'dateset' => $dateset,
            'dateset2' => $dateset2,
            'levelreport' => $levelreport,
            'empgroup' => $empgroup,
            'gender' => $gender,
            'nationality' => $nationality,
            'worktype' => $worktype,
        );
        $this->load->view('templates/index', $data);
    }

    public function form_ep_service_year(){
        $this->load->model('HrreportModel');
        $dateset=$this->input->post('dateset');
        $levelreport=$this->input->post('levelreport');
        $empgroup=$this->input->post('empgroup');
        $gender=$this->input->post('gender');
        $nationality=$this->input->post('nationality');
        $worktype=$this->input->post('worktype');

        if(empty($dateset)) $dateset=date("Y-m-d");
        if(empty($levelreport)) $levelreport=0;
        if(empty($empgroup)) $empgroup="All";
        if(empty($gender)) $gender=0;
        if(empty($nationality)) $nationality=0;
        if(empty($worktype)) $worktype=0;

        $data = array(
            'content' => 'hrreport/form_ep_service_year',
            'getGender' => $this->HrreportModel->getGender(),
            'getNationality' => $this->HrreportModel->getNationality(),
            'getWorkType' => $this->HrreportModel->getWorkType(),
            'dateset' => $dateset,
            'levelreport' => $levelreport,
            'empgroup' => $empgroup,
            'gender' => $gender,
            'nationality' => $nationality,
            'worktype' => $worktype,
        );
        $this->load->view('templates/index', $data);
    }
    public function form_list_ep(){
        $this->load->model('HrreportModel');
        $dateset=$this->input->post('dateset');
        $dateset2=$this->input->post('dateset2');
        $levelreport=$this->input->post('levelreport');
        $empgroup=$this->input->post('empgroup');
        $emplevel=$this->input->post('emplevel');
        $nationality=$this->input->post('nationality');
        $worktype=$this->input->post('worktype');

        if(empty($dateset)) $dateset=date("Y-m-d");
        if(empty($dateset2)) $dateset2=date("Y-m-d");
        if(empty($levelreport)) $levelreport=0;
        if(empty($empgroup)) $empgroup="All";
        if(empty($emplevel)) $emplevel=0;
        if(empty($nationality)) $nationality=0;
        if(empty($worktype)) $worktype=0;

        $data = array(
            'content' => 'hrreport/form_list_ep',
            'getEmployeeLevel' => $this->HrreportModel->getEmployeeLevel(),
            'getNationality' => $this->HrreportModel->getNationality(),
            'getWorkType' => $this->HrreportModel->getWorkType(),
            'dateset' => $dateset,
            'dateset2' => $dateset2,
            'levelreport' => $levelreport,
            'empgroup' => $empgroup,
            'emplevel' => $emplevel,
            'nationality' => $nationality,
            'worktype' => $worktype,
        );
        $this->load->view('templates/index', $data);
    }
    public function certify_ep_douments(){
        $this->load->model('HrreportModel');
        $showyear=$this->input->post('showyear');
        $dateset=$this->input->post('dateset');
        $datesign = explode("-", $dateset);
        if(empty($dateset)) $datesign = array( 0 => date("Y"), 1 => date("m"), 2=> date("d"));

        // print_r($datesign);
        // exit();
        $data = array(
            'content' => 'Hrreport/certify_ep_documents',
            'results_paymentperiod' => $this->HrreportModel->getPaymentPeriod(),
            'results_All' => $this->HrreportModel->getEmployeeYearlyIncomeTaxDetails($showyear),
            'datesign' => $datesign,
            'showyear' => $showyear,
        );
        $this->load->view('templates/index', $data);
    }

    public function YearlyIncomeTaxDetails(){
        $this->load->model('hrreportModel');
        $cover = $this->input->post('btn_submit');
        $datasearch1 = $this->input->post('datasearch1');
        $dateset = $this->input->post('dateset');
        if(!empty($datasearch1)){
            $page = 'hrreport/YearlyIncomeTaxDetailsPDF';
            $datesign = explode("-", $dateset);
            // if(sizeof($datesign) == 3 ) $datasearch1 = $datesign[0];
        }
        else{
            $datesign = array( 0 => date("Y"), 1 => date("m"), 2=> date("d"));
            $page = 'hrreport/YearlyIncomeTaxDetails';
        }

        $data = array(
            'content' => $page,
            // 'results_paymentperiod' => $this->PayrollreportModel->getPaymentPeriod(),

            'results_All' => $this->hrreportModel->getEmployeeYearlyIncomeTaxDetails($datasearch1),
            'datesign' => $datesign,
            'datasearch1' => $datasearch1,
            'cover' => $cover,
        );
        if(!empty($datasearch1)) $this->load->view('hrreport/YearlyIncomeTaxDetailsPDF', $data);
        else $this->load->view('templates/index', $data);
    }


}//END CLASS
