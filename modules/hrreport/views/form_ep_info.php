<?php
// echo "<pre>";
// print_r($_POST);
// exit();
 ?>
<head>
    <link href="<?php echo base_url();?>assets/bootstrap-select/css/bootstrap-select.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/bootstrap/css/datepicker.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/jquery-datatable/skin/bootstrap/css/buttons.dataTables.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/jquery-datatable/skin/bootstrap/css/buttons.bootstrap.min.css" rel="stylesheet">

    <meta charset="UTF-8">

    <style>
    @media print {
        canvas {
            min-height: 100%;max-width: 100%;max-height: 100%;height: auto!important;width: auto!important;
        }
        table {
            min-height: 100%;max-width: 100%;max-height: 100%;height: auto!important;width: auto!important;
            page-break-inside: : auto;
        }
        tr{
            page-break-inside: avoid;
            page-break-after: auto;

        }
    }
    canvas{-moz-user-select: none;-webkit-user-select: none;-ms-user-select: none;}
    .font-weight-bold{
        font-weight: bold;
    }
    </style>
</head>

<div class="box box-success">
    <div class="box-header" align="left">
        <i class="fa fa-sign-in"></i>
        <h3 class="box-title"><?php echo 'View '.$this->systemmodel->get_menuname($this->uri->segment(1) . '/' . $this->uri->segment(2)); // แสดงชื่อเมนู  ?></h3>
    </div>
    <div class="box-body">
        <div class="box box-success">
            <div class="box-header">
                <?php
                $this->load->model('HrreportModel');
                // $this->load->model('HrreportModel2');
                $typereport="detail";

                $Division = $this->systemmodel->changelng("ฝ่าย");
                $Department = $this->systemmodel->changelng("แผนก");
                $Section = $this->systemmodel->changelng("หน่วย");

                $TOTALDIVISION = array( "SalaryCal"=>0,"SalaryAdj"=>0,"SSAllowance"=>0,"TaxAllowance"=>0,"OtherIncome"=>0,"TotalIncome"=>0
                ,"SS"=>0,"Tax"=>0,"OtherDeduct"=>0,"TotalDeduct"=>0,"NetToPay"=>0);

                $TotalINC_DIV=array(
                    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
                    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
                    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
                );
                $TotalDED_DIV=array(
                    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
                    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
                    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
                );
                $TOTALDEPARTMENT = array(
                    "SalaryCal"=>0,"SalaryAdj"=>0,"SSAllowance"=>0,"TaxAllowance"=>0,"OtherIncome"=>0,"TotalIncome"=>0
                    ,"SS"=>0,"Tax"=>0,"OtherDeduct"=>0,"TotalDeduct"=>0,"NetToPay"=>0);
                    $TotalINC_DEP=array(
                        0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
                        0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
                        0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
                    );
                    $TotalDED_DEP=array(
                        0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
                        0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
                        0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
                    );
                    $TOTALSECTION = array(
                        "SalaryCal"=>0,"SalaryAdj"=>0,"SSAllowance"=>0,"TaxAllowance"=>0,"OtherIncome"=>0,"TotalIncome"=>0
                        ,"SS"=>0,"Tax"=>0,"OtherDeduct"=>0,"TotalDeduct"=>0,"NetToPay"=>0);

                        $TotalINC_SEC=array(
                            0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
                            0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
                            0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
                        );
                        $TotalDED_SEC=array(
                            0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
                            0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
                            0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
                        );


                        ?>
                    </div>

                    <script type="text/javascript">
                    $(document).ready(function(){
                        $('.selectpicker').selectpicker({style: 'btn-warning',size: 5});
                        var numCols = $('#example').DataTable().columns().nodes().length;

                        $('.js-exportable').DataTable({
                            dom: 'Blfrtip',responsive: true,paging: false,info: false,
                            buttons: [
                                {extend:'copyHtml5',text:' <i class="fa fa-files-o fa-2x "></i> ',titleAttr: ' Copy ',footer:true},
                                {extend:'excelHtml5'

                                ,title: 'Any title for file',
                                message: "Any message for header inside the file. I am not able to put message in next row in excel file but you can use \n"

                                ,filename:'<?php echo $this->systemmodel->get_menuname($this->uri->segment(1) . '/' . $this->uri->segment(2));?>'
                                ,text:' <i class="fa fa-file-excel-o fa-2x "></i> '
                                ,titleAttr: ' Excel '
                                ,footer:true
                                ,exportOptions: {
                                    format: {
                                        body: function(data, row, column, node) {
                                            return column === 12 ? data.replace(/[\$,]/g,'') : "\0"+data;
                                        }
                                    }
                                }
                                // ,exportOptions: { format: { body: function(data, row, column, node) { return "\0"+data.replace(',',''); } } }
                            },
                            <?php
                            //Add custom button
                            if ($levelreport == 3) {
                            ?>
                            {
                                text: '<i class="fa fa-file-excel-o fa-2x "></i> Export (ดาวน์โหลด) ',
                                action: function ( e, dt, node, config ) {
                                    var param = $('#frmSearch').serialize();
                                    window.open('<?php echo  site_url('hrreport/export/hr_excel_filter_by_unit'); ?>?'+param);

                                }
                            },
                            <?php
                            }
                            ?>
                            {extend:'print',text:' <i class="fa fa-print fa-2x "></i> ',titleAttr: ' Print ',footer:true},


                        ],
                        "lengthMenu": [ [10, 25, 50, -1], [10, 25, 50, "All"] ],
                        "ordering": false

                    });

                    $('.js-exportable2').DataTable({
                        dom: 'Bfrtip',responsive: true,paging: true,info: false,
                        buttons: [
                            {extend:'copyHtml5',text:' <i class="fa fa-files-o fa-2x "></i> ',titleAttr: ' Copy '},
                            {extend:'excelHtml5',text:' <i class="fa fa-file-excel-o fa-2x "></i> ',titleAttr: ' Excel '},
                            {extend:'print',text:' <i class="fa fa-print fa-2x "></i> ',titleAttr: ' Print '},


                    ]
                    // ,"columnDefs": [{"targets": [ 0,1,2,3,4 ],"visible": false,"searchable": true}]
                });

                $('.js-exportable3').DataTable({
                    dom: 'Bfrtip',responsive: true,paging: true,info: false,
                    buttons: [
                        {extend:'copyHtml5',text:' <i class="fa fa-files-o fa-2x "></i> ',titleAttr: ' Copy '},
                        {extend:'excelHtml5',text:' <i class="fa fa-file-excel-o fa-2x "></i> ',titleAttr: ' Excel '},
                        {
                            extend:'print',
                            text:' <i class="fa fa-print fa-2x "></i> ',
                            titleAttr: ' Print ',
                            exportOptions:{
                                columns:[0,1,2,3,4,5,6]
                            }
                        }
                ],
                "columnDefs": [{"targets": [ 5 ],"visible": false,"searchable": true}]
            });
        });
        </script>

        <!-- เริ่มข้อมูลในการเลือกวันที่ในการออกรายงาน wrote by bancha -->
        <div class="box-body" align="left" id="resultDIV">
            <div class="table-responsive fixed-table-body">
                <form class="" id="frmSearch" name="form1" action="<?php echo $this->uri->segment(2);?>"  method="post">
                    <div class="row clearfix">
                        <div class="col-md-2">
                            <p><b>Date Selected | เลือกวันที่รายงาน </b></p>
                            <div class="form-group">
                                <div class='input-group date' id='datetimepicker1'>
                                    <input type='text' class="form-control" id='dateset' name="dateset" value="<?php echo $dateset;?>" />
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                            <script>
                            $(document).ready(function(){
                                var date_input=$('input[name="dateset"]'); //our date input has the name "date"
                                var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
                                date_input.datepicker({
                                    format: 'yyyy-mm-dd',
                                    container: container,
                                    todayHighlight: true,
                                    autoclose: true,
                                    viewMode:"days",
                                    minViewMode:"days"
                                })
                            })
                            </script>
                        </div>
                        <!-- เปลี่ยน md-4 เป็น md-2 เพื่อให้เมนูเลือกแสดงเป็น 4 คอลัม -->
                        <div class="col-md-2">
                            <p><b>Date Selected | ถึงวันที่ </b></p>
                            <div class="form-group">
                                <div class='input-group date' id='datetimepicker1'>
                                    <input type='text' class="form-control" id='dateset2' name="dateset2" value="<?php echo $dateset2;?>" />
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                            <script>
                            $(document).ready(function(){
                                var date_input=$('input[name="dateset2"]'); //our date input has the name "date"
                                var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
                                date_input.datepicker({
                                    format: 'yyyy-mm-dd',
                                    container: container,
                                    todayHighlight: true,
                                    autoclose: true,
                                    viewMode:"days",
                                    minViewMode:"days"
                                })
                            })
                            </script>
                        </div>
                        <!-- จบการเลือกวันที่ในการออกรายงาน (By Naburi wrote-->

                        <div class="col-md-4">
                            <p><b>Report Level | ระดับรายงาน</b></p>
                            <?php
                            $Division = $this->systemmodel->changelng("ฝ่าย");
                            $Department = $this->systemmodel->changelng("แผนก");
                            $Section = $this->systemmodel->changelng("หน่วย");
                            ?>
                            <select class="form-control show-tick selectpicker" id="levelreport" name="levelreport">
                                <option value="0" <?php if($levelreport=="0") echo "selected"; ?>>All | ทั้งหมด</option>
                                <option value="1" <?php if($levelreport=="1") echo "selected"; ?>><?=$Division ?></option>
                                <option value="2" <?php if($levelreport=="2") echo "selected"; ?>><?=$Department?></option>
                                <option value="3" <?php if($levelreport=="3") echo "selected"; ?>><?=$Section?></option>
                                <?php
                                if($levelreport=="0") $levelreporttxt="All";
                                else if($levelreport=="1") $levelreporttxt=$Division;
                                else if($levelreport=="2") $levelreporttxt=$Department;
                                else if($levelreport=="3") $levelreporttxt=$Section;
                                ?>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <p><b>Employee Type | ประเภทพนักงาน</b></p>
                            <select class="form-control show-tick selectpicker" id="empgroup" name="empgroup">
                                <option value="All" <?php if($empgroup=="All") echo "selected"; ?>>All</option>
                                <option value="Current" <?php if($empgroup=="Current") echo "selected"; ?>>Current Employee | พนักงานปัจจุบัน</option>
                                <option value="New Employee" <?php if($empgroup=="New Employee") echo "selected"; ?>>New Employee | เข้าใหม่</option>
                                <option value="Resign" <?php if($empgroup=="Resign") echo "selected"; ?>>Resigned | ลาออก</option>
                            </select>
                        </div>
                    </div>

                    <!-- เลือกเพศพนักงาน -->
                    <div class="row clearfix">
                        <div class="col-md-4">
                            <p><b>Gendar | เพศ</b></p>
                            <select class="form-control show-tick selectpicker" id="gender" name="gender">
                                <option value="0" <?php if($gender=="0") echo "selected"; ?>>All</option>
                                <?php
                                $gendertxt = "All";
                                foreach ($getGender as $row) {
                                    if($gender==$row->GenderID){
                                        $selected = "selected";
                                        $gendertxt = $row->GenderEN." | ".$row->GenderTH;
                                    }
                                    else $selected = "";
                                    echo "<option value='".$row->GenderID."' $selected>".$row->GenderEN." | ".$row->GenderTH."</option>";
                                }
                                ?>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <p><b>Nationality | สัญชาติ</b></p>
                            <select class="form-control show-tick selectpicker" id="nationality" name="nationality">
                                <option value="0" <?php if($nationality=="0") echo "selected"; ?>>All</option>
                                <?php
                                $nationalitytxt = "All";
                                foreach ($getNationality as $row) {
                                    if($nationality==$row->NationlityID){
                                        $selected = "selected";
                                        $nationalitytxt=$row->NationalityNameEN." | ".$row->NationalityNameLL;
                                    }
                                    else $selected = "";
                                    echo "<option value='".$row->NationlityID."' $selected>".$row->NationalityNameEN." | ".$row->NationalityNameLL."</option>";
                                }
                                ?>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <p><b>Work Type | ประเภทงาน</b></p>
                            <select class="form-control show-tick selectpicker" id="worktype" name="worktype">
                                <option value="0" <?php if($worktype=="") echo "selected"; ?>>All</option>
                                <?php
                                $worktypetxt = "All";
                                foreach ($getWorkType as $row) {
                                    if($worktype==$row->WorkTypeID){
                                        $selected = "selected";
                                        $worktypetxt = $row->WorkTypeNameEng." | ".$row->WorkTypeNameThai;
                                    }
                                    else $selected = "";
                                    echo "<option value='".$row->WorkTypeID."' $selected>".$row->WorkTypeNameEng." | ".$row->WorkTypeNameThai."</option>";
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-md-12" align="center">
                            <p>&nbsp;</p>
                            <button type="submit" class="btn btn-primary btn-md btn-block" id="btn_reportgraph">แสดงรายงาน</button>
                        </div>
                    </div>
                </form>



                <BR>
                    <table class="table table-bordered table-striped table-hover dataTable  js-exportable">
                        <thead>
                            <tr>
                                <th scope="col"><center>NO.</center></th>
                                <th scope="col"><center>Emp ID</center></th>
                                <th scope="col"><center>Employee Name (TH)</center></th>
                                <th scope="col"><center>Employee Name (EN)</center></th>
                                <th scope="col"><center>ID_Card</center></th>
                                <th scope="col"><center>SS_ID</center></th>
                                <th scope="col"><center>Birthday</center></th>
                                <th scope="col"><center>Age</center></th>
                                <th scope="col"><center>Start Working</center></th>
                                <th scope="col"><center>Resigned</center></th>
                                <th scope="col"><center>Department</center></th>
                                <th scope="col"><center>Position</center></th>
                                <th scope="col"><center>Salary</center></th>
                                <th scope="col"><center>Gender</center></th>
                                <th scope="col"><center>Nationality</center></th>
                                <th scope="col"><center>Duration of Employment</center></th>
                                <th scope="col"><center>Bank Account No.</center></th>
                                <th scope="col"><center>Address</center></th>
                            </tr>
                        </thead>

                        <tbody>
                            <!-- <tr>
                            <td colspan="18">
                            <div style="font-weight: bold;"><?php echo $this->session->CompNameEng; ?></div>
                            <div style="float: left;width: 80px;font-weight: bold;">Report :</div><div>Employee Information | รายละเอียดของพนักงาน</div>
                            <div style="float: left;width: 80px;font-weight: bold;">As of :</div>
                            <div>
                            <?php echo $dateset;?>
                        </div>
                        <div style="float: left;width: 80px;font-weight: bold;">Selected by :</div>
                        <div>
                        <?php echo $levelreporttxt;?>
                    </div>
                </td>
                <?php $i2=0;while($i2<17){echo "<td style='display: none;'></td>";$i2++;}?>
            </tr> -->
            <?php



            if($levelreport==0){
                $displaySection = "none;"; $displayDepartment="none;"; $displayDivision="none";
            }else if($levelreport==1){
                $displaySection = "none;"; $displayDepartment="none;"; $displayDivision="";
            }else if($levelreport==2){
                $displaySection = "none;"; $displayDepartment=""; $displayDivision="";
            }else{
                $displaySection = ""; $displayDepartment=""; $displayDivision="";
            }
            if($typereport=="summary"){
                $displayDetail="none;"; $displaySummary="";
            }else {
                $displayDetail=""; $displaySummary="none;";
            }
            $i=0;


            $Grandtotal = array(
                "SalaryCal"=>0
                ,"SalaryAdj"=>0
                ,"SSAllowance"=>0
                ,"TaxAllowance"=>0
                ,"OtherIncome"=>0
                ,"TotalIncome"=>0
                ,"SS"=>0
                ,"Tax"=>0
                ,"OtherDeduct"=>0
                ,"TotalDeduct"=>0
                ,"NetToPay"=>0
            );
            $TRTOTAL = array(
                "Division"=>0
                ,"Department"=>0
                ,"Section"=>0
                ,"DivisionID"=>0
                ,"DepartmentID"=>0
                ,"SectionID"=>0
                ,"DivisionName"=>""
                ,"DepartmentName"=>""
                ,"SectionName"=>""
                ,"DivisionPeople"=>0
                ,"DepartmentPeople"=>0
                ,"SectionPeople"=>0
            );
            $chkTRDivision=0;
            $chkTRDepartment=0;
            $chkTRSection=0;
            $totalpeople=0;

            $cnt_in_sec=0;
            $colspan_val=12;
            $colspan = $colspan_val+6;

            $sql="select * from Division  where (CompID=".$this->session->CompUSERID.") ";
            $query=$this->db->query($sql);
            if(!empty($query->result())){
                foreach ($query->result() as $rowD) {
                    $DivisionID = $rowD->DivisionID;
                    $cnt_in_div=0;

                    if($TRTOTAL["DivisionID"] != $rowD->DivisionID){
                        $TRTOTAL["DivisionID"] = $rowD->DivisionID;
                        $TRTOTAL["DivisionName"] = $rowD->DivisionNameEng." | ".$rowD->DivisionNameThai;

                        if($displayDivision==""){
                            echo "<tr bgcolor='#f1dae1'>
                            <td colspan='$colspan'>$Division | ".$TRTOTAL["DivisionName"]."</td>";
                            $i2=0;while($i2<$colspan-1){echo "<td style='display: none;'></td>";$i2++;}
                            echo "</tr>";
                        }

                        if($displayDivision == "") $i=0;
                        $TRTOTAL["DivisionPeople"]=0;
                    }
                    // Department
                    $sql2="select * from Department  where DivisionID =  ".$rowD->DivisionID;
                    $query2=$this->db->query($sql2);
                    if(!empty($query2->result())){
                        foreach ($query2->result() as $row2) {

                            $DepartmentID = $row2->DepartmentID;
                            $cnt_in_dep=0;

                            if($TRTOTAL["DepartmentID"] != $row2->DepartmentID){
                                $TRTOTAL["DepartmentID"] = $row2->DepartmentID;
                                $TRTOTAL["DepartmentName"] = $row2->DepartmentNameEng." | ".$row2->DepartmentNameThai;

                                if($displayDepartment==""){
                                    echo "<tr bgcolor='#d3ffce' style='display: $displayDepartment'>
                                    <td colspan='$colspan'>$Department | ".$TRTOTAL["DepartmentName"]."</td>";
                                    $i2=0;while($i2<$colspan-1){echo "<td style='display: none;'></td>";$i2++;}
                                    echo "</tr>";
                                }
                                if($displayDepartment == "") $i=0;
                                $TRTOTAL["DepartmentPeople"]=0;
                            }

                            // Section
                            $sql3="select a.SectionID,SectionNameEng,SectionNameThai from Section a left outer join Employee c on c.SectionID=a.SectionID
                            where c.EmployeeID is not null and  a.DepartmentID =  ".$row2->DepartmentID. "  group by a.SectionID,SectionNameEng,SectionNameThai";
                            $query3=$this->db->query($sql3);
                            if(!empty($query3->result())){
                                foreach ($query3->result() as $row3) {
                                    if($TRTOTAL["SectionID"] != $row3->SectionID){
                                        $TRTOTAL["SectionID"] = $row3->SectionID;
                                        $TRTOTAL["SectionName"] = $row3->SectionNameEng." | ".$row3->SectionNameThai;

                                        if($displaySection == ""){
                                            echo "<tr bgcolor='#e6e6fa' style='display: $displaySection'>
                                            <td colspan='$colspan'>$Section | ".$TRTOTAL["SectionName"]."</td>";
                                            $i2=0;while($i2<$colspan-1){echo "<td style='display: none;'></td>";$i2++;}
                                            echo "</tr>";
                                        }
                                        if($displaySection =="" )$i=0;
                                    }

                                    // employee


                                    $sql4="
                                    select *,a.Postcode as zipcode
                                    from Employee a
                                    left outer join Company b on b.CompID=a.CompID
                                    left outer join Division c on c.DivisionID=a.DivisionID
                                    left outer join Department d on d.DepartmentID=a.DepartmentID
                                    left outer join Section e on e.SectionID=a.SectionID
                                    left outer join Position f on f.PositionID=a.PositionID
                                    left outer join Title g on g.TitleID=a.TitleID
                                    left outer join Gender h on a.GenderID = h.GenderID
                                    left outer join Nationality i on a.NationalityID = i.NationlityID
                                    left outer join districts i1 on a.SubDistrictID = i1.DISTRICT_CODE
                                    left outer join amphures i2 on a.DistrictID = i2.AMPHUR_ID
                                    left outer join provinces i3 on a.ProvinceID = i3.ProvinceID


                                    where (a.CompID=".$this->session->CompID.")
                                    and a.SectionID=".$row3->SectionID."

                                    and (a.GenderID = $gender or $gender=0)
                                    and (NationalityID = $nationality or $nationality=0)
                                    and (WorkingType = $worktype or $worktype=0)
                                    and ServiceStartDate between '$dateset' and '$dateset2'
                                    ";

                                    if($empgroup=="Resign") $sql4.=" and (EffectiveDateOut<='$dateset2') and ResignationTypeStatusOut=1 ";
                                    else if($empgroup=="Current") $sql4.=" and ResignationTypeStatusOut=0 ";
                                    else if($empgroup=="All") $sql4.=" and (EffectiveDateOut<='$dateset2') ";


                                    $sql4 .=" order by a.EmployeeCode ";
                                    // echo "<BR>";


                                    $query4=$this->db->query($sql4);
                                    if(!empty($query4->result())){

                                        foreach ($query4->result() as $row) {

                                            $bdate = $row->Birthday;
                                            $AgeY = date_diff(date_create($bdate), date_create('now'))->y;
                                            $AgeM = date_diff(date_create($bdate), date_create('now'))->m;
                                            $AgeD = date_diff(date_create($bdate), date_create('now'))->d;
                                            if($AgeY>100){
                                                $AgeY=0;
                                                $AgeM=0;
                                                $AgeD=0;
                                            }
                                            $bdate = $row->ServiceStartDate;
                                            $edate = $row->EffectiveDateOut;

                                            if($edate="0000-00-00") $edate = date("Y-m-d");

                                            $date1=date_create($bdate);
                                            $date2=date_create($edate);
                                            $diff=date_diff($date1,$date2);

                                            $SSDY = $diff->y;
                                            $SSDM = $diff->m;
                                            $SSDD = $diff->d;


                                            if(empty($row->EmployeeID)){?>
                                                <tr id="TREmployee" style="display: <?php echo $displayDetail;?>">
                                                    <td scope="col">0000000000</td>
                                                </tr>
                                                <?php
                                            }
                                            else{
                                                if($DivisionID==$row->DivisionID) $cnt_in_div +=1 ;
                                                if($DepartmentID==$row2->DepartmentID) $cnt_in_dep +=1;
                                                $totalpeople ++;
                                                $i++;
                                                if($displayDetail==""){
                                                    ?>
                                                    <tr id="TREmployee" style="display: <?php echo $displayDetail;?>">
                                                        <td scope="col"><?php echo ($i);?></td>
                                                        <td ><?php echo $row->EmployeeCode;?></td>
                                                        <td ><?php echo $row->TitleTH.$row->FirstName." ".$row->LastName;?></td>
                                                        <td ><?php echo $row->FullNameLL;?></td>
                                                        <td ><?php echo " ".strval($row->CardID)." ";?></td>
                                                        <td ><?php echo " ".strval($row->SSOID)." ";?></td>
                                                        <td ><?php echo $row->Birthday;?></td>
                                                        <td ><?php echo "$AgeY Y $AgeM M $AgeD D";?></td>
                                                        <td ><?php echo $row->ServiceStartDate;?></td>
                                                        <td ><?php echo $row->EffectiveDateOut;?></td>
                                                        <td ><?php echo $row->DepartmentNameThai;?></td>
                                                        <td ><?php echo $row->PositionNameThai;?></td>
                                                        <td align="right"><?php echo number_format($row->MonthlySalary,2);?></td>
                                                        <td ><?php echo $row->GenderTH;?></td>
                                                        <td ><?php echo $row->NationalityNameLL;?></td>
                                                        <td ><?php echo "$SSDY Y $SSDM M $SSDD D";?></td>
                                                        <td ><?php echo $row->BankAccountNo;?></td>
                                                        <td ><?php echo $row->AddressPermanent." แขวง/ตำบล ".$row->DISTRICT_NAME."  เขต/อำเภอ ".$row->AMPHUR_NAME." จังหวัด ".$row->ProvinceNameLocal." รหัสไปรษณีย์  ".$row->zipcode;?></td>
                                                    </tr>
                                                    <?php
                                                }

                                                $TOTALSECTION["SalaryCal"] += $row->MonthlySalary;
                                                // $TOTALSECTION["SalaryAdj"] += $SalaryAdj;
                                                // $TOTALSECTION["SSAllowance"] += $SSAllowance;
                                                // $TOTALSECTION["TaxAllowance"] += $TaxAllowance;
                                                // $TOTALSECTION["OtherIncome"] += $OtherIncome;
                                                // $TOTALSECTION["TotalIncome"] += $TotalIncome;
                                                // $TOTALSECTION["SS"] += $SS;
                                                // $TOTALSECTION["Tax"] += $Tax;
                                                // $TOTALSECTION["OtherDeduct"] += $OtherDeduct;
                                                // $TOTALSECTION["TotalDeduct"] += $TotalDeduct;
                                                // $TOTALSECTION["NetToPay"] += $NetToPay;


                                                $Grandtotal["SalaryCal"] += $row->MonthlySalary;
                                                // $Grandtotal["SalaryAdj"] += $SalaryAdj;
                                                // $Grandtotal["SSAllowance"] += $SSAllowance;
                                                // $Grandtotal["TaxAllowance"] += $TaxAllowance;
                                                // $Grandtotal["OtherIncome"] += $OtherIncome;
                                                // $Grandtotal["TotalIncome"] += $TotalIncome;
                                                // $Grandtotal["SS"] += $SS;
                                                // $Grandtotal["Tax"] += $Tax;
                                                // $Grandtotal["OtherDeduct"] += $OtherDeduct;
                                                // $Grandtotal["TotalDeduct"] += $TotalDeduct;
                                                // $Grandtotal["NetToPay"] += $NetToPay;
                                            }

                                        }
                                    }
                                    //employee
                                    if($displaySection == ""){

                                        echo "<tr class='font-weight-bold' bgcolor='#e6e6fa' style='display: $displaySection'>";
                                        echo "<td  colspan='$colspan_val'>Total $Section ($i) : ".$TRTOTAL["SectionName"]."</td>";
                                        if($typereport == "detail"){
                                            $i2=0;while($i2<$colspan_val-1){echo "<td style='display: none;'></td>";$i2++;}
                                        }
                                        echo "<td class='text-right'>".number_format($TOTALSECTION["SalaryCal"],2)."</td>";
                                        $i2=0;while($i2<($colspan-$colspan_val-1)){echo "<td></td>";$i2++;}
                                        echo "</tr>";
                                    }else{


                                    }

                                    $TOTALDEPARTMENT["SalaryCal"] += $TOTALSECTION["SalaryCal"];
                                    // $TOTALDEPARTMENT["SalaryAdj"] += $TOTALSECTION["SalaryAdj"];
                                    // $TOTALDEPARTMENT["SSAllowance"] += $TOTALSECTION["SSAllowance"];
                                    // $TOTALDEPARTMENT["TaxAllowance"] += $TOTALSECTION["TaxAllowance"];
                                    // $TOTALDEPARTMENT["OtherIncome"] += $TOTALSECTION["OtherIncome"];
                                    // $TOTALDEPARTMENT["TotalIncome"] += $TOTALSECTION["TotalIncome"];
                                    // $TOTALDEPARTMENT["SS"] += $TOTALSECTION["SS"];
                                    // $TOTALDEPARTMENT["Tax"] += $TOTALSECTION["Tax"];
                                    // $TOTALDEPARTMENT["OtherDeduct"] += $TOTALSECTION["OtherDeduct"];
                                    // $TOTALDEPARTMENT["TotalDeduct"] += $TOTALSECTION["TotalDeduct"];
                                    // $TOTALDEPARTMENT["NetToPay"] += $TOTALSECTION["NetToPay"];

                                    $TOTALSECTION["SalaryCal"] =0;
                                    // $TOTALSECTION["SalaryAdj"] =0;
                                    // $TOTALSECTION["SSAllowance"] =0;
                                    // $TOTALSECTION["TaxAllowance"] =0;
                                    // $TOTALSECTION["OtherIncome"] =0;
                                    // $TOTALSECTION["TotalIncome"] =0;
                                    // $TOTALSECTION["SS"] =0;
                                    // $TOTALSECTION["Tax"] =0;
                                    // $TOTALSECTION["OtherDeduct"] =0;
                                    // $TOTALSECTION["TotalDeduct"] =0;
                                    // $TOTALSECTION["NetToPay"] =0;
                                    $TotalINC_SEC=array(
                                        0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
                                        0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
                                        0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
                                    );
                                    $TotalDED_SEC=array(
                                        0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
                                        0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
                                        0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
                                    );



                                }
                            }
                            //Section
                            if($displayDepartment==""){
                                echo "<tr  class='font-weight-bold' bgcolor='#d3ffce' style='display: $displayDepartment'>";
                                echo "<td  colspan='$colspan_val'>Total $Department ($cnt_in_dep) : ".$TRTOTAL["DepartmentName"]."</td>";
                                $TRTOTAL["DepartmentPeople"]=0;
                                if($typereport == "detail"){

                                    $i2=0;while($i2<($colspan_val-1)){echo "<td style='display: none;'></td>";$i2++;}
                                }
                                echo "<td class='text-right'>".number_format($TOTALDEPARTMENT["SalaryCal"],2)."</td>";
                                $i2=0;while($i2<($colspan-$colspan_val-1)){echo "<td></td>";$i2++;}
                                echo "</tr>";
                            }else{



                            }
                            $TOTALDIVISION["SalaryCal"] += $TOTALDEPARTMENT["SalaryCal"];
                            // $TOTALDIVISION["SalaryAdj"] += $TOTALDEPARTMENT["SalaryAdj"];
                            // $TOTALDIVISION["SSAllowance"] += $TOTALDEPARTMENT["SSAllowance"];
                            // $TOTALDIVISION["TaxAllowance"] += $TOTALDEPARTMENT["TaxAllowance"];
                            // $TOTALDIVISION["OtherIncome"] += $TOTALDEPARTMENT["OtherIncome"];
                            // $TOTALDIVISION["TotalIncome"] += $TOTALDEPARTMENT["TotalIncome"];
                            // $TOTALDIVISION["SS"] += $TOTALDEPARTMENT["SS"];
                            // $TOTALDIVISION["Tax"] += $TOTALDEPARTMENT["Tax"];
                            // $TOTALDIVISION["OtherDeduct"] += $TOTALDEPARTMENT["OtherDeduct"];
                            // $TOTALDIVISION["TotalDeduct"] += $TOTALDEPARTMENT["TotalDeduct"];
                            // $TOTALDIVISION["NetToPay"] += $TOTALDEPARTMENT["NetToPay"];
                            // $TRTOTAL["DivisionPeople"] +=$TRTOTAL["DepartmentPeople"];

                            $TOTALDEPARTMENT = array(
                                "SalaryCal"=>0,"SalaryAdj"=>0,"SSAllowance"=>0,"TaxAllowance"=>0,"OtherIncome"=>0,"TotalIncome"=>0
                                ,"SS"=>0,"Tax"=>0,"OtherDeduct"=>0,"TotalDeduct"=>0,"NetToPay"=>0);
                                $TotalINC_DEP=array(
                                    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
                                    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
                                    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
                                );
                                $TotalDED_DEP=array(
                                    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
                                    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
                                    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
                                );

                            }
                        }
                        //Department
                        if($displayDivision==""){
                            echo "<tr class='font-weight-bold' bgcolor='#f1dae1'>";
                            echo "<td   colspan='$colspan_val'>Total $Division ($cnt_in_div) : ".$TRTOTAL["DivisionName"]."</td>";
                            $TRTOTAL["DivisionPeople"]=0;

                            if($typereport == "detail")
                            $i2=0;while($i2<($colspan_val-1)){echo "<td style='display: none;'></td>";$i2++;}

                            echo "<td class='text-right'>".number_format($TOTALDIVISION["SalaryCal"],2)."</td>";
                            $i2=0;while($i2<($colspan-$colspan_val-1)){echo "<td style='display: none;'></td>";$i2++;}
                            echo "</tr>";
                        }

                        $TOTALDIVISION = array(
                            "SalaryCal"=>0,"SalaryAdj"=>0,"SSAllowance"=>0,"TaxAllowance"=>0,"OtherIncome"=>0,"TotalIncome"=>0
                            ,"SS"=>0,"Tax"=>0,"OtherDeduct"=>0,"TotalDeduct"=>0,"NetToPay"=>0);
                            $TotalINC_DIV=array(
                                0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
                                0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
                                0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
                            );
                            $TotalDED_DIV=array(
                                0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
                                0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
                                0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
                            );


                        }
                    }


                    ?>


                    <tr class="  font-weight-bold">
                        <td  class="text-right font-weight-bold" colspan="<?php echo $colspan_val;?>">Grand Total (<?php echo $totalpeople; ?>)</td>
                        <?php if($typereport == "detail"){
                            $i2=0;while($i2<($colspan_val-1)){echo "<td style='display: none;'></td>";$i2++;}
                        } ?>
                        <td  class="text-right"><?php echo number_format($Grandtotal["SalaryCal"],2);?></td>
                        <td class='text-right'></td>
                        <td class='text-right'></td>
                        <td class='text-right'></td>
                        <td class='text-right'></td>
                        <td class='text-right'></td>
                    </tr>

                </tbody>

            </table>

        </div>
    </div>
</div>

</div>
<script src="<?php echo base_url();?>assets/bootstrap-select/js/bootstrap-select.js"></script>
<script src="<?php echo base_url();?>assets/bootstrap/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url();?>assets/jquery-datatable/jquery.dataTables.js"></script>
<script src="<?php echo base_url();?>assets/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
<script src="<?php echo base_url();?>assets/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url();?>assets/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
<script src="<?php echo base_url();?>assets/jquery-datatable/extensions/export/jszip.min.js"></script>
<!-- <script src="<?php echo base_url();?>assets/jquery-datatable/extensions/export/pdfmake.min.js"></script> -->
<script src="<?php echo base_url();?>assets/jquery-datatable/extensions/export/vfs_fonts.js"></script>
<script src="<?php echo base_url();?>assets/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
<script src="<?php echo base_url();?>assets/jquery-datatable/extensions/export/buttons.print.min.js"></script>
