<?php
    $this->load->model('hrreportModel'); 
    
	$m = 5;
	$y = $datasearch1+543;
	$thaimonth=array("มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม"); 

     $FULL_MONTH2 = array(
        "01" => "มกราคม",
        "02" => "กุมภาพันธ์",
        "03" => "มีนาคม",
        "04" => "เมษายน",
        "05" => "พฤษภาคม",
        "06" => "มิถุนายน",
        "07" => "กรกฏาคม",
        "08" => "สิงหาคม",
        "09" => "กันยายน",
        "10" => "ตุลาคม",
        "11" => "พฤศจิกายน",
        "12" => "ธันวาคม"
    );
	if($m==12){
		$m=0;
		$y +=1;
	}

	$nameMonth = $thaimonth[$m];
	$TAXID="";
	$HeadOffice="";    

	$mname="";
	$mnametxt="";
	$CompNameThai = "";
	$YPay = $datasearch1;
	$MPay = $m;
	$mname=substr("00".$MPay,-2);
	$mnametxt=date('F', mktime(0, 0, 0, $MPay, 10));
	$dateToTest = $YPay."-".$mname."-01";
	$lastdaytxt = $YPay."-".$mnametxt."-".date('t',strtotime($dateToTest));
	$lastday = $YPay."-".$mname."-".date('t',strtotime($dateToTest));
	$dlastday = date('t',strtotime($dateToTest));

	$datesend = date('t',strtotime($dateToTest))."/".$mname."/".($YPay+543);

	$i=0;
	$Grand_salary = 0;
	$Grand_salary_ALL = 0;
	$Grand_ssa = 0;
	$Grand_tax = 0;
	$SS = 0;
	$Tax = 0;
        if(!empty($results_All)){
          foreach ($results_All as $row) {
            $EmployeeID = $row->EmployeeID;
            $NetToPayTAX = 0;
            $Tax = 0;
            for ($k=1; $k <= 12 ; $k++) { 
            	$data = $this->hrreportModel->getMultiDATA($EmployeeID,$YPay,$k,1);
	            if($data['isinreport']){
	            	$NetToPayTAX += $data['NetToPayTAX'];
	            	$Tax += $data['Tax'];
	            }
            }
            $SS = $data['SS'];
        	if($row->ProvinceID == 1){ $tm="แขวง";$amp=""; }else{  $tm="ต.";$amp="อ.";}
        	$id13_arr[$i]= $row->CardID;
			$emp_title[$i] = $row->TitleEN;
			$emp_firstname[$i] = $row->FirstName;
			$emp_lastname[$i] = $row->LastName;
			$emp_address[$i] = $row->AddressPermanent." ".$tm.$row->DISTRICT_NAME." ".$amp.$row->AMPHUR_NAME." ".$row->ProvinceNameLocal." ".$row->Postcode;
			$name_arr[$i] = $row->FullNameLL;
			$PITCode[$i] = $row->PITCode;
			$salary_arr[$i] = round(($NetToPayTAX),2);
			$ssallowance_arr[$i] = round($Tax,2);

			$Grand_salary += $salary_arr[$i];
			$Grand_ssa += $SS;
			$Grand_tax += $ssallowance_arr[$i];
			$CompNameThai = $row->CompNameThai;
			$comAddress1 = $row->CompAddress1;
			$comAddress2 = $row->CompAddress2;
			$comAddress3 = $row->CompAddress3;
			$comAddress4 = $row->CompAddress4;
			$amp = $row->District;
			$pv = $row->Province;
			$postcode_com = $row->postcode_com;

			$TAXID = $row->TaxID;
			$TAXID = $this->hrportModel->taxformat($TAXID);
			$HeadOffice = $row->HeadOffice;
			$BossName = $row->BossName;
			$BossPosition = $row->BossPosition;
            $i++; 
          } 
        }  
        $totalpeople = $i;
        $row = 7;
        if($i<$row) $totalpage = 1;
        else $totalpage = round($i/$row);
?>
<?php
// $cover="yes";
// echo $cover;exit();
    if($cover=="cover"){
    	$this->load->library('Pdf');


        $text1 = "aaa";
        $filename = "aaaa.pdf";
        $text_y = 140;
        $path_cert = base_url().'modules/hrreport/views/images/pnd1k-1.jpg';
        $path_cert2 = base_url().'modules/hrreport/views/images/pnd1k-2.jpg';

        $pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);
        // $pdf->SetTitle('CERT | EVENT BY วัชรพัฐ');

        $pdf->SetCreator(PDF_CREATOR);
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(0);
        $pdf->SetFooterMargin(0);
        $pdf->setPrintFooter(false);
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
        if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
            require_once(dirname(__FILE__).'/lang/eng.php');
            $pdf->setLanguageArray($l);
        }        

        $pdf->AddPage('P', 'A4');

        $bMargin = $pdf->getBreakMargin();
        $auto_page_break = $pdf->getAutoPageBreak();

        $pdf->SetAutoPageBreak(false, 0);

        $pdf->Image($path_cert, 0, 0, 210, 0,'jpg', '#', '', true, 150, '', false, false, 1, false, false, false);

        $pdf->SetAutoPageBreak($auto_page_break, $bMargin);
        $pdf->setPageMark();

        $TAXID = str_replace("-", "", $TAXID);
        $TaxID_Split = str_split($TAXID);

        $pdf->SetFont('thsarabun', '', 18); 
        $pdf->SetXY(59, 37); $html = '<B>'.$TaxID_Split[0].'</B>'; $pdf->writeHTML($html, true, false, false, false, '');
        $pdf->SetXY(65, 37); $html = '<B>'.$TaxID_Split[1].'</B>'; $pdf->writeHTML($html, true, false, false, false, '');
        $pdf->SetXY(69, 37); $html = '<B>'.$TaxID_Split[2].'</B>'; $pdf->writeHTML($html, true, false, false, false, '');
        $pdf->SetXY(73, 37); $html = '<B>'.$TaxID_Split[3].'</B>'; $pdf->writeHTML($html, true, false, false, false, '');
        $pdf->SetXY(77, 37); $html = '<B>'.$TaxID_Split[4].'</B>'; $pdf->writeHTML($html, true, false, false, false, '');
        $pdf->SetXY(82, 37); $html = '<B>'.$TaxID_Split[5].'</B>'; $pdf->writeHTML($html, true, false, false, false, '');
        $pdf->SetXY(86, 37); $html = '<B>'.$TaxID_Split[6].'</B>'; $pdf->writeHTML($html, true, false, false, false, '');
        $pdf->SetXY(90, 37); $html = '<B>'.$TaxID_Split[7].'</B>'; $pdf->writeHTML($html, true, false, false, false, '');
        $pdf->SetXY(94, 37); $html = '<B>'.$TaxID_Split[8].'</B>'; $pdf->writeHTML($html, true, false, false, false, '');
        $pdf->SetXY(98, 37); $html = '<B>'.$TaxID_Split[9].'</B>'; $pdf->writeHTML($html, true, false, false, false, '');
        $pdf->SetXY(103, 37); $html = '<B>'.$TaxID_Split[10].'</B>'; $pdf->writeHTML($html, true, false, false, false, '');
        $pdf->SetXY(107, 37); $html = '<B>'.$TaxID_Split[11].'</B>'; $pdf->writeHTML($html, true, false, false, false, '');
        $pdf->SetXY(113, 37); $html = '<B>'.$TaxID_Split[12].'</B>'; $pdf->writeHTML($html, true, false, false, false, '');

		$HeadOfficeSplit = str_split($HeadOffice);
        $pdf->SetFont('thsarabun', '', 18); 
        $pdf->SetXY(98, 47);$html = '<B>'.$HeadOfficeSplit[0].'</B>'; $pdf->writeHTML($html, true, false, false, false, '');
        $pdf->SetXY(102, 47);$html = '<B>'.$HeadOfficeSplit[1].'</B>'; $pdf->writeHTML($html, true, false, false, false, '');
        $pdf->SetXY(106, 47);$html = '<B>'.$HeadOfficeSplit[2].'</B>'; $pdf->writeHTML($html, true, false, false, false, '');
        $pdf->SetXY(110, 47);$html = '<B>'.$HeadOfficeSplit[3].'</B>'; $pdf->writeHTML($html, true, false, false, false, '');
        $pdf->SetXY(114, 47);$html = '<B>'.$HeadOfficeSplit[4].'</B>'; $pdf->writeHTML($html, true, false, false, false, '');

        $pdf->SetFont('thsarabun', '', 18); $pdf->SetXY(180, 43);
        $html = '<B>'.($YPay+543).'</B>'; $pdf->writeHTML($html, true, false, false, false, '');

        $pdf->SetFont('thsarabun', '', 18); $pdf->SetXY(30, 52);
        $html = '<B>'.$CompNameThai.'</B>'; $pdf->writeHTML($html, true, false, false, false, '');

        $pdf->SetFont('thsarabun', '', 16); $pdf->SetXY(30, 63);
        $html = '<B>'.$comAddress1.'</B>'; $pdf->writeHTML($html, true, false, false, false, '');
        $pdf->SetXY(75, 64); $html = '<B>'.$comAddress3.'</B>'; $pdf->writeHTML($html, true, false, false, false, '');
        $pdf->SetXY(30, 69); $html = '<B>'.$comAddress2.'</B>'; $pdf->writeHTML($html, true, false, false, false, '');
        $pdf->SetXY(77, 69); $html = '<B>'.$comAddress4.'</B>'; $pdf->writeHTML($html, true, false, false, false, '');
        $pdf->SetXY(35, 74); $html = '<B>'.$amp.'</B>'; $pdf->writeHTML($html, true, false, false, false, '');
        $pdf->SetXY(77, 74); $html = '<B>'.$pv.'</B>'; $pdf->writeHTML($html, true, false, false, false, '');
        $postcode_com_split = str_split($postcode_com);
        $pdf->SetXY(38, 80); $html = '<B>'.$postcode_com_split[0].'</B>'; $pdf->writeHTML($html, true, false, false, false, '');
        $pdf->SetXY(42, 80); $html = '<B>'.$postcode_com_split[1].'</B>'; $pdf->writeHTML($html, true, false, false, false, '');
        $pdf->SetXY(46, 80); $html = '<B>'.$postcode_com_split[2].'</B>'; $pdf->writeHTML($html, true, false, false, false, '');
        $pdf->SetXY(50, 80); $html = '<B>'.$postcode_com_split[3].'</B>'; $pdf->writeHTML($html, true, false, false, false, '');
        $pdf->SetXY(54, 80); $html = '<B>'.$postcode_com_split[4].'</B>'; $pdf->writeHTML($html, true, false, false, false, '');

        $pdf->SetFont('thsarabun', '', 30); $pdf->SetXY(123, 63);
        $html = '<B>/</B>'; $pdf->writeHTML($html, true, false, false, false, '');

        $pdf->SetFont('thsarabun', '', 30); $pdf->SetXY(92, 112);
        $html = '<B>/</B>'; $pdf->writeHTML($html, true, false, false, false, '');
        $pdf->SetFont('thsarabun', '', 18); $pdf->SetXY(178, 113);
        $html = '<B>'.$totalpage.'</B>'; $pdf->writeHTML($html, true, false, false, false, '');

        $pdf->SetFont('thsarabun', '', 16); $pdf->SetXY(120, 153);
        $html = '<B>'.$totalpeople.'</B>'; $pdf->writeHTML($html, true, false, false, false, '');
        $Grand_salary = explode(".", number_format($Grand_salary,2));
        $Grand_tax = explode(".", number_format($Grand_tax,2));
        $pdf->SetXY(139, 153); $html = '<B>'.$Grand_salary[0].'</B>'; $pdf->writeHTML($html, true, false, false, false, '');
        $pdf->SetXY(178, 153); $html = '<B>'.$Grand_tax[0].'</B>'; $pdf->writeHTML($html, true, false, false, false, '');
        $pdf->SetXY(155, 153); $html = '<B>'.$Grand_salary[1].'</B>'; $pdf->writeHTML($html, true, false, false, false, '');
        $pdf->SetXY(186, 153); $html = '<B>'.$Grand_tax[1].'</B>'; $pdf->writeHTML($html, true, false, false, false, '');

        $pdf->SetXY(120, 201);$html = '<B>'.$totalpeople.'</B>'; $pdf->writeHTML($html, true, false, false, false, '');
        $pdf->SetXY(139, 201);$html = '<B>'.$Grand_salary[0].'</B>'; $pdf->writeHTML($html, true, false, false, false, '');
        $pdf->SetXY(178, 201);$html = '<B>'.$Grand_tax[0].'</B>'; $pdf->writeHTML($html, true, false, false, false, '');
        $pdf->SetXY(155, 201); $html = '<B>'.$Grand_salary[1].'</B>'; $pdf->writeHTML($html, true, false, false, false, '');
        $pdf->SetXY(186, 201); $html = '<B>'.$Grand_tax[1].'</B>'; $pdf->writeHTML($html, true, false, false, false, '');

        $pdf->SetXY(92, 233);$html = '<B>'.$BossName.'</B>'; $pdf->writeHTML($html, true, false, false, false, '');
        $pdf->SetXY(98, 238);$html = '<B>'.$BossPosition.'</B>'; $pdf->writeHTML($html, true, false, false, false, '');
        $pdf->SetFont('thsarabun', '', 18); $pdf->SetXY(82, 243);
        $html = '<B>'.$datesign[2].'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$FULL_MONTH2[$datesign[1]].'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.($datesign[0]+543).'</B>'; $pdf->writeHTML($html, true, false, false, false, '');


        // page2

        $pdf->AddPage('P', 'A4');
        $bMargin = $pdf->getBreakMargin();
        $auto_page_break = $pdf->getAutoPageBreak();
        $pdf->SetAutoPageBreak(false, 0);
        $pdf->Image($path_cert2, 0, 0, 210, 0,'jpg', '#', '', true, 150, '', false, false, 1, false, false, false);
        $pdf->SetAutoPageBreak($auto_page_break, $bMargin);
        $pdf->setPageMark();

        $pdf->Output($filename, 'I');
    }else{
?>
<head>
  <meta charset="UTF-8">
</head>
<style type="text/css">
@import url(<?php echo base_url();?>assets/fonts/thsarabunnew.css);

*{ margin: 0; padding: 0; }
body{ font-family: 'THSarabunNew', sans-serif; font-size: 0.5em; line-height: 1.7em; }
p{ padding: 0 0 0 0; }
.n{ font-weight: normal; font-style: normal; }
.b{ font-weight: bold; font-style: normal; }
.i{ font-weight: normal; font-style: italic; }
.bi{ font-weight: bold; font-style: italic; }
ul,ol{padding: 0 0 10px 0;}
li{ margin: 0 0 0 2em; padding: 0 0 5px 0; }
code{ background: #FFF; }
#container{ width: 900px; margin: 20px auto 0; border: 1px solid #333; padding: 20px; background: #FFF; }
#instruction{ padding: 10px 10px 0; background: #e6e6e6; }
#footer{ width: 940px; margin: 0 auto; padding: 10px 0 20px; }
.right{text-align: right;}
.type{ font-family: 'THSarabunNew', sans-serif; height: 5em; width: 98%;font-size: 1em;clear: both;margin: 20px auto 10px;padding: 5px 1%; }
table {
    border-collapse: collapse;
}
.pagebreak { page-break-before: always; }
span {
  display: inline-block;
  width: 200px;
}

</style>
<body style="padding: 20px;">
<p class="pagebreak" style="display: none;">
<table border="1" width="100%">
	<tr>
		<td style="padding: 15px;">
			<table width="100%">
				<tr>
					<td colspan="3" width="100%" align="center"><h2>สรุปรายการภาษีที่นำส่ง  ภ.ง.ด.1 ก</h2></td>
				</tr>		
				<tr>
					<td colspan="3" width="100%" align="center">ประจำเดือน <?php echo $nameMonth;?> พ.ศ. <?php echo $y;?></td>
				</tr>
				<tr>
					<td width=33%>&nbsp;</td>
					<td width="33%">เลขประจำตัวผู้เสียภาษีอากร&nbsp;&nbsp;<span align="center" style="border: 1px solid black;"><?php echo $TAXID;?></span><BR>(ของผู้มีหน้าที่หัก ณ ที่จ่าย)</td>
					<td width="33%" valign="top">สาขาที่ &nbsp;&nbsp;<span align="center" style="border: 1px solid black;"><?php echo $HeadOffice;?></span></td>
				</tr>
				<tr>
					<td colspan="3">
						<table border="1" width="100%">
							<tr>
								<td height="50px;" align="center"><b>รายการ</b></td>
								<td height="50px;" align="center"><b>จำนวนราย</b></td>
								<td height="50px;" align="center"><b>เงินได้ทั้งสิ้น</b></td>
								<td height="50px;" align="center"><b>ภาษีที่นำส่งทั้งสิ้น</b></td>
							</tr>
							<tr>
								<td style="padding: 15px;">1. เงินได้ตามมาตรา 40 (1) เงินเดือน เงินจ้าง ฯลฯ กรณีทั่วไป</td>
								<td align="center"><b><?php echo $totalpeople; ?></b></td>
								<td align="center"><b><?php echo number_format($Grand_salary,2); ?></b></td>
								<td align="center"><b><?php echo number_format($Grand_tax,2); ?></b></td>
							</tr>
							<tr>
								<td style="padding: 15px;">2. เงินได้ตามมาตรา 40 (1) เงินเดือน เงินจ้าง ฯลฯ กรณีได้รับอนุมัติจากกรมสรรพากรให้หักอัตราร้อยละ 3</td>
								<td align="center"><b></b></td>
								<td align="center"><b></b></td>
								<td align="center"><b></b></td>
							</tr>
							<tr>
								<td style="padding: 15px;">3. เงินได้ตามมาตรา 40 (1)(2) กรณีนายจ้างจ่ายให้ครั้งเดียวเพราะเหตุออกจากงาน</td>
								<td align="center"><b></b></td>
								<td align="center"><b></b></td>
								<td align="center"><b></b></td>
							</tr>
							<tr>
								<td style="padding: 15px;">4. เงินได้ตามมาตรา 40 (2) กรณีผู้รับเงินได้เป็นผู้อยู่ในประเทศไทย</td>
								<td align="center"><b></b></td>
								<td align="center"><b></b></td>
								<td align="center"><b></b></td>
							</tr>
							<tr>
								<td style="padding: 15px;">5. เงินได้ตามมาตรา 40 (2) กรณีผู้รับเงินมิได้เป็นผู้อยู่ในประเทศไทย</td>
								<td align="center"><b></b></td>
								<td align="center"><b></b></td>
								<td align="center"><b></b></td>
							</tr>
							<tr>
								<td style="padding: 15px;" align="right"><b>รวม</b></td>
								<td align="center"><b><?php echo $totalpeople; ?></b></td>
								<td align="center"><b><?php echo number_format($Grand_salary,2); ?></b></td>
								<td align="center"><b><?php echo number_format($Grand_tax,2); ?></b></td>
							</tr>
						</table>
						<BR>
						<HR>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</p>
<?php 
$p=1;
$i=1;
	while($p<=$totalpage){
?>
<p class="pagebreak">	
<table border="1" width="100%">
	<tr>
		<td style="padding: 10px;" width="100%">
			<table width="100%">
				<tr>
					<td >
						<p><h2>ใบแนบ ภ.ง.ด.1 ก</h2></p>
					</td>
					<td width="60%" align="center">
						<p>เลขประจำตัวผู้เสียภาษีอากร (ของผู้มีหน้าที่หักภาษี ณ ที่จ่าย) | &nbsp;&nbsp;<span align="center" style="border: 1px solid black;"><?php echo $TAXID;?></p>
					</td>
					<td align="right">
						<p>สาขาที่&nbsp;&nbsp;<span align="center" style="border: 1px solid black;"><?php echo $HeadOffice;?></p>
					</td>
				</tr>
			</table>
			<table width="100%" ><tr>
				<td width="75%">
					<table width="100%" border="1">
					<tr><td style="padding: 10px;" width="100%"><table width="100%">
						<tr>
							<td colspan="3">
								<p style="font-size:80%;">(ให้แยกกรอกรายการในใบต่อนี้ตามแต่ละอัตราภาษีหัก ณ ที่จ่าย โดยใส่เครื่องหมาย <img src="<?php echo base_url();?>modules/hrreport/views/checkbox.png" width="18">  ลงใน หน้าข้อความแล้วแต่กรณี)</p>
							</td>
						</tr>
						<tr>
							<td><p style="font-size:80%;">ประเภท&nbsp;&nbsp;</p></td>
							<td><p style="font-size:80%;">
								<img src="<?php echo base_url();?>modules/hrreport/views/checkbox.png" width="18">&nbsp;(1) เงินได้ตามมาตรา 40(1) เงินเดือน ค่าจ้าง ฯลฯ กรณีทั่วไป เงินได้ </p></td>
							<td><p style="font-size:80%;">&nbsp;&nbsp;<img src="<?php echo base_url();?>modules/hrreport/views/checkboxnone.png" width="18">&nbsp;(3) เงินได้ตามมาตรา 40(1) (2) กรณีนายจ้างจ่ายให้ครั้งเดียวเพราะเหตุออกจากงาน  </p></td>
						</tr>
						<tr>
							<td><p style="font-size:80%;">เงินได้&nbsp;&nbsp;</p></td>
							<td><p style="font-size:80%;">
								<img src="<?php echo base_url();?>modules/hrreport/views/checkboxnone.png" width="18">&nbsp;(2) เงินได้ตามมาตรา 44 1) เงินเดือน ค่าจ้าง ฯลฯ</p></td>
							<td><p style="font-size:80%;">&nbsp;&nbsp;<img src="<?php echo base_url();?>modules/hrreport/views/checkboxnone.png" width="18">&nbsp;(4) เงินได้ตามมาตรา 40(2) กรณีผู้รับเงินได้เป็นผู้อยู่ในประเทศไทย</p></td>
						</tr>
						<tr>
							<td></td>
							<td><p style="font-size:80%;">กรณีได้รับอนุมัติจากกรมสรรพากร ให้หักอัตราร้อยละ 3</p></td>
							<td><p style="font-size:80%;">&nbsp;&nbsp;<img src="<?php echo base_url();?>modules/hrreport/views/checkboxnone.png" width="18">&nbsp;(5) เงินได้ตามมาตรา 40(2) กรณีผู้รับเงินได้มิได้เป็นผู้อยู่ในประเทศไทย</p></td>
						</tr>
					</table></td></tr></table>
				</td>
				<td align="right" width="25%">
					<p>แผ่นที่  <?php echo $p;?> ในจำนวน <?php echo $totalpage;?> แผ่น</p>
				</td>
			</tr>
			</table>
			&nbsp;
			<table width="100%">
				<tr style="border:1px solid black;">
					<td style="border:1px solid black;" align="center"><b>ลำดับที่</b></td>
					<td style="border:1px solid black;" align="center" colspan="2" width="25%"><b>เลขประจำตัวผู้เสียภาษีอากร (ของผู้มีเงินได้)</b></td>
					<td style="border:1px solid black;" align="left" width="40%">
						<b>
							ชื่อผู้มิเงินได้(ให้ระบุให้ชัดเจนว่า นาย นาง นางสาวหรือยศ)<BR>ที่อยู่ผู้มิเงินได้(ให้ระบุเลขที่ตรอก/ซอย ถนน ตำบล/แขวง อำเภอ/เขต จังหวัด)
						</b>
					</td>
					<td style="border:1px solid black;" align="center" colspan="2"><b>จำนวนเงินที่จ่ายทั้งปี</b></td>
					<td style="border:1px solid black;" align="center" colspan="2"><b>จำนวนเงินที่หักภาษี<BR>และนำส่งในครั้งนี้</b></td>
					<td style="border:1px solid black;" align="center"><b>*<BR>เงื่อนไข</b></td>
				</tr>
				<?php 
				// $i=1;
				$totalsalary = 0;
				$totalss = 0;
				while( ($i<=($p*7)) && ($i<=$totalpeople) ){
					$fname = explode(" ",$name_arr[$i-1]);
				?>
				<tr style="border:1px solid black;">
					<td style="border:1px solid black;" align="center"><b><?php echo $i;?></b></td>
					<td align="left" style="padding: 5px;" >
						<span align="center" style="border: 1px solid black;"><?php echo $this->hrreportModel->taxformat($id13_arr[$i-1]);?></span>
					</td>
					<td style="border-left:0px;">
						ชื่อ<BR>ที่อยู่
					</td>
					<td style="border:1px solid black;">
						<span>&nbsp;<?php echo $emp_title[$i-1]." ".$emp_firstname[$i-1];?></span>
						<span>ชื่อสกุล&nbsp;<?php echo $emp_lastname[$i-1];?></span><BR>
						&nbsp;<u><?php echo $emp_address[$i-1];?></u>
					</td>
					<td style="border:1px solid black;text-align:right;padding-right:10px;"><?php echo number_format($salary_arr[$i-1]);?></td>
					<td style="border:1px solid black;text-align:center;" width="35px">
						<?php $tmp = explode(".", number_format($salary_arr[$i-1],2));echo $tmp[1];?>
					</td>
					<td style="border:1px solid black;text-align:right;padding-right:10px;">
						<?php $tmp = explode(".", number_format($ssallowance_arr[$i-1],2));echo $tmp[0];?>
					</td>
					<td style="border:1px solid black;text-align:center;" width="35px">
						<?php $tmp = explode(".", number_format($ssallowance_arr[$i-1],2));echo $tmp[1];?>
					</td>
					<td style="border:1px solid black;text-align:center;padding-right:10px;"><?php echo $PITCode[$i-1];?></td>
				</tr>
				<?php

				$totalsalary += round($salary_arr[$i-1],2);
				$totalss += $ssallowance_arr[$i-1];
				$i++;
			}
				?>
				<tr>
					<td style="border:1px solid black;text-align:right;padding-right:10px;"colspan="4"><b>รวมยอดเงินได้และภาษีที่นำส่ง (นำไปรวมกับใบแนบ ภ.ง.ด.1 ก แผ่นอื่น (ถ้ามี))&nbsp;&nbsp;&nbsp;</b></td>
					<td style="border:1px solid black;text-align:right;padding-right:10px;"><?php echo number_format($totalsalary);?></td>
					<td style="border:1px solid black;text-align:center;" width="35px">
						<?php $tmp = explode(".", number_format($totalsalary,2));echo $tmp[1];?>
					</td>
					<td style="border:1px solid black;text-align:right;padding-right:10px;">
						<?php $tmp = explode(".", number_format($totalss,2));echo $tmp[0];?>
						
					</td>
					<td style="border:1px solid black;text-align:center;" width="35px">
						<?php $tmp = explode(".", number_format($totalss,2));echo $tmp[1];?>
					</td>
					<td style="border:1px solid black;text-align:right;padding-right:10px;">&nbsp;</td>
				</tr>
				<tr>
					<td colspan="9" width="100%">
						<table width="100%">
							<tr>
								<td style="border:1px solid black;text-align:left;padding-right:10px;" width="50%">
									<p style="font-size:80%;">(ให้กรอกลำดับที่ต่อเนื่องกันไปทุกแผ่นตามเงินได้แต่ละประเภท)</p>
									<p style="font-size:80%;">หมายเหตุ * เงื่อนไขการหักภาษี ให้กรอกดังนี้</p>
									<p style="font-size:80%;"><span></span><span>- หัก ณ ที่จ่าย </span><span>กรอก 1</span></p>
									<p style="font-size:80%;"><span></span><span>- ออกให้ตลอดไป </span><span>กรอก 2</span></p>
									<p style="font-size:80%;"><span></span><span>- ออกให้ครั้งเดียว </span><span>กรอก 3</span></p>
								</td>
								<td style="border:1px solid black;text-align:center;">
									   <BR><BR>ลงชื่อ...................................................................................................ผู้จ่ายเงิน
									   <BR><?php echo $BossName;?>
									   <BR><?php echo $BossPosition;?>
									   <BR>ยื่นวันที่ <?php echo $datesign[2];?> เดือน  <?php echo $FULL_MONTH2[$datesign[1]];?> <?php echo $datesign[0]+543;?>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</p>
<?php
$p++;
}
?>
</body>
<script type="text/javascript">window.print();</script>
<?php } ?>