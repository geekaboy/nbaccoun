<script type="text/javascript">
  $(document).ready(function(){
    //เพิ่มเงื่อนไขตาราง
    $('.js-exportable').DataTable({
        dom: 'Blfrtip',responsive: true,paging: true,info: true,
        buttons: [
            {extend:'copyHtml5',text:' <i class="fa fa-files-o fa-2x "></i> ',titleAttr: ' Copy '},
            {extend:'excelHtml5',text:' <i class="fa fa-file-excel-o fa-2x "></i> ',titleAttr: ' Excel '},
            {extend:'print',text:' <i class="fa fa-print fa-2x "></i> <BR>&nbsp;',titleAttr: ' Print '}
          ,
        ]
        ,"lengthMenu": [ [10, 25, 50, -1], [10, 25, 50, "All"] ], 
      });

    $('.js-exportable2').DataTable({
        dom: 'Bfrtip',responsive: true,paging: true,info: true,
        buttons: [
            {extend:'copyHtml5',text:' <i class="fa fa-files-o fa-2x "></i> ',titleAttr: ' Copy '},
            {extend:'excelHtml5',text:' <i class="fa fa-file-excel-o fa-2x "></i> ',titleAttr: ' Excel '},
            {extend:'print',text:' <i class="fa fa-print fa-2x "></i> ',titleAttr: ' Print '
            // ,exportOptions:{columns:[0,1,2,3,4,5,6]}
          },
        ]
        // ,"columnDefs": [{"targets": [ 0,1,2,3,4 ],"visible": false,"searchable": true}]
      });
//กรณีต้องการปิดการแสดงบาง column แต่สามารถ export file ได้
    $('.js-exportable3').DataTable({
        dom: 'Bfrtip',responsive: true,paging: true,info: true,
        buttons: [
            {extend:'copyHtml5',text:' <i class="fa fa-files-o fa-2x "></i> ',titleAttr: ' Copy '},
            {extend:'excelHtml5',text:' <i class="fa fa-file-excel-o fa-2x "></i> ',titleAttr: ' Excel '},
            {extend:'print',text:' <i class="fa fa-print fa-2x "></i> ',titleAttr: ' Print '
            ,exportOptions:{columns:[0,1,2,3,4,5,6]}
          },
        ],
        "columnDefs": [{"targets": [ 5 ],"visible": false,"searchable": true}]
      });
  });
</script>
<style>
    @page {
        size: A4;
        margin: 1cm;
    }

    .print {
        display: none;
    }

    @media print {
        div.fix-break-print-page {
            page-break-inside: avoid;
        }

        .print {
            display: block;
        }
    }

    .print:last-child {
        page-break-after: auto;
    }

</style>
<!-- begin add require script -->
<head>
  <link href="<?php echo base_url();?>assets/bootstrap-select/css/bootstrap-select.css" rel="stylesheet">
  <link href="<?php echo base_url();?>assets/bootstrap/css/datepicker.css" rel="stylesheet">
  <link href="<?php echo base_url();?>assets/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">
  <meta charset="UTF-8">
  
  <style>
  @media print {
    canvas {
      min-height: 100%;max-width: 100%;max-height: 100%;height: auto!important;width: auto!important;
    }
    table {
      min-height: 100%;max-width: 100%;max-height: 100%;height: auto!important;width: auto!important;
      page-break-inside: : auto;
    }
    tr{
      page-break-inside: avoid;
      page-break-after: auto;

    }
  }
  canvas{-moz-user-select: none;-webkit-user-select: none;-ms-user-select: none;}
  </style>
</head>

<!-- end add require script -->
<div class="box box-success">
  <div class="box-header" align="left">
        <i class="fa fa-sign-in"></i>
    <h3 class="box-title"><?php echo $this->systemmodel->changelng($this->systemmodel->get_menuname($this->uri->segment(1) . '/' . $this->uri->segment(2)));?></h3>
  </div>
  <div align="center">
  <div class="box-body">
    <!-- ส่วนแสดงผล -->

<div class="box box-success">
  <div class="box-header">
    <?php
    $this->load->model('HrreportModel'); 
     
     $FULL_MONTH2 = array(
        "01" => "มกราคม",
        "02" => "กุมภาพันธ์",
        "03" => "มีนาคม",
        "04" => "เมษายน",
        "05" => "พฤษภาคม",
        "06" => "มิถุนายน",
        "07" => "กรกฏาคม",
        "08" => "สิงหาคม",
        "09" => "กันยายน",
        "10" => "ตุลาคม",
        "11" => "พฤศจิกายน",
        "12" => "ธันวาคม"
    );
      $mname="";
      $mnametxt="";
      foreach($results_paymentperiod as $result){ 
        $YPay = $result->PPYear;
        $MPay = substr("00".$result->PPMonth,-2);
        $mname=substr("00".$result->PPMonth,-2);
        $mnametxt=date('F', mktime(0, 0, 0, $result->PPMonth, 10));
        $dateToTest = $result->PPYear."-".$mname."-01";
        $lastdaytxt = $result->PPYear."-".$mnametxt."-".date('t',strtotime($dateToTest));
        $lastday = $result->PPYear."-".$mname."-".date('t',strtotime($dateToTest));
        $dlastday = date('t',strtotime($dateToTest));
        // echo "<h2>".$this->systemmodel->changelng("งวดการจ่ายเงิน")." | $lastdaytxt";
      }?> 
    </h2>
    <form class="" name="form1" action="WithholdingTax50tavi"  method="post">
        <div class="row clearfix">
            <div class="col-md-5">
            <p><b>Select Date</b></p>
            <div class="form-group">
                <div class='input-group date' id='datetimepicker1'>
                    <input type='text' class="form-control" id='showyear' name="showyear" value="<?php echo $showyear;?>" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>
            <script>
              $(document).ready(function(){
                var date_input=$('input[name="showyear"]'); //our date input has the name "date"
                var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
                date_input.datepicker({
                  format: 'yyyy',
                  container: container,
                  todayHighlight: true,
                  autoclose: true,
                  viewMode:"years",
                  minViewMode:"years"
                })
              })
            </script>
            </div>
          <div class="col-md-5">
            <p><b>Select Dateaaa</b></p>
            <div class="form-group">
                <div class='input-group date' id='datetimepicker1'>
                    <input type='text' class="form-control" id='dateset' name="dateset" value="<?php echo $datesign[0]."-".$datesign[1]."-".$datesign[2];?>" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>
            <script>
              $(document).ready(function(){
                var date_input=$('input[name="dateset"]'); //our date input has the name "date"
                var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
                date_input.datepicker({
                  format: 'yyyy-mm-dd',
                  container: container,
                  todayHighlight: true,
                  autoclose: true,
                  viewMode:"days",
                  minViewMode:"days"
                })
              })
            </script>
          </div>

            <div class="col-md-2" align="center">
              <p>&nbsp;</p>
              <button type="submit" class="btn btn-primary btn-md btn-block" id="btn_reportgraph">แสดงรายงาน</button>
            </div>
          </div>
          <div class="row clearfix">
            <div class="col-md-4" align="center">
              <button type="button" class="btn btn-info btn-md btn-block" onclick="printAll(1)"> <span class="glyphicon glyphicon-print"></span>&nbsp;<b>หนังสือรับรองเงินเดือน</b>&nbsp;&nbsp;<img src="<?=base_url();?>assets/images/ajax-loader.gif" id="loading1" style="display: none;" ></button>
            </div>
            <div class="col-md-4" align="center">
              <button type="button" class="btn btn-warning btn-md btn-block" onclick="printAll(2)"> <span class="glyphicon glyphicon-print"></span>&nbsp;<b>หนังสือรับรองการทำงาน</b>&nbsp;&nbsp;<img src="<?=base_url();?>assets/images/ajax-loader.gif" id="loading2" style="display: none;" ></button>
            </div>
            <div class="col-md-4" align="center">
              <button type="button" class="btn btn-success btn-md btn-block" onclick="printAll(3)"> <span class="glyphicon glyphicon-print"></span>&nbsp;<b> ทั้ง 2 ฉบับทั้งหมด</b>&nbsp;&nbsp;<img src="<?=base_url();?>assets/images/ajax-loader.gif" id="loading3" style="display: none;" ></button>
            </div>
        </div>
      </form>

  </div>
  
  <div class="box-body" align="left" id="resultDIV">
	
        <!-- ตาราง แสดงข้อมูล -->
        <table class="table table-bordered table-striped table-hover dataTable  js-exportable" id="tabledata">
		
          <thead>
            <tr>
              <th scope="col"><center><?=$this->systemmodel->changelng("ลำดับที่")?></center></th>
              <th scope="col"><center><?=$this->systemmodel->changelng("ชื่อ")?></center></th>
              <th scope="col"><center><?=$this->systemmodel->changelng("วันที่เริ่มงาน")?></center></th>
              <th scope="col"><center><?=$this->systemmodel->changelng("จำนวนเงินที่จ่าย")?></center></th>
              <th scope="col"><center><?=$this->systemmodel->changelng("ภาษีที่หัก<BR>และนำส่งไว้")?></center></th>
              <th scope="col"><center><?=$this->systemmodel->changelng("หนังสือรับรอง")?></center></th>
              <th scope="col"><center><?=$this->systemmodel->changelng("พิมพ์")?></center></th>
            </tr>
          </thead>
          <tbody>
          <?php 
        $i=0;

        if(!empty($results_All)){
          foreach ($results_All as $row) {
            $EmployeeID = $row->EmployeeID;

            $NetToPayTAX = 0;
            $Tax = 0;
            $SSAllowance=0;
            // echo $EmployeeID.":".$showyear."<BR>";
            for ($k=1; $k <= 12 ; $k++) { 
              $data = $this->hrreportModel->getMultiDATA($EmployeeID,$showyear,$k,1);

              if($data['isinreport']){
                $NetToPayTAX += $data['NetToPayTAX'];
                $Tax += $data['Tax'];
                $SSAllowance += $data['SS'];
              }
            }

            // $data = $this->PayrollreportModel->getMultiDATA($EmployeeID,$YPay,$MPay,0);

            $Effectdate = $data['Effectdate'];
            // $WorkingDays = $data['WorkingDays'];
            // $SalaryCal = $data['SalaryCal'];
            // $SalaryAdj = $data['SalaryAdj'];
            // $TaxAllowance = $data['TaxAllowance'];
            // $SSAllowance = $data['SSAllowance'];
            // $TotalIncome = $data['TotalIncome'];
            // $Tax = $data['Tax'];
            // $SS = $data['SS'];
            $isinreport = $data['isinreport'];
            // $TotalDeduct = $data['TotalDeduct'];
            // $NetToPay = $data['NetToPay'];
            // $data_inc = $data['data_inc'];
            // $data_ded = $data['data_ded'];
            if($row->ProvinceID == 1){ $tm="แขวง";$amp=""; }else{  $tm="ต.";$amp="อ.";}

            $TAXID = $row->TaxID;
			
            // $TAXID = $this->PayrollreportModel->taxformat($TAXID);
            $comAddress = $row->CompAddress1." ".$row->CompAddress2." ".$row->CompAddress3." ".$row->CompAddress4." ".$row->District." ".$row->Province." ".$row->Postcode;

            // if($isinreport){
          ?>
            <tr>
              <td><?php echo $i+1;?></td>
              <td><?php echo $row->TitleEN." ".$row->FirstName." ".$row->LastName." (".$row->FullNameLL.")";?></td>
              <td ><?php echo $Effectdate;?></td>
              <td align="center"><?php echo number_format($NetToPayTAX,2);?></td>
              <td align="right"><?php echo number_format($Tax,2);?></td>                            
              <td >
                <center>
                  <button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#myModal1<?=$i;?>"><b>DOC</b> <center></button>
                  <!-- <button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#myModal2<?=$i;?>"><b>2</b> <center></button> -->
                </center>
              </td>
              <td width="125px">
                <center>
                  <button type="button" class="btn btn-success btn-xs" onclick="printDiv('<?=$i;?>',1)"><span class="glyphicon glyphicon-print"></span> <B>1</B></button> 
                  <button type="button" class="btn btn-success btn-xs" onclick="printDiv('<?=$i;?>',2)"><span class="glyphicon glyphicon-print"></span> <B>2</B></button> 
                  <button type="button" class="btn btn-success btn-xs" onclick="printDiv('<?=$i;?>',3)"><span class="glyphicon glyphicon-print"></span> <B>ALL</B></button> 
                </center>                

                <!-- Modal 1 -->
                <div class="modal fade" id="myModal1<?=$i;?>" role="dialog">
                  <div class="modal-dialog modal-lg">
                  
                    <!-- Modal content-->
                    <div class="modal-content">
                      <div class="modal-body">
                      	<table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-size: 12px;">
                      		<tr><td align="right" id="headertype<?=$i;?>"></td></tr>
                      	</table>
                        <table width="100%" border="1" cellspacing="0" cellpadding="0" style="font-size: 11px;">
                          <tr>
                            <td  style="padding: 2px 2px 2px 2px;">
                              <table width="100%" border="0" cellspacing="0" cellpadding="50">
                                <tr>
                                  <td  style="padding: 2px 2px 2px 2px;" style="font-size: 14px;"><b>หนังสือรับรองการหักภาษี ณ ที่จ่าย</b></td>
                                </tr>
                                <tr>
                                  <td  style="padding: 2px 2px 2px 2px;"> ตามมาตรา  50  ทวิ  แห่งประมวลรัษฎากร </td>
                                  <td  style="padding: 2px 2px 2px 2px;" align="right"> เล่มที่/เลขที่ <?php echo $YPay+543;?>/ <?php echo $i+1;?></td>
                                  <td  style="padding: 2px 2px 2px 2px;" width="150px">&nbsp;&nbsp; </td>
                                </tr>
                              </table>
                            </td>
                          </tr>
                          <tr> <td  style="padding: 2px 2px 2px 2px;" height="5px"></td> </tr>
                          <tr>
                            <td  style="padding: 2px 2px 2px 2px;">
                              <table width="100%" border="0" cellspacing="0" cellpadding="50">
                                <tr>
                                  <td  style="padding: 2px 2px 2px 2px;" colspan="2"> ผู้มีหน้าที่หักภาษี  ณ  ที่จ่าย :- </td>
                                  <td  style="padding: 2px 2px 2px 2px;" align="right"> เลขประจำตัวประชาชน </td>
                                  <td  style="padding: 2px 2px 2px 2px;" width="150px">&nbsp;&nbsp; </td>
                                </tr>
                                <tr>
                                  <td  style="padding: 2px 2px 2px 2px;"> ชื่อ </td>
                                  <td  style="padding: 2px 2px 2px 2px;"> <u><?php echo $row->CompNameThai?></u> </td>
                                  <td  style="padding: 2px 2px 2px 2px;" align="right"> เลขประจำตัวผู้เสียภาษีอากร </td>
                                  <td  style="padding: 2px 2px 2px 2px;">&nbsp;&nbsp;<?php echo $TAXID;?>     </td>
                                </tr>
                                <tr>
                                  <td  style="padding: 2px 2px 2px 2px;"></td>
                                  <td  style="padding: 2px 2px 2px 2px;"> ( ให้ระบุว่าเป็น บุคคล นิติบุคคล บริษัท สมาคม หรือ คณะบุคคล ) </td>
                                </tr>
                                <tr>
                                  <td  style="padding: 2px 2px 2px 2px;"> ที่อยู่ </td>
                                  <td  style="padding: 2px 2px 2px 2px;"><u><?php echo $comAddress; ?></u> </td>
                                </tr>
                                <tr>
                                  <td  style="padding: 2px 2px 2px 2px;"></td>
                                  <td  style="padding: 2px 2px 2px 2px;"> ( ให้ระบุ  เลขที่  ตรอก/ซอย  หมู่ที่  ถนน  ตำบล/แขวง  อำเภอ/เขต  จังหวัด ) </td>
                                </tr>
                              </table>
                            </td>
                          </tr>
                          <tr> <td  style="padding: 2px 2px 2px 2px;" height="5px"></td> </tr>
                          <tr>
                            <td  style="padding: 2px 2px 2px 2px;">
                              <table width="100%" border="0" cellspacing="0" cellpadding="50">
                                <tr>
                                  <td  style="padding: 2px 2px 2px 2px;" colspan="2"> ผู้ถูกหักภาษี  ณ  ที่จ่าย :- </td>
                                  <td  style="padding: 2px 2px 2px 2px;" align="right"> เลขประจำตัวประชาชน </td>
                                  <td  style="padding: 2px 2px 2px 2px;" width="150px">&nbsp;&nbsp; </td>
                                </tr>
                                <tr>
                                  <td  style="padding: 2px 2px 2px 2px;"> ชื่อ </td>
                                  <td  style="padding: 2px 2px 2px 2px;"> <u><?php echo $row->TitleEN." ".$row->FirstName." ".$row->LastName;?></u> </td>
                                  <td  style="padding: 2px 2px 2px 2px;" align="right"> เลขประจำตัวผู้เสียภาษีอากร </td>
                                  <td  style="padding: 2px 2px 2px 2px;">&nbsp;&nbsp;<?php echo $row->CardID;?>     </td>
                                </tr>
                                <tr>
                                  <td  style="padding: 2px 2px 2px 2px;"></td>
                                  <td  style="padding: 2px 2px 2px 2px;"> ( ให้ระบุว่าเป็น บุคคล นิติบุคคล บริษัท สมาคม หรือ คณะบุคคล ) </td>
                                </tr>
                                <tr>
                                  <td  style="padding: 2px 2px 2px 2px;"> ที่อยู่ </td>
                                  <td  style="padding: 2px 2px 2px 2px;"><u><?php echo $row->AddressPermanent." ".$tm.$row->DISTRICT_NAME." ".$amp.$row->AMPHUR_NAME." ".$row->ProvinceNameLocal." ".$row->Postcode?></u> </td>
                                </tr>
                                <tr>
                                  <td  style="padding: 2px 2px 2px 2px;"></td>
                                  <td  style="padding: 2px 2px 2px 2px;"> ( ให้ระบุ  เลขที่  ตรอก/ซอย  หมู่ที่  ถนน  ตำบล/แขวง  อำเภอ/เขต  จังหวัด ) </td>
                                </tr>
                                <?php
                                $img_checkboxnone = '<img src="'.base_url().'modules/hrreport/views/checkboxnone.png" width="18">';
                                $img_checkclose = '<img src="'.base_url().'modules/hrreport/views/checkclose.png" width="18">';
                                ?>
                                <tr>
                                  <td  style="padding: 2px 2px 2px 2px;" colspan="4">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="50">
                                      <tr>
                                        <td  style="padding: 2px 2px 2px 2px;">ลำดับที่&nbsp;&nbsp;<?=$i+1?>&nbsp;&nbsp;ในแบบ </td>
                                        <td  style="padding: 2px 2px 2px 2px;"><?php echo $img_checkclose;?>&nbsp;(1) ภ.ง.ด. 1ก.</td>
                                        <td  style="padding: 2px 2px 2px 2px;"><?php echo $img_checkboxnone;?>&nbsp;(2) ภ.ง.ด. 1ก.พิเศษ</td>
                                        <td  style="padding: 2px 2px 2px 2px;"><?php echo $img_checkboxnone;?>&nbsp;(3) ภ.ง.ด. 2</td>
                                        <td  style="padding: 2px 2px 2px 2px;"><?php echo $img_checkboxnone;?>&nbsp;(4) ภ.ง.ด. 2ก.</td>
                                      </tr>
                                      <tr>
                                        <td  style="padding: 2px 2px 2px 2px;">
                                        (ให้สามารถอ้างอิงหรือสอบยันกันได้
                                        <BR>ระหว่างลำดับที่ตาม หนังสือรับรองฯ
                                        <BR>กับแบบยื่นรายการภาษีหักที่จ่าย)</td>
                                        <td  style="padding: 2px 2px 2px 2px;"><?php echo $img_checkboxnone;?>&nbsp;(5) ภ.ง.ด. 3</td>
                                        <td  style="padding: 2px 2px 2px 2px;"><?php echo $img_checkboxnone;?>&nbsp;(6) ภ.ง.ด. 3ก.</td>
                                        <td  style="padding: 2px 2px 2px 2px;"><?php echo $img_checkboxnone;?>&nbsp;(7) ภ.ง.ด. 53</td>
                                      </tr>
                                    </table>
                                  </td>
                                </tr>
                              </table>
                            </td>
                          </tr>
                          <tr> <td  style="padding: 2px 2px 2px 2px;" height="5px"></td> </tr>
                          <tr>
                            <td  style="padding: 2px 2px 2px 2px;">
                              <table width="100%" border="1" cellspacing="0" cellpadding="50">
                                <tr>
                                  <td  style="padding: 2px 2px 2px 2px;">ประเภทเงินได้พึงประเมินที่จ่าย</td>
                                  <td  style="padding: 2px 2px 2px 2px;">วัน เดือน หรือปีภาษี ที่จ่าย</td>
                                  <td  style="padding: 2px 2px 2px 2px;">จำนวนเงินที่จ่าย</td>
                                  <td  style="padding: 2px 2px 2px 2px;">ภาษีที่หัก และนำส่งไว้</td>
                                </tr>
                                <tr>
                                  <td  style="padding: 2px 2px 2px 2px;">
                                    1.  เงินเดือน ค่าจ้าง เบี้ยเลี้ยง โบนัส ฯลฯ  ตามมาตรา 40 (1)<BR>
                                    2.  ค่าธรรมเนียม  ค่านายหน้า  ฯลฯ  ตามมาตรา 40 (2)<BR>
                                    3.  ค่าแห่งลิขสิทธิ์  ฯลฯ  ตามมาตรา 40 (3)<BR>
                                    4.  (ก) ค่าดอกเบี้ย ฯลฯ  ตามมาตรา 40 (4) (ก)<BR>
                                      (ข) เงินปันผล ส่วนแบ่งของกำไร ฯลฯ ตามมาตรา 40 (4) (ข)<BR>
                                        (1) กรณีผู้ด้รับเงินปันผลได้รับเครดิตภาษี โดยจ่ายจาก<BR>
                                        กำไรสุทธิของกิจการที่ต้องเสียภาษีเงินได้นิติบุคคลในอัตราดังนี้<BR>
                                          (1.1) อัตราร้อยละ 30 ของกำไรสุทธิ<BR>
                                          (1.2) อัตราร้อยละ 25 ของกำไรสุทธิ<BR>
                                          (1.3) อัตราร้อยละ 20 ของกำไรสุทธิ<BR>
                                          (1.4) อัตราอื่น ๆ ( ระบุ ) ..................... ของกำไรสุทธิ<BR>
                                        (2) กิจการที่ได้รับยกเว้นภาษีเงินได้นิติบุคคลซึ่ง ผู้รับเงินปันผลไม่ได้รับเครดิตภาษี<BR>
                                          (2.1) กำไรสุทธิของกิจการที่ได้รับยกเว้นภาษีเงินได้นิติบุคคล<BR>
                                          (2.2) เงินปันผลหรือเงินส่วนแบ่งของกำไรที่ได้รับยกเว้นไม่ต้องนำมารวม<BR>
                                            คำนวณเป็นรายได้เพื่อเสียภาษีนิติบุคคล<BR>
                                          (2.3) กำไรสุทธิส่วนที่ได้หักผลขาดทุนสุทธิยกมาไม่เกิน 5 ปี<BR>
                                            ก่อนรอบระยะเวลาบัญชีปัจจุบัน<BR>
                                          (2.4) กำไรที่รับรู้ทางบัญชีโดยวิธีส่วนได้เสีย (equity method)<BR>
                                          (2.5) อัตราอื่น ๆ ( ระบุ ) .........................................<BR>
                                    5.  การจ่ายเงินได้ที่ต้องหักภาษี ณ. ที่จ่าย ตามคำสั่งกรมสรรพากรที่ออกตาม<BR>
                                      มาตรา 3 เตรส เช่น รางวัล ส่วนลดหรือประโยชน์ใดๆ เนื่องจากการส่งเสริมการขาย<BR>
                                      รางวัลในการประกวด การแข่งขัน การชิงโชค ค่าแสดงของนักแสดงสาธารณะ<BR>
                                      ค่าบริการ ค่าขนส่ง ค่าจ้างทำของ ค่าจ้างโฆษณา ค่าเช่า ค่าเบี้ยประกันวินาศภัย ฯลฯ<BR>
                                    6.  อื่นๆ(ระบุ)                   หัก<BR>
                                  </td>
                                  <td  style="padding: 2px 2px 2px 2px;" align="center" valign="top">31/12/<?=$showyear+543?></td>
                                  <td  style="padding: 2px 2px 2px 2px;" align="right" valign="top"><?php echo number_format($NetToPayTAX,2);?>&nbsp;</td>
                                  <td  style="padding: 2px 2px 2px 2px;" align="right" valign="top"><?php echo number_format($Tax,2);?>&nbsp;</td>
                                </tr>
                                <tr>
                                  <td  style="padding: 2px 2px 2px 2px;" align="right" colspan="2">รวมเงินที่จ่าย และ ภาษีที่นำส่ง</td>
                                  <td  style="padding: 2px 2px 2px 2px;" align="right"><?php echo number_format($NetToPayTAX,2);?>&nbsp;</td>
                                  <td  style="padding: 2px 2px 2px 2px;" align="right"><?php echo number_format($Tax,2);?>&nbsp;</td>
                                </tr>
                                <tr>
                                  <td  style="padding: 2px 2px 2px 2px;" colspan="4">รวมเงินภาษีที่นำส่ง (ตัวอักษร) <span>-- <?php echo $this->hrreportModel->convert(number_format($Tax,2));?> --</span></td>
                                </tr>
                              </table>
                            </td>
                          </tr>
                          <tr>
                            <td  style="padding: 2px 2px 2px 2px;">
                              <table width="100%" border="0" cellspacing="0" cellpadding="50">
                                <tr>
                                  <td  style="padding: 2px 2px 2px 2px;"> เงินสะสมจ่ายเข้ากองทุนสำรองเลี้ยงชีพ ใบอนุญาตเลขทึ่ </td>
                                  <td  style="padding: 2px 2px 2px 2px;"> จำนวนเงิน </td>
                                  <td  style="padding: 2px 2px 2px 2px;">  </td>
                                  <td  style="padding: 2px 2px 2px 2px;"> บาท </td>
                                </tr>
                                <tr>
                                  <td  style="padding: 2px 2px 2px 2px;"> เงินสมทบจ่ายเข้ากองทุนประกันสังคม </td>
                                  <td  style="padding: 2px 2px 2px 2px;"> จำนวนเงิน </td>
                                  <td  style="padding: 2px 2px 2px 2px;"> <?php echo number_format($SSAllowance,2) ;?> </td>
                                  <td  style="padding: 2px 2px 2px 2px;"> บาท </td>
                                </tr>
                              </table>
                            </td>
                          </tr>
                          <tr>
                            <td  style="padding: 2px 2px 2px 2px;">
                              <table width="100%" border="1" cellspacing="0" cellpadding="50">
                                <tr>
                                  <td  style="padding: 2px 2px 2px 2px;">
                                    ผู้จ่ายเงิน<br>
                                    <?php 
                                    if($row->PITCode==1){
                                    	$chk1= $img_checkclose;
                                    	$chk2= $img_checkboxnone;
                                    	$chk3= $img_checkboxnone;
                                    	$chk4= $img_checkboxnone;
                                    }elseif($row->PITCode==2){
                                    	$chk2= $img_checkclose;
                                    	$chk1= $img_checkboxnone;
                                    	$chk3= $img_checkboxnone;
                                    	$chk4= $img_checkboxnone;
                                    }elseif($row->PITCode==3){
                                    	$chk3= $img_checkclose;
                                    	$chk1= $img_checkboxnone;
                                    	$chk2= $img_checkboxnone;
                                    	$chk4= $img_checkboxnone;
                                    }else{
                                    	$chk1= $img_checkboxnone;
                                    	$chk2= $img_checkboxnone;
                                    	$chk3= $img_checkboxnone;
                                    	$chk4= $img_checkboxnone;
                                    }
                                    ?>
                                    <?php echo $chk1;?>หักภาษี  ณ  ที่จ่าย<br>
                                    <?php echo $chk2;?>ออกภาษีให้ตลอดไป<br>
                                    <?php echo $chk3;?>ออกภาษีให้ครั้งเดียว<br>
                                    <?php echo $chk4;?>อื่นๆ (ระบุ) 
                                  </td>
                                  <td  style="padding: 2px 2px 2px 2px;">
                                    ขอรับรองว่าข้อความและตัวเลขดังกล่าวข้างต้น ถูกต้องตรงกับความจริงทุกประการ<br><br><br>
                                    ลงชื่อ  <?php echo $row->BossName;?>         ผู้มีหน้าที่หักภาษี ณ ที่จ่าย<br>
                                    <?php echo $datesign[2];?> เดือน  <?php echo $FULL_MONTH2[$datesign[1]];?> <?php echo $showyear+543;?>         วัน เดือน ปี  ที่ออกหนังสือรับรองฯ
                                  </td>
                                </tr>
                              </table>
                            </td>
                          </tr>
                          <tr>
                            <td  style="padding: 2px 2px 2px 2px;">
                              <table width="100%" border="0" cellspacing="0" cellpadding="50">
                                <tr>
                                  <td  style="padding: 2px 2px 2px 2px;" valign="top"> คำเตือน</td>
                                  <td  style="padding: 2px 2px 2px 2px;" valign="top"> 
                                    ผู้มีหน้าที่ออกหนังสือรับรองการหักภาษี ณ. ที่จ่าย<br>
                                    ฝ่าฝืนไม่ปฏิบัติตามมาตรา  50  ทวิ  แห่งประมวลรัษฎากร<br>
                                  ต้องรับโทษทางอาญา  ตามมาตรา  35  แห่งประมวลรัษฎากร<br>
                                </td>
                                  <td  style="padding: 2px 2px 2px 2px;" valign="top">
                                    หมายเหตุ เลขประจำตัวผู้เสียภาษีอากร (13 หลัก)* หมายถึง
                                    <br>1. กรณีบุคคลธรรมดาไทย ให้ใช้เลขประจำตัวประชาชนของกรมการปกครอง
                                    <br>2. กรณีนิติบุคคล ให้ใช้เลขทะเบียนนิติบุคคลของกรมพัฒนาธุรกิจการค้า
                                    <br>3. กรณีอื่น ๆ นอกเหนือจาก 1. และ 2. ให้ใช้เลขประจำตัวผู้เสียภาษีอากร (13 หลัก) 
                                  </td>
                                </tr>
                              </table>
                            </td>
                          </tr>
                        </table>

                      </div>
                    </div>
                    
                  </div>
                </div>
                <!-- Modal 1 -->
                
              </td>
              </td>
            </tr>
          <?php 
            $i++; 
            $OtherIncome = 0;
            $OtherDeduct = 0;
          // }
          } 
        }  
          ?>
          </tbody>
        </table>
  </div>
</div>
</div>

<script src="<?php echo base_url();?>assets/bootstrap/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url();?>assets/jquery-datatable/jquery.dataTables.js"></script>
<script src="<?php echo base_url();?>assets/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
<script src="<?php echo base_url();?>assets/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url();?>assets/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
<script src="<?php echo base_url();?>assets/jquery-datatable/extensions/export/jszip.min.js"></script>
<script src="<?php echo base_url();?>assets/jquery-datatable/extensions/export/pdfmake.min.js"></script>
<script src="<?php echo base_url();?>assets/jquery-datatable/extensions/export/vfs_fonts.js"></script>
<script src="<?php echo base_url();?>assets/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
<script src="<?php echo base_url();?>assets/jquery-datatable/extensions/export/buttons.print.min.js"></script>

  <script>
    function printDiv(i,id){
      var headerpage = "ฉบับที่ 1   ( สำหรับผู้ถูกหักภาษี ณ ที่จ่าย ใช้แนบพร้อมกับแบบแสดงรายการภาษี )";
      var headerpage2 = "ฉบับที่ 2   ( สำหรับผู้ถูกหักภาษี ณ ที่จ่าย เก็บไว้เป็นหลักฐาน )";

      divName = "myModal1"+i;
      headertype = "headertype"+i;

      if(id==2) headerpage = headerpage2;

      // $("#headertype").html(headerpage+"<BR>&nbsp;");
      document.getElementById(headertype).innerHTML = headerpage+"<BR>&nbsp;";

      var printContents = document.getElementById(divName).innerHTML;
      var originalContents = document.body.innerHTML;

      if(id==3){
      	$p1 = printContents;
      	document.getElementById(headertype).innerHTML = headerpage2+"<BR>&nbsp;";
      	$p2 = document.getElementById(divName).innerHTML;
        document.body.innerHTML = $p1 + "<p style='page-break-before: always;'>"+$p2+"</p>";
      }else document.body.innerHTML = printContents;

      window.print();
      document.body.innerHTML = originalContents;
    }
    function printAll(id){     
      // setTimeout(loadingshow(id), 5000) 
      var my_table = $('#tabledata').DataTable()
      my_table.page.len(-1).draw()



      var total = <?php echo $i;?>;
      var modalname = 'myModal1';
      var pagebreak = "<p style='page-break-before: always;'>";
      var headerpage = "ฉบับที่ 1   ( สำหรับผู้ถูกหักภาษี ณ ที่จ่าย ใช้แนบพร้อมกับแบบแสดงรายการภาษี )";
      var headerpage2 = "ฉบับที่ 2   ( สำหรับผู้ถูกหักภาษี ณ ที่จ่าย เก็บไว้เป็นหลักฐาน )";
      var originalContents = document.body.innerHTML;
      var contenttxt = "";

      if(id==2) headerpage = headerpage2;     
      for (var i = 0; i < total; i++) {
      	headertype = "headertype"+i;
      	document.getElementById(headertype).innerHTML = headerpage+"<BR>&nbsp;";
        divName=modalname+i;
        printContents = document.getElementById(divName).innerHTML;
      	$p1 = printContents;
      	document.getElementById(headertype).innerHTML = headerpage2+"<BR>&nbsp;";
      	$p2 = document.getElementById(divName).innerHTML;
        if(id==3) contenttxt += pagebreak + $p1 + "</p>"+$p2+"</p>";
        else contenttxt += pagebreak + printContents+"</p>";
      }
      document.body.innerHTML = contenttxt;      
      // setTimeout(loadinghide(id), 3000)
      window.print();
      document.body.innerHTML = originalContents;
    }
    function loadingshow(id){  
      $("#loading"+id).show();
    }
    function loadinghide(id){  
      $("#loading"+id).hide();
    }
  </script>