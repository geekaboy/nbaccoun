<?php
    // echo "<pre>";
    // print_r($this->session->userdata());
    // exit();

    // โหลด excel library
    $this->load->model('ExportModel', 'export_model');

    $this->load->library('excel');

    // เรียนกใช้ PHPExcel
    $objPHPExcel = new PHPExcel();
    // เราสามารถเรียกใช้เป็น  $this->excel แทนก็ได้

    // กำหนดค่าต่างๆ ของเอกสาร excel
    $objPHPExcel->getProperties()->setCreator("nbaccount")
    ->setLastModifiedBy("nbaccount")
    ->setTitle("HR Employee report level section")
    ->setSubject("HR Employee report level section");

    // กำหนดชื่อให้กับ worksheet ที่ใช้งาน
    $objPHPExcel->getActiveSheet()->setTitle('รายงานพนักงานแบบแยกหน่วย');

    // กำหนด worksheet ที่ต้องการให้เปิดมาแล้วแสดง ค่าจะเริ่มจาก 0 , 1 , 2 , ......
    $objPHPExcel->setActiveSheetIndex(0);

    // การจัดรูปแบบของ cell
    $objPHPExcel->getDefaultStyle()
    ->getAlignment()
    ->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP)
    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

    $styleBold= array(
    'font'  => array(
        'bold'  => true
    ));

    //HORIZONTAL_CENTER //VERTICAL_CENTER

    $objPHPExcel->getActiveSheet()->getStyle('A5:R5')->getAlignment()
        ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

    $objPHPExcel->getActiveSheet()->getStyle('M')->getAlignment()
        ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

    // $objPHPExcel->getActiveSheet()->getStyle('M')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);

    $objPHPExcel->getActiveSheet()->getStyle('M')->getNumberFormat()->setFormatCode('#,##0.00');


    // จัดความกว้างของคอลัมน์
    $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(70);
    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
    $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
    $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
    $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
    $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
    $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(20);
    $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(20);
    $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(20);
    $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(30);
    $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(15);
    $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(10);
    $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(15);
    $objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(15);
    $objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(25);
    $objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(80);


    $objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue('A1', $this->session->userdata('CompNameThai').' ('.$this->session->userdata('CompNameEng').')');
    $objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray($styleBold);

    $objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue('A2', 'รายงานแบบแยกหน่วย');
    $objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray($styleBold);

    $Nation = $this->db->get_where('Nationality', array('NationlityID'=>$dataset['nationality']))->row();
    $gender = $this->db->get_where('Gender', array('GenderID'=>$dataset['gender']))->row();
    $worktype = $this->db->get_where('worktype', array('WorkTypeID'=>$dataset['worktype']))->row();

    switch ($dataset['levelreport']) {
        case '1':
            $textReportLevel = "ฝ่าย";
            break;
        case '2':
            $textReportLevel = "แผนก";
            break;
        case '3':
            $textReportLevel = "หน่วย";
            break;

        default:
            $textReportLevel = "ทั้งหมด";
            break;
    }
    $objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue('A3', 'Report Level [ระดับรายงาน] :'.$textReportLevel
                    .' | Employee Type[ประเภทพนักงาน] :'.$dataset['empgroup']
                    .' | Gendar[เพศ] :'.(is_null($gender->GenderEN)? 'ทั้งหมด':$gender->GenderEN)
                    .' | Nationality[สัญชาติ] :'.(is_null($Nation->NationalityNameLL)? 'ทั้งหมด':$Nation->NationalityNameLL)
                    .' | Work Type[ประเภทงาน] :'.(is_null($worktype->WorkTypeNameThai)? 'ทั้งหมด':$worktype->WorkTypeNameThai));
    $objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray($styleBold);

    $objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue('A4', 'ข้อมูลจากวันที่ '.$dataset['dateset'].' ถึง '.$dataset['dateset2']);
    $objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray($styleBold);


    $i_row = 5;
    // กำหนดหัวข้อให้กับแถวแรก
    $objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue('A'.$i_row, 'NO.')
    ->setCellValue('B'.$i_row, 'Emp ID')
    ->setCellValue('C'.$i_row, 'Employee Name (TH)')
    ->setCellValue('D'.$i_row, 'Employee Name (EN)')
    ->setCellValue('E'.$i_row, 'ID_Card')
    ->setCellValue('F'.$i_row, 'SS_ID')
    ->setCellValue('G'.$i_row, 'Birthday')
    ->setCellValue('H'.$i_row, 'Age')
    ->setCellValue('I'.$i_row, 'Start Working')
    ->setCellValue('J'.$i_row, 'Resigned')
    ->setCellValue('K'.$i_row, 'Department')
    ->setCellValue('L'.$i_row, 'Position')
    ->setCellValue('M'.$i_row, 'Salary')
    ->setCellValue('N'.$i_row, 'Gender')
    ->setCellValue('O'.$i_row, 'Nationality')
    ->setCellValue('P'.$i_row, 'Duration of Employment')
    ->setCellValue('Q'.$i_row, 'Bank Account No.')
    ->setCellValue('R'.$i_row, 'Address');
    $objPHPExcel->getActiveSheet()->getStyle('A'.$i_row.':R'.$i_row)->applyFromArray($styleBold);

    $sqlDivision= "SELECT d.DivisionID, d.DivisionNameThai, d.DivisionNameEng
    FROM  Division d
    WHERE d.CompID = {$this->session->CompID}
    ORDER BY d.DivisionID";
    $q = $this->db->query($sqlDivision);

    if($q->num_rows() > 0){
        $totalDiv = 0;
        $employeeAll = 0;
        $dCount = 0;
        $DivisionID = 0;
        $DepartmentID = 0;
        $SectionID = 0;
        $totalSaralyAll = 0;
        $listSumDivision_arr = array();
        foreach ($q->result() as $key => $rDiv) {
            $i_row++;
            $dCount++;
            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A'.$i_row, ' ฝ่าย | '.$dCount.'.'.$rDiv->DivisionNameEng.' | '.$rDiv->DivisionNameThai);


            //Set Department
            $sqlDept= "SELECT d.DepartmentID, d.DepartmentNameEng, d.DepartmentNameThai
            FROM  Department d
            WHERE d.DivisionID = {$rDiv->DivisionID}
            ORDER BY d.DepartmentID";
            $q = $this->db->query($sqlDept);

            $deptRowsCount = 0;
            $depCount = 0;
            $listSumDept_arr = array();
            foreach ($q->result() as $key => $rDept) { //Department
                $i_row++;
                $deptRowsCount++;
                $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A'.$i_row, ' แผนก | '.$dCount.'.'.$deptRowsCount.' '.$rDept->DepartmentNameEng.' | '.$rDept->DepartmentNameThai);


                //Set Section
                $sqlSec= "SELECT s.SectionID, s.SectionNameEng, s.SectionNameThai
                FROM  Section s
                WHERE s.DepartmentID= {$rDept->DepartmentID}
                ORDER BY s.SectionID";
                $q = $this->db->query($sqlSec);
                $sCount = 0;
                $listSumSec_arr = array();

                if(empty($q->result())) break;

                foreach ($q->result() as $key => $rSec) { //Section
                    $i_row++;
                    $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A'.$i_row, ' หน่วย | '.$rSec->SectionNameEng.' | '.$rSec->SectionNameThai);

                    //Set Employee data
                    $employeeDataList = $this->export_model->getEmployeeBySection(
                        $dataset['dateset'],
                        $dataset['dateset2'],
                        $dataset['empgroup'],
                        $dataset['gender'],
                        $dataset['nationality'],
                        $dataset['worktype'],
                        $rDiv->DivisionID,
                        $rDept->DepartmentID,
                        $rSec->SectionID
                    );

                    $totalSalarySection =0;
                    $empCount = 0;
                    if($employeeDataList) {

                        foreach ($employeeDataList as $key => $emp) { //SetRowsEmployee
                            $i_row++;
                            $empCount++;
                            $employeeAll++;
                            $totalSalarySection = $totalSalarySection+$emp->MonthlySalary;
                            $totalSalaryDept = $totalSalaryDept+$emp->MonthlySalary;
                            $totalSalaryDiv = $totalSalaryDiv+$emp->MonthlySalary;
                            $totalSaralyAll = $totalSaralyAll+$emp->MonthlySalary;

                            $bdate = $emp->Birthday;
                            $AgeY = date_diff(date_create($bdate), date_create('now'))->y;
                            $AgeM = date_diff(date_create($bdate), date_create('now'))->m;
                            $AgeD = date_diff(date_create($bdate), date_create('now'))->d;

                            //Calculate Duration of Employment
                            $bdate = $emp->ServiceStartDate;
                            $edate = $emp->EffectiveDateOut;

                            if ( $edate=="0000-00-00" ) {
                                $edate = date("Y-m-d");


                            }
                            $date1=date_create($bdate);
                            $date2=date_create($edate);
                            $diff=date_diff($date1,$date2);

                            $SSDY = $diff->y;
                            $SSDM = $diff->m;
                            $SSDD = $diff->d;


                            $type = PHPExcel_Cell_DataType::TYPE_STRING;

                            $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(4, $i_row)->setValueExplicit($emp->CardID, $type);

                            $objPHPExcel->getActiveSheet()->getStyle('E')
                            ->getNumberFormat()
                            ->setFormatCode( PHPExcel_Cell_DataType::TYPE_STRING );
                            $objPHPExcel->getActiveSheet()->getStyle('F')
                            ->getNumberFormat()
                            ->setFormatCode( PHPExcel_Cell_DataType::TYPE_STRING );

                            $objPHPExcel->getActiveSheet()->getStyle('A'.$i_row)->getAlignment()
                                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                                
                            $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValue('A'.$i_row, $empCount)
                            ->setCellValue('B'.$i_row, $emp->EmployeeCode)
                            ->setCellValue('C'.$i_row, $emp->TitleTH.$emp->FirstName.'  '.$emp->LastName)
                            ->setCellValue('D'.$i_row, $emp->FullNameLL)
                            ->setCellValue('G'.$i_row, $emp->Birthday)
                            ->setCellValue('H'.$i_row, "$AgeY Y $AgeM M $AgeD D")
                            ->setCellValue('I'.$i_row, $emp->ServiceStartDate)
                            ->setCellValue('J'.$i_row, $emp->EffectiveDateOut)
                            ->setCellValue('K'.$i_row, $emp->DepartmentNameThai)
                            ->setCellValue('L'.$i_row, $emp->PositionNameThai)
                            ->setCellValue('M'.$i_row, $emp->MonthlySalary)
                            ->setCellValue('N'.$i_row, $emp->GenderTH)
                            ->setCellValue('O'.$i_row, $emp->NationalityNameLL)
                            ->setCellValue('P'.$i_row, "$SSDY Y $SSDM M $SSDD D")
                            ->setCellValue('Q'.$i_row, $emp->BankAccountNo)
                            ->setCellValue('R'.$i_row, $emp->AddressResidence." แขวง/ตำบล ".$emp->DISTRICT_NAME."  เขต/อำเภอ ".$emp->AMPHUR_NAME." จังหวัด ".$emp->ProvinceNameLocal." รหัสไปรษณีย์  ".$emp->zipcode);

                            $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(5, $i_row)->setValueExplicit($emp->SSOID, $type);
                        }

                    }//END IF
                    $sCount = $sCount+$empCount;

                    $i_row++;
                    $rowsRef = ($empCount==0)? $i_row-1:($i_row) - $empCount;
                    if($empCount > 1){
                        array_push($listSumSec_arr,'M'.$rowsRef.':M'.($i_row-1));
                        array_push($listSumDept_arr,'M'.$rowsRef.':M'.($i_row-1));
                    } else {
                        array_push($listSumSec_arr,'M'.($i_row-1).':M'.($i_row-1));
                        array_push($listSumDept_arr,'M'.($i_row-1).':M'.($i_row-1));

                    }

                    $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A'.$i_row, 'Total หน่วย ('.$empCount.') : '.$rSec->SectionNameEng.' | '.$rSec->SectionNameThai);
                    $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('M'.$i_row, '=subtotal(109, M'.$rowsRef.':M'.($i_row-1).') ');




                    $objPHPExcel->getActiveSheet()->getStyle('A'.$i_row.':R'.$i_row)->applyFromArray($styleBold);
                    $objPHPExcel->getActiveSheet()->getStyle('A'.$i_row.':R'.$i_row)->applyFromArray(
                        array(
                            'fill' => array(
                                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                                'color' => array('rgb' => 'c5d9f1')
                            )
                        )
                    );


                }//end foreach section

                $depCount = $depCount+$sCount;//นับฝ่าย
                $i_row++;
                $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A'.$i_row, 'Total แผนก ('.$sCount.') : '.$rDept->DepartmentNameEng.' | '.$rDept->DepartmentNameThai);
                $totalDeptRow = implode( ", ", $listSumSec_arr );
                $objPHPExcel->setActiveSheetIndex(0)->setCellValue('M'.$i_row, '=SUBTOTAL(109, '.$totalDeptRow.')');

                $objPHPExcel->getActiveSheet()->getStyle('A'.$i_row.':R'.$i_row)->applyFromArray($styleBold);
                $objPHPExcel->getActiveSheet()->getStyle('A'.$i_row.':R'.$i_row)->applyFromArray(
                    array(
                        'fill' => array(
                            'type' => PHPExcel_Style_Fill::FILL_SOLID,
                            'color' => array('rgb' => 'c3d79a')
                        )
                    )
                );

            }//end foreach Department
            $i_row++;
            array_push($listSumDivision_arr,'M'.$i_row);
            $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A'.$i_row, 'Total ฝ่าย ('.$depCount.') : '.$rDiv->DivisionNameEng.' | '.$rDiv->DivisionNameThai);
            $totalDivRow = implode( ", ", $listSumDept_arr );
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('M'.$i_row, '=SUBTOTAL(109, '.$totalDivRow.')');

            $objPHPExcel->getActiveSheet()->getStyle('A'.$i_row.':R'.$i_row)->applyFromArray($styleBold);
            $objPHPExcel->getActiveSheet()->getStyle('A'.$i_row.':R'.$i_row)->applyFromArray(
                array(
                    'fill' => array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => array('rgb' => 'f9bf8f')
                    )
                )
            );

            $i_row++;

        }//foreach

        $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A'.$i_row, 'ยอดรวม ('.$employeeAll.') ');
        $objPHPExcel->getActiveSheet()->getStyle('A'.$i_row.':R'.$i_row)->applyFromArray($styleBold);

        // $objPHPExcel->setActiveSheetIndex(0)->setCellValue('M'.$i_row, $totalSaralyAll);
        $totalDivRow = implode( ", ", $listSumDivision_arr );
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('M'.$i_row, '=SUM('.$totalDivRow.')');

        $objPHPExcel->getActiveSheet()->getStyle('A'.$i_row.':R'.$i_row)->applyFromArray($styleBold);
        $objPHPExcel->getActiveSheet()->getStyle('A'.$i_row.':R'.$i_row)->applyFromArray(
            array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => 'df6d0d')
                )
            )
        );




        $BORDER_DOUBLE = array(
          'borders' => array(
            'outline' => array(
              'style' => PHPExcel_Style_Border::BORDER_DOUBLE
            )
          )
        );
        $objPHPExcel->getActiveSheet()->getStyle('A5:R5')->applyFromArray($BORDER_DOUBLE);
        $objPHPExcel->getActiveSheet()->getStyle('A'.$i_row.':R'.$i_row)->applyFromArray($BORDER_DOUBLE);


        $i_row++;
        $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A'.$i_row, 'ผู้ออกรายงาน '.$this->session->userdata('fullname_position'). ' เมื่อ '.date('Y-m-d h:i:s'));
        $objPHPExcel->getActiveSheet()->getStyle('A'.$i_row.':R'.$i_row)->applyFromArray($styleBold);




        //Set Style Sheet
        $styleArray = array(
          'borders' => array(
            'outline' => array(
              'style' => PHPExcel_Style_Border::BORDER_THIN
            )
          )
        );


        $objPHPExcel->getActiveSheet()->getStyle('A5:A'.$i_row)->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->getStyle('B5:B'.$i_row)->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->getStyle('C5:C'.$i_row)->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->getStyle('D5:D'.$i_row)->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->getStyle('E5:E'.$i_row)->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->getStyle('F5:F'.$i_row)->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->getStyle('G5:G'.$i_row)->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->getStyle('H5:H'.$i_row)->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->getStyle('I5:I'.$i_row)->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->getStyle('J5:J'.$i_row)->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->getStyle('K5:K'.$i_row)->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->getStyle('L5:L'.$i_row)->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->getStyle('M5:M'.$i_row)->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->getStyle('N5:N'.$i_row)->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->getStyle('O5:O'.$i_row)->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->getStyle('P5:P'.$i_row)->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->getStyle('Q5:Q'.$i_row)->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->getStyle('R5:R'.$i_row)->applyFromArray($styleArray);






    }


    // กำหนดรูปแบบของไฟล์ที่ต้องการเขียนว่าเป็นไฟล์ excel แบบไหน ในที่นี้เป้นนามสกุล xlsx  ใช้คำว่า Excel2007
    // แต่หากต้องการกำหนดเป็นไฟล์ xls ใช้กับโปรแกรม excel รุ่นเก่าๆ ได้ ให้กำหนดเป็น  Excel5
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');  // Excel2007 (xlsx) หรือ Excel5 (xls)

    $filename='HREmployeeInfoLevelSection-'.date("dmYHi").'.xlsx'; //  กำหนดชือ่ไฟล์ นามสกุล xls หรือ xlsx
    // บังคับให้ทำการดาวน์ดหลดไฟล์
    header('Content-Type: application/vnd.ms-excel'); //mime type
    header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
    header('Cache-Control: max-age=0'); //no cache
    ob_end_clean();
    $objWriter->save('php://output'); // ดาวน์โหลดไฟล์รายงาน
    // แล้วตัด header ดัานบนทั้ง 3 อันออก
    exit;
?>
