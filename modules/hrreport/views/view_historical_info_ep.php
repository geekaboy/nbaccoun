<script type="text/javascript">
  $(document).ready(function(){
    //เพิ่มเงื่อนไขตาราง
    $('.js-exportable').DataTable({
        dom: 'Bfrtip',responsive: true,paging: true,info: false,
        buttons: [
            {extend:'copyHtml5',text:' <i class="fa fa-files-o fa-2x "></i> ',titleAttr: ' Copy '},
            {extend:'excelHtml5',text:' <i class="fa fa-file-excel-o fa-2x "></i> ',titleAttr: ' Excel '},
            {extend:'print',text:' <i class="fa fa-print fa-2x "></i> ',titleAttr: ' Print '}
          ,
        ]
      });

    $('.js-exportable2').DataTable({
        dom: 'Bfrtip',responsive: true,paging: true,info: false,
        buttons: [
            {extend:'copyHtml5',text:' <i class="fa fa-files-o fa-2x "></i> ',titleAttr: ' Copy '},
            {extend:'excelHtml5',text:' <i class="fa fa-file-excel-o fa-2x "></i> ',titleAttr: ' Excel '},
            {extend:'print',text:' <i class="fa fa-print fa-2x "></i> ',titleAttr: ' Print '
            // ,exportOptions:{columns:[0,1,2,3,4,5,6]}
          },
        ],
        "columnDefs": [{"targets": [ 0,1,2,3,4 ],"visible": false,"searchable": true}]
      });
//กรณีต้องการปิดการแสดงบาง column แต่สามารถ export file ได้
    $('.js-exportable3').DataTable({
        dom: 'Bfrtip',responsive: true,paging: true,info: false,
        buttons: [
            {extend:'copyHtml5',text:' <i class="fa fa-files-o fa-2x "></i> ',titleAttr: ' Copy '},
            {extend:'excelHtml5',text:' <i class="fa fa-file-excel-o fa-2x "></i> ',titleAttr: ' Excel '},
            {extend:'print',text:' <i class="fa fa-print fa-2x "></i> ',titleAttr: ' Print '
            ,exportOptions:{columns:[0,1,2,3,4,5,6]}
          },
        ],
        "columnDefs": [{"targets": [ 5 ],"visible": false,"searchable": true}]
      });
  });
</script>
<!-- begin add require script -->
<head>
  <meta charset="UTF-8">
  
  <style>
  @media print {
    canvas {
      min-height: 100%;max-width: 100%;max-height: 100%;height: auto!important;width: auto!important;
    }
    table {
      min-height: 100%;max-width: 100%;max-height: 100%;height: auto!important;width: auto!important;
      page-break-inside: : auto;
    }
    tr{
      page-break-inside: avoid;
      page-break-after: auto;

    }
  }
  canvas{-moz-user-select: none;-webkit-user-select: none;-ms-user-select: none;}
  </style>
</head>

<!-- end add require script -->
<div class="box box-success">
  <div class="box-header" align="left">
        <i class="fa fa-sign-in"></i>
    <h3 class="box-title"><?php echo 'แสดงผลข้อมูล'.$this->systemmodel->get_menuname($this->uri->segment(1) . '/' . $this->uri->segment(2)); // แสดงชื่อเมนู  ?></h3>
  </div>
  <div class="box-body">
    <!-- ส่วนแสดงผล -->
<script>

function getRandomColor() {
    var letters = '0123456789ABCDEF'.split('');
    var color = '#';
    for (var i = 0; i < 6; i++ ) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}
  </script>


<div class="box box-success">
  <div class="box-header">
    <b>ตารางแสดงข้อมูลทั้งหมด</b>
  </div>
  <div class="box-body" align="left">

        <!-- ตาราง แสดงข้อมูล -->
        <table class="table table-bordered table-striped table-hover dataTable  js-exportable2">
          <thead>
            <tr>
              <th scope="col"><center>No</center></th>
              <th scope="col"><center>ID</center></th>
              <th scope="col"><center>Name  </center></th>
              <th scope="col"><center>Department  </center></th>
              <th scope="col"><center>Section </center></th>
              <th scope="col"><center>Working Days  </center></th>
              <th scope="col"><center>Basic Salary  </center></th>
              <th scope="col"><center>Adjust Salary+/-  </center></th>
              <th scope="col"><center>Daily Wage  </center></th>
              <th scope="col"><center>Service Charge  </center></th>
              <th scope="col"><center>Adjust Service Charge </center></th>
              <th scope="col"><center>Position Allowance  </center></th>
              <th scope="col"><center>Food Allowance  </center></th>
              <th scope="col"><center>Housing Allowance </center></th>
              <th scope="col"><center>Transportation Allowance  </center></th>
              <th scope="col"><center>Mobile Phone Allowance  </center></th>
              <th scope="col"><center>Overtime & Compensation</center></th>
              <th scope="col"><center>Other Allowance</center></th>
              <th scope="col"><center>Other Allowance (Non Tax)</center></th>
              <th scope="col"><center>Night Shift</center></th>
              <th scope="col"><center>Bonus </center></th>
              <th scope="col"><center>Acc.Allowance</center></th>
              <th scope="col"><center>Total Income</center></th>
              <th scope="col"><center>Late In</center></th>
              <th scope="col"><center>Early Out</center></th>
              <th scope="col"><center>Salary Deduction</center></th>
              <th scope="col"><center>Service Charge Deduction</center></th>
              <th scope="col"><center>Food Deduction</center></th>
              <th scope="col"><center>Housing Deduction</center></th>
              <th scope="col"><center>Uniform Deduction</center></th>
              <th scope="col"><center>Mobile Phone Deduction</center></th>
              <th scope="col"><center>Absent & Unpaid Leave </center></th>
              <th scope="col"><center>Over Leave  </center></th>
              <th scope="col"><center>Other Deduction </center></th>
              <th scope="col"><center>Other Deduction (Non Tax)</center></th>
              <th scope="col"><center>Social Security</center></th>
              <th scope="col"><center>Tax</center></th>
              <th scope="col"><center>Total Deduction</center></th>
              <th scope="col"><center>Net</center></th>
            </tr>
          </thead>
          <tbody>
          <?php 
        $i=0;
        if(!empty($all_project)){
          foreach ($all_project as $row) {
          ?>
            <tr>


              <td scope="col"><?php echo $i+1;?></td>
              <td scope="col"><?php echo $row->department_id;?></td>
              <td scope="col"><?php echo $row->department_name;?></td>
              <td scope="col"><?php echo $row->department_name;?></td>
              <td scope="col"><?php echo $row->department_name;?></td>
              <td scope="col"><?php echo $row->department_name;?></td>
              <td scope="col"><?php echo $row->department_name;?></td>
              <td scope="col"><?php echo $row->department_name;?></td>
              <td scope="col"><?php echo $i+1;?></td>
              <td scope="col"><?php echo $row->department_id;?></td>
              <td scope="col"><?php echo $row->department_name;?></td>
              <td scope="col"><?php echo $row->department_name;?></td>
              <td scope="col"><?php echo $row->department_name;?></td>
              <td scope="col"><?php echo $row->department_name;?></td>
              <td scope="col"><?php echo $row->department_name;?></td>
              <td scope="col"><?php echo $row->department_name;?></td>
              <td scope="col"><?php echo $i+1;?></td>
              <td scope="col"><?php echo $row->department_id;?></td>
              <td scope="col"><?php echo $row->department_name;?></td>
              <td scope="col"><?php echo $row->department_name;?></td>
              <td scope="col"><?php echo $row->department_name;?></td>
              <td scope="col"><?php echo $row->department_name;?></td>
              <td scope="col"><?php echo $row->department_name;?></td>
              <td scope="col"><?php echo $row->department_name;?></td>
              <td scope="col"><?php echo $i+1;?></td>
              <td scope="col"><?php echo $row->department_id;?></td>
              <td scope="col"><?php echo $row->department_name;?></td>
              <td scope="col"><?php echo $row->department_name;?></td>
              <td scope="col"><?php echo $row->department_name;?></td>
              <td scope="col"><?php echo $row->department_name;?></td>
              <td scope="col"><?php echo $row->department_name;?></td>
              <td scope="col"><?php echo $row->department_name;?></td>
              <td scope="col"><?php echo $row->department_id;?></td>
              <td scope="col"><?php echo $row->department_name;?></td>
              <td scope="col"><?php echo $row->department_name;?></td>
              <td scope="col"><?php echo $row->department_name;?></td>
              <td scope="col"><?php echo $row->department_name;?></td>
              <td scope="col"><?php echo $row->department_name;?></td>
              <td scope="col"><?php echo $row->department_name;?></td>
            </tr>
          <?php
          
          $i++;
        } 
        }  
          ?>
          </tbody>
        </table>
  </div>
</div>
    <!-- จบส่วนแสดงผล -->
  </div>
</div>
