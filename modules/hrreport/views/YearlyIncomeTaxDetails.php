<script type="text/javascript">
  $(document).ready(function(){
    $('.selectpicker').selectpicker({style: 'btn-warning',size: 5});
    $('#username').focus();
    $('#btn_login').click(function(){
      $.ajax({
        url:'<?php echo base_url(); ?>index.php/home/login',
        data:'username='+$('#username').val()+'&password='+$('#password').val(),
        type:'POST',
        success:function(res){
          if(res=='true'){
            swal({title : 'แสดงเมือทำงานสำเร็จ',text : '',type : 'success'},
              function(){window.location.replace("<?php echo base_url() ?>index.php/member");
            }
            );
          }else{swal({title : 'แสดงเมื่อทำงานไม่สำเร็จ',text : '',type : 'error'});}
        },error:function(err){swal({title : 'เกิดข้อผิดพลาด',text : err,type : 'error'});}
      });
    });
  });
</script>
<head>
  <meta charset="UTF-8">
  <title><?php echo $this->uri->segment(2);?></title>
  <link href="<?php echo base_url();?>assets/bootstrap-select/css/bootstrap-select.css" rel="stylesheet">
  <link href="<?php echo base_url();?>assets/bootstrap/css/datepicker.css" rel="stylesheet">
  <link href="<?php echo base_url();?>assets/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">
<style>
  canvas{
    -moz-user-select: none;
    -webkit-user-select: none;
    -ms-user-select: none;
  }
  </style>
</head>

<div class="box box-success">
  <div class="box-header">
        <i class="fa fa-sign-in"></i>
    <h3 class="box-title"><?php echo $this->systemmodel->get_menuname($this->uri->segment(1) . '/' . $this->uri->segment(2)); // แสดงชื่อเมนู  ?></h3>
  </div>
  <div class="box-body">
    <form class="" name="form1" action="YearlyIncomeTaxDetails"  method="post" target="_blank">
      <div class="row clearfix">
          <div class="col-md-3">
            <p><b>Select Year</b></p>
            <div class="form-group">
                <div class='input-group date' id='datetimepicker1'>
                    <input type='text' class="form-control" id='datasearch1' name="datasearch1" value="<?php echo $datasearch1;?>" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>
            <script>
              $(document).ready(function(){
                var date_input=$('input[name="datasearch1"]'); //our date input has the name "date"
                var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
                date_input.datepicker({
                  format: 'yyyy',
                  container: container,
                  todayHighlight: true,
                  autoclose: true,
                  viewMode:"years",
                  minViewMode:"years"
                })
              })
            </script>
          </div>
          <div class="col-md-3">
            <p><b>Select Date</b></p>
            <div class="form-group">
                <div class='input-group date' id='datetimepicker1'>
                    <input type='text' class="form-control" id='dateset' name="dateset" value="<?php echo $datesign[0]."-".$datesign[1]."-".$datesign[2];?>" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>
            <script>
              $(document).ready(function(){
                var date_input=$('input[name="dateset"]'); //our date input has the name "date"
                var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
                date_input.datepicker({
                  format: 'yyyy-mm-dd',
                  container: container,
                  todayHighlight: true,
                  autoclose: true,
                  viewMode:"days",
                  minViewMode:"days"
                })
              })
            </script>
          </div>
           <div class="col-md-3" align="center">
            <p><b>&nbsp;</b></p>
            <button type="submit" class="btn btn-warning btn-md btn-block"name="btn_submit" id="btn_submit"value="cover">แบบยื่น ภ.ง.ด.1ก</button>
          </div>
           <div class="col-md-3" align="center">
            <p><b>&nbsp;</b></p>
            <button type="submit" class="btn btn-primary btn-md btn-block"name="btn_submit" id="btn_submit"value="">รายละเอียด ภ.ง.ด.1ก</button>
          </div>
    </form>
<hr>
<div class="col-md-12">
    <!-- ส่วนแสดงผล -->
<?php if(!empty($datasearch1)){ ?>
<div class="box box-success">
  <div class="box-body" align="left">
        <!-- ตาราง แสดงข้อมูล -->
        <table class="table table-bordered table-striped table-hover dataTable  js-exportable">
          <thead>
            <tr>
              <th scope="col" width="5%"><center>No</center></th>
              <th scope="col"><center>Monthly</center></th>
              <th scope="col" width="20%"><center>PDF  </center></th>
            </tr>
          </thead>
          <tbody>
          <?php 
        $i=1;
        while($i<=12){
          $mtxt = $datasearch1."-".$i."-1";
          $date=date_create($mtxt);
          
          ?>
            <tr>
              <td scope="col" align="center"><?php echo $i;?></td>
              <td scope="col"><?php echo date_format($date,"F")."/".$datasearch1;?></td>
              <td scope="col" align="center">
                <a href="<?php echo base_url('index.php/Hrreport/YearlyIncomeTaxDetails/'.$i.'/'.$datasearch1.'/PDF'); ?>" target="_blank">
                  <button type="button" class="btn btn-info">
                    <span class="glyphicon glyphicon-print"></span> PDF
                  </button>
                </a>
              </td>
            </tr>
          <?php
          
          $i++;
        } 
      
          ?>
          </tbody>
        </table>
  </div>
  <?php } ?>
</div>
    <!-- จบส่วนแสดงผล -->
  </div>
    <!-- จบส่วนแสดงผล -->
  </div>
</div>
<!-- Bootstrap-Select -->
<script src="<?php echo base_url();?>assets/bootstrap-select/js/bootstrap-select.js"></script>
<script src="<?php echo base_url();?>assets/chartjs/Chart.bundle.js"></script>
<script src="<?php echo base_url();?>assets/chartjs/Chart.js"></script>
<script src="<?php echo base_url();?>assets/bootstrap/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url();?>assets/jquery-datatable/jquery.dataTables.js"></script>
<script src="<?php echo base_url();?>assets/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
<script src="<?php echo base_url();?>assets/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url();?>assets/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
<script src="<?php echo base_url();?>assets/jquery-datatable/extensions/export/jszip.min.js"></script>
<script src="<?php echo base_url();?>assets/jquery-datatable/extensions/export/pdfmake.min.js"></script>
<script src="<?php echo base_url();?>assets/jquery-datatable/extensions/export/vfs_fonts.js"></script>
<script src="<?php echo base_url();?>assets/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
<script src="<?php echo base_url();?>assets/jquery-datatable/extensions/export/buttons.print.min.js"></script>