<?php
    // โหลด excel library
    $this->load->model('HrreportModel', 'hrreport');
    $this->load->library('excel');

    // เรียนกใช้ PHPExcel
    $objPHPExcel = new PHPExcel();
    // เราสามารถเรียกใช้เป็น  $this->excel แทนก็ได้

    // กำหนดค่าต่างๆ ของเอกสาร excel
    $objPHPExcel->getProperties()->setCreator("Ninenik.com")
    ->setLastModifiedBy("Ninenik.com")
    ->setTitle("PHPExcel Test Document")
    ->setSubject("PHPExcel Test Document")
    ->setDescription("Test document for PHPExcel, generated using PHP classes.")
    ->setKeywords("office PHPExcel php")
    ->setCategory("Test result file");

    // กำหนดชื่อให้กับ worksheet ที่ใช้งาน
    $objPHPExcel->getActiveSheet()->setTitle('Product Report');

    // กำหนด worksheet ที่ต้องการให้เปิดมาแล้วแสดง ค่าจะเริ่มจาก 0 , 1 , 2 , ......
    $objPHPExcel->setActiveSheetIndex(0);

    // การจัดรูปแบบของ cell
    $objPHPExcel->getDefaultStyle()
    ->getAlignment()
    ->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP)
    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

    $styleBold= array(
    'font'  => array(
        'bold'  => true
    ));

    //HORIZONTAL_CENTER //VERTICAL_CENTER

    // จัดความกว้างของคอลัมน์
    $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);

    $i_row = 5;
    // กำหนดหัวข้อให้กับแถวแรก
    $objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue('A'.$i_row, 'NO.')
    ->setCellValue('B'.$i_row, 'Emp ID')
    ->setCellValue('C'.$i_row, 'Employee Name (TH)')
    ->setCellValue('D'.$i_row, 'Employee Name (EN)')
    ->setCellValue('E'.$i_row, 'ID_Card')
    ->setCellValue('F'.$i_row, 'SS_ID')
    ->setCellValue('G'.$i_row, 'Birthday')
    ->setCellValue('H'.$i_row, 'Age')
    ->setCellValue('I'.$i_row, 'Start Working')
    ->setCellValue('J'.$i_row, 'Resigned')
    ->setCellValue('K'.$i_row, 'Department')
    ->setCellValue('L'.$i_row, 'Position')
    ->setCellValue('M'.$i_row, 'Salary')
    ->setCellValue('N'.$i_row, 'Gender')
    ->setCellValue('O'.$i_row, 'Nationality')
    ->setCellValue('P'.$i_row, 'Duration of Employment')
    ->setCellValue('Q'.$i_row, 'Bank Account No.')
    ->setCellValue('R'.$i_row, 'Address');
    $objPHPExcel->getActiveSheet()->getStyle('A'.$i_row.':R'.$i_row)->applyFromArray($styleBold);

    $sqlSecsion = "SELECT d.DivisionID, d.DivisionNameThai, d.DivisionNameEng,
	dept.DepartmentID, dept.DepartmentNameThai, dept.DepartmentNameEng, s.SectionID, s.SectionNameThai, s.SectionNameEng
    FROM  division d
        LEFT JOIN department dept ON dept.DivisionID = d.DivisionID
        LEFT JOIN section s ON s.DepartmentID = dept.DepartmentID
    WHERE d.CompID = 4
    ORDER BY d.DivisionID, dept.DepartmentID, s.SectionID";
    $q = $this->db->query($sqlSecsion);

    if($q->num_rows() > 0){
        $dCount = 0;
        $deptCount = 0;
        $sCount = 0;
        $DivisionID = 0;
        $DepartmentID = 0;
        $SectionID = 0;
        foreach ($q->result() as $key => $rData) {

            if($DivisionID != $rData->DivisionID){
                //Set Dividion
                $i_row++;
                $i_row = ($i_row>6)? $i_row+1:$i_row++;

                $dCount++;
                $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A'.$i_row, ' ฝ่าย | '.$dCount.'.'.$rData->DivisionNameEng.' | '.$rData->DivisionNameThai);

                $DivisionID = $rData->DivisionID;
            }

            if ($DepartmentID != $rData->DepartmentID) {
                //Set Department
                $i_row++;
                $deptCount++;
                $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A'.$i_row, ' แผนก | '.$dCount.'.'.$deptCount.'.'.$rData->DepartmentNameEng.' | '.$rData->DepartmentNameThai);
                $DepartmentID = $rData->DepartmentID;
            }

            if ($SectionID != $rData->SectionID) {
                //Set Section
                $i_row++;
                $sCount++;
                $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A'.$i_row, ' หน่วย | '.$dCount.'.'.$deptCount.'.'.$sCount.'.'.$rData->SectionNameEng.' | '.$rData->SectionNameThai);
                $SectionID = $rData->SectionID;


                //Set Employee data
                $employeeDataList = $this->hrreport->getEmployeeBySection(
                    $dataset['dateset'],
                    $dataset['dateset2'],
                    $dataset['empgroup'],
                    $dataset['gender'],
                    $dataset['nationality'],
                    $dataset['worktype'],
                    $rData->DivisionID,
                    $rData->DepartmentID,
                    $rData->SectionID
                );
                if($employeeDataList) {
                    $c = 0;
                    foreach ($employeeDataList as $key => $emp) {
                        $i_row++;
                        $c++;
                        $bdate = $emp->Birthday;
                        $AgeY = date_diff(date_create($bdate), date_create('now'))->y;
                        $AgeM = date_diff(date_create($bdate), date_create('now'))->m;
                        $AgeD = date_diff(date_create($bdate), date_create('now'))->d;

                        //Calculate Duration of Employment
                        $bdate = $emp->ServiceStartDate;
                        $edate = $emp->EffectiveDateOut;

                        if($edate="0000-00-00") $edate = date("Y-m-d");

                        $date1=date_create($bdate);
                        $date2=date_create($edate);
                        $diff=date_diff($date1,$date2);

                        $SSDY = $diff->y;
                        $SSDM = $diff->m;
                        $SSDD = $diff->d;

                        $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A'.$i_row, $c)
                        ->setCellValue('B'.$i_row, $emp->EmployeeID)
                        ->setCellValue('C'.$i_row, $emp->TitleTH.$emp->FirstName.'  '.$emp->LastName)
                        ->setCellValue('D'.$i_row, $emp->FullNameLL)
                        ->setCellValue('E'.$i_row, $emp->CardID)
                        ->setCellValue('F'.$i_row, $emp->SSOID)
                        ->setCellValue('G'.$i_row, $emp->Birthday)
                        ->setCellValue('H'.$i_row, "$AgeY Y $AgeM M $AgeD D")
                        ->setCellValue('I'.$i_row, $emp->ServiceStartDate)
                        ->setCellValue('J'.$i_row, $emp->EffectiveDateOut)
                        ->setCellValue('K'.$i_row, $emp->DepartmentNameThai)
                        ->setCellValue('L'.$i_row, $emp->PositionNameThai)
                        ->setCellValue('M'.$i_row, number_format($emp->MonthlySalary,2))
                        ->setCellValue('N'.$i_row, $emp->GenderTH)
                        ->setCellValue('O'.$i_row, $emp->NationalityNameLL)
                        ->setCellValue('P'.$i_row, "$SSDY Y $SSDM M $SSDD D")
                        ->setCellValue('Q'.$i_row, $emp->BankAccountNo)
                        ->setCellValue('R'.$i_row, $emp->AddressPermanent);
                    }
                        $objPHPExcel->getActiveSheet()->getStyle('E')
                        ->getNumberFormat()
                        ->setFormatCode( '0000000000000' );
                        $objPHPExcel->getActiveSheet()->getStyle('F')
                        ->getNumberFormat()
                        ->setFormatCode( '0000000000000' );

                        $i_row++;
                        $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A'.$i_row, 'Total หน่วย ('.$c.') | '.$dCount.'.'.$deptCount.'.'.$sCount.'.'.$rData->SectionNameEng.' | '.$rData->SectionNameThai);
                        $SectionID = $rData->SectionID;
                        $objPHPExcel->getActiveSheet()->getStyle('A'.$i_row.':R'.$i_row)->applyFromArray($styleBold);


                }//END IF

            }






        }

    }











    // กำหนดรูปแบบของไฟล์ที่ต้องการเขียนว่าเป็นไฟล์ excel แบบไหน ในที่นี้เป้นนามสกุล xlsx  ใช้คำว่า Excel2007
    // แต่หากต้องการกำหนดเป็นไฟล์ xls ใช้กับโปรแกรม excel รุ่นเก่าๆ ได้ ให้กำหนดเป็น  Excel5
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');  // Excel2007 (xlsx) หรือ Excel5 (xls)

    $filename='HRExcelFilterByUnit-'.date("dmYHi").'.xlsx'; //  กำหนดชือ่ไฟล์ นามสกุล xls หรือ xlsx
    // บังคับให้ทำการดาวน์ดหลดไฟล์
    header('Content-Type: application/vnd.ms-excel'); //mime type
    header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
    header('Cache-Control: max-age=0'); //no cache
    ob_end_clean();
    $objWriter->save('php://output'); // ดาวน์โหลดไฟล์รายงาน
    // หากต้องการบันทึกเป็นไฟล์ไว้ใน server  ใช้คำสั่งนี้ $this->excel->save("/path/".$filename);
    // แล้วตัด header ดัานบนทั้ง 3 อันออก
    exit;
?>
