<script type="text/javascript">
  $(document).ready(function(){
    $('.selectpicker').selectpicker({style: 'btn-warning',size: 5});
    $('#username').focus();
    $('#btn_login').click(function(){
      $.ajax({
        url:'<?php echo base_url(); ?>index.php/home/login',
        data:'username='+$('#username').val()+'&password='+$('#password').val(),
        type:'POST',
        success:function(res){
          if(res=='true'){
            swal({title : 'แสดงเมือทำงานสำเร็จ',text : '',type : 'success'},
              function(){window.location.replace("<?php echo base_url() ?>index.php/member");
            }
            );
          }else{swal({title : 'แสดงเมื่อทำงานไม่สำเร็จ',text : '',type : 'error'});}
        },error:function(err){swal({title : 'เกิดข้อผิดพลาด',text : err,type : 'error'});}
      });
    });
  });
</script>
<head>
  <meta charset="UTF-8">
  <title><?php echo $this->uri->segment(2);?></title>
  <link href="<?php echo base_url();?>assets/bootstrap-select/css/bootstrap-select.css" rel="stylesheet">
  <link href="<?php echo base_url();?>assets/bootstrap/css/datepicker.css" rel="stylesheet">
  <link href="<?php echo base_url();?>assets/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">
<style>
  canvas{
    -moz-user-select: none;
    -webkit-user-select: none;
    -ms-user-select: none;
  }
  </style>
</head>
<div class="box box-success">
  <div class="box-header">
        <i class="fa fa-sign-in"></i>
    <h3 class="box-title"><?php echo $this->systemmodel->get_menuname($this->uri->segment(1) . '/' . $this->uri->segment(2)); // แสดงชื่อเมนู  ?></h3>
  </div>
  <div class="box-body">
    <form class="" name="form1" action=""  method="post">
      <div class="row clearfix">
          <div class="col-md-4">
            <p><b>Gender</b></p>
            <select class="form-control show-tick selectpicker" name="type_graph" id="type_graph">
              <option value="bar">แนวตั้ง</option>
              <option value="horizontalBar">แนวนอน</option>
              <option value="radar">เรดาร์</option>
            </select>
          </div>
          <div class="col-md-4">
            <p><b>Department</b></p>
            <select class="form-control show-tick selectpicker" name="sel_dep" id="sel_dep">
              <?php
              foreach ($department as $row) {
                  if ($row->department_id == $this->session->department_id) {
                      $selected = ' selected';
                  } else {
                      $selected = '';
                  }
                echo "<option value='".$row->department_id."' $selected>".$row->departmentNameEng."</option>";
              }
              ?>
            </select>
          </div>
          <div class="col-md-4">
            <p><b>New Employee</b></p>
            <select class="form-control show-tick selectpicker" name="showyear" id="showyear">
              <option value="5">5 ปี</option>
              <option value="4">4 ปี</option>
              <option value="3">3 ปี</option>
              <option value="2">2 ปี</option>
              <option value="1">1 ปี</option>
            </select>
          </div>
      </div>
      <div class="row clearfix">
          <div class="col-md-4">
            <p><b>Starting Date</b></p>
            <div class="form-group">
                <div class='input-group date' id='datetimepicker1'>
                    <input type='text' class="form-control" id='date1' name="date1" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>
            <script>
              $(document).ready(function(){
                var date_input=$('input[name="date1"]'); //our date input has the name "date"
                var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
                date_input.datepicker({
                  format: 'mm/dd/yyyy',
                  container: container,
                  todayHighlight: true,
                  autoclose: true,
                })
              })
            </script>
          </div>
          <div class="col-md-4">
            <p><b>Birthday</b></p>
            <div class="form-group">
                <div class='input-group date' id='datetimepicker1'>
                    <input type='text' class="form-control" id='date2' name="date2" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>
            <script>
              $(document).ready(function(){
                var date_input=$('input[name="date2"]'); //our date input has the name "date"
                var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
                date_input.datepicker({
                  format: 'mm/dd/yyyy',
                  container: container,
                  todayHighlight: true,
                  autoclose: true,
                })
              })
            </script>
          </div>
          <div class="col-md-4">
            <p><b>Resigned</b></p>
            <select class="form-control show-tick selectpicker" name="showyear" id="showyear">
              <option value="5">5 ปี</option>
              <option value="4">4 ปี</option>
              <option value="3">3 ปี</option>
              <option value="2">2 ปี</option>
              <option value="1">1 ปี</option>
            </select>
      </div>
      <div class="row clearfix">
        <div class="col-md-12" align="center">
          <br>
           <div class="col-md-4" align="center"></div>
           <div class="col-md-4" align="center">
            <button type="submit" class="btn btn-primary btn-md btn-block" id="btn_reportgraph">แสดงรายงาน</button>
          </div>
        </div>
      </div>
    </form>
<hr>
    <div class="row clearfix">
        <div class="col-md-12" align="center">
          <div class="loading"></div>
          <div  id="showreultgraph" class="table-responsive">
          </div>
        </div>
    </div>
    <!-- จบส่วนแสดงผล -->
  </div>
</div>
<!-- Bootstrap-Select -->
<script src="<?php echo base_url();?>assets/bootstrap-select/js/bootstrap-select.js"></script>
<?php if (isset($approve_js)) { echo $approve_js; }?>
<!-- Ajax Search -->
    <script type="text/javascript">
          $('form').submit(function(event) {
              $.ajax({
                url: "view_demoreport",
                type: "post",
                data: {
                       type_graph: $("#type_graph").val(),
                       showdep: $("#showdep").val(),
                       showyear: $("#showyear").val()
                      },
                beforeSend: function () {$(".loading").show();},
                complete: function () {$(".loading").hide();},
                success: function (data) {$("#showreultgraph").html(data);}
            }).done(function(data) {console.log(data);});event.preventDefault();});
    </script>
<script src="<?php echo base_url();?>assets/chartjs/Chart.bundle.js"></script>
<script src="<?php echo base_url();?>assets/chartjs/Chart.js"></script>
<script src="<?php echo base_url();?>assets/bootstrap/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url();?>assets/jquery-datatable/jquery.dataTables.js"></script>
<script src="<?php echo base_url();?>assets/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
<script src="<?php echo base_url();?>assets/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url();?>assets/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
<script src="<?php echo base_url();?>assets/jquery-datatable/extensions/export/jszip.min.js"></script>
<script src="<?php echo base_url();?>assets/jquery-datatable/extensions/export/pdfmake.min.js"></script>
<script src="<?php echo base_url();?>assets/jquery-datatable/extensions/export/vfs_fonts.js"></script>
<script src="<?php echo base_url();?>assets/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
<script src="<?php echo base_url();?>assets/jquery-datatable/extensions/export/buttons.print.min.js"></script>