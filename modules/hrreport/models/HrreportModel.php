<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

// เปลี่ยนชื่อโมเดล (MemberModel) ให้เป็นชื่อโมเดล อักษาตัวแรกให้เป็นตัวใหญ่และตามด้วยคำว่า Model
class HrreportModel extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }
    public function form_ep_info($dateset,$emplevel,$empgroup,$gender,$nationality,$worktype){
        $query_str="SELECT *
        from Employee a
            left outer join Company b on b.CompID=a.CompID
            left outer join Division c on c.DivisionID=a.DivisionID
            left outer join Department d on d.DepartmentID=a.DepartmentID
            left outer join Section e on e.SectionID=a.SectionID
            left outer join Position f on f.PositionID=a.PositionID
            left outer join Title g on g.TitleID=a.TitleID
            left outer join Gender h on a.GenderID = h.GenderID
            left outer join Nationality i on a.NationalityID = i.NationlityID
            left outer join districts i1 on a.SubDistrictID = i1.DISTRICT_ID
            left outer join amphures i2 on a.DistrictID = i2.AMPHUR_ID
            left outer join provinces i3 on a.ProvinceID = i3.ProvinceID
            left outer join EmplyeeLevel i4 on a.EmployeeLevel = i4.EmpLevelID
            left outer join zipcodes i5 on a.Postcode = i5.zipcode
        where (a.CompID={$this->session->CompID})

            and (EmployeeType = '$empgroup' or '$empgroup'='All' or ('$empgroup'='Current' and EmployeeType <>'Resign'))
            and (a.GenderID = $gender or $gender=0)
            and (NationalityID = $nationality or $nationality=0)
            and (WorkingType = $worktype or $worktype=0)
            and (ServiceStartDate< '$dateset' and EffectiveDateOut<'$dateset')


        order by a.EmployeeCode
        ";
        $query=$this->db->query($query_str);
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    public function getEmployeeYearlyIncomeTaxDetails($year){

        if(empty($year)) $year=0;

        $query_str="
        select *,b.Postcode as postcode_com
        from Employee a
            left outer join Company b on b.CompID=a.CompID
            left outer join Division c on c.DivisionID=a.DivisionID
            left outer join Department d on d.DepartmentID=a.DepartmentID
            left outer join Section e on e.SectionID=a.SectionID
            left outer join Position f on f.PositionID=a.PositionID
            left outer join Title g on g.TitleID=a.TitleID
            left outer join districts a1 on a1.DISTRICT_CODE = a.SubDistrictID
            left outer join amphures a2 on a2.AMPHUR_ID = a.DistrictID
            left outer join provinces a3 on a3.ProvinceID = a.ProvinceID
        where (a.CompID=".$this->session->CompID.")
            and a.PITCode <> 4
            and (a.ResignationTypeStatusOut=0 or (a.ResignationTypeStatusOut=1 and year(a.EffectiveDateOut)=$year) )
            and year(a.ServiceStartDate) <= $year
        order by a.EmployeeCode
        "
        ;
        $query=$this->db->query($query_str); if ($query->num_rows() > 0) { return $query->result();  } else { return false; }
    }


    public function getMultiDATA($EmployeeID,$YPay,$MPay,$typereport){
        $data = array(
            'isinreport' => false ,
            'dayStartWorking' => 0 ,
            'SalaryCal' => 0 ,
            'SalaryAdj' => 0 ,
            'Effectdate' => '',
            'WorkingDays' => 0,
            'SS' =>0 ,
            'data_inc' => array() ,
            'data_ded' => array() ,
            'SSAllowance' => 0 ,
            'TaxAllowance'=> 0 ,
            'OtherIncome'=> 0 ,
            'TotalIncome'=> 0 ,
            'OtherIncome_tax'=> 0 ,
            'OtherIncome_SS'=> 0 ,

            'Tax'=> 0 ,
            'OtherDeduct'=> 0 ,
            'OtherDeduct_tax'=> 0 ,
            'OtherDeduct_SS'=> 0 ,
            'TotalDeduct'=> 0 ,
            'NetToPay'=> 0 ,
            'NetToPaySS'=> 0 ,
            'NetToPayTAX'=> 0 ,
        );

        if(!empty($EmployeeID)){

            $dateToTest = $YPay."-".$MPay."-01";
            $lastday = $YPay."-".$MPay."-".date('t',strtotime($dateToTest));
            // $dlastday = date('t',strtotime($dateToTest));
            $d1lastday = date('t',strtotime($dateToTest)); //จะนำไปใช้คำนวณเงินเดือนในช่อง adjust
            $dlastday = 30;
            if($typereport==1){
                $data['data_inc'] = $this->hrreportModel->getDataINCDED_TAX($EmployeeID,'INC',$lastday);
                $data['data_ded'] = $this->hrreportModel->getDataINCDED_TAX($EmployeeID,'DED',$lastday);
                $data['OtherIncome'] = $this->hrreportModel->getDataINCDED_TAX_SUM($EmployeeID,'INC',$lastday);
                $data['OtherDeduct'] = $this->hrreportModel->getDataINCDED_TAX_SUM($EmployeeID,'DED',$lastday);
            }else if ($typereport==2) {
                $data['data_inc'] = $this->hrreportModel->getDataINCDED_SOCIAL($EmployeeID,'INC',$lastday);
                $data['data_ded'] = $this->hrreportModel->getDataINCDED_SOCIAL($EmployeeID,'DED',$lastday);
                $data['OtherIncome'] = $this->hrreportModel->getDataINCDED_SOCIAL_SUM($EmployeeID,'INC',$lastday);
                $data['OtherDeduct'] = $this->hrreportModel->getDataINCDED_SOCIAL_SUM($EmployeeID,'DED',$lastday);
            }else{
                $data['data_inc'] = $this->hrreportModel->getDataINCDED($EmployeeID,'INC',$lastday);
                $data['data_ded'] = $this->hrreportModel->getDataINCDED($EmployeeID,'DED',$lastday);
                $data['OtherIncome'] = $this->hrreportModel->getDataINCDED_SUM($EmployeeID,'INC',$lastday);
                $data['OtherDeduct'] = $this->hrreportModel->getDataINCDED_SUM($EmployeeID,'DED',$lastday);
            }
            $data['OtherIncome_tax'] = $this->hrreportModel->getDataINCDED_TAX_SUM($EmployeeID,'INC',$lastday);
            $data['OtherDeduct_tax'] = $this->hrreportModel->getDataINCDED_TAX_SUM($EmployeeID,'DED',$lastday);
            $data['OtherIncome_SS'] = $this->hrreportModel->getDataINCDED_SOCIAL_SUM($EmployeeID,'INC',$lastday);
            $data['OtherDeduct_SS'] = $this->hrreportModel->getDataINCDED_SOCIAL_SUM($EmployeeID,'DED',$lastday);

            // $data['OtherIncome'] = 0;
            // $data['OtherDeduct'] = 0;
            // if(!empty($data['data_inc'])){  foreach ($data['data_inc'] as $row2) {$data['OtherIncome'] += $row2->Amount;} }
            // if(!empty($data['data_ded'])){  foreach ($data['data_ded'] as $row2) {$data['OtherDeduct'] += $row2->Amount;} }

            $this->load->model('hrreportModel');
            $results_All = $this->hrreportModel->getEmployeeByID($EmployeeID);

            foreach ($results_All as $row) {
                $data['isinreport']=true;
                $Effectdate = $row->ServiceStartDate;
                $data['Effectdate'] = $Effectdate;
                $dayStartWorking = intval(date("d",strtotime($Effectdate)));
                $MonthStartWorking = intval(date("m",strtotime($Effectdate)));
                $YearStartWorking = intval(date("Y",strtotime($Effectdate)));
                $data['dayStartWorking'] = $dayStartWorking;

                $config_dlastday = $dlastday; // *************** ค่าเดิม 20 กำหนด วันที่รับเงินเดือน ในเดือนถัดไป dlastday คือ ถ้าวันมากกว่า ให้ไป เดือนถัดไป Icha ตรงนี้เดิมเราต้องการให้เงินเดือนคำนวณจาก timesheet เนื่องจากการจัดทำเงินเดือนต้องมีวัน cutoff date เพื่อให้ทางผู้จัดทำเงินเดือนมีเวลาในการจัดทำ

                if($YPay==$YearStartWorking && $MPay==$MonthStartWorking && $dayStartWorking>$config_dlastday) $data['isinreport']=false;
                else if($YPay==$YearStartWorking && $MPay<$MonthStartWorking) $data['isinreport']=false;
                else if (
                $row->ResignationTypeStatusOut == 1 &&
                (
                // ( (intval(date("d",strtotime($row->EffectiveDateOut)))) < 20) &&
                ( ($MPay - intval(date("m",strtotime($row->EffectiveDateOut)))) == 1) &&
                $YPay == intval(date("Y",strtotime($row->EffectiveDateOut)))
                )
                ) $data['isinreport'] = false;
                else if (
                $row->ResignationTypeStatusOut == 1 &&
                (
                ( ($MPay - intval(date("m",strtotime($row->EffectiveDateOut)))) > 1) &&
                $YPay == intval(date("Y",strtotime($row->EffectiveDateOut)))
                )
                ) $data['isinreport'] = false;
                else if ($row->ResignationTypeStatusOut == 1 && $YPay > intval(date("Y",strtotime($row->EffectiveDateOut)))) $data['isinreport'] = false;
            }

            // echo intval(date("m",strtotime($row->EffectiveDateOut)))."-".intval(date("Y",strtotime($row->EffectiveDateOut)))."|".$row->EffectiveDateOut.":".$row->EmployeeID."|$YPay-$MPay<BR>";

            $resultLastSalary = $this->hrreportModel->getLastSalary($EmployeeID,$lastday);
            if(!empty($resultLastSalary)){
                foreach($resultLastSalary as $result2){
                    $oldsalary = $this->hrreportModel->getOldSalary($EmployeeID,$result2->Salary);
                    $data["SalaryCal"] = $result2->Salary;
                    $MMDate_d = intval(date("d",strtotime($result2->MMDate)));
                    $MMDate_m = intval(date("m",strtotime($result2->MMDate)));
                    $MMDate_y = intval(date("Y",strtotime($result2->MMDate)));
                    if($MMDate_m==$MPay && $MMDate_y == $YPay){
                        $SalaryCal2 = ($oldsalary/$dlastday)*($MMDate_d-1);
                        $SalaryCal3 = ($result2->Salary/$dlastday)*($dlastday-($MMDate_d-1));
                        $SalaryCal2 = $SalaryCal2+$SalaryCal3;
                        $SalaryAdj2 = ($SalaryCal2 - $data['SalaryCal']);
                    }

                    $dateCalSalary = "$YPay-$MPay-01";
                    $datetime1 = date_create($Effectdate);
                    $datetime2 = date_create($lastday);

                    $interval = date_diff($datetime1, $datetime2);
                    $datechk = intval($interval->format('%a'));

                    $config_daystart_check = 1; // เดิม เชค วันที่ 21
                    $config_total_daycheck = $config_dlastday+$config_daystart_check+1;

                    if($datechk>28 && $datechk<=$config_total_daycheck){
                        if($dayStartWorking<$config_daystart_check){
                            $datetime1 = date_create($dateCalSalary);
                            $datetime2 = date_create($lastday);
                            $interval = date_diff($datetime1, $datetime2);
                        }
                    }else if($datechk>$config_total_daycheck){
                        $datetime1 = date_create($dateCalSalary);
                        $datetime2 = date_create($lastday);
                        $interval = date_diff($datetime1, $datetime2);
                    }


                    if($row->ResignationTypeStatusOut == 1 && $MPay == intval(date("m",strtotime($row->EffectiveDateOut)))){
                        if($MPay == intval(date("m",strtotime($Effectdate)))){
                            // echo $row->EffectiveDateOut.":".$Effectdate."<BR>";
                            // echo intval(date("d",strtotime($row->EffectiveDateOut)))."-".intval(date("d",strtotime($Effectdate)))."<BR>";
                            $data['WorkingDays'] = intval(date("d",strtotime($row->EffectiveDateOut)))-intval(date("d",strtotime($Effectdate)));
                        }
                        else $data['WorkingDays'] = intval(date("d",strtotime($row->EffectiveDateOut)));
                    }else{
                        $data['WorkingDays'] = intval($interval->format('%a'))+1;
                    }

                    $perdays = $data['SalaryCal']/$dlastday;
                    if($MMDate_m==$MPay && $MMDate_y == $YPay) $data['SalaryAdj'] = $SalaryAdj2;
                    else if($data['WorkingDays']>=$dlastday) $data['SalaryAdj'] = ($perdays)*($data['WorkingDays']-$dlastday);
                    else $data['SalaryAdj'] = -1*($perdays)*($dayStartWorking-1);

                    $totaldayOut = 0;
                    $totaldayofMonth = 0;

                    if($row->ResignationTypeStatusOut == 1){
                        $totaldayOut = date('t',strtotime($dateToTest)) - $data['WorkingDays'];
                        $totaldayofMonth = date('t',strtotime($dateToTest));
                        $data['SalaryAdj'] = -1*($data['SalaryCal']/$totaldayofMonth) * $totaldayOut;
                    }
                }
            }

            $data['SS'] = $data['SalaryCal']+$data['SalaryAdj'] + $data['OtherIncome_SS'] - $data['OtherDeduct_SS'];

            // echo $data['SalaryCal']."+".$data['SalaryAdj']." - ".$data['OtherDeduct_tax']."=".$data['SS']."<BR>";
            $data['TaxAllowance']=0;

            //get ค่า ประกันสังคม
            $ss_cal = $this->hrreportModel->getSSCal($lastday)/100;

            // echo $ss_cal."<BR>";

            if($row->SocialID > 2 )$data['SS'] = 0;  // กรณีนี้คือ Non Applicable
            else if($data['SS'] <= 0 ) $data['SS']=0*$ss_cal;
            else if($data['SS'] < 1650 ) $data['SS']=1650*$ss_cal;
            else if($data['SS']>15000) $data['SS']=15000*$ss_cal;
            else $data['SS'] = $data['SS']*$ss_cal;

            $data['SS'] = round($data['SS']);
            // echo "<BR>";

            if($row->SocialID == 2 )$data['SSAllowance'] = $data['SS'];
            else $data['SSAllowance']=0;

            // if ($typereport==2) $data['TotalIncome'] = $data['OtherIncome']+$data['SalaryCal']+$data['SalaryAdj'];
            // else
            // $data['TotalIncome'] = $data['OtherIncome']+$data['SalaryCal']+$data['SalaryAdj'];//+$data['SSAllowance']+$data['TaxAllowance'];
            // echo "1 | ". $data['TotalIncome'] .">>>>>".$data['OtherIncome_tax'].">>>>>".$data['OtherDeduct_SS']."<BR>";

            if($row->PITCode ==4 ) $data['Tax'] = 0;  // PITCODE = 4 คือ non applicance
            else $data['Tax'] = $this->hrreportModel->GenTax( round( ($data['OtherIncome_tax']+$data['SalaryCal']+$data['SalaryAdj'] - $data['OtherDeduct_tax']),2) , $EmployeeID);

            if($row->PITCode == 2 )$data['TaxAllowance'] = $data['Tax'];
            else $data['TaxAllowance']=0;

            if ($typereport==2)  $data['TotalIncome'] = $data['OtherIncome']+$data['SalaryCal']+$data['SalaryAdj'];
            else $data['TotalIncome'] = $data['OtherIncome']+$data['SalaryCal']+$data['SalaryAdj']+$data['SSAllowance']+$data['TaxAllowance'];

            $data['TotalDeduct'] = $data['OtherDeduct']+$data['SS']+$data['Tax'];
            $data['NetToPay'] = $data['TotalIncome'] - $data['TotalDeduct'];
            $data['NetToPaySS'] = $data['SalaryCal']+$data['SalaryAdj']+$data['OtherIncome_SS'] - $data['OtherDeduct_SS'];
            $data['NetToPayTAX'] = $data['SalaryCal']+$data['SalaryAdj']+$data['SSAllowance']+$data['TaxAllowance']+$data['OtherIncome_tax'] - $data['OtherDeduct_tax'];
        }

        return $data;
    }

    public function getDataINCDED_TAX($EmployeeID,$IDCCode,$PaymentPeriod){
        $query_str="
        select a.*,b.NameEN
        from  IncomeDeductionConfig b
        left outer join IncomeDeduction a on a.IncomeDeductionConfigID = b.IncomeDeductionConfigID
        where a.EmployeeID = $EmployeeID
        and IDCCode='$IDCCode'
        and PaymentPeriod = '$PaymentPeriod'
        and EffecttoWorking = 1
        and b.ApplyIDC in (1,3)
        order by a.IncomeDeductionConfigID ";
        $query=$this->db->query($query_str);
        if ($query->num_rows() > 0) return $query->result();
        else  return false;
    }
    public function getDataINCDED_TAX_SUM($EmployeeID,$IDCCode,$PaymentPeriod){
        $query_str="
        select sum(a.Amount) as sumAmount
        from  IncomeDeductionConfig b
        left outer join IncomeDeduction a on a.IncomeDeductionConfigID = b.IncomeDeductionConfigID
        where a.EmployeeID = $EmployeeID
        and IDCCode='$IDCCode'
        and PaymentPeriod = '$PaymentPeriod'
        and EffecttoWorking = 1
        and b.ApplyIDC in (1,3)
        order by a.IncomeDeductionConfigID ";
        $query=$this->db->query($query_str);

        if ($query->num_rows() > 0) {
            $DATA =  $query->result();
            foreach ($DATA as $row) return $row->sumAmount;
        }
        else  return 0;
    }

    public function getDataINCDED_SOCIAL($EmployeeID,$IDCCode,$PaymentPeriod){
        $query_str="
        select a.*,b.NameEN
        from  IncomeDeductionConfig b
        left outer join IncomeDeduction a on a.IncomeDeductionConfigID = b.IncomeDeductionConfigID
        where a.EmployeeID = $EmployeeID
        and IDCCode='$IDCCode'
        and PaymentPeriod = '$PaymentPeriod'
        and EffecttoWorking = 1
        and b.ApplyIDC in (2,3)
        order by a.IncomeDeductionConfigID ";
        $query=$this->db->query($query_str);
        if ($query->num_rows() > 0) return $query->result();
        else  return false;
    }
    public function getDataINCDED_SOCIAL_SUM($EmployeeID,$IDCCode,$PaymentPeriod){
        $query_str="
        select sum(a.Amount) as sumAmount
        from  IncomeDeductionConfig b
            left outer join IncomeDeduction a on a.IncomeDeductionConfigID = b.IncomeDeductionConfigID
        where a.EmployeeID = $EmployeeID
            and IDCCode='$IDCCode'
            and PaymentPeriod = '$PaymentPeriod'
            and EffecttoWorking = 1
            and b.ApplyIDC in (2,3)
        order by a.IncomeDeductionConfigID ";
        $query=$this->db->query($query_str);
        if ($query->num_rows() > 0) {
            $DATA =  $query->result();
            foreach ($DATA as $row) return $row->sumAmount;
        }
        else  return 0;
    }

    public function getEmployeefromsocialnewcoming($year){

        if(empty($year)) $year=0;

        $query_str="
        select *,b.Postcode as postcode_com
        from Employee a
            left outer join Company b on b.CompID=a.CompID
            left outer join Division c on c.DivisionID=a.DivisionID
            left outer join Department d on d.DepartmentID=a.DepartmentID
            left outer join Section e on e.SectionID=a.SectionID
            left outer join Position f on f.PositionID=a.PositionID
            left outer join Title g on g.TitleID=a.TitleID
            left outer join districts a1 on a1.DISTRICT_CODE = a.SubDistrictID
            left outer join amphures a2 on a2.AMPHUR_ID = a.DistrictID
            left outer join provinces a3 on a3.ProvinceID = a.ProvinceID
        where (a.CompID=".$this->session->CompID.")
            and a.PITCode <> 4
            and (a.ResignationTypeStatusOut=0 or (a.ResignationTypeStatusOut=1 and year(a.EffectiveDateOut)=$year) )
            and year(a.ServiceStartDate) <= $year
        order by a.EmployeeCode
        "
        ;
        $query=$this->db->query($query_str); if ($query->num_rows() > 0) { return $query->result();  } else { return false; }
    }

    public function form_ep_contract($dateset,$emplevel,$empgroup,$gender,$nationality,$worktype){
        $query_str="
        select *
        from Employee a
            left outer join Company b on b.CompID=a.CompID
            left outer join Division c on c.DivisionID=a.DivisionID
            left outer join Department d on d.DepartmentID=a.DepartmentID
            left outer join Section e on e.SectionID=a.SectionID
            left outer join Position f on f.PositionID=a.PositionID
            left outer join Title g on g.TitleID=a.TitleID
            left outer join Gender h on a.GenderID = h.GenderID
            left outer join Nationality i on a.NationalityID = i.NationlityID
            left outer join districts i1 on a.SubDistrictID = i1.DISTRICT_ID
            left outer join amphures i2 on a.DistrictID = i2.AMPHUR_ID
            left outer join provinces i3 on a.ProvinceID = i3.ProvinceID
            left outer join EmplyeeLevel i4 on a.EmployeeLevel = i4.EmpLevelID
            left outer join zipcodes i5 on a.Postcode = i5.zipcode
        where (a.CompID=".$this->session->CompID.")

            and (EmployeeType = '$empgroup' or '$empgroup'='All' or ('$empgroup'='Current' and EmployeeType <>'Resign'))
            and (a.GenderID = $gender or $gender=0)
            and (NationalityID = $nationality or $nationality=0)
            and (WorkingType = $worktype or $worktype=0)
            and (ServiceStartDate< '$dateset' and EffectiveDateOut<'$dateset')


        order by a.EmployeeCode
        ";
        $query=$this->db->query($query_str);
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    public function getEmployeeLevel(){
        $query_str=" select * from EmplyeeLevel a  where (a.CompID=".$this->session->CompUSERID.")";
        $query=$this->db->query($query_str);
        if ($query->num_rows() > 0) return $query->result();
        else return false;
    }
    public function getEmployeeGroup(){
        $query_str=" select * from EmployeeGroup a  where (a.CompID=".$this->session->CompUSERID.")";
        $query=$this->db->query($query_str);
        if ($query->num_rows() > 0) return $query->result();
        else return false;
    }
    public function getGender(){
        $query_str=" select * from Gender";
        $query=$this->db->query($query_str);
        if ($query->num_rows() > 0) return $query->result();
        else return false;
    }
    public function getNationality(){
        $query_str=" select * from Nationality a  where (a.CompID=".$this->session->CompUSERID.")";
        $query=$this->db->query($query_str);
        if ($query->num_rows() > 0) return $query->result();
        else return false;
    }
    public function getWorkType(){
        $query_str=" select * from WorkType a  where (a.CompID=".$this->session->CompUSERID.")";
        $query=$this->db->query($query_str);
        if ($query->num_rows() > 0) return $query->result();
        else return false;
    }

    public function getcertify_ep_documents($year){

        if(empty($year)) $year=0;

        $query_str="
        select *
        from Employee a
            left outer join Company b on b.CompID=a.CompID
            left outer join Division c on c.DivisionID=a.DivisionID
            left outer join Department d on d.DepartmentID=a.DepartmentID
            left outer join Section e on e.SectionID=a.SectionID
            left outer join Position f on f.PositionID=a.PositionID
            left outer join Title g on g.TitleID=a.TitleID
            left outer join districts a1 on a1.DISTRICT_CODE = a.SubDistrictID
            left outer join amphures a2 on a2.AMPHUR_ID = a.DistrictID
            left outer join provinces a3 on a3.ProvinceID = a.ProvinceID
        where (a.CompID=".$this->session->CompID.")
            and (a.ResignationTypeStatusOut=0 or (a.ResignationTypeStatusOut=1 and year(a.EffectiveDateOut)=$year) )
            and year(a.ServiceStartDate) <= $year
        order by a.EmployeeCode
        "
        ;
        $query=$this->db->query($query_str); if ($query->num_rows() > 0) { return $query->result();  } else { return false; }
    }

}
