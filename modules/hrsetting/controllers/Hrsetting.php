<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

// เปลี่ยนชื่อคลาส (Name) ให้เป็นชื่อคอนโทรลเลอร์ อักษาตัวแรกให้เป็นตัวใหญ่
class Hrsetting extends Smart_Controller
{

    public function __construct(){
        parent::__construct();
        // ตรวจสอบการเข้าสู่ระบบ ถ้ายังไม่เข้าสู่ระบบให้กลับไปที่หน้า login
        if (!isset($_SESSION['username'])) {
            redirect(base_url() . 'index.php/home/login_form');
        }
        // จบ ตรวจสอบการเข้าสู่ระบบ
        // ตรวจสอบสิทธิ์การใช้งานเมนู
        $res_permission = $this->systemmodel->get_menulink_permission($this->session->person_id);
        if (!in_array($this->uri->segment(1) . '/' . $this->uri->segment(2), $res_permission)) {
            redirect(base_url() . 'index.php/member/nopermission');
        }

        // จบ ตรวจสอบสิทธิ์การใช้งานเมนู

        $this->load->library('grocery_CRUD');
    }

    public function index(){
        // หน้าแรกที่จะแสดงเมื่อเข้าสู่โมดูล
        $data = 'member/ready';
        // ส่งัวแปรหน้าแรกที่จะแสดงผลไปแสดงผลในไฟล์แทมเพลต
        $this->load->view('templates/index', $data);
    }
    public function _example_output($output = null)
    {
        // ส่วนแสดงผลใช้กับ grocery crud
        $data['content'] = 'system/grocery_crud';
        $data['output']  = $output;
        $this->load->view('templates/index', $data);
    }

    public function Bank()
    {
        $crud = new grocery_CRUD();
        $crud->set_table('Bank')
        ->set_subject($this->systemmodel->changeLng("ธนาคาร"))
        ->columns(
            'BankID'
            ,'BankNameEN'
            ,'BankNameLL'
            ,'BranchNameEN'
            ,'BranchNameLL'
            )
        ->display_as('BankNameEN',$this->systemmodel->changeLng("ชื่อธนาคารภาษาอังกฤษ"))
        ->display_as('BankNameLL',$this->systemmodel->changeLng("ชื่อธนาคารภาษาไทย"))
        ->display_as('BranchNameEN',$this->systemmodel->changeLng("ชื่อสาขาภาษาอังกฤษ"))
        ->display_as('BranchNameLL',$this->systemmodel->changeLng("ชื่อสาขาภาษาไทย"))
        ;
        $crud->where('CompID',$this->session->CompUSERID);
        $crud->fields(
        	'BankID'
            ,'BankNameEN'
            ,'BankNameLL'
            ,'BranchNameEN'
            ,'BranchNameLL'
            ,'CompID'
        );
        $crud->required_fields(
            'BankID'
            ,'BankNameEN'
            ,'BankNameLL'
            ,'BranchNameEN'
            ,'BranchNameLL'
        );

        // hidden field\
        $crud->field_type('CompID', 'hidden', $this->session->CompUSERID);


        $output = $crud->render();
        $this->_example_output($output);
    }
    public function ProvidentFundRate()
    {
        $crud = new grocery_CRUD();
        $crud->set_table('ProvidentFundRate')
        //->set_subject('Add ProvidentFundRate')
        ->columns(
            'ProvidentID'
            ,'ProvidentRate'
            );
        
        $crud->where('CompID',$this->session->CompUSERID);
        $crud->field_type('CompID', 'hidden', $this->session->CompUSERID);
        $crud->fields(
            'ProvidentRate'
            ,'CompID'
        );
        $crud->required_fields(
            'ProvidentRate'
        );


        $output = $crud->render();
        $this->_example_output($output);
    }
    public function Title()
    {
        $crud = new grocery_CRUD();
        $crud->set_table('Title')
        //->set_subject('Add Title')
        ->display_as('TitleID',$this->systemmodel->changeLng('รหัสคำนำหน้า'))
        ->display_as('TitleTH',$this->systemmodel->changeLng('ชื่อคำนำหน้าภาษาท้องถิ่น'))
        ->display_as('TitleEN',$this->systemmodel->changeLng('ชื่อคำนำหน้าภาษาอังกฤษ'))
        ->columns(
            'TitleID'
            ,'TitleTH'
            ,'TitleEN'
            );
        $crud->where('CompID',$this->session->CompUSERID);
        $crud->fields(
            'TitleTH'
            ,'TitleEN'
            ,'CompID'
        );
        $crud->required_fields(
            'TitleTH','TitleEN','CompID'
        );
        $crud->field_type('CompID', 'hidden', $this->session->CompUSERID);


        $output = $crud->render();
        $this->_example_output($output);
    }
    public function FamilyStatus()
    {
        $crud = new grocery_CRUD();
        $crud->set_table('FamilyStatus')
        //->set_subject('Add FamilyStatus')
->display_as('FamilyStatusID',$this->systemmodel->changeLng('รหัสสถานะครอบครัว'))
->display_as('FamilyStatusNameEN',$this->systemmodel->changeLng('ชื่อสถานะครอบครัวภาษาอังกฤษ'))
->display_as('FamilyStatusNameLL',$this->systemmodel->changeLng('ชื่อสถานะครอบครัวภาษาท้องถิ่น'))
        ->columns(
            'FamilyStatusID'
            ,'FamilyStatusNameEN'
            ,'FamilyStatusNameLL'
            );

        $crud->where('CompID',$this->session->CompUSERID);
        $crud->field_type('CompID', 'hidden', $this->session->CompUSERID);
        $crud->fields(
            'FamilyStatusNameEN'
            ,'FamilyStatusNameLL'
            ,'CompID'
        );
        $crud->required_fields(
            'FamilyStatusNameEN','FamilyStatusNameLL'
        );
        $output = $crud->render();
        $this->_example_output($output);
    }
    public function EmployeeGroup()
    {
        $crud = new grocery_CRUD();
        $crud->set_table('EmployeeGroup')
        //->set_subject('Add EmployeeGroup')
        ->display_as('EmpGroupID',$this->systemmodel->changeLng('รหัสกลุ่มพนักงาน'))
        ->display_as('EmpGroupNameLL',$this->systemmodel->changeLng('ชื่อกลุ่มพนักงานภาษาท้องถิ่น'))
        ->display_as('EmpGroupNameEN',$this->systemmodel->changeLng('ชื่อกลุ่มพนักงานภาษาอังกฤษ'))
        ->columns(
            'EmpGroupID'
            ,'EmpGroupNameEN'
            ,'EmpGroupNameLL'
            );
        $crud->where('CompID',$this->session->CompUSERID);
        $crud->field_type('CompID', 'hidden', $this->session->CompUSERID);
        $crud->fields(
            'EmpGroupNameEN'
            ,'EmpGroupNameLL'
            ,'CompID'
        );
        $crud->required_fields(
            'EmpGroupNameEN','EmpGroupNameLL'
        );
        

        $output = $crud->render();
        $this->_example_output($output);
    }
    public function EmplyeeLevel()
    {
        $crud = new grocery_CRUD();
        $crud->set_table('EmplyeeLevel')
        //->set_subject('Add EmplyeeLevel')
        ->display_as('EmpLevelID',$this->systemmodel->changeLng('รหัสระดับพนักงาน'))
        ->display_as('EmpLevelNameEN',$this->systemmodel->changeLng('ชื่อระดับพนักงานภาษาอังกฤษ'))
        ->display_as('EmpLevelNameLL',$this->systemmodel->changeLng('ชื่อระดับพนักงานภาษาท้องถิ่น'))
        ->columns(
            'EmpLevelID'
            ,'EmpLevelNameEN'
            ,'EmpLevelNameLL'
            );
        $crud->where('CompID',$this->session->CompUSERID);
        $crud->field_type('CompID', 'hidden', $this->session->CompUSERID);
        $crud->fields(
            'EmpLevelNameEN'
            ,'EmpLevelNameLL'
            ,'CompID'
        );
        $crud->required_fields(
            'EmpLevelNameEN','EmpLevelNameLL'
        );

        $output = $crud->render();
        $this->_example_output($output);
    }
    public function ContractType()
    {
        $crud = new grocery_CRUD();
        $crud->set_table('ContractType')
        //->set_subject('Add ContractType')
        ->columns(
            'ContractID'
            ,'ContractNameEN'
            ,'ContractNameLL'
            )
        ->display_as('ContractID',$this->systemmodel->changeLng('รหัสสัญญา'))
        ->display_as('ContractNameEN',$this->systemmodel->changeLng('ชื่อสัญญาภาษาอังกฤษ'))
        ->display_as('ContractNameLL',$this->systemmodel->changeLng('ชื่อสัญญาภาษาท้องถิ่น'))
        ;
        $crud->where('CompID',$this->session->CompUSERID);
        $crud->fields(
            'ContractNameEN'
            ,'ContractNameLL'
            ,'CompID'
        );
        $crud->required_fields(
            'ContractNameEN'
            ,'ContractNameLL'
        );
        $crud->field_type('CompID','hidden',$this->session->CompUSERID);
        $output = $crud->render();
        $this->_example_output($output);
    }
    public function SalaryType()
    {
        $crud = new grocery_CRUD();
        $crud->set_table('SalaryType')
        //->set_subject('Add SalaryType')
->display_as('SalaryTypeID',$this->systemmodel->changeLng('รหัสประเภทเงินเดือน'))
->display_as('SalaryTypeEN',$this->systemmodel->changeLng('ชื่อประเภทเงินเดือนภาษาอังกฤษ'))
->display_as('SalaryTypeLL',$this->systemmodel->changeLng('ชื่อประเภทเงินเดือนภาษาท้องถิ่น'))
        ->columns(
            'SalaryTypeID'
            ,'SalaryTypeEN'
            ,'SalaryTypeLL'
            );
        $crud->where('CompID',$this->session->CompUSERID);
        $crud->field_type('CompID', 'hidden', $this->session->CompUSERID);
        $crud->fields(
            'SalaryTypeEN'
            ,'SalaryTypeLL'
            ,'CompID'
        );
        $crud->required_fields(
            'SalaryTypeEN'
            ,'SalaryTypeLL'
        );
        $output = $crud->render();
        $this->_example_output($output);
    }
    public function PaymentMethod()
    {
        $crud = new grocery_CRUD();
        $crud->set_table('PaymentMethod')
        //->set_subject('Add PaymentMethod')
->display_as('PaymentMethodID',$this->systemmodel->changeLng('หรัสวิธีการจ่ายเงิน'))
->display_as('PaymentMethodNameEN',$this->systemmodel->changeLng('ชื่อวิธีการจ่ายเงินภาษาอังกฤษ'))
->display_as('PaymentMethodNameLL',$this->systemmodel->changeLng('ชื่อวิธีการจ่ายเงินภาษาท้องถิ่น'))
        ->columns(
            'PaymentMethodTypeID'
            ,'PaymentMethodTypeNameEN'
            ,'PaymentMethodTypeNameLL'
            );
        $crud->where('CompID',$this->session->CompUSERID);
        $crud->field_type('CompID', 'hidden', $this->session->CompUSERID);
        $crud->fields(
            'PaymentMethodTypeNameEN'
            ,'PaymentMethodTypeNameLL'
            ,'CompID'
        );
        $crud->required_fields(
            'PaymentMethodTypeNameEN'
            ,'PaymentMethodTypeNameLL'
        );
        $output = $crud->render();
        $this->_example_output($output);
    }
    public function PITCode()
    {
        $crud = new grocery_CRUD();
        $crud->set_table('PITCode')
        //->set_subject('Add PITCode')
->display_as('PITCodeTypeID',$this->systemmodel->changeLng('รหัสภาษี'))
->display_as('PITCodeTypeNameEN',$this->systemmodel->changeLng('ชื่อประเภทภาษีเงินได้ภาษาอังกฤษ'))
->display_as('PITCodeTypeNameLL',$this->systemmodel->changeLng('ชื่อประเภทภาษีเงินได้ภาษาท้องถิ่น'))
        ->columns(
            'PITCodeTypeID'
            ,'PITCodeTypeNameEN'
            ,'PITCodeTypeNameLL'
            );
        $crud->fields(
            'PITCodeTypeNameEN'
            ,'PITCodeTypeNameLL'
        );
        $crud->required_fields(
            'PITCodeTypeNameEN'
            ,'PITCodeTypeNameLL'
        );
        $output = $crud->render();
        $this->_example_output($output);
    }
    public function SSOCode()
    {
        $crud = new grocery_CRUD();
        $crud->set_table('SSOCode')
        //->set_subject('Add SSOCode')
->display_as('SSOCodeTypeID',$this->systemmodel->changeLng('รหัสประเภทกจ่ายเงินประกันสังคม'))
->display_as('SSOCodeTypeNameEN',$this->systemmodel->changeLng('ชื่อประเภทการจ่ายเงินประกันสังคมภาษาอังกฤษ'))
->display_as('SSOCodeTypeNameLL',$this->systemmodel->changeLng('ชื่อประเภทการจ่ายเงินประกันสังคมภาษาท้องถิ่น'))
        ->columns(
            'SSOCodeTypeID'
            ,'SSOCodeTypeNameEN'
            ,'SSOCodeTypeNameLL'
            );
        // $crud->where('CompID',$this->session->CompUSERID);
        $crud->field_type('CompID', 'hidden', $this->session->CompUSERID);
        $crud->fields(
            'SSOCodeTypeNameEN'
            ,'SSOCodeTypeNameLL'
            ,'CompID'
        );
        $crud->required_fields(
            'SSOCodeTypeNameEN'
            ,'SSOCodeTypeNameLL'
        );
        $output = $crud->render();
        $this->_example_output($output);
    }
    public function Religion()
    {
        $crud = new grocery_CRUD();
        $crud->set_table('Religion')
        //->set_subject('Add Religion')
->display_as('ReligionID',$this->systemmodel->changeLng('รหัสศาสนา'))
->display_as('ReligionNameEN',$this->systemmodel->changeLng('ชื่อศาสนาภาษาอังกฤษ'))
->display_as('ReligionNameLL',$this->systemmodel->changeLng('ชื่อศาสนาภาษาท้องถิ่น'))
        ->columns(
            'ReligionID'
            ,'ReligionNameEN'
            ,'ReligionNameLL'
            );
        $crud->where('CompID',$this->session->CompUSERID);
        $crud->field_type('CompID', 'hidden', $this->session->CompUSERID);
        $crud->fields(
            'ReligionNameEN'
            ,'ReligionNameLL'
        );
        $crud->required_fields(
            'ReligionNameEN'
            ,'ReligionNameLL'
        );
        $output = $crud->render();
        $this->_example_output($output);
    }
    public function SocialSecurityRate()
    {
        $crud = new grocery_CRUD();
        $crud->set_table('SocialSecurityRate')
        //->set_subject('Add SocialSecurityRate')
->display_as('SocialID',$this->systemmodel->changeLng('รหัสประกันสังคม'))
->display_as('SocialRate',$this->systemmodel->changeLng('อัตราประกันสังคม'))
->display_as('SocialStartDate',$this->systemmodel->changeLng('วันเริ่มต้น'))
->display_as('SocialEndDate',$this->systemmodel->changeLng('วันสิ้นสุด'))
        ->columns(
            'SocialID'
            ,'SocialRate'
            ,'SocialStartDate'
            ,'SocialEndDate'
            );
        $crud->where('CompID',$this->session->CompUSERID);
        $crud->field_type('CompID', 'hidden', $this->session->CompUSERID);
        $crud->fields(
            'SocialRate'
            ,'SocialStartDate'
            ,'SocialEndDate'
            ,'CompID'
        );
        $crud->required_fields(
            'SocialRate'
            ,'SocialStartDate'
            ,'SocialEndDate'
        );
        $output = $crud->render();
        $this->_example_output($output);
    }
    public function Nationality()
    {
        $crud = new grocery_CRUD();
        $crud->set_table('Nationality')
        //->set_subject('Add Nationality')
->display_as('NationalityID',$this->systemmodel->changeLng('รหัสสัญชาติ'))
->display_as('NationalityNameEN',$this->systemmodel->changeLng('ชื่อสัญชาติภาษาอังกฤษ'))
->display_as('NationalityNameLL',$this->systemmodel->changeLng('ชื่อสัญชาติภาษาท้องถิ่น'))
        ->columns(
            'NationlityID'
            ,'NationalityNameEN'
            ,'NationalityNameLL'
            );
        
        $crud->where('CompID',$this->session->CompUSERID);
        $crud->field_type('CompID', 'hidden', $this->session->CompUSERID);
        $crud->fields(
            'NationalityNameEN'
            ,'NationalityNameLL'
            ,'CompID'
        );
        $crud->required_fields(
            'NationalityNameEN'
            ,'NationalityNameLL'
        );
        // hidden field
        
        $output = $crud->render();
        $this->_example_output($output);
    }
    public function Race()
    {
        $crud = new grocery_CRUD();
        $crud->set_table('Race')
        //->set_subject('Add Race')
->display_as('RaceID',$this->systemmodel->changeLng('รหัสเชื้อชาติ'))
->display_as('RaceNameEN',$this->systemmodel->changeLng('ชื่อเชื้อชาติภาษาอังกฤษ'))
->display_as('RaceNameLL',$this->systemmodel->changeLng('ชื่อเชื้อชาติภาษาท้องถิ่น'))
        ->columns(
            'RaceID'
            ,'RaceNameEN'
            ,'RaceNameLL'
            );
        $crud->where('CompID',$this->session->CompUSERID);
        $crud->field_type('CompID', 'hidden', $this->session->CompUSERID);
        $crud->fields(
            'RaceNameEN'
            ,'RaceNameLL'
            ,'CompID'
        );
        $crud->required_fields(
            'RaceNameEN'
            ,'RaceNameLL'
        );
        $output = $crud->render();
        $this->_example_output($output);
    }
    public function ResignationType()
    {
        $crud = new grocery_CRUD();
        $crud->set_table('ResignationType')
        //->set_subject('Add ResignationType')
->display_as('ResignID',$this->systemmodel->changeLng('รหัสการลาออก'))
->display_as('ResignNameEN',$this->systemmodel->changeLng('ชื่อประเภทการลาออกภาษาอังกฤษ'))
->display_as('ResignNameLL',$this->systemmodel->changeLng('ชื่อประเภทการลาออกภาษาท้องถิ่น'))
        ->columns(
            'ResignID'
            ,'ResignNameEN'
            ,'ResignNameLL'
            );
        $crud->where('CompID',$this->session->CompUSERID);
        $crud->field_type('CompID', 'hidden', $this->session->CompUSERID);
        $crud->fields(
            'ResignNameEN'
            ,'ResignNameLL'
            ,'CompID'
        );
        $crud->required_fields(
            'ResignNameEN'
            ,'ResignNameLL'
        );
        $output = $crud->render();
        $this->_example_output($output);
    }
	
	public function ResignationReason()
    {
        $crud = new grocery_CRUD();
        $crud->set_table('ResignationReason')
        //->set_subject('Add ResignationReason')
->display_as('ResignationReasonID',$this->systemmodel->changeLng('รหัสเหตุผลการลาออก'))
->display_as('ResignationReasonNameEN',$this->systemmodel->changeLng('ชือเหตุผลการลาออกภาษาอังกฤษ'))
->display_as('ResignationReasonLL',$this->systemmodel->changeLng('ชือเหตุผลการลาออกภาษาท้องถิ่น'))
        ->columns(
            'ResignationReasonID'
            ,'ResignationReasonNameEN'
            ,'ResignationReasonLL'
            );
        $crud->where('CompID',$this->session->CompUSERID);
        $crud->field_type('CompID', 'hidden', $this->session->CompUSERID);
        $crud->fields(
            'ResignationReasonNameEN'
            ,'ResignationReasonLL'
            ,'CompID'
        );
        $crud->required_fields(
            'ResignationReasonNameEN'
            ,'ResignationReasonLL'
        );
        $output = $crud->render();
        $this->_example_output($output);
    }
	
    public function Company()
    {
        $crud = new grocery_CRUD();
        $crud->set_table('Company')
        //->set_subject('Company')
        ->columns(
            'CompNameThai'
            , 'CompNameEng'
            , 'CompAddress1'
            , 'CompAddress2'
            , 'CompAddress3'
            , 'CompAddress4'
			, 'Logo'
            )
        ->display_as('CompNameThai',$this->systemmodel->changeLng("ชื่อบริษัทภาษาไทย"))
        ->display_as('CompNameEng',$this->systemmodel->changeLng("ชื่อบริษัทภาษาอังกฤษ"))
        ->display_as('CompAddress1',$this->systemmodel->changeLng("เลขที่ ที่อยู่"))
        ->display_as('CompAddress2',$this->systemmodel->changeLng("ถนน"))
        ->display_as('CompAddress3',$this->systemmodel->changeLng("ซอย"))
        ->display_as('CompAddress4',$this->systemmodel->changeLng("แขวง/ตำบล"))
        ->display_as('District',$this->systemmodel->changeLng('เขต/อำเภอ'))
        ->display_as('Province',$this->systemmodel->changeLng('จังหวัด'))
        ->display_as('Postcode',$this->systemmodel->changeLng('รหัสไปรษณีย์'))
        ->display_as('Telephone',$this->systemmodel->changeLng('โทรศัพท์'))
        ->display_as('Email',$this->systemmodel->changeLng('อีเมล์'))
        ->display_as('HeadOffice',$this->systemmodel->changeLng('สำนักงานใหญ่'))
        ->display_as('BranchNo',$this->systemmodel->changeLng('หมายเลขสาขา'))
        ->display_as('TaxID',$this->systemmodel->changeLng('รหัสภาษี'))
        ->display_as('DivisionID',$this->systemmodel->changeLng('รหัสฝ่าย'))
        ->display_as('SSID',$this->systemmodel->changeLng('รหัสประกันสังคม'))
        ->display_as('SSBranch',$this->systemmodel->changeLng('สาขาประกันสังคม'))
        ->display_as('Logo',$this->systemmodel->changeLng('เครื่องหมายการค้า'))
        ->display_as('ProvidentID',$this->systemmodel->changeLng('รหัสกองทุนเงินสะสม'))
        ->display_as('BossName',$this->systemmodel->changeLng('ชื่อผู้มีอำนาจ'))
        ->display_as('BossPosition',$this->systemmodel->changeLng('ตำแหน่งผู้มีอำนาจ'))
        ->display_as('IsActive',$this->systemmodel->changeLng('การกระทำ'))
        ;
        $crud->where('CompUSERID',$this->session->CompUSERID);
        $crud->fields(
            'CompNameThai'
            , 'CompNameEng'
            , 'CompAddress1'
            , 'CompAddress2'
            , 'CompAddress3'
            , 'CompAddress4','District','Province','Postcode','Telephone','Email','HeadOffice','BranchNo','TaxID','DivisionID','SSID','SSBranch','Logo','ProvidentID','BossName','BossPosition', 'IsActive','CompUSERID');
        $crud->required_fields(
            'CompNameThai'
            , 'CompNameEng'
            , 'CompAddress1'
            , 'CompAddress4'
            ,'BossName'
            ,'BossPosition'
        );
        // hidden field
        $crud->field_type('CompUSERID', 'hidden', $this->session->CompUSERID);
		$crud->set_field_upload('Logo', 'assets/uploads/Logo', 'jpeg|jpg|png');
        // $crud->unset_add();
        $crud->unset_delete();
        $output = $crud->render();
        $this->_example_output($output);
    }

    public function Division()
    {
        $crud = new grocery_CRUD();
        $crud->set_table('Division')
        //->set_subject('Division')
        ->display_as('CompID',$this->systemmodel->changeLng('บริษัท'))
        ->display_as('DivisionID',$this->systemmodel->changeLng('รหัสฝ่าย'))
        ->display_as('DivisionNameThai',$this->systemmodel->changeLng('ชื่อฝ่ายภาษาท้องถิ่น'))
        ->display_as('DivisionNameEng',$this->systemmodel->changeLng('ชื่อฝ่านภาษาอังกฤษ'))
        ->columns(
            'DivisionID'
            ,'DivisionNameThai'
            ,'DivisionNameEng'
            );
        $crud->where('CompID',$this->session->CompUSERID);
        $crud->fields(
            'DivisionNameThai'
            ,'DivisionNameEng'
            ,'CompID'
        );
        $crud->required_fields(
            'DivisionNameThai'
            ,'DivisionNameEng'
            ,'CompID'
        );
        // hidden field
        $crud->field_type('CompID', 'hidden', $this->session->CompUSERID);
        // $crud->set_relation('CompID','Company','CompNameEng');
        $output = $crud->render();
        $this->_example_output($output);
    }
    public function Department()
    {
        $crud = new grocery_CRUD();
        $crud->set_table('Department')
        //->set_subject('Department')
        ->display_as('CompID',$this->systemmodel->changeLng('บริษัท'))
        ->display_as('DepartmentID',$this->systemmodel->changeLng('รหัสแผนก'))
        ->display_as('DepartmentNameThai',$this->systemmodel->changeLng('ชื่อแผนกภาษาไทย'))
        ->display_as('DepartmentNameEng',$this->systemmodel->changeLng('ชื่อแผนกภาษาอังกฤษ'))
        ->display_as('DivisionID',$this->systemmodel->changeLng('ฝ่าย'))
        ->columns(
            'DepartmentID'
            ,'DepartmentNameThai'
            ,'DepartmentNameEng'
            ,'DivisionID'
            );
        $crud->where('Department.CompID',$this->session->CompUSERID);
        $crud->fields(
            'DepartmentNameThai'
            ,'DepartmentNameEng'
            ,'DivisionID'
            ,'CompID'
        );
        $crud->required_fields(
            'DepartmentNameThai'
            ,'DepartmentNameEng'
            ,'DivisionID'
            ,'CompID'
        );
        // hidden field
        $crud->field_type('CompID', 'hidden', $this->session->CompUSERID);
        $crud->set_relation('DivisionID','Division','DivisionNameEng',array('CompID' => $this->session->CompUSERID ));
        // $crud->set_relation('CompID','Company','CompNameEng');
        // $crud->callback_field('CompID',array($this,'_callback_company'));
        $output = $crud->render();
        $this->_example_output($output);
    }
    function _callback_company($value = '', $primary_key = null){
        return "<select id='CompID' onchange=\"alert('a')\">
        <option value='dd'>a</option>
        <option value='dd'>a</option>
        <option value='dd'>a</option>
        <option value='dd'>a</option>
        </select>";
    }
    public function Section()
    {
        $crud = new grocery_CRUD();
        $crud->set_table('Section')
        //->set_subject('Section')
->display_as('SectionID',$this->systemmodel->changeLng('รหัสหน่วยงาน'))
->display_as('SectionNameEng',$this->systemmodel->changeLng('ชื่อหน่วยงานภาษาอังกฤษ'))
->display_as('SectionNameThai',$this->systemmodel->changeLng('ชื่อหน่วยงานภาษาท้องถิ่น'))
        ->display_as('DepartmentID',$this->systemmodel->changeLng('แผนก'))
        ->display_as('CompID','Company')
        ->columns(
            'SectionID'
            ,'SectionNameThai'
            ,'SectionNameEng'
            ,'DepartmentID'
            );
        $crud->where('Section.CompID',$this->session->CompUSERID);
        $crud->fields(
            'SectionNameThai'
            ,'SectionNameEng'
            ,'DepartmentID'
            ,'CompID'
        );
        $crud->required_fields(
            'SectionNameThai'
            ,'SectionNameEng'
            ,'DepartmentID'
            ,'CompID'
        );
        // hidden field
        $crud->field_type('CompID', 'hidden', $this->session->CompUSERID);
        $crud->set_relation('DepartmentID','Department','DepartmentNameEng',array('CompID'=> $this->session->CompUSERID));
        // $crud->set_relation('CompID','Company','CompNameEng');
        $output = $crud->render();
        $this->_example_output($output);
    }
    public function Position()
    {
        $crud = new grocery_CRUD();
        $crud->set_table('Position')
        //->set_subject('Position')
->display_as('PositionID',$this->systemmodel->changeLng('รหัสตำแหน่ง'))
->display_as('PositionNameEng',$this->systemmodel->changeLng('ชื่อตำแหน่งภาษาอังกฤษ'))
->display_as('PositionNameThai',$this->systemmodel->changeLng('ชื่อตำแหน่งภาษาท้องถิ่น'))
->display_as('SectionID',$this->systemmodel->changeLng('หน่วย'))
        ->display_as('CompID','Company')
        ->columns(
            'PositionID'
            ,'PositionNameThai'
            ,'PositionNameEng'
            ,'SectionID'
            );
        $crud->where('Position.CompID',$this->session->CompUSERID);
        $crud->fields(
            'PositionNameThai'
            ,'PositionNameEng'
            ,'SectionID'
            ,'CompID'
        );
        $crud->required_fields(
            'PositionNameThai'
            ,'PositionNameEng'
            ,'SectionID'
            ,'CompID'
        );
        // hidden field
        $crud->field_type('CompID', 'hidden', $this->session->CompUSERID);
        $crud->set_relation('SectionID','Section','SectionNameEng',array('CompID'=>$this->session->CompUSERID));
        // $crud->set_relation('CompID','Company','CompNameEng');
        $output = $crud->render();
        $this->_example_output($output);
    }
    public function AnnualLeave()
    {
        $crud = new grocery_CRUD();
        $crud->set_table('AnnualLeave')
        ->set_subject($this->systemmodel->changeLng("วันหยุดประจำปี"))
        ->columns(
            'AnnleaveID'
            ,'AnnleaveNameLL'
            ,'AnnleaveNameEN'
            )
        ->display_as('AnnleaveID',$this->systemmodel->changeLng("รหัสวันหยุดประจำปี"))
        ->display_as('AnnleaveNameLL',$this->systemmodel->changeLng("ชื่อวันหยุดประจำปีภาษาท้องถิ่น"))
        ->display_as('AnnleaveNameEN',$this->systemmodel->changeLng("วันหยุดประจำปี"))
        ;
        $crud->where('CompID',$this->session->CompUSERID);
        $crud->fields(
            'AnnleaveNameLL'
            ,'AnnleaveNameEN'
            ,'CompID'
        );
        $crud->required_fields(
            'AnnleaveNameLL'
            ,'AnnleaveNameEN'
        );
        $crud->field_type('CompID', 'hidden', $this->session->CompUSERID);
        $output = $crud->render();
        $this->_example_output($output);
    }
    public function FamilyRelation()
    {
        $crud = new grocery_CRUD();
        $crud->set_table('FamilyRelation')
        //->set_subject('FamilyRelation')
->display_as('FMRelationID',$this->systemmodel->changeLng('รหัสความสัมพันธ์ครอบครัว'))
->display_as('FMRelationNameEN',$this->systemmodel->changeLng('ชื่อความสัมพันธ์ครอบครัวภาษาอังกฤษ'))
->display_as('FMRelationNameLL',$this->systemmodel->changeLng('ชื่อความสัมพันธ์ครอบครัวภาษาท้องถิ่น'))
->display_as('GenderID',$this->systemmodel->changeLng('รหัสเพศ'))
        ->columns(
            'FMRelationID'
            ,'FMRelationNameEN'
            ,'FMRelationNameLL'
            ,'GenderID'
            );
        $crud->where('CompID',$this->session->CompUSERID);
        $crud->field_type('CompID', 'hidden', $this->session->CompUSERID);
        $crud->fields(
            'FMRelationNameEN'
            ,'FMRelationNameLL'
            ,'GenderID'
            ,'CompID'
        );
        $crud->required_fields(
            'FMRelationNameEN'
            ,'FMRelationNameLL'
            ,'GenderID'
        );
        $crud->set_relation('GenderID','Gender','GenderEN');
        $output = $crud->render();
        $this->_example_output($output);
    }
    public function Skills()
    {
        $crud = new grocery_CRUD();
        $crud->set_table('Skills')
        //->set_subject('Skills')
->display_as('SkillID',$this->systemmodel->changeLng('รหัสความสามารถ'))
->display_as('SkillNameEN',$this->systemmodel->changeLng('ชื่อความสมารถภาษาอังกฤษ'))
->display_as('SkillNameLL',$this->systemmodel->changeLng('ชื่อความสมารถภาษาท้องถิ่น'))
        ->columns(
            'SkillID'
            ,'SkillNameEN'
            ,'SkillNameLL'
            );
        $crud->where('CompID',$this->session->CompUSERID);
        $crud->field_type('CompID', 'hidden', $this->session->CompUSERID);
        $crud->fields(
            'SkillNameEN'
            ,'SkillNameLL'
            ,'CompID'
        );
        $crud->required_fields(
            'SkillNameEN'
            ,'SkillNameLL'
        );
        $output = $crud->render();
        $this->_example_output($output);
    }
    public function WorkType()
    {
        $crud = new grocery_CRUD();
        $crud->set_table('WorkType')
        //->set_subject('WorkType')
->display_as('WorkTypeID',$this->systemmodel->changeLng('รหัสประเภทการทำงาน'))
->display_as('WorkTypeNameEng',$this->systemmodel->changeLng('ชื่อประเภทการทำงานภาษาอังกฤษ'))
->display_as('WorkTypeNameThai',$this->systemmodel->changeLng('ชื่อประเภทการทำงานภาษาท้องถิ่น'))
        ->columns(
            'WorkTypeID'
            ,'WorkTypeNameThai'
            ,'WorkTypeNameEng'
            );
        $crud->where('CompID',$this->session->CompUSERID);
        $crud->field_type('CompID', 'hidden', $this->session->CompUSERID);
        $crud->fields(
            'WorkTypeNameThai'
            ,'WorkTypeNameEng'
            ,'CompID'
        );
        $crud->required_fields(
            'WorkTypeNameThai'
            ,'WorkTypeNameEng'
        );
        $output = $crud->render();
        $this->_example_output($output);
    }
    public function Qualification()
    {
        $crud = new grocery_CRUD();
        $crud->set_table('Qualification')
        //->set_subject('Qualification')
->display_as('QualificationID',$this->systemmodel->changeLng('รหัสวุฒิการศึกษา'))
->display_as('QualificationNameEN',$this->systemmodel->changeLng('ชื่อวุฒิการศึกษาภาษาอังกฤษ'))
->display_as('QualificationNameLL',$this->systemmodel->changeLng('ชื่อวุฒิการศึกษาภาษาท้องถิ่น'))
->display_as('QualificationMajorEN',$this->systemmodel->changeLng('ชื่อวุฒิการศึกษาวิชาเอกภาษาอังกฤษ'))
->display_as('QualificationMajorLL',$this->systemmodel->changeLng('ชื่อวุฒิการศึกษาวิชาเอกภาษาท้องถิ่น'))
        ->columns(
            'QualificationID'
            ,'QualificationNameEN'
            ,'QualificationNameLL'
            ,'QualificationMajorEN'
            ,'QualificationMajorLL'
            );
        $crud->where('CompID',$this->session->CompUSERID);
        $crud->field_type('CompID', 'hidden', $this->session->CompUSERID);
        $crud->fields(
            'QualificationNameEN'
            ,'QualificationNameLL'
            ,'QualificationMajorEN'
            ,'QualificationMajorLL'
            ,'CompID'
        );
        $crud->required_fields(
            'QualificationNameEN'
            ,'QualificationNameLL'
            ,'QualificationMajorEN'
            ,'QualificationMajorLL'
        );
        $output = $crud->render();
        $this->_example_output($output);
    }
    public function PublicHoliday()
    {
        $crud = new grocery_CRUD();
        $crud->set_table('PublicHoliday')
        //->set_subject('PublicHoliday')
->display_as('PublicHolidayID',$this->systemmodel->changeLng('รหัสวันหยุดประจำปี'))
->display_as('PublicHolidayNameEN',$this->systemmodel->changeLng('ชื่อวันหยุดประจำปีภาษาอังกฤษ'))
->display_as('PublicHolidayNameLL',$this->systemmodel->changeLng('ชื่อวันหยุดประจำปีภาษาท้องถิ่น'))
->display_as('PublicHolidayNameDate',$this->systemmodel->changeLng('วันที่ของวันหยุดประจำปี'))
->display_as('PublicHolidayNameYear',$this->systemmodel->changeLng('ปีของวันหยุดประจำปี'))
        ->columns(
            'PublicHolidayID'
            ,'PublicHolidayNameEN'
            ,'PublicHolidayNameLL'
            ,'PublicHolidayNameDate'
            ,'PublicHolidayNameYear'
            );
        $crud->where('CompID',$this->session->CompUSERID);
        $crud->field_type('CompID', 'hidden', $this->session->CompUSERID);
        $crud->fields(
            'PublicHolidayNameEN'
            ,'PublicHolidayNameLL'
            ,'PublicHolidayNameDate'
            ,'PublicHolidayNameYear'
            ,'CompID'
        );
        $crud->required_fields(
            'PublicHolidayNameEN'
            ,'PublicHolidayNameLL'
            ,'PublicHolidayNameDate'
            ,'PublicHolidayNameYear'
        );
        $output = $crud->render();
        $this->_example_output($output);
    }
    public function MovementType()
    {
        $crud = new grocery_CRUD();
        $crud->set_table('MovementType')
        //->set_subject('MovementType')
->display_as('MovementTypeID',$this->systemmodel->changeLng('รหัสการเปลี่ยนแปลงสถานะ'))
->display_as('MovementTypeNameEN',$this->systemmodel->changeLng('ชื่อการเปลี่ยนแปลงสถานะภาษาอังกฤษ'))
->display_as('MovementTypeNameLL',$this->systemmodel->changeLng('ชื่อการเปลี่ยนแปลงสถานะภาษาท้องถิ่น'))
        ->columns(
            'MovementTypeID'
            ,'MovementTypeNameEN'
            ,'MovementTypeNameLL'
            ,'NewEmployee'
            );

        $crud->where('CompID',$this->session->CompUSERID);
        $crud->field_type('CompID', 'hidden', $this->session->CompUSERID);
        $crud->fields(
            'MovementTypeNameEN'
            ,'MovementTypeNameLL'
            ,'NewEmployee'
            ,'CompID'
        );
        $crud->required_fields(
            'MovementTypeNameEN'
            ,'MovementTypeNameLL'
            ,'NewEmployee'
        );
        $crud->where('MovementTypeID !=',9999);
        $output = $crud->render();
        $this->_example_output($output);
    }

    public function Position2(){
        $this->load->model('HrsettingModel'); 
        $colHeader = array("No.","PositionNameThai","PositionNameEng","Section","Company","Actions");
        $data = array(
            'content' => 'hrsetting/Position',
            'results_Company'=> $this->HrsettingModel->getDataCompany(),
            'resultsDATA'=> $this->HrsettingModel->getDataPosition(),
            'colHeader' => $colHeader,
            );
        $this->load->view('templates/index', $data);
    }
    public function getSection(){ 
        $postData = $this->input->post();
        $this->load->model('HrsettingModel');
        $data = $this->HrsettingModel->getSection($postData);
        echo json_encode($data); 
    }
    
}
