<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

// เปลี่ยนชื่อโมเดล (MemberModel) ให้เป็นชื่อโมเดล อักษาตัวแรกให้เป็นตัวใหญ่และตามด้วยคำว่า Model
class HrsettingModel extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }
    public function getDataPosition(){
        $query_str="select * from Position a 
        left outer join Section b on a.SectionID = b.SectionID 
        left outer join Company c on a.CompID = c.CompID
        ";
        $query=$this->db->query($query_str);
        if ($query->num_rows() > 0) return $query->result();
        else return false;
    }
    public function getDataCompany(){
        $query_str="select * from Company
        ";
        $query=$this->db->query($query_str);
        if ($query->num_rows() > 0) return $query->result();
        else return false;
    }
    
    function getSection($postData){
    $response = array();
    $this->db->select('*');
    $this->db->where('CompID', $postData['CompID']);
    $q = $this->db->get('Section');
    $response = $q->result_array();
    return $response;
    }
}
