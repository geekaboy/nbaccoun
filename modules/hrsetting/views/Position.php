<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    //เพิ่มเงื่อนไขตาราง
    $('.js-exportable').DataTable({
        dom: 'Bfrtip',responsive: true,paging: true,info: false,
        buttons: [
            {extend:'copyHtml5',text:' <i class="fa fa-files-o fa-2x "></i> ',titleAttr: ' Copy '},
            {extend:'excelHtml5',text:' <i class="fa fa-file-excel-o fa-2x "></i> ',titleAttr: ' Excel '},
            {extend:'print',text:' <i class="fa fa-print fa-2x "></i> ',titleAttr: ' Print '}
          ,
        ]
      });

    $('.js-exportable2').DataTable({
        dom: 'Bfrtip',responsive: true,paging: true,info: false,
        buttons: [
            {extend:'copyHtml5',text:' <i class="fa fa-files-o fa-2x "></i> ',titleAttr: ' Copy '},
            {extend:'excelHtml5',text:' <i class="fa fa-file-excel-o fa-2x "></i> ',titleAttr: ' Excel '},
            {extend:'print',text:' <i class="fa fa-print fa-2x "></i> ',titleAttr: ' Print '
            // ,exportOptions:{columns:[0,1,2,3,4,5,6]}
          },
        ]
        // ,"columnDefs": [{"targets": [ 0,1,2,3,4 ],"visible": false,"searchable": true}]
      });
//กรณีต้องการปิดการแสดงบาง column แต่สามารถ export file ได้
    $('.js-exportable3').DataTable({
        dom: 'Bfrtip',responsive: true,paging: true,info: false,
        buttons: [
            {extend:'copyHtml5',text:' <i class="fa fa-files-o fa-2x "></i> ',titleAttr: ' Copy '},
            {extend:'excelHtml5',text:' <i class="fa fa-file-excel-o fa-2x "></i> ',titleAttr: ' Excel '},
            {extend:'print',text:' <i class="fa fa-print fa-2x "></i> ',titleAttr: ' Print '
            ,exportOptions:{columns:[0,1,2,3,4,5,6]}
          },
        ],
        "columnDefs": [{"targets": [ 5 ],"visible": false,"searchable": true}]
      });
  });
</script>
<!-- begin add require script -->
<head>
  <link href="<?php echo base_url();?>assets/bootstrap-select/css/bootstrap-select.css" rel="stylesheet">
  <link href="<?php echo base_url();?>assets/bootstrap/css/datepicker.css" rel="stylesheet">
  <link href="<?php echo base_url();?>assets/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">
  <meta charset="UTF-8">
  
  <style>
  @media print {
    canvas {
      min-height: 100%;max-width: 100%;max-height: 100%;height: auto!important;width: auto!important;
    }
    table {
      min-height: 100%;max-width: 100%;max-height: 100%;height: auto!important;width: auto!important;
      page-break-inside: : auto;
    }
    tr{
      page-break-inside: avoid;
      page-break-after: auto;

    }
  }
  canvas{-moz-user-select: none;-webkit-user-select: none;-ms-user-select: none;}
  </style>
</head>

<!-- end add require script -->
<div class="box box-success">
  <div class="box-header" align="left">
        <i class="fa fa-sign-in"></i>
    <h3 class="box-title"><?php echo 'View '.$this->systemmodel->get_menuname($this->uri->segment(1) . '/' . $this->uri->segment(2)); // แสดงชื่อเมนู  ?></h3>
  </div>
  <div class="box-body">
    <!-- ส่วนแสดงผล -->

<?php date_default_timezone_set("Asia/Bangkok");?>

<div class="box box-success">
  <div class="box-header"> 
  </div>
  <div class="box-body" align="left" id="resultDIV">
    <div class="panel panel-success">
      <div class="panel-heading"><?php echo $this->systemmodel->get_menuname($this->uri->segment(1) . '/' . $this->uri->segment(2)); // แสดงชื่อเมนู  ?></div>
      <div class="panel-body">
        <p><button type="button" class="btn btn-primary btn-md" data-toggle="modal" data-target="#myModal">Add New</button></p>
         <table id="datatable1" class="table table-bordered table-striped table-hover dataTable">
            <thead>
              <tr>
                <?php 
                foreach ($colHeader as &$value) {
                  echo "<th scope='col'><center>".$value."</center></th>";
                }
                ?>
              </tr>
            </thead>
            <tbody>
          <?php 
            $i=0;
          if(!empty($resultsDATA)){            
            foreach($resultsDATA as $row){ 
            ?>
            <tr>
              <td><?php echo $i+1;?></td>
              <td><?php echo $row->PositionNameEng;?></td>
              <td><?php echo $row->PositionNameEng;?></td>
              <td><?php echo $row->SectionNameEng;?></td>
              <td><?php echo $row->CompNameEng;?></td>
              <td>
                <button type="button" class="btn btn-info btn-xs" onclick="edit(<?php echo $row->CompID;?>,<?php echo $row->SectionID;?>,<?php echo $row->PositionNameThai;?>,<?php echo $row->PositionNameEng;?>,<?php echo $row->PositionID;?>)">
                  <span class="glyphicon glyphicon-edit"></span>
                </button>&nbsp;&nbsp;
                <button type="button" class="btn btn-danger btn-xs" onclick="del(<?php echo $row->PositionID;?>)">
                  <span class="glyphicon glyphicon-trash"></span>
                </button>
              </td>
              
            </tr>
          <?php
          
          $i++;
              }
            }?>
            
          </tbody>
          </table>

      </div>
    </div>
        
  </div>
</div>
</div>



<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><?php echo $this->systemmodel->get_menuname($this->uri->segment(1) . '/' . $this->uri->segment(2));?></h4>
      </div>
      <div class="modal-body">
        <p>
          <?php echo "$colHeader[1]";?>*
          <input type="text" class="form-control" id="<?php echo "$colHeader[1]";?>" required>
        </p>
        <p>
          <?php echo "$colHeader[2]";?>*
          <input type="text" class="form-control" id="<?php echo "$colHeader[2]";?>" required>
        </p>
        <p>
          <?php echo "$colHeader[4]";?>*
          <select class="form-control" name="CompID" id='CompID' required>
              <option value="">
                SELECT
              </option>
              <?php
              foreach($results_Company as $result){
                ?>
                <option value="<?php echo $result->CompID; ?>">
                  <?php echo $result->CompNameEng; ?>
                </option>
                <?php
              } ?>
         </select>
        </p>
        <p>
          <?php echo "$colHeader[3]";?>*
          <select class="form-control" name="SectionID" id='SectionID' required>
              <option value="">
                SELECT
              </option>
         </select>
        </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" >Save</button>&nbsp;<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>


<script type="text/javascript">
$(document).ready(function() {
  $('#datatable1').DataTable();

  $('#CompID').change(function(){
    var CompID = $(this).val();
    $.ajax({ url:'<?=base_url()?>index.php/hrsetting/getSection', method: 'post', data: {CompID: CompID}, dataType: 'json',
      success: function(response){
        $('#SectionID').find('option').not(':first').remove();
        $.each(response,function(index,data){
          $('#SectionID').append('<option value="'+data['SectionID']+'">'+data['SectionNameThai']+' | '+data['SectionNameEng']+'</option>');
        });
      }
    });
  });

} );

function del(id){ 
  swal({
  title: "Are you sure?",
  text: "You will not be able to recover this product",
  type: "warning",
  showCancelButton: true,
  confirmButtonClass: "btn-danger",
  confirmButtonText: "Yes, delete it!",
  cancelButtonText: "No, cancel!",
  closeOnConfirm: false,
  closeOnCancel: false
  },
  function(isConfirm) {
    if (isConfirm) {
      $.ajax({
        url:'<?=base_url()?>index.php/Hrsetting/delData',
        type: "post",
        data: {
          id: id,
        },
        beforeSend: function () {$(".loading").show();},
        complete: function () {$(".loading").hide();},
        success: function (datatxt) {          
          swal({
            title: "Deleted!",
            text: "Your product has been deleted.",
            type: "success",
            showCancelButton: false,
            closeOnConfirm: false,
            showLoaderOnConfirm: false
          }, function () {
            setTimeout(function () {
              location.reload();
            }, 100);
          });
          
        }
      }); 
      
    } else {
      swal("Cancelled", "Your product is safe :)", "error");
    }
  });
  
}
function edit(){

}

</script>

<script src="<?php echo base_url();?>assets/bootstrap/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url();?>assets/jquery-datatable/jquery.dataTables.js"></script>
<script src="<?php echo base_url();?>assets/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
<script src="<?php echo base_url();?>assets/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url();?>assets/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
<script src="<?php echo base_url();?>assets/jquery-datatable/extensions/export/jszip.min.js"></script>
<script src="<?php echo base_url();?>assets/jquery-datatable/extensions/export/pdfmake.min.js"></script>
<script src="<?php echo base_url();?>assets/jquery-datatable/extensions/export/vfs_fonts.js"></script>
<script src="<?php echo base_url();?>assets/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
<script src="<?php echo base_url();?>assets/jquery-datatable/extensions/export/buttons.print.min.js"></script>