<?php
@$act=$_GET['act'];
if($act=='excel'){
header("Content-Type: application/xls");
header("Content-Disposition: attachment; filename=export.xls");
header("Pragma: no-cache");
header("Expires: 0");
}
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>PhP Export to Excel</title>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	</head>
	<body>
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<br /><br /><br />
					<h4> ::PHP EXPORT TO EXCEL by iCha ::
					</h4>
					
					<p>
						<!--<a href="?act=excel" class="btn btn-primary"> Export->Excel </a>-->
						<a href="?act=excel" class="btn btn-success"> <b>Export->Excel</b> </a>
					</p>
					
					<table border="2" class="table table-hover">
						<thead>
							<tr class="success">
								<th>คำนำหน้า</th>
								<th>ชื่อ</th>
								<th>นามสกุล</th>
								<th>วันที่</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>นาย</td>
								<td>บัญชา</td>
								<td>แตงสี</td>
								<td>19-09-2019</td>
							</tr>
							<tr>
								<td>นาย</td>
								<td>อนุพงศ์</td>
								<td>แตงสี</td>
								<td>19-09-2019</td>
							</tr>
							<tr>
								<td>นางสาว</td>
								<td>วรรณิภา</td>
								<td>ไทรน้อย</td>
								<td>19-09-2019</td>
							</tr>
							<tr>
								<td>นางสาว</td>
								<td>พรทิพย์</td>
								<td>หว่านพิช</td>
								<td>19-09-2019</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</body>
</html>