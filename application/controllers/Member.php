<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Member extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if (!isset($this->session->member)) {
            redirect(base_url() . 'index.php/home/login_form');
        }
        //$this->load->library('grocery_CRUD');

    }

    public function index(){
        $this->load->model('MemberModel');
        if(isset($this->session->CompID)){
              $data = array(
            'content' => 'member/dashboard',
            'results_probation' => $this->MemberModel->results_probation(),
            'results_birthday' => $this->MemberModel->results_birthday(),
            'results_contract_end_date' => $this->MemberModel->results_contract_end_date(),
            );
        }else{
              $data = array(
            'content' => 'member/company',
            'results_company' => $this->MemberModel->getCompany(),
            );
        }
        $this->load->view('templates/index', $data);
    }

    public function logout()
    {
        session_destroy();
        redirect(base_url());
    }

    public function nopermission()
    {
        $data['content'] = 'member/nopermission';
        $this->load->view('templates/index', $data);
    }
    public function setcompany()
    {
        $_SESSION['CompID'] = $this->input->post('CompID');
        $_SESSION['CompNameEng'] = $this->input->post('CompNameEng');
        $_SESSION['CompNameThai'] = $this->input->post('CompNameThai');
        echo "true";
    }
    public function setLanguage(){
        // $lng = $this->uri->segment(3);
        $lng = $this->input->post('lng');
        $_SESSION['lng'] = $lng;
        return true;
        // redirect(base_url() . 'index.php/member');
    }

}
