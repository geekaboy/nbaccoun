<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajaxservice extends CI_Controller {


    function __construct()
    {
        parent::__construct();
        $this->load->model('Ajaxmodel');
    }
	public function index()
	{
		$this->load->view('welcome_message');
	}

    public function ajaxdata() {
        $tb = $_POST['table'];
        $fl = $_POST['field'];
        $ajax = $this->Ajaxmodel->list_field($tb,$fl);
        echo $ajax;
    }
}
