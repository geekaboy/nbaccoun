<script type="text/javascript">
  $(document).ready(function(){
    $('#table1').DataTable({
      responsive: true
    });
    $('#username').focus();
    $('#btn_login').click(function(){
      $.ajax({
        url:'<?php echo base_url(); ?>index.php/home/login',
        data:'username='+$('#username').val()+'&password='+$('#password').val(),
        type:'POST',
        success:function(res){
          if(res=='true'){
            swal({
                  title : 'แสดงเมือทำงานสำเร็จ',
                  text : '',
                  type : 'success'
                  },
                  function(){
                    // เมื่อทำงานสำเร็จให้ไปที่หน้านี้
                    window.location.replace("<?php echo base_url() ?>index.php/member");
                  }
              );

          }else{
            swal({
                  title : 'แสดงเมื่อทำงานไม่สำเร็จ',
                  text : '',
                  type : 'error'
              });
          }
        },
        error:function(err){
          swal({
                  title : 'เกิดข้อผิดพลาด',
                  text : err,
                  type : 'error'
              });
        }
      });
    });
  });
</script>
<script type="text/javascript">

window.onload = function () {
  var chart = new CanvasJS.Chart("chartContainer", {
    title:{
      text: "แผนภาพแสดงจำนวนประชากรรายอำเภอ"              
    },
    axisY:{    
        valueFormatString:  "#,##0", // move comma to change formatting
        prefix: ""
    },
    axisX: {
        //labelFontSize: 0,
        labelAngle: -30
    },
    data: [              
    {
      // Change type to "doughnut", "line", "splineArea", etc.
      type: "column",
      dataPoints: [
        { label: "เมือง",  y: 94547  },
        { label: "ยางตลาด", y: 82281  },
        { label: "กมลาไสย", y: 46353  },
        { label: "สหัสขันธ์",  y: 34709  },
        { label: "กุฉินารายณ์",  y: 62092  },
        { label: "ท่าคันโท",  y: 28776  },
        { label: "สมเด็จ",  y: 55151  },
        { label: "เขาวง",  y: 20923  },
        { label: "ห้วยเม็ก",  y: 40406  },
        { label: "คำม่วง",  y: 26923  },
        { label: "ร่องคำ",  y: 10441  },
        { label: "หนองกุงศรี",  y: 52065  },
        { label: "นามน",  y: 29699  },
        { label: "ห้วยผึ้ง",  y: 14057  },
        { label: "สามชัย",  y: 25701  },
        { label: "นาคู",  y: 25852  },
        { label: "ดอนจาน",  y: 25979  },
        { label: "ฆ้องชัย",  y: 27130  }
      ]
    }
    ]
  });
  chart.render();
}
</script>
<!-- begin add require script -->
  <!-- Datatable -->
  <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.css">
  <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.js"></script>
  <!-- Chart -->
  <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
<!-- end add require script -->
<div class="box box-success">
  <div class="box-header">
        <i class="fa fa-sign-in"></i>
    <h3 class="box-title">ข้อมูลแสดงจำนวน/ประชากร  จังหวัดกาฬสินธุ์ จำแนกรายตำบล หมู่บ้าน อปท. และพื้นที่ </h3>
  </div>
  <div class="box-body">
    <!-- ส่วนแสดงผล -->
    <div id="chartContainer" style="height: 300px; width: 100%;"></div>
    <!-- Post -->
    <div class="post">
      <!--
      <div align="center">
        <img class="img-responsive" src="<?php //echo base_url(); ?>assets/images/organization-chart.png">
      </div>
      -->
      <div class="box-header">
        <p><h3 class="box-title">ข้อมูล ณ วันที่ 31 ธันวาคม  2559</h3></p>
        <p><h3 class="box-title">ที่มา : กรมส่งเสริมการปกครองท้องถิ่น ข้อมูล ณ วันที่ 9 มีนาคม 2558</h3></p>
        <p><h3 class="box-title">ที่มา : กรมการปกครอง กระทรวงมหาดไทย ข้อมูล ณ วันที่ 31 ธันวาคม 2559</h3></p>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <table id="table1" class="table table-bordered table-striped">
          <thead>
          <tr>
            <th rowspan="2">#</th>
            <th rowspan="2">อำเภอ</th>
            <th rowspan="2">ตำบล</th>
            <th rowspan="2">หมู่บ้าน</th>
            <th rowspan="2">อบต.</th>
            <th rowspan="2">เทศบาล</th>
            <th rowspan="2">พื้นที่ ตร.กม.</th>
            <th rowspan="2">ห่างจากจังหวัด(กม.)</th>
            <th colspan="3">ประชากร</th>
          </tr>
          <tr>
            <th>ชาย</th>
            <th>หญิง</th>
            <th>รวม</th>
          </tr>
          </thead>
          <tbody>
          <?php 
          if($data!=false) 
          {
            // set default
            $total_tambon = 0;
            $total_mooban = 0;
            $total_obt = 0;
            $total_tedsaban = 0;
            $total_area = 0;
            $total_m = 0;
            $total_f = 0;
            $total_total = 0;
            foreach ($data as $row)
            { 
              ?>
              <tr>
                <td><?php echo $row->id; ?></td>
                <td><?php echo $row->amper; ?></td>
                <td align="right"><?php echo $row->tambon; ?></td>
                <td align="right"><?php echo $row->mooban; ?></td>
                <td align="right"><?php echo $row->obt; ?></td>
                <td align="right"><?php echo $row->tedsaban; ?></td>
                <td align="right"><?php echo $row->area; ?></td>
                <td align="right"><?php echo $row->far; ?></td>
                <td align="right"><?php echo $row->m; ?></td>
                <td align="right"><?php echo $row->f; ?></td>
                <td align="right"><?php echo $row->total; ?></td>
              </tr>
              <?php 
              // sum total
              $total_tambon = $total_tambon+$row->tambon;
              $total_mooban = $total_mooban+$row->mooban;
              $total_obt = $total_obt+$row->obt;
              $total_tedsaban = $total_tedsaban+$row->tedsaban;
              $total_area = $total_area+$row->area;
              $total_m = $total_m+$row->m;
              $total_f = $total_f+$row->f;
              $total_total = $total_total+$row->total;
            }
          }
            ?>
          </tbody>
          <tfoot>
          <tr>
            <th colspan="2">รวมทั้งหมด</th>
            <th align="right"><?php echo $total_tambon; ?></th>
            <th align="right"><?php echo $total_mooban; ?></th>
            <th align="right"><?php echo $total_obt; ?></th>
            <th align="right"><?php echo $total_tedsaban; ?></th>
            <th align="right"><?php echo $total_area; ?></th>
            <th align="right">-</th>
            <th align="right"><?php echo $total_m; ?></th>
            <th align="right"><?php echo $total_f; ?></th>
            <th align="right"><?php echo $total_total; ?></th>
          </tr>
          </tfoot>
        </table>
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.post -->
    <!-- จบส่วนแสดงผล -->
  </div>
</div>