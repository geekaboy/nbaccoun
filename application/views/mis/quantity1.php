<script type="text/javascript">
  $(document).ready(function(){
    $('#table1').DataTable({
      responsive: true
    });
    $('#username').focus();
    $('#btn_login').click(function(){
      $.ajax({
        url:'<?php echo base_url(); ?>index.php/home/login',
        data:'username='+$('#username').val()+'&password='+$('#password').val(),
        type:'POST',
        success:function(res){
          if(res=='true'){
            swal({
                  title : 'แสดงเมือทำงานสำเร็จ',
                  text : '',
                  type : 'success'
                  },
                  function(){
                    // เมื่อทำงานสำเร็จให้ไปที่หน้านี้
                    window.location.replace("<?php echo base_url() ?>index.php/member");
                  }
              );

          }else{
            swal({
                  title : 'แสดงเมื่อทำงานไม่สำเร็จ',
                  text : '',
                  type : 'error'
              });
          }
        },
        error:function(err){
          swal({
                  title : 'เกิดข้อผิดพลาด',
                  text : err,
                  type : 'error'
              });
        }
      });
    });
  });
</script>
<script type="text/javascript">

window.onload = function () {
  var chart = new CanvasJS.Chart("chartContainer", {
    title:{
      text: "แผนภาพแสดงจำนวนผู้เรียนแยกระดับ"              
    },
    axisY:{    
        valueFormatString:  "#,##0", // move comma to change formatting
        prefix: ""
    },
    axisX: {
        //labelFontSize: 0,
        labelAngle: -30
    },
    data: [              
    {
      // Change type to "doughnut", "line", "splineArea", etc.
      type: "column",
      dataPoints: [
        { label: "เตรียมความพร้อม",  y: 488  },
        { label: "ก่อนประถม", y: 23328  },
        { label: "ประถม", y: 65168  },
        { label: "ม.ต้น",  y: 44998  },
        { label: "ม.ปลาย",  y: 38511  },
        { label: "ปวช.",  y: 9643  },
        { label: "ปวส.",  y: 3779  },
        { label: "ป.ตรี",  y: 5650  }
      ]
    }
    ]
  });
  chart.render();
}
</script>
<!-- begin add require script -->
  <!-- Datatable -->
  <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.css">
  <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.js"></script>
  <!-- Chart -->
  <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
<!-- end add require script -->
<div class="box box-success">
  <div class="box-header">
        <i class="fa fa-sign-in"></i>
    <h3 class="box-title">จำนวนสถานศึกษา จำนวนผู้เรียนทุกสังกัดในจังหวัดกาฬสินธุ์ จำแนกตามระดับการศึกษา</h3>
  </div>
  <div class="box-body">
    <!-- ส่วนแสดงผล -->
    <div id="chartContainer" style="height: 300px; width: 100%;"></div>
    <!-- Post -->
    <div class="post">
      <!--
      <div align="center">
        <img class="img-responsive" src="<?php //echo base_url(); ?>assets/images/organization-chart.png">
      </div>
      -->
      <div class="box-header">
        <h3 class="box-title">ข้อมูล ณ วันที่ 10  มิถุนายน 2560</h3>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <table id="table1" class="table table-bordered table-striped">
          <thead>
          <tr>
            <th rowspan="2">#</th>
            <th rowspan="2">สังกัด</th>
            <th rowspan="2">จำนวนสถานศึกษา</th>
            <th colspan="9">จำนวนผู้เรียน</th>
          </tr>
          <tr>
            <th>เตรียมความพร้อม</th>
            <th>ก่อนประถม</th>
            <th>ประถม</th>
            <th>ม.ต้น</th>
            <th>ม.ปลาย</th>
            <th>ปวช.</th>
            <th>ปวส.</th>
            <th>ป.ตรี</th>
            <th>รวม</th>
          </tr>
          </thead>
          <tbody>
          <?php 
          if($data!=false) 
          {
            // set default
            $total_school = 0;
            $total_r = 0;
            $total_n = 0;
            $total_p = 0;
            $total_s13 = 0;
            $total_s46 = 0;
            $total_pvc = 0;
            $total_pvs = 0;
            $total_b = 0;
            $total_total = 0;
            foreach ($data as $row)
            { 
              ?>
              <tr>
                <td><?php echo $row->id; ?></td>
                <td><?php echo $row->office; ?></td>
                <td align="right"><?php echo $row->school; ?></td>
                <td align="right"><?php echo $row->r; ?></td>
                <td align="right"><?php echo $row->n; ?></td>
                <td align="right"><?php echo $row->p; ?></td>
                <td align="right"><?php echo $row->s13; ?></td>
                <td align="right"><?php echo $row->s46; ?></td>
                <td align="right"><?php echo $row->pvc; ?></td>
                <td align="right"><?php echo $row->pvs; ?></td>
                <td align="right"><?php echo $row->b; ?></td>
                <td align="right"><?php echo $row->total; ?></td>
              </tr>
              <?php 
              // sum total
              $total_school = $total_school+$row->school;
              $total_r = $total_r+$row->r;
              $total_n = $total_n+$row->n;
              $total_p = $total_p+$row->p;
              $total_s13 = $total_s13+$row->s13;
              $total_s46 = $total_s46+$row->s46;
              $total_pvc = $total_pvc+$row->pvc;
              $total_pvs = $total_pvs+$row->pvs;
              $total_b = $total_b+$row->b;
              $total_total = $total_total+$row->total;
            }
          }
            ?>
          </tbody>
          <tfoot>
          <tr>
            <th colspan="2">รวมทั้งหมด</th>
            <th align="right"><?php echo $total_school; ?></th>
            <th align="right"><?php echo $total_r; ?></th>
            <th align="right"><?php echo $total_n; ?></th>
            <th align="right"><?php echo $total_p; ?></th>
            <th align="right"><?php echo $total_s13; ?></th>
            <th align="right"><?php echo $total_s46; ?></th>
            <th align="right"><?php echo $total_pvc; ?></th>
            <th align="right"><?php echo $total_pvs; ?></th>
            <th align="right"><?php echo $total_b; ?></th>
            <th align="right"><?php echo $total_total; ?></th>
          </tr>
          </tfoot>
        </table>
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.post -->
    <!-- จบส่วนแสดงผล -->
  </div>
</div>