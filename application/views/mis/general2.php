<script type="text/javascript">
  $(document).ready(function(){
    $('#table1').DataTable();
    $('#username').focus();
    $('#btn_login').click(function(){
      $.ajax({
        url:'<?php echo base_url(); ?>index.php/home/login',
        data:'username='+$('#username').val()+'&password='+$('#password').val(),
        type:'POST',
        success:function(res){
          if(res=='true'){
            swal({
                  title : 'แสดงเมือทำงานสำเร็จ',
                  text : '',
                  type : 'success'
                  },
                  function(){
                    // เมื่อทำงานสำเร็จให้ไปที่หน้านี้
                    window.location.replace("<?php echo base_url() ?>index.php/member");
                  }
              );

          }else{
            swal({
                  title : 'แสดงเมื่อทำงานไม่สำเร็จ',
                  text : '',
                  type : 'error'
              });
          }
        },
        error:function(err){
          swal({
                  title : 'เกิดข้อผิดพลาด',
                  text : err,
                  type : 'error'
              });
        }
      });
    });
  });
</script>
<!-- begin add require script -->
<!-- Datatable -->
  <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.css">
  <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.js"></script>
<!-- end add require script -->
<div class="box box-success">
  <div class="box-header">
        <i class="fa fa-sign-in"></i>
    <h3 class="box-title">โครงสร้างการบริหาร ศธจ.</h3>
  </div>
  <div class="box-body">
    <!-- ส่วนแสดงผล -->
    <!-- Post -->
    <div class="post">
      <div align="center">
        <img class="img-responsive" src="<?php echo base_url(); ?>assets/images/organization-chart.png">
      </div>
      <div class="box-header">
        <h3 class="box-title">รายละเอียดเพิ่มเติม</h3>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <table id="table1" class="table table-bordered table-striped">
          <thead>
          <tr>
            <th>#</th>
            <th>ชื่อ-สกุล</th>
            <th>ตำแหน่ง</th>
            <th>หมายเลขโทรศัพท์(สำนักงาน)</th>
            <th>โทรศัพท์มือถือ</th>
          </tr>
          </thead>
          <tbody>
          <?php 
          if($data!=false) 
          {
            foreach ($data as $row)
            { 
              ?>
              <tr>
                <td><?php echo $row->id; ?></td>
                <td><?php echo $row->name; ?></td>
                <td><?php echo $row->position; ?></td>
                <td><?php echo $row->tel_office; ?></td>
                <td><?php echo $row->tel_mobile; ?></td>
              </tr>
              <?php 
            }
          }
            ?>
          </tbody>
        </table>
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.post -->
    <!-- จบส่วนแสดงผล -->
  </div>
</div>