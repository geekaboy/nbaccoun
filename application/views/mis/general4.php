<script type="text/javascript">
  $(document).ready(function(){
    $('#table1').DataTable({
      responsive: true
    });
    $('#username').focus();
    $('#btn_login').click(function(){
      $.ajax({
        url:'<?php echo base_url(); ?>index.php/home/login',
        data:'username='+$('#username').val()+'&password='+$('#password').val(),
        type:'POST',
        success:function(res){
          if(res=='true'){
            swal({
                  title : 'แสดงเมือทำงานสำเร็จ',
                  text : '',
                  type : 'success'
                  },
                  function(){
                    // เมื่อทำงานสำเร็จให้ไปที่หน้านี้
                    window.location.replace("<?php echo base_url() ?>index.php/member");
                  }
              );

          }else{
            swal({
                  title : 'แสดงเมื่อทำงานไม่สำเร็จ',
                  text : '',
                  type : 'error'
              });
          }
        },
        error:function(err){
          swal({
                  title : 'เกิดข้อผิดพลาด',
                  text : err,
                  type : 'error'
              });
        }
      });
    });
  });
</script>
<!-- begin add require script -->
<!-- Datatable -->
  <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.css">
  <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.js"></script>
<!-- end add require script -->
<div class="box box-success">
  <div class="box-header">
        <i class="fa fa-sign-in"></i>
    <h3 class="box-title">หน่วยงานด้านการศึกษา</h3>
  </div>
  <div class="box-body">
    <!-- ส่วนแสดงผล -->
    <!-- Post -->
    <div class="post">
      <!--
      <div align="center">
        <img class="img-responsive" src="<?php //echo base_url(); ?>assets/images/organization-chart.png">
      </div>
      
      <div class="box-header">
        <h3 class="box-title">รายละเอียด</h3>
      </div>
      -->
      <!-- /.box-header -->
      <div class="box-body">
        <table id="table1" class="table table-bordered table-striped">
          <thead>
          <tr>
            <th>#</th>
            <th>หน่วยงาน</th>
            <th>หัวหน้าหน่วยงาน</th>
            <th>ตำแหน่ง</th>
            <th>โทรศัพท์</th>
          </tr>
          </thead>
          <tbody>
          <?php 
          if($data!=false) 
          {
            // set default
            /*
            $total_manage_m = 0;
            $total_manage_f = 0;
            $total_officer_m = 0;
            $total_officer_f = 0;
            $total_other_m = 0;
            $total_other_f = 0;
            $total_total = 0;
            */
            foreach ($data as $row)
            { 
              ?>
              <tr>
                <td><?php echo $row->id; ?></td>
                <td><?php echo $row->office; ?></td>
                <td><?php echo $row->leader; ?></td>
                <td><?php echo $row->position; ?></td>
                <td><?php echo $row->tel; ?></td>
              </tr>
              <?php 
              // sum total
              /*
              $total_manage_m = $total_manage_m+$row->manage_m;
              $total_manage_f = $total_manage_f+$row->manage_f;
              $total_officer_m = $total_officer_m+$row->officer_m;
              $total_officer_f = $total_officer_f+$row->officer_f;
              $total_other_m = $total_other_m+$row->other_m;
              $total_other_f = $total_other_f+$row->other_f;
              $total_total = $total_total+$row->total;
              */
            }
          }
            ?>
          </tbody>
        </table>
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.post -->
    <!-- จบส่วนแสดงผล -->
  </div>
</div>