<script type="text/javascript">
  $(document).ready(function(){
    $('#table1').DataTable({
      responsive: true
    });
    $('#username').focus();
    $('#btn_login').click(function(){
      $.ajax({
        url:'<?php echo base_url(); ?>index.php/home/login',
        data:'username='+$('#username').val()+'&password='+$('#password').val(),
        type:'POST',
        success:function(res){
          if(res=='true'){
            swal({
                  title : 'แสดงเมือทำงานสำเร็จ',
                  text : '',
                  type : 'success'
                  },
                  function(){
                    // เมื่อทำงานสำเร็จให้ไปที่หน้านี้
                    window.location.replace("<?php echo base_url() ?>index.php/member");
                  }
              );

          }else{
            swal({
                  title : 'แสดงเมื่อทำงานไม่สำเร็จ',
                  text : '',
                  type : 'error'
              });
          }
        },
        error:function(err){
          swal({
                  title : 'เกิดข้อผิดพลาด',
                  text : err,
                  type : 'error'
              });
        }
      });
    });
  });
</script>
<script type="text/javascript">

window.onload = function () {
  var chart = new CanvasJS.Chart("chartContainer", {
    title:{
      text: "แผนภาพแสดงจำนวนผู้เรียนแยกหน่วยงาน"              
    },
    axisY:{    
        valueFormatString:  "#,##0", // move comma to change formatting
        prefix: ""
    },
    axisX: {
        //labelFontSize: 0,
        labelAngle: -30
    },
    data: [              
    {
      // Change type to "doughnut", "line", "splineArea", etc.
      type: "column",
      dataPoints: [
        { label: "สพป.กส.1",  y: 23191  },
        { label: "สพป.กส.2", y: 20964  },
        { label: "สพป.กส.3", y: 28121  },
        { label: "สพม.24",  y: 33382  },
        { label: "ศึกษาพิเศษ",  y: 488  },
        { label: "สช.",  y: 20272  },
        { label: "อาชีวศึกษา(รัฐ)",  y: 9791  },
        { label: "อาชีวศึกษา(เอกชน)",  y: 3481  },
        { label: "กศน.",  y: 33500  },
        { label: "ม.กาฬสินธุ์",  y: 5225  },
        { label: "อบต.",  y: 724  },
        { label: "เทศบาลตำบล",  y: 1311  },
        { label: "เทศบาลเมือง",  y: 2411  },
        { label: "อบจ.",  y: 7057  },
        { label: "สำนักพุทธฯ",  y: 856  },
        { label: "นาฏศิลป์",  y: 784  }
      ]
    }
    ]
  });
  chart.render();
}
</script>
<!-- begin add require script -->
  <!-- Datatable -->
  <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.css">
  <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.js"></script>
  <!-- Chart -->
  <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
<!-- end add require script -->
<div class="box box-success">
  <div class="box-header">
        <i class="fa fa-sign-in"></i>
    <h3 class="box-title">จำนวนสถานศึกษา ผู้เรียน ห้องเรียน ครู /ผู้สอน และบุคลากรทางการศึกษา ปีการศึกษา 2560 </h3>
  </div>
  <div class="box-body">
    <!-- ส่วนแสดงผล -->
    <div id="chartContainer" style="height: 300px; width: 100%;"></div>
    <!-- Post -->
    <div class="post">
      <!--
      <div align="center">
        <img class="img-responsive" src="<?php //echo base_url(); ?>assets/images/organization-chart.png">
      </div>
      -->
      <div class="box-header">
        <h3 class="box-title">ข้อมูล ณ วันที่ 10  มิถุนายน 2560</h3>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <table id="table1" class="table table-bordered table-striped">
          <thead>
          <tr>
            <th>#</th>
            <th>หน่วยงาน</th>
            <th>จำนวนสถานศึกษา</th>
            <th>จำนวนผู้เรียน</th>
            <th>จำนวนครู/ผู้สอน</th>
            <th>จำนวนผู้บริหาร</th>
          </tr>
          </thead>
          <tbody>
          <?php 
          if($data!=false) 
          {
            // set default
            $total_school = 0;
            $total_student = 0;
            $total_teacher = 0;
            $total_manage = 0;
            foreach ($data as $row)
            { 
              ?>
              <tr>
                <td><?php echo $row->id; ?></td>
                <td><?php echo $row->office; ?></td>
                <td align="right"><?php echo $row->school; ?></td>
                <td align="right"><?php echo $row->student; ?></td>
                <td align="right"><?php echo $row->teacher; ?></td>
                <td align="right"><?php echo $row->manage; ?></td>
              </tr>
              <?php 
              // sum total
              $total_school = $total_school+$row->school;
              $total_student = $total_student+$row->student;
              $total_teacher = $total_teacher+$row->teacher;
              $total_manage = $total_manage+$row->manage;
            }
          }
            ?>
          </tbody>
          <tfoot>
          <tr>
            <th colspan="2">รวมทั้งหมด</th>
            <th align="right"><?php echo $total_school; ?></th>
            <th align="right"><?php echo $total_student; ?></th>
            <th align="right"><?php echo $total_teacher; ?></th>
            <th align="right"><?php echo $total_manage; ?></th>
          </tr>
          </tfoot>
        </table>
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.post -->
    <!-- จบส่วนแสดงผล -->
  </div>
</div>