<!-- Google Chart -->
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
  google.charts.load('current', {'packages':['corechart']});
  google.charts.setOnLoadCallback(drawChart);  
  function drawChart() {
    var data = google.visualization.arrayToDataTable([
      <?php 
      $department = "";
      $student = "";
      $zero = "";
      foreach ($view_mis_dashboard_student as $row_student) {
        if($department!=""){ $department=$department.","; }
        $department = $department."'".$row_student->master_department_name."'";
        if($student!=""){ $student=$student.","; }
        $student = $student.$row_student->student;
        $row_zero = 0;
        if($zero!=""){ $zero=$zero.","; }
        $zero = $zero.$row_zero;
      }
      ?>
      ['Year',<?php echo $department; ?>],
      ['2558',<?php echo $zero; ?>],
      ['2559',<?php echo $zero; ?>],
      ['2560',<?php echo $student; ?>]
    ]);    
    var options = {
      //title: 'Company Performance',
      //curveType: 'function',
      legend: { position: 'top' },
      hAxis: {title: 'ปีการศึกษา',  titleTextStyle: {color: '#333'}},
      vAxis: {title: 'จำนวนนักเรียน/นักศึกษา (คน)',  titleTextStyle: {color: '#333'}}
    };    
    var chart = new google.visualization.LineChart(document.getElementById('curve_chart'));    
    chart.draw(data, options);
  }
</script>
<!-- /.Google Chart -->

<!-- Small boxes (Stat box) -->
<div class="row">
  <div class="col-lg-3 col-xs-6">
    <!-- small box -->
    <div class="small-box bg-aqua">
      <div class="inner">
        <h3>15<sup style="font-size: 20px"> หน่วยงาน</sup></h3>        
        <p>หน่วยงานการศึกษาระดับจังหวัด</p>
      </div>
      <div class="icon">
        <i class="ion ion-bag"></i>
      </div>
      <a href="<?php echo base_url(); ?>index.php/mis_report/department" class="small-box-footer">รายละเอียดเพิ่มเติม <i class="fa fa-arrow-circle-right"></i></a>
    </div>
  </div>
  <!-- ./col -->
  <div class="col-lg-3 col-xs-6">
    <!-- small box -->
    <div class="small-box bg-green">
      <div class="inner">
        <?php //foreach($mis_sum as $row_mis_sum){} ?>
        <h3><?php echo number_format($mis_sum->school); ?><sup style="font-size: 20px"> แห่ง</sup></h3>        
        <p>สถานศึกษา</p>
      </div>
      <div class="icon">
        <i class="ion ion-stats-bars"></i>
      </div>
      <a href="<?php echo base_url(); ?>index.php/mis_report/department_master" class="small-box-footer">รายละเอียดเพิ่มเติม <i class="fa fa-arrow-circle-right"></i></a>
    </div>
  </div>
  <!-- ./col -->
  <div class="col-lg-3 col-xs-6">
    <!-- small box -->
    <div class="small-box bg-yellow">
      <div class="inner">
        <h3><?php echo number_format($mis_sum->student); ?><sup style="font-size: 20px"> คน</sup></h3>        
        <p>นักเรียน/นักศึกษา</p>
      </div>
      <div class="icon">
        <i class="ion ion-person-add"></i>
      </div>
      <a href="<?php echo base_url(); ?>index.php/mis_report/department_master" class="small-box-footer">รายละเอียดเพิ่มเติม <i class="fa fa-arrow-circle-right"></i></a>
    </div>
  </div>
  <!-- ./col -->
  <div class="col-lg-3 col-xs-6">
    <!-- small box -->
    <div class="small-box bg-red">
      <div class="inner">
        <h3><?php echo number_format($mis_sum->person); ?><sup style="font-size: 20px"> คน</sup></h3>        
        <p>บุคลากร</p>
      </div>
      <div class="icon">
        <i class="ion ion-pie-graph"></i>
      </div>
      <a href="<?php echo base_url(); ?>index.php/mis_report/department_master" class="small-box-footer">รายละเอียดเพิ่มเติม <i class="fa fa-arrow-circle-right"></i></a>
    </div>
  </div>
  <!-- ./col -->
</div>
<!-- /.row -->

<!-- Content Wrapper. Contains page content -->
<!-- Content Header (Page header) -->
<div class="row">
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <!-- BAR CHART -->
        <div class="box box-success">
          <div class="box-header with-border">
            <h3 class="box-title">สรุปจำนวนนักเรียน/นักศึกษา แยกสังกัด</h3>          
            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
              </button>
              <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
            </div>
          </div>
          <div class="box-body">
            <div class="chart chart-responsive">
              <div id="curve_chart" style="width: 100%; height: 500px"></div>
            </div>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->         
      </div>
      <!-- /.col (LEFT) -->
    </div>
    <!-- /.row -->
  </section>
</div>
<!-- /.content -->
<!-- /.content-wrapper -->