
<div class='modal fade' id='myModal' role='dialog' data-keyboard="false" data-backdrop="static">
<div class='modal-dialog modal-lg'>
  <div class='modal-content'>
    <div class='modal-header'>      
      <h4 class='modal-title'>Select Company | เลือกบริษัท</h4>
    </div>
    <div class='modal-body'>
            <!-- Small boxes (Stat box) -->
      <div class="row">

        <?php 
        if(!empty($results_company)){
          $i=0;
          foreach ($results_company as $row) {
            $i++;
            switch ($i%4) {
                case 0:
                    $color = "bg-aqua";
                    break;
                case 1:
                    $color = "bg-green";
                    break;
                case 2:
                    $color = "bg-yellow";
                    break;
                case 3:
                    $color = "bg-red";
                    break;
                default:
                    $color = "bg-aqua";
                    break;
            }
        ?>

        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box <?php echo $color;?>">
            <div class="inner">
              <h3><?php echo $i;?></h3>
              <p><?php echo $row->CompNameEng." <BR> ".$row->CompNameThai;?></p>
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div>
            <a href="#" onclick="SetSessionCompany(<?php echo $row->CompID;?>,'<?php echo $row->CompNameEng;?>','<?php echo $row->CompNameThai;?>')" class="small-box-footer">Select | เลือก <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <?php
          }
        }
        ?>

        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3>+</h3>
              <p>Add New Company <BR> เพิ่มบริษัทใหม่</p>
            </div>
            <div class="icon">
              <span class="glyphicon glyphicon-home"></span>
            </div>
            <a href="#" onclick="AddNewCompany()" class="small-box-footer">Select | เลือก <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        
        
      </div>
    </div>
  </div>
</div>
</div>
<script>
 $('#myModal').modal();
 function SetSessionCompany(CompID,CompNameEng,CompNameThai){
  $.ajax({
    url:'<?php echo base_url(); ?>index.php/member/setcompany',
    data: {
      CompID:CompID,
      CompNameEng:CompNameEng,
      CompNameThai:CompNameThai
    },
    type:'POST',
    success:function(res){
      if(res=='true'){
        window.location.replace("<?php echo base_url() ?>index.php/member");
      }
    }
  });
 }
 function AddNewCompany() {
   window.location.replace("<?php echo base_url() ?>index.php/hrsetting/Company");
 }
</script>

