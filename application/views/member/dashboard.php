<script type="text/javascript">

  $(document).ready(function(){
    //เพิ่มเงื่อนไขตาราง
    $('.js-exportable').DataTable({
        dom: 'Blfrtip',responsive: true,paging: true,info: true,
        buttons: [
            {extend:'copyHtml5',text:' <i class="fa fa-files-o fa-2x "></i> ',titleAttr: ' Copy '},
            {extend:'excelHtml5',text:' <i class="fa fa-file-excel-o fa-2x "></i> ',titleAttr: ' Excel '},
            {extend:'print',text:' <i class="fa fa-print fa-2x "></i><BR>&nbsp; ',titleAttr: ' Print '},
        ],
        "lengthMenu": [ [10, 25, 50, -1], [10, 25, 50, "All"] ],
      });

  });
</script>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/adminlte233/plugins/iCheck/square/blue.css">
<link href="<?php echo base_url();?>assets/bootstrap-select/css/bootstrap-select.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/bootstrap/css/datepicker.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3><?=$this->systemmodel->changeLng("ทดลองงาน")?></h3>
              <p><?=$this->systemmodel->changeLng("แสดงรายชื่อพนักงานที่จะผ่านทดลองงานในเดือนนี้")?></p>
            </div>
            <div class="icon">
              <span class="glyphicon glyphicon-bullhorn"></span>
            </div>
            <a href="#" onclick="displayDashboard(1)" class="small-box-footer"><?=$this->systemmodel->changeLng("แสดงรายงาน")?> <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3><?=$this->systemmodel->changeLng("วันเกิด")?></h3>
              <p><?=$this->systemmodel->changeLng("แสดงรายชื่อพนักงานที่มีวันเกิดในเดือนนี้")?></p>
            </div>
            <div class="icon">
              <span class="glyphicon glyphicon-gift"></span>
            </div>
            <a href="#" onclick="displayDashboard(2)" class="small-box-footer"><?=$this->systemmodel->changeLng("แสดงรายงาน")?> <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3><?=$this->systemmodel->changeLng("วันที่สิ้นสุดสัญญาจ้าง")?></h3>
              <p><?=$this->systemmodel->changeLng("แสดงรายชื่อพนักงานที่จะสิ้นสุดสัญญาจ้างในเดือนนี้")?></p>
            </div>
            <div class="icon">
              <span class="glyphicon glyphicon-plus"></span>
            </div>
            <a href="#" onclick="displayDashboard(3)" class="small-box-footer"><?=$this->systemmodel->changeLng("แสดงรายงาน")?> <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
      </div>
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <section class="col-lg-12 connectedSortable" id="Dashboard1">
          <div class="box box-info">
            <div class="box-header">
              <i class="ion ion-clipboard"></i>
              <h3 class="box-title"><?=$this->systemmodel->changeLng("แสดงรายชื่อพนักงานที่จะผ่านทดลองงานในเดือนนี้")?></h3>
            </div>
            <div class="box-body">
              <table class="table table-bordered table-striped table-hover dataTable  js-exportable">
                <thead>
                  <tr>
                    <th>#</th>
                    <th scope="col"><center><span class="glyphicon glyphicon-user"></span></center></th>
                    <th><?=$this->systemmodel->changeLng("ชื่อพนักงาน")?></th>
                    <th><?=$this->systemmodel->changeLng("แผนก")?></th>
                    <th><?=$this->systemmodel->changeLng("วันที่ผ่านทดลองงาน")?></th>
                    <th><?=$this->systemmodel->changeLng("อนุมัติ")?></th>
                  </tr>
                </thead>
                <tbody>
                  <?php 
                    if(!empty($results_probation)){
                      $i=0;
                      foreach ($results_probation as $row) {
                        $i++;
                  ?>
                  <tr>
                    <td><?php echo $i;?></td>
                    <td scope="col">
                      <!-- Trigger the modal with a button -->
                      <?php if(!empty($row->EmployeePhoto)){?>
                      <img src="<?php echo base_url(); ?>assets/uploads/company/nc/files/<?php echo $row->EmployeePhoto;?>" id="EmployeePhoto_show" class="img-circle" width="50px;"data-toggle="modal" data-target="#myModal<?php echo $i;?>" style="cursor: pointer;">
                      <!-- Modal -->
                      <div id="myModal<?php echo $i;?>" class="modal fade" role="dialog">
                        <div class="modal-dialog">

                          <!-- Modal content-->
                          <div class="modal-content">                      
                            <div class="modal-body">
                              <img src="<?php echo base_url(); ?>assets/uploads/company/nc/files/<?php echo $row->EmployeePhoto;?>" id="EmployeePhoto_show" class="img-responsive">
                            </div>
                          </div>

                        </div>
                      </div>
                    <?php }?>
                    </td>
                    <td><?php echo $row->FirstName." ".$row->LastName;?></td>
                    <td><?php echo $row->SectionNameEng." | ".$row->SectionNameThai;?></td>
                    <td><?php echo $row->ProbationEndDate;?></td>
                    <td>
                      <a href="hr/form_employee_movement/edit/<?php echo $row->EmployeeID?>" class="btn btn-warning btn-sm">
                        <span class="glyphicon glyphicon-bullhorn"></span>
                      </a>
                    </td>
                  </tr>
                <?php 
                      }
                  }
                ?>
                </tbody>
              </table>
            </div>
            <div class="box-footer clearfix no-border">
              
            </div>
          </div>
        </section>
        <section class="col-lg-12 connectedSortable" id="Dashboard2">
          <div class="box box-success">
            <div class="box-header">
              <i class="ion ion-clipboard"></i>
              <h3 class="box-title"><?=$this->systemmodel->changeLng("แสดงรายชื่อพนักงานที่เกิดในเดือนนี้")?></h3>
            </div>
            <div class="box-body">
              <table class="table table-bordered table-striped table-hover dataTable  js-exportable">
                <thead>
                  <tr>
                    <th>#</th>
                    <th scope="col"><center><span class="glyphicon glyphicon-user"></span></center></th>
                    <th><?=$this->systemmodel->changeLng("ชื่อพนักงาน")?></th>
                    <th><?=$this->systemmodel->changeLng("แผนก")?></th>
                    <th><?=$this->systemmodel->changeLng("วันเกิด")?></th>
                    <th><?=$this->systemmodel->changeLng("พิมพ์บัตรอวยพร")?></th>
                  </tr>
                </thead>
                <tbody>
                  <?php 
                    if(!empty($results_birthday)){
                      $i=0;
                      foreach ($results_birthday as $row) {
                        $i++;
                  ?>
                  <tr>
                    <td><?php echo $i;?></td>
                    <td scope="col">
                      <!-- Trigger the modal with a button -->
                      <?php if(!empty($row->EmployeePhoto)){?>
                      <img src="<?php echo base_url(); ?>assets/uploads/company/nc/files/<?php echo $row->EmployeePhoto;?>" id="EmployeePhoto_show" class="img-circle" width="50px;"data-toggle="modal" data-target="#myModal<?php echo $i;?>" style="cursor: pointer;">
                      <!-- Modal -->
                      <div id="myModal<?php echo $i;?>" class="modal fade" role="dialog">
                        <div class="modal-dialog">

                          <!-- Modal content-->
                          <div class="modal-content">                      
                            <div class="modal-body">
                              <img src="<?php echo base_url(); ?>assets/uploads/company/nc/files/<?php echo $row->EmployeePhoto;?>" id="EmployeePhoto_show" class="img-responsive">
                            </div>
                          </div>

                        </div>
                      </div>
                    <?php }?>
                    </td>
                    <td><?php echo $row->FirstName." ".$row->LastName;?></td>
                    <td><?php echo $row->SectionNameEng." | ".$row->SectionNameThai;?></td>
                    <td><?php echo $row->Birthday;?></td>
                    <td>
                      <button type="button" class="btn btn-danger btn-sm">
                        <span class="glyphicon glyphicon-gift"></span>
                      </button>
                    </td>
                  </tr>
                <?php 
                      }
                  }
                ?>
                </tbody>
              </table>
            </div>
            <div class="box-footer clearfix no-border">
              
            </div>
          </div>
        </section>
        <section class="col-lg-12 connectedSortable" id="Dashboard3">
          <div class="box box-info">
            <div class="box-header">
              <i class="ion ion-clipboard"></i>
              <h3 class="box-title"><?=$this->systemmodel->changeLng("แสดงรายชื่อพนักงานที่จะสิ้นสุดสัญญาจ้างในเดือนนี้")?></h3>
            </div>
            <div class="box-body">
              <table class="table table-bordered table-striped table-hover dataTable  js-exportable">
                <thead>
                  <tr>
                    <th>#</th>
                    <th scope="col"><center><span class="glyphicon glyphicon-user"></span></center></th>
                    <th><?=$this->systemmodel->changeLng("ชื่อพนักงาน")?></th>
                    <th><?=$this->systemmodel->changeLng("แผนก")?></th>
                    <th><?=$this->systemmodel->changeLng("วันที่สิ้นสุดสัญญาจ้าง")?></th>
                    <!-- <th><?=$this->systemmodel->changeLng("อนุมัติ")?></th> -->
                  </tr>
                </thead>
                <tbody>
                  <?php 
                    if(!empty($results_contract_end_date)){
                      $i=0;
                      foreach ($results_contract_end_date as $row) {
                        $i++;
                  ?>
                  <tr>
                    <td><?php echo $i;?></td>
                    <td scope="col">
                      <!-- Trigger the modal with a button -->
                      <?php if(!empty($row->EmployeePhoto)){?>
                      <img src="<?php echo base_url(); ?>assets/uploads/company/nc/files/<?php echo $row->EmployeePhoto;?>" id="EmployeePhoto_show" class="img-circle" width="50px;"data-toggle="modal" data-target="#myModal<?php echo $i;?>" style="cursor: pointer;">
                      <!-- Modal -->
                      <div id="myModal<?php echo $i;?>" class="modal fade" role="dialog">
                        <div class="modal-dialog">

                          <!-- Modal content-->
                          <div class="modal-content">                      
                            <div class="modal-body">
                              <img src="<?php echo base_url(); ?>assets/uploads/company/nc/files/<?php echo $row->EmployeePhoto;?>" id="EmployeePhoto_show" class="img-responsive">
                            </div>
                          </div>

                        </div>
                      </div>
                    <?php }?>
                    </td>
                    <td><?php echo $row->FirstName." ".$row->LastName;?></td>
                    <td><?php echo $row->SectionNameEng." | ".$row->SectionNameThai;?></td>
                    <td><?php echo $row->ContractEndDate;?></td>
                    <!-- <td>
                      <a href="hr/form_employee_movement/edit/<?php echo $row->EmployeeID?>" class="btn btn-warning btn-sm">
                        <span class="glyphicon glyphicon-bullhorn"></span>
                      </a>
                    </td> -->
                  </tr>
                <?php 
                      }
                  }
                ?>
                </tbody>
              </table>
            </div>
            <div class="box-footer clearfix no-border">
              
            </div>
          </div>
        </section>

      </div>

<script type="text/javascript">
  $('#Dashboard2').hide();
  $('#Dashboard3').hide();
  function displayDashboard(index){
    $('#Dashboard1').hide();
    $('#Dashboard2').hide();
    $('#Dashboard3').hide();
    $('#Dashboard'+index).show();
  }
</script>

<script src="<?php echo base_url();?>assets/bootstrap/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url();?>assets/jquery-datatable/jquery.dataTables.js"></script>
<script src="<?php echo base_url();?>assets/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
<script src="<?php echo base_url();?>assets/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url();?>assets/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
<script src="<?php echo base_url();?>assets/jquery-datatable/extensions/export/jszip.min.js"></script>
<script src="<?php echo base_url();?>assets/jquery-datatable/extensions/export/pdfmake.min.js"></script>
<script src="<?php echo base_url();?>assets/jquery-datatable/extensions/export/vfs_fonts.js"></script>
<script src="<?php echo base_url();?>assets/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
<script src="<?php echo base_url();?>assets/jquery-datatable/extensions/export/buttons.print.min.js"></script>      