<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

// เปลี่ยนชื่อโมเดล (MemberModel) ให้เป็นชื่อโมเดล อักษาตัวแรกให้เป็นตัวใหญ่และตามด้วยคำว่า Model
class Ajaxmodel extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    function list_field($table,$field)
    {
        $query = $this->db->query("SELECT ".$field." FROM ".$table);
        $ret = array();
        foreach ($query->result_array() as $row) {
            $ret[] = $row[$field];
        }
        return json_encode($ret);
    }

    function ajax($ajax) {
        $html = "";
if (!empty($ajax)) {
    $html .= '<script>
     $(document).ready(function () { ';
     foreach ($ajax as $aj) {
     $html .= "\n           \$(\"#field-".$aj[0]."\").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: '".base_url()."index.php/Ajaxservice/ajaxdata',
                        data: {
                            table: '".$aj[1]."',
                            field: '".$aj[2]."'
                        },
                        dataType: 'json',
                        type: 'POST',
                        success: function (data) {
                            response($.ui.autocomplete.filter(data, request.term));
                        }
                    });
                }";
            if (!empty($aj[3])) {
                $html .= ", minLength : ".$aj[3];
            } else {
                $html .= ", minLength : 3";
            }
                $html .= "
            });\n";
            }
    $html .= "\n      });

</script>\n";
        }

        return $html;
    }
    
    function is_table_approve($tb,$id) {
		$sql_time = "SELECT * FROM pcsh_approve_time Where tablename = '".$tb."'";
        $query_time = $this->db->query($sql_time);
		foreach ($query_time->result_array() as $row_time) {
			if ($row_time['status'] == '0') {
				
				
				

			}else{
				if(date("Y-m-d")>=$row_time['datestart'] && date("Y-m-d")<=$row_time['dateend']){
					echo "true";
				}else{
					echo "false";
				}
		
			}
		}	
			
        $sql = "SELECT status_approve FROM ".$tb." Where id = '".$id."'";
        $query = $this->db->query($sql);
        foreach ($query->result_array() as $row) {
            if ($row['status_approve'] == '1') {
    
                $js = "
            <script>
                swal({
                    title: 'ยืนยันการรับรองข้อมูลแล้ว !!!',
                    text: 'ติดต่อเจ้าหน้าที่หากต้องการแก้ไขข้อมูล',
                    type: 'error'
                },
                  function(){
                    window.history.back();
                });
            </script>
            ";
                return $js;
            }
        }
        return '';
    }

}
