<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class MemberModel extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function checklogin($username, $password)
    {
        $query = $this->db
            ->where('status', 1)
            ->where('username', $username)
            ->where('password', md5($password))
            ->get('person_main');
        if ($query->num_rows() == 1) {
            foreach ($query->result() as $row) {
                return $row->person_id;
            }
        } else {
            return false;
        }
    }

    public function checkuser_exist($username)
    {
        $query = $this->db
            ->where('username', $username)
            ->get('person_main');
        if ($query->num_rows() == 1) {
            foreach ($query->result() as $row) {
                return true;
            }
        } else {
            return false;
        }
    }
    public function getCompany(){
        $query = $this->db
            ->where('CompUSERID', $this->session->CompUSERID)
            ->where('IsActive', 1)
            ->get('Company');
        if ($query->num_rows() > 0) return $query->result();
        else return false;    
    }
    public function results_probation(){
        $sql = "
            SELECT * FROM Employee a 
            LEFT OUTER  JOIN Section b on a.SectionID = b.SectionID
            WHERE a.CompID = ".$this->session->CompID."
            and YEAR(CURDATE()) = YEAR(a.ProbationEndDate)
            and MONTH(CURDATE()) = MONTH(a.ProbationEndDate)
            ORDER BY a.ProbationEndDate
        ";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) return $query->result();
        else return false;    
    }
    public function results_birthday(){
        $sql = "
            SELECT * FROM Employee a 
            LEFT OUTER  JOIN Section b on a.SectionID = b.SectionID
            WHERE a.CompID = ".$this->session->CompID."            
            and MONTH(CURDATE()) = MONTH(a.Birthday)
            ORDER BY a.Birthday
        ";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) return $query->result();
        else return false;    
    }
    public function results_contract_end_date(){
        $sql = "
            SELECT * FROM Employee a 
            LEFT OUTER  JOIN Section b on a.SectionID = b.SectionID
            WHERE a.CompID = ".$this->session->CompID."
            and YEAR(CURDATE()) = YEAR(a.ContractEndDate)
            and MONTH(CURDATE()) = MONTH(a.ContractEndDate)
            ORDER BY a.ContractEndDate
        ";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) return $query->result();
        else return false;    
    }
}
