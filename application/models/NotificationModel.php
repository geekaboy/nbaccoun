<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

// เปลี่ยนชื่อโมเดล (MemberModel) ให้เป็นชื่อโมเดล อักษาตัวแรกให้เป็นตัวใหญ่และตามด้วยคำว่า Model
class NotificationModel extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    // Begin Ioffice
    function notification_bookcomment($person_id) {  
        $this->db->select('ib.*');
        $this->db->from('ioffice_book as ib');   
        $this->db->join('ioffice_bookcomment ibc','ibc.bookid=ib.bookid');
        $this->db->where('ibc.comment_personid', $person_id);
        $this->db->where('ibc.bookstatusid',2);
        $query = $this->db->get();
        return $query->num_rows();
    }

    function notification_book2comment_income($nodeid) {  
        $this->db->select('ib.*');
        $this->db->from('ioffice2_book ib');   
        $this->db->join('ioffice2_bookcomment ibc','ib.bookid=ibc.bookid');
        $this->db->where('((ibc.bookstatusid=2) or (ibc.bookstatusid=40))');
        $this->db->where('comment_nodeid in('.$nodeid.')');
        $query = $this->db->get();
        return $query->num_rows();
    }

    function check_person_group($person_groupid){
        $this->db->where('person_id',$this->session->person_id);
        $this->db->where('person_groupid',$person_groupid);
        $query = $this->db->get('view_person_group');
        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    // End Ioffice

}
