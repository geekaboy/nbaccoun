-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 20, 2020 at 11:58 AM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 5.6.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `nbaccounts`
--

-- --------------------------------------------------------

--
-- Table structure for table `view_node`
--

CREATE TABLE `view_node` (
  `nodeid` varchar(20) NOT NULL,
  `nodename` varchar(255) DEFAULT NULL,
  `nodeprecis` varchar(255) DEFAULT NULL,
  `department_id` int(11) DEFAULT NULL,
  `department_masterid` int(11) DEFAULT NULL,
  `department_groupid` int(11) NOT NULL,
  `department_typeid` int(11) DEFAULT NULL,
  `department_typename` varchar(255) DEFAULT NULL,
  `nodetype` varchar(30) DEFAULT NULL,
  `nodetypename` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `view_node`
--

INSERT INTO `view_node` (`nodeid`, `nodename`, `nodeprecis`, `department_id`, `department_masterid`, `department_groupid`, `department_typeid`, `department_typename`, `nodetype`, `nodetypename`) VALUES
('D1', 'Naburi Hospitality', 'NBH', 1, 0, 1, 1, 'ศูนย์พัฒนาโรงเรียนวิทยาศาสตร์', 'department', 'หน่วยงาน'),
('P15', NULL, 'Sybrite', 1, 0, 0, 1, 'บริษัท', 'person', 'บุคคล'),
('P17', 'Mrs. Kittima Tanprasert | Staff | NBH', 'Kittima', 1, 0, 0, 1, 'บริษัท', 'person', 'บุคคล'),
('P18', 'Miss Wannipa Sainoi | Administrator | NBH', 'Wannipa', 1, 0, 0, 1, 'บริษัท', 'person', 'บุคคล'),
('P19', 'Miss Pornthip Whanpuch | Staff | NBH', 'Pornthip', 1, 0, 0, 1, 'บริษัท', 'person', 'บุคคล'),
('P20', 'Miss Sukanya Kingnok | Staff | NBH', 'Sukanya', 1, 0, 0, 1, 'บริษัท', 'person', 'บุคคล'),
('P21', 'Mr. Anuphong Tangsee | Administrator | NBH', 'Anuphong', 1, 0, 0, 1, 'บริษัท', 'person', 'บุคคล'),
('P22', 'Miss Tunsuda Rakchom | Staff | NBH', 'Tunsuda', 1, 0, 0, 1, 'บริษัท', 'person', 'บุคคล'),
('P23', 'Mrs. Sasipa Sainoi | Staff | NBH', 'Sasipa', 1, 0, 0, 1, 'บริษัท', 'person', 'บุคคล'),
('P24', 'Mr. Bancha Tangsee | Administrator | NBH', 'Bancha', 1, 0, 0, 1, 'บริษัท', 'person', 'บุคคล'),
('P25', NULL, 'HRstaff', 1, 0, 0, 1, 'บริษัท', 'person', 'บุคคล'),
('P27', NULL, 'HR Manager', 1, 0, 0, 1, 'บริษัท', 'person', 'บุคคล'),
('P28', NULL, 'Payroll Staff', 1, 0, 0, 1, 'บริษัท', 'person', 'บุคคล'),
('P29', NULL, 'Payroll Manager', 1, 0, 0, 1, 'บริษัท', 'person', 'บุคคล'),
('P30', 'Mr. Suphachai Ngamphuphan | Manager | NBH', 'Suphachai', 1, 0, 0, 1, 'บริษัท', 'person', 'บุคคล'),
('P31', NULL, 'Staff', 1, 0, 0, 1, 'บริษัท', 'person', 'บุคคล'),
('P32', NULL, 'purchase Request', 1, 0, 0, 1, 'บริษัท', 'person', 'บุคคล'),
('P33', NULL, 'Purchase', 1, 0, 0, 1, 'บริษัท', 'person', 'บุคคล'),
('P34', NULL, 'Receiving', 1, 0, 0, 1, 'บริษัท', 'person', 'บุคคล'),
('P35', NULL, 'cost', 1, 0, 0, 1, 'บริษัท', 'person', 'บุคคล'),
('P36', NULL, 'Ball', 1, 0, 0, 1, 'บริษัท', 'person', 'บุคคล'),
('P38', NULL, 'store', 1, 0, 0, 1, 'บริษัท', 'person', 'บุคคล'),
('P39', NULL, 'non sme', 1, 0, 0, 1, 'บริษัท', 'person', 'บุคคล'),
('P40', NULL, 'Payable', 1, 0, 0, 1, 'บริษัท', 'person', 'บุคคล'),
('P41', 'Miss Kanokporn Srikacha | Manager | NBH', 'Kanokporn', 1, 0, 0, 1, 'บริษัท', 'person', 'บุคคล'),
('P42', 'Miss Nittaya Saelee | Manager | NBH', 'Nittaya', 1, 0, 0, 1, 'บริษัท', 'person', 'บุคคล'),
('P43', 'Miss Sukanya Saelo | Staff | NBH', 'Sukanya', 1, 0, 0, 1, 'บริษัท', 'person', 'บุคคล'),
('P44', NULL, 'cashier', 1, 0, 0, 1, 'บริษัท', 'person', 'บุคคล'),
('P45', NULL, 'receivable', 1, 0, 0, 1, 'บริษัท', 'person', 'บุคคล'),
('P46', NULL, 'test', 1, 0, 0, 1, 'บริษัท', 'person', 'บุคคล'),
('P47', NULL, 'sme', 1, 0, 0, 1, 'บริษัท', 'person', 'บุคคล'),
('P48', NULL, 'test 2', 1, 0, 0, 1, 'บริษัท', 'person', 'บุคคล');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `view_node`
--
ALTER TABLE `view_node`
  ADD PRIMARY KEY (`nodeid`,`department_groupid`),
  ADD KEY `view_node_idx1` (`department_groupid`) USING BTREE,
  ADD KEY `view_node_idx2` (`department_typeid`) USING BTREE,
  ADD KEY `view_node_idx3` (`nodetype`) USING BTREE,
  ADD KEY `view_node_idx4` (`nodeid`) USING BTREE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
